var gulp = require('gulp');
var gcmq = require('gulp-group-css-media-queries');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
//var scsslint = require('gulp-scss-lint');
var rename = require('gulp-rename');
var concatCss = require('gulp-concat-css');
var concat = require('gulp-concat');
var postcss = require('gulp-postcss');
var cssnext = require('postcss-cssnext');
var cssnano = require('cssnano');
var args = require('yargs').argv;
var fs = require('fs');

gulp.Gulp.prototype.__runTask = gulp.Gulp.prototype._runTask;
gulp.Gulp.prototype._runTask = function(task) {
  this.currentTask = task;
  this.__runTask(task);
}
var tasks = [];
var folders = {
    frontend: {
        scss: 'resources/frontend/DefaultTemplate/assets/scss/',
        css: 'public/DefaultTemplate/assets/css/',
    },
    backend: {
        scss: 'resources/backend/assets/scss/',
        css: 'public/backend/assets/css/',
    },
};

/* Add current timestamp as unique number to avoid cache */
function replaceUnique()
{
    fs.writeFile('cacheVersion', Date.now(), function(err) {
        if (err) return console.log(err);
    });
}

var scssPaths = [];

for (var key in folders) {
    if (folders.hasOwnProperty(key) === false || folders[key].scss == undefined) {
        continue;
    }


    tasks.push(key);
    scssPaths.push(folders[key].scss +'*.scss');

    gulp.task(key, function() {
        var currentTask = this.currentTask;
        replaceUnique();

        var scssFolder = folders[currentTask.name].scss;
        var cssFolder = folders[currentTask.name].css;
        return gulp.src(scssFolder +'*.scss')
            .pipe(plumber({
                errorHandler: function(err) {
                    notify.onError({
                        title: 'Gulp Error',
                        message: '<%= error.message %>',
                        sound: 'Bottle',
                    })(err);
                    this.emit('end');
                }
            }))
            .pipe(sass()) // compile css from scss
            .pipe(postcss([cssnext({browsers: ['iOS 7', 'ie 9', 'last 3 versions']})])) // autoprefix and fallback on future css
            .pipe(gcmq())
            .pipe(gulp.dest(cssFolder)) // write file
            .pipe(browserSync.reload({stream: true})); // enable file sync with browsersync
    });
}


gulp.task('watch', tasks, function() {
    if (args.proxy) { // if args is passed: gulp watch --proxy=devadress.dev
        browserSync.init({
            proxy: args.proxy // initialize browsersync with proxy
        });
    }

    gulp.watch(scssPaths, tasks); // watch scss files

    if (args.proxy) {
        //gulp.watch([]).on('change', browserSync.reload); // watch tpl and script files and reload page on save
    }
});

gulp.task('default', tasks); // default task to run with only gulp

<?php
use App\Models\Country;

class CountriesTableSeeder extends TableSeeder
{
    public function local()
    {
        $this->save('se', [
            'name' => 'Sverige',
            'language' => 'sv',
        ]);
    }

    public function production()
    {
        if (Country::all()->isEmpty()) {
            $this->local();
        }
    }

    public function save($countryCode, $data)
    {
        Country::firstOrCreate(['country_code' => $countryCode])->translations()->create($data);
    }
}

<?php
use App\Models\Permission;
use App\Models\Role;
use App\Repositories\PermissionRepository;

class RolesTableSeeder extends TableSeeder
{
    public function local()
    {
        $this->saveDefaults();
    }

    public function production()
    {
        if (Role::all()->isEmpty()) {
            $this->saveDefaults();
        }
    }

    public function saveDefaults()
    {
        $this->save([
            'name' => 'Huvudadministratör',
            'identifier' => 'mainAdmin',
        ], Permission::all());

        $this->save([
            'name' => 'Administratör',
            'identifier' => 'admin',
        ], Permission::all());

        $this->save([
            'name' => 'User',
            'identifier' => 'user',
        ], Permission::onlyFrontend()->get());
    }

    public function save($role, $permissions = [])
    {
        return Role::firstOrCreate( $role )->syncPermissions( $permissions );
    }
}

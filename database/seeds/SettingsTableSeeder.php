<?php
use App\Models\Setting;

class SettingsTableSeeder extends TableSeeder
{
    public function local()
    {
        $this->save([
            'companyName' => 'Company',
            'companyCorporateNr' => '000000-0000',
            'companyPhone' => '0700-000000',
            'mailOut' => 'info@example.se',
            'mailIn' => 'info@example.se',
            'facebookLink' => 'https://www.facebook.com/',
            'instagramLink' => 'https://instagram.com/',
            'twitterLink' => 'https://twitter.com/',
            'linkedinLink' => 'https://se.linkedin.com/',
            'fullCompanyName' => 'Company AB',
            'defaultUserRole' => 3,
            'activateUserOnRegistration' => '1',
            'googleMaps' => NULL,
            'googleAnalytics' => NULL,
            'companyZipcode' => '12345',
            'companyCity' => 'Staden',
            'companyAddress' => 'Adressen',
        ]);
    }

    public function production()
    {
        if (Setting::all()->isEmpty()) {
            $this->local();
        }
    }

    public function save($settings)
    {
        foreach ($settings as $key => $value) {
            Setting::firstOrCreate([
                'key' => $key,
                'value' => $value,
            ]);
        }
    }
}

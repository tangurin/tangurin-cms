<?php
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $environment = App::environment();
        if (method_exists($this, $environment)) {
            $this->{$environment}();
        }
    }
}

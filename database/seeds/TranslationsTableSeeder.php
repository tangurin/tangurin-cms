<?php
use App\Models\Translation;

class TranslationsTableSeeder extends TableSeeder
{
    public function local()
    {
        $this->save([
            'navigation' => [
                'navigation' => [
                    'sv' => 'Navigation',
                    'en' => 'Navigation',
                ],
                'account' => [
                    'sv' => 'Konto',
                    'en' => 'Account',
                ],
                'start' => [
                    'sv' => 'Start',
                    'en' => 'Start',
                ],
                'breadcrumbs' => [
                    'start' => [
                        'sv' => 'Start',
                        'en' => 'Start',
                    ],
                    'filter' => [
                        'sv' => 'Filtrering',
                        'en' => 'Filter',
                    ],
                    'searchResult' => [
                        'sv' => 'Resultat',
                        'en' => 'Result',
                    ],
                    'requestPreview' => [
                        'sv' => 'Förhandsgranskning',
                        'en' => 'Preview',
                    ],
                    'resetPassword' => [
                        'sv' => 'Återställ lösenord',
                        'en' => 'Reset password',
                    ],
                ],
            ],
            'account' => [
                'acceptTheTerms' => [
                    'sv' => 'Jag accepterar villkoren.',
                    'en' => 'I accept the terms',
                ],
                'register' => [
                    'sv' => 'Nytt konto',
                    'en' => 'Create account',
                ],
                'login' => [
                    'sv' => 'Logga in',
                    'en' => 'Login',
                ],
                'logout' => [
                    'sv' => 'Logga ut',
                    'en' => 'Logout',
                ],
                'emailAndPassword' => [
                    'sv' => 'E-post & Lösenord',
                    'en' => 'Email & Password',
                ],
                'account' => [
                    'sv' => 'Konto',
                    'en' => 'Account',
                ],
                'myAccount' => [
                    'sv' => 'Mitt konto',
                    'en' => 'My account',
                ],
                'forgotYourPassword' => [
                    'sv' => 'Glömt ditt lösenord?',
                    'en' => 'Forgout your password?',
                ],
                'forgotPassword' => [
                    'sv' => 'Glömt lösenord?',
                    'en' => 'Forgot password?',
                ],
                'resetPasswordButton' => [
                    'sv' => 'Återställ lösenord',
                    'en' => 'Reset password',
                ],
                'rememberMe' => [
                    'sv' => 'Kom ihåg mig',
                    'en' => 'Remember me',
                ],
            ],
            'form' => [
                'password' => [
                    'sv' => 'Lösenord',
                    'en' => 'Password',
                ],
                'passwordConfirm' => [
                    'sv' => 'Bekräfta lösenord',
                    'en' => 'Confirm password',
                ],
                'passwordCurrent' => [
                    'sv' => 'Nuvarande lösenord',
                    'en' => 'Current password',
                ],
                'emailAdress' => [
                    'sv' => 'Epostadress',
                    'en' => 'Email',
                ],
                'email' => [
                    'sv' => 'E-post',
                    'en' => 'Email',
                ],
                'firstname' => [
                    'sv' => 'Namn',
                    'en' => 'First name',
                ],
                'lastname' => [
                    'sv' => 'Efternamn',
                    'en' => 'Surname',
                ],
                'name' => [
                    'sv' => 'Namn',
                    'en' => 'Name',
                ],
                'message' => [
                    'sv' => 'Meddelande',
                    'en' => 'Message',
                ],
                'send' => [
                    'sv' => 'Skicka',
                    'en' => 'Send',
                ],
                'ssn' => [
                    'sv' => 'Personnr/Org.nr',
                    'en' => 'SSN',
                ],
                'companyName' => [
                    'sv' => 'Företagsnamn',
                    'en' => 'Companyname',
                ],
                'adress' => [
                    'sv' => 'Adress',
                    'en' => 'Address',
                ],
                'streetAddress' => [
                    'sv' => 'Gatuadress',
                    'en' => 'Street address',
                ],
                'zipcode' => [
                    'sv' => 'Postnr',
                    'en' => 'Zip code',
                ],
                'city' => [
                    'sv' => 'Stad',
                    'en' => 'City',
                ],
                'country' => [
                    'sv' => 'Land',
                    'en' => 'Country',
                ],
                'phone' => [
                    'sv' => 'Telefon',
                    'en' => 'Phone',
                ],
                'remove' => [
                    'sv' => 'Ta bort',
                    'en' => 'Remove',
                ],
                'delete' => [
                    'sv' => 'Ta bort',
                    'en' => 'Delete',
                ],
                'url' => [
                    'sv' => 'URL (Till hemsida)',
                    'en' => 'URL',
                ],
                'optional' => [
                    'sv' => 'Frivilligt',
                    'en' => 'Optional',
                ],
                'save' => [
                    'sv' => 'Spara',
                    'en' => 'Save',
                ],
            ],
            'success' => [
                'success' => [
                    'sv' => 'Klart',
                    'en' => 'Success',
                ],
                'saved' => [
                    'sv' => 'Sparat & Klart',
                    'en' => 'Saved',
                ],
                'contactForm' => [
                    'sv' => 'Tack för ditt meddelande',
                    'en' => 'Thank you for your message',
                ],
                'registered' => [
                    'sv' => 'Ditt konto är skapat och du är nu inloggad!',
                    'en' => 'Your account is not created and you are loged in',
                ],
            ],
            'error' => [
                'error' => [
                    'sv' => 'Fel',
                    'en' => 'Error',
                ],
                'unknownError' => [
                    'sv' => 'Ett okänt fel uppstod!',
                    'en' => 'Unknown error',
                ],
                'pageNotFound' => [
                    'sv' => 'Sidan kunde inte hittas',
                    'en' => 'Page not found',
                ],
                'pageUnauthorize' => [
                    'sv' => 'Behörighet saknas',
                    'en' => 'Not authorized',
                ],
                'notSaved' => [
                    'sv' => 'Något gick fel',
                    'en' => 'Something went wrong',
                ],
                'currentPasswordNotMatch' => [
                    'sv' => 'Ditt nuvarande lösenord stämmer inte.',
                    'en' => 'The passwords don\'t match',
                ],
                'validation' => [
                    'sv' => 'Formuläret innehåller valideringsfel',
                    'en' => 'The form input is not valid',
                ],
            ],
            'word' => [
                'year' => [
                    'sv' => 'År',
                    'en' => 'Year',
                ],
                'info' => [
                    'sv' => 'Information',
                    'en' => 'Information',
                ],
            ]
        ]);
    }

    public function production()
    {
        if (Translation::all()->isEmpty()) {
            $this->local();
        }
    }

    public function save($translations)
    {
        $errors = [];
        foreach (array_dot($translations) as $identifier => $value) {
            $languageCode = collect(explode('.', $identifier))->last();
            if (strlen($languageCode) > 3) {
                $errors[] = $languageCode;
                continue;
            }

            Translation::firstOrCreate([
                'identifier' => str_replace('.'. $languageCode, '', $identifier),
            ])->values()->firstOrCreate([
                'value' => $value,
                'language' => $languageCode,
            ]);
        }

        if (!empty($errors)) {
            echo "Language code is to long (max length 3)\n";
            echo "----------------------\n";
            foreach ($errors as $error) {
                echo $error ."\n";
            }
            echo "----------------------\n";
        }
    }
}

<?php
use App\Models\Permission;

class PermissionsTableSeeder extends TableSeeder
{
    public function local()
    {
        $this->saveBackendPermissions();
        $this->saveFrontendPermissions();
    }

    public function production()
    {
        if (Permission::all()->isEmpty()) {
            $this->saveBackendPermissions();
            $this->saveFrontendPermissions();
        }
    }

    public function save($name)
    {
        Permission::firstOrCreate(['name' => $name]);
    }

    protected function saveBackendPermissions()
    {
        $this->save('backend_view_user');
        $this->save('backend_create_user');
        $this->save('backend_update_user');
        $this->save('backend_delete_user');

        $this->save('backend_view_role');
        $this->save('backend_create_role');
        $this->save('backend_update_role');
        $this->save('backend_delete_role');

        $this->save('backend_view_page');
        $this->save('backend_create_page');
        $this->save('backend_update_page');
        $this->save('backend_delete_page');

        $this->save('backend_view_setting');
        $this->save('backend_update_setting');

        $this->save('backend_view_translation');
        $this->save('backend_update_translation');

        $this->save('backend_view_mediamanager');

        $this->save('backend_view_text');
        $this->save('backend_update_text');

        $this->save('backend_view_country');
        $this->save('backend_create_country');
        $this->save('backend_update_country');
        $this->save('backend_delete_country');

        $this->save('backend_view_county');
        $this->save('backend_create_county');
        $this->save('backend_update_county');
        $this->save('backend_delete_county');

        $this->save('backend_view_city');
        $this->save('backend_create_city');
        $this->save('backend_update_city');
        $this->save('backend_delete_city');

        $this->save('backend_view_linker');
        $this->save('backend_create_linker');
        $this->save('backend_update_linker');
        $this->save('backend_delete_linker');

        $this->save('backend_view_slideshow');
        $this->save('backend_create_slideshow');
        $this->save('backend_update_slideshow');
        $this->save('backend_delete_slideshow');

        $this->save('backend_view_customfields');
        $this->save('backend_create_customfields');
        $this->save('backend_update_customfields');
        $this->save('backend_delete_customfields');
    }

    protected function saveFrontendPermissions()
    {
        $this->save('frontend_update_myAccount');
        $this->save('frontend_update_emailAndPassword');
    }
}

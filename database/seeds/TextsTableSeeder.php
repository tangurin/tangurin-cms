<?php
use App\Models\Text;

class TextsTableSeeder extends TableSeeder
{
    public function local()
    {
        $this->save([
            //'exampleGroup' => [
            //    'exampleText' => [
            //        'sv' => '<p>content</p>',
            //    ],
            //],
        ]);
    }

    public function production()
    {
        if (Text::all()->isEmpty()) {
            $this->local();
        }
    }

    public function save($texts)
    {
        foreach ($texts as $group => $text) {
            foreach ($text as $identifier => $languages) {
                foreach ($languages as $languageCode => $value) {
                    Text::firstOrCreate([
                        'identifier' => $identifier,
                        'group' => $group,
                    ])->translations()->create([
                        'language' => $languageCode,
                        'content' => $value,
                    ]);
                }
            }
        }
    }
}

<?php
use App\Models\Role;
use App\Models\User;
use App\Repositories\RoleRepository;

class UsersTableSeeder extends TableSeeder
{
    protected $roles = [];

    public function __construct()
    {
        $roleRepository = resolve(RoleRepository::class);

        $this->roles = [
            'mainAdmin' => $roleRepository->getMainAdmin(),
            'admin' => $roleRepository->getAdmin(),
            'default' => $roleRepository->getDefaultUserRole(),
        ];
    }

    public function production()
    {
        if (User::all()->isEmpty()) {
            $this->createAdministrators();
        }
    }
    
    public function local()
    {
        $this->createAdministrators();
    }

    public function createAdministrators()
    {
        User::firstOrCreate([
            'firstname' => 'Albert',
            'lastname' => 'lastname',
            'email' => 'tangurin.se@gmail.com',
            'password' => Hash::make('12311231'),
        ])
        ->assignRole( $this->roles['mainAdmin'] )
        ->activateAccount();

        User::firstOrCreate([
            'firstname' => 'Admin',
            'lastname' => 'lastname',
            'email' => 'admin@example.se',
            'password' => Hash::make('123123'),
        ])
        ->assignRole( $this->roles['admin'] )
        ->activateAccount();
    }
}

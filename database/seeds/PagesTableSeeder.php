<?php
use App\Models\Page;

class PagesTableSeeder extends TableSeeder
{
    public function local()
    {
        $this->save([
            //Start
            [
                'template' => 'default',
                'in_menu' => 0,
                'translations' => [
                    'sv' => [
                        'slug' => 'start',
                        'title' => 'Start',
                        'content' => 'Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin. Lorem ipsum har varit standard ända sedan 1500-talet, när en okänd boksättare tog att antal bokstäver och blandade dem för att göra ett provexemplar av en bok. Lorem ipsum har inte bara överlevt fem århundraden, utan även övergången till elektronisk typografi utan större förändringar. Det blev allmänt känt på 1960-talet i samband med lanseringen av Letraset-ark med avsnitt av Lorem Ipsum, och senare med mjukvaror som Aldus PageMaker.',
                    ],
                    'en' => [
                        'slug' => 'start',
                        'title' => 'Start',
                        'content' => 'Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin. Lorem ipsum har varit standard ända sedan 1500-talet, när en okänd boksättare tog att antal bokstäver och blandade dem för att göra ett provexemplar av en bok. Lorem ipsum har inte bara överlevt fem århundraden, utan även övergången till elektronisk typografi utan större förändringar. Det blev allmänt känt på 1960-talet i samband med lanseringen av Letraset-ark med avsnitt av Lorem Ipsum, och senare med mjukvaror som Aldus PageMaker.',
                    ],
                ],
            ],
            //About
            [
                'template' => 'default',
                'in_menu' => 1,
                'translations' => [
                    'sv' => [
                        'slug' => 'om-oss',
                        'title' => 'Om oss',
                        'content' => 'Om oss...',
                    ],
                    'en' => [
                        'slug' => 'about-us',
                        'title' => 'About us',
                        'content' => 'Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin. Lorem ipsum har varit standard ända sedan 1500-talet, när en okänd boksättare tog att antal bokstäver och blandade dem för att göra ett provexemplar av en bok. Lorem ipsum har inte bara överlevt fem århundraden, utan även övergången till elektronisk typografi utan större förändringar. Det blev allmänt känt på 1960-talet i samband med lanseringen av Letraset-ark med avsnitt av Lorem Ipsum, och senare med mjukvaror som Aldus PageMaker.',
                    ],
                ],
            ],
            //Contact
            [
                'template' => 'contact',
                'in_menu' => 1,
                'translations' => [
                    'sv' => [
                        'slug' => 'kontakt',
                        'title' => 'Kontakt',
                        'content' => 'Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin. Lorem ipsum har varit standard ända sedan 1500-talet, när en okänd boksättare tog att antal bokstäver och blandade dem för att göra ett provexemplar av en bok. Lorem ipsum har inte bara överlevt fem århundraden, utan även övergången till elektronisk typografi utan större förändringar. Det blev allmänt känt på 1960-talet i samband med lanseringen av Letraset-ark med avsnitt av Lorem Ipsum, och senare med mjukvaror som Aldus PageMaker.',
                    ],
                    'en' => [
                        'slug' => 'contact',
                        'title' => 'Contact',
                        'content' => 'Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin. Lorem ipsum har varit standard ända sedan 1500-talet, när en okänd boksättare tog att antal bokstäver och blandade dem för att göra ett provexemplar av en bok. Lorem ipsum har inte bara överlevt fem århundraden, utan även övergången till elektronisk typografi utan större förändringar. Det blev allmänt känt på 1960-talet i samband med lanseringen av Letraset-ark med avsnitt av Lorem Ipsum, och senare med mjukvaror som Aldus PageMaker.',
                    ],
                ],
                'children' => [
                    [
                        'template' => 'default',
                        'in_menu' => 1,
                        'translations' => [
                            'sv' => [
                                'slug' => 'hitta-hit',
                                'title' => 'Hitta hit',
                                'content' => 'Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin. Lorem ipsum har varit standard ända sedan 1500-talet, när en okänd boksättare tog att antal bokstäver och blandade dem för att göra ett provexemplar av en bok. Lorem ipsum har inte bara överlevt fem århundraden, utan även övergången till elektronisk typografi utan större förändringar. Det blev allmänt känt på 1960-talet i samband med lanseringen av Letraset-ark med avsnitt av Lorem Ipsum, och senare med mjukvaror som Aldus PageMaker.',
                            ],
                            'en' => [
                                'slug' => 'find-us',
                                'title' => 'Find us',
                                'content' => 'Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin. Lorem ipsum har varit standard ända sedan 1500-talet, när en okänd boksättare tog att antal bokstäver och blandade dem för att göra ett provexemplar av en bok. Lorem ipsum har inte bara överlevt fem århundraden, utan även övergången till elektronisk typografi utan större förändringar. Det blev allmänt känt på 1960-talet i samband med lanseringen av Letraset-ark med avsnitt av Lorem Ipsum, och senare med mjukvaror som Aldus PageMaker.',
                            ],
                        ],
                    ]
                ]
            ],
        ]);
    }

    public function production()
    {
        if (Page::all()->isEmpty()) {
            $this->local();
        }
    }

    public function save($pages, $parentId = 0)
    {
        foreach ($pages as $sortOrder => $pageData) {
            $translations = !empty($pageData['translations']) ? $pageData['translations'] : null;
            $children = !empty($pageData['children']) ? $pageData['children'] : null;
            if (is_null($translations)) {
                continue;
            }

            unset($pageData['translations']);
            if (isset($children)) {
                unset($pageData['children']);
            }

            //Set parent ID
            $pageData['parent'] = $parentId;
            //Set sort order
            $pageData['sort_order'] = $sortOrder;
            
            //Create page
            $pageObject = Page::firstOrCreate($pageData);

            //Insert all translations for the page
            foreach ($translations as $languageCode => $translation) {
                $translation['language'] = $languageCode;
                $pageObject->translations()->firstOrCreate($translation);
            }

            //If page has children
            if (is_array($children)) {
                $this->save($children, $pageObject->id);
            }
        }
    }
}

<?php
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    protected $seeds = [
        SettingsTableSeeder::class,
        PermissionsTableSeeder::class,
        RolesTableSeeder::class,
        TranslationsTableSeeder::class,
        PagesTableSeeder::class,
        TextsTableSeeder::class,
        CountriesTableSeeder::class,
        CountiesTableSeeder::class,
        UsersTableSeeder::class,
    ];

    public function run()
    {
        foreach ($this->seeds as $class) {
            $this->call($class);
        }
    }
}

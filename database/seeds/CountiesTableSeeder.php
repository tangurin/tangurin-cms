<?php
use App\Models\Country;
use App\Models\County;

class CountiesTableSeeder extends TableSeeder
{
    protected $countriesWithCounties = [
        'se' => [
            'Blekinge län',
            'Dalarnas län',
            'Gotlands län',
            'Gävleborgs län',
            'Hallands län',
            'Jämtlands län',
            'Jönköpings län',
            'Kalmar län',
            'Kronobergs län',
            'Norrbottens län',
            'Skåne län',
            'Stockholms län',
            'Södermanlands län',
            'Uppsala län',
            'Värmlands län',
            'Västerbottens län',
            'Västernorrlands län',
            'Västmanlands län',
            'Västra Götalands län',
            'Örebro län',
            'Östergötlands län',
        ],
    ];

    public function local()
    {
        $this->save($this->countriesWithCounties);
    }

    public function production()
    {
        if (County::all()->isEmpty()) {
            $this->local();
        }
    }

    public function save($countriesWithCounties)
    {
        foreach ($countriesWithCounties as $countryCode => $counties) {
            $country = Country::where('country_code', 'se')->first();
            if (empty($country)) {
                continue;
            }
            foreach ($counties as $county) {
                $country->counties()->create(['name' => $county]);
            }
        }
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomfieldsCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('customfields_collections')) {
            Schema::create('customfields_collections', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->nullable();
                $table->string('identifier')->nullable();
                $table->text('description')->nullable();
                $table->string('category')->nullable();
                $table->integer('sort_order');
                $table->integer('created_at')->nullable();
                $table->integer('updated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customfields_collections');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomfieldsCollectionRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('customfields_collection_relations')) {
            Schema::create('customfields_collection_relations', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('collection_id')->nullable();
                $table->string('class')->nullable();
                $table->string('relation')->nullable();
                $table->string('relation_id')->nullable();
                $table->integer('created_at')->nullable();
                $table->integer('updated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customfields_collection_relations');
    }
}

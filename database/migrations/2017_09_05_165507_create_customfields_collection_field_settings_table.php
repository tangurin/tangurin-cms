<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomfieldsCollectionFieldSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('customfields_collection_field_settings')) {
            Schema::create('customfields_collection_field_settings', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('field_id')->nullable();
                $table->string('key', 255)->nullable();
                $table->string('value', 255)->nullable();
                $table->integer('created_at')->nullable();
                $table->integer('updated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customfields_collection_field_settings');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomfieldsCollectionFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('customfields_collection_fields')) {
            Schema::create('customfields_collection_fields', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('collection_id')->nullable();
                $table->integer('parent_id');
                $table->string('name')->nullable();
                $table->string('identifier')->nullable();
                $table->text('description')->nullable();
                $table->string('type', 100)->nullable();
                $table->integer('sort_order')->default(0);
                $table->tinyInteger('active')->default(0);
                $table->integer('created_at')->nullable();
                $table->integer('updated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customfields_collection_fields');
    }
}

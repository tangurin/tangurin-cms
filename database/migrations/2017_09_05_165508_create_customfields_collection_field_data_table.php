<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomfieldsCollectionFieldDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('customfields_collection_field_data')) {
            Schema::create('customfields_collection_field_data', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('field_id')->nullable();
                $table->string('value', 255)->nullable();
                $table->string('label', 255)->nullable();
                $table->integer('sort_order')->default(0);
                $table->integer('created_at')->nullable();
                $table->integer('updated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customfields_collection_field_data');
    }
}

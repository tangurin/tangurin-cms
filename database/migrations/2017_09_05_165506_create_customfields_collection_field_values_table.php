<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomfieldsCollectionFieldValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('customfields_collection_field_values')) {
            Schema::create('customfields_collection_field_values', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('field_id')->nullable();
                $table->integer('relation_id')->nullable();
                $table->integer('parent_value_id')->nullable();
                $table->integer('sort_order')->default(0);
                $table->integer('created_at')->nullable();
                $table->integer('updated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customfields_collection_field_values');
    }
}

<?php
return [
        
    'upload_directory' => 'uploads',
    'thumbnail_folder_name' => 'filemanager_thumbs',

    'route_prefix' => 'filemanager',

    'ignore_hidden_files' => false,
    'ignore_hidden_directories' => false,

    'ignored_directories' => [
        'filemanager_thumbs',
    ],

    'ignored_files' => [
        '.DS_Store',
    ],

    'onlyImagesAsDefault' => true,
    
    'files_per_page' => 200,

    'types' => [
        'application/pdf' => 'pdf',
        '3gp' => 'Video',
    ]
];
?>

<?php
namespace App\Repositories;

use App\Models\Filemanager as FilemanagerModel;
use App\Http\Resources\Filemanager\FileResource;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class FilemanagerRepository
{
    public $settingsPrefix = 'filemanager.';
    protected $config = [];
    protected $defaultPaginationNumber = 20;

    public function __construct()
    {
        $this->config = [
            'directory' => $this->getRootDirectory(),
            'ignoreHiddenFiles' => config('filemanager.ignore_hidden_files'),
            'ignoreHiddenDirectories' => config('filemanager.ignore_hidden_directories'),
            'onlyImages' => config('filemanager.onlyImagesAsDefault'),
            'allowedExtensions' => [],
            'filesPerPage' => config('filemanager.files_per_page'),
            'page' => 1,
        ];
    }

    public function refreshIndex()
    {
        FilemanagerModel::truncate();

        $filesWithData = [];
        $uploadDirectory = config('filemanager.upload_directory', 'uploads');
        $ignoredDirectories = array_map(function($item) use ($uploadDirectory) {
            return $uploadDirectory .'/'. $item;
        }, config('filemanager.ignored_directories', []));
        $ignoredFiles = [];
        foreach ($this->getAllFiles() as $filepath) {
            //If directory or file is set as ignored in the config
            if (in_array(dirname($filepath), $ignoredDirectories) || in_array(basename($filepath), $ignoredFiles)) {
                continue;
            }
            //Get all file data
            $fileData = $this->collectFileData($filepath);
            $filesWithData[] = $fileData;
        }
        
        //Insert all files to the index
        if (!empty($filesWithData)) {
            FilemanagerModel::insert($filesWithData);
        }
        return true;
    }

    public function insertToIndex($filepath)
    {
        //Insert the file
        $model = new FilemanagerModel();
        $model->fill( $this->collectFileData($filepath) );
        $model->save();
        return new FileResource($model);
    }

    public function updateInIndex($filepath)
    {
        $model = $this->findByPath($filepath);
        
        if (!empty($model)) {
            $fileData = $this->collectFileData($filepath);
            $model->update($fileData);
            return $model;
        }

        return false;
    }

    public function deleteFromIndex($filepath)
    {
        $model = $this->findByPath($filepath);
        if (!empty($model)) {
            $model->delete();
            return $model;
        }

        return false;
    }

    public function findByPath($filepath)
    {
        return FilemanagerModel::where('path', $filepath)->first();
    }


    public function getAllFiles()
    {
        return collect(Storage::allFiles( $this->getConfig('directory') ) );
    }

    public function collectFileData($filepath)
    {
        $availableTypes = config('filemanager.types');
        $absolutPath = Storage::disk()->path($filepath);
        $pathinfo = pathinfo($filepath);
        $extension = $pathinfo['extension'];
        $name = !empty($pathinfo['filename']) ? $pathinfo['filename'] : $pathinfo['basename'];
        $hidden = $name[0] == '.';
        $filesize = filesize($absolutPath);
        $imagesize = $hidden ? 0 : getimagesize($absolutPath);
        $isImage = exif_imagetype($absolutPath);
        $mimetype = mime_content_type($absolutPath);
        $type = isset($availableTypes[$mimetype]) ? $availableTypes[$mimetype] : explode('/', $mimetype)[0];
        $type = isset($availableTypes[$extension]) ? $availableTypes[$extension] : $type;
        $directory = $pathinfo['dirname'];

        $fileData['directory'] = $pathinfo['dirname'];
        $fileData['path'] = $filepath;
        $fileData['name'] = $name;
        $fileData['extension'] = $extension;
        $fileData['size'] = $filesize;
        $fileData['mimetype'] = $mimetype;
        $fileData['type'] = $type;
        $fileData['width'] = isset($imagesize[0]) ? $imagesize[0] : 0;
        $fileData['height'] = isset($imagesize[1]) ? $imagesize[1] : 0;
        $fileData['hidden'] = intval($hidden);
        $fileData['created_at'] = Carbon::createFromTimestamp( filemtime($absolutPath) )->toDateTimeString();

        return $fileData;
    }

    public function isFileExists($filepath)
    {
        $absolutPath = Storage::disk()->path($filepath);
        return file_exists($absolutPath);
    }

    public function checkFile($filepath)
    {
        if (!$this->isFileExists($filepath)) {
            $this->deleteFromIndex($filepath);
            return false;
        }

        return true;
    }

    public function getFiles($directory)
    {
        return FileResource::collection(
            FilemanagerModel::where('directory', $directory)
                ->where('hidden', 0)
                ->orderBy('created_at', 'asc')
                ->orderBy('type', 'asc')
                ->paginate(config('filemanager.files_per_page', $this->defaultPaginationNumber))
        );
    }

    public function searchFiles($keywords, $directory = null, $request)
    {
        if (empty($keywords)) {
            return [];
        }

        $directory = empty($directory) ? $this->getRootDirectory() : $directory;

        $searchResult = FilemanagerModel::where('directory', $directory)
            ->where(function($query) use ($keywords) {
                return $query->where('name', 'like', '%'. $keywords .'%')
                ->orWhere('extension', '=', $keywords)
                ->orWhere('type', 'like', '%'. $keywords .'%');
            })
            ->paginate(config('filemanager.files_per_page', $this->defaultPaginationNumber));

        return $searchResult;
    }


    public function getDirectoriesTree()
    {
        $rootDirectory = $this->getRootDirectory();
        $tree[] = [
            'path' => $rootDirectory,
            'pathinfo' => pathinfo($rootDirectory),
            'children' => $this->_recursiveDirectories($rootDirectory),
        ];

        return $tree;
    }

    public function _recursiveDirectories($root = null)
    {
        $root = is_null($root) ? $this->getRootDirectory() : $root;
        $currentDirectories = collect(Storage::directories( $root ));

        if ($currentDirectories->isEmpty()) {
            return [];
        }

        //Ignore hidden directories
        if ($this->getConfig('ignoreHiddenDirectories')) {
            $currentDirectories = $currentDirectories->filter(function($value) {
                $basename = basename($value);
                return $basename[0] != '.';
            });
        }

        $directories = [];
        foreach ($currentDirectories as $directory) {
            $directories[] = [
                'path' => $directory,
                'pathinfo' => pathinfo($directory),
                'children' => $this->_recursiveDirectories($directory),
            ];
        }

        return $directories;
    }

    public function getCurrentDirectory()
    {
        $user = Auth::user();
        if (!empty($user)) {
            $directory = $user->preferences->get($this->settingsPrefix .'currentDirectory', null);
            if (!empty($directory) && Storage::disk()->exists($directory)) {
                return $directory;
            }
        }

        return $this->getRootDirectory();
    }

    public function getAvailableFiletypes()
    {
        return FilemanagerModel::select('type')->groupBy('type')->pluck('type');
    }

    public function getConfig($key, $default = null)
    {
        return isset($this->config[$key]) ? $this->config[$key] : $default;
    }

    public function getRootDirectory()
    {
        return config('filemanager.upload_directory');
    }

    public function storeFile($file, $directory = null)
    {
        $directory = $directory ?: userSetting($this->settingsPrefix . 'currentDirectory', $this->getRootDirectory());
        return $file->store($directory, 'public');
    }

    public function saveSetting($user, $key, $value)
    {
        if (empty($user)) {
            return false;
        }

        return resolve(UserPreferenceRepository::class)->save(
            $user,
            $this->settingsPrefix . $key,
            $value
        );
    }
}

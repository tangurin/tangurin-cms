<?php
namespace App\Repositories;

use App\Models\Text AS TextModel;

class TextRepository
{
    protected $texts;

    public function fetchTexts($force = false)
    {
        if (is_null($this->texts)) {
            $this->texts = \Cache::rememberForever('texts', function() {
                $texts = [];
                foreach (TextModel::with('translations')->get() as $text) {
                    foreach ($text->translations as $translation) {
                        $texts[$translation->language][$text->identifier] = $translation->content;
                    }
                }
                return $texts;
            });
        }

        return $this->texts;
    }

    public function get($identifier, $default = '', $language = null)
    {
        $this->fetchTexts();
        $language = is_null($language) ? resolve(LanguageRepository::class)->current() : $language;

        if (isset($this->texts[$language][$identifier])) {
            return $this->texts[$language][$identifier];
        }

        return !empty($default) ? $default : '';
    }

    public function getList($language = null)
    {
        $this->fetchTexts();
        
        $language = is_null($language) ? resolve(LanguageRepository::class)->current() : $language;
        return $this->texts[$language];
    }

    public function getAllLists()
    {
        $this->fetchTexts();
        return $this->texts;
    }

    public function getListsWithModel()
    {
        $translations = [];
        if (empty($this->translations)) {
            foreach (TextModel::with('translations')->get() as $translation) {
                if (empty($translation->translation->language)) {
                    continue;
                }
                $translations[$translation->translation->language][$translation->identifier] = $translation;
            }
        }

        return $translations;
    }

    public function getListWithModel($language = null)
    {
        $language = is_null($language) ? resolve(LanguageRepository::class)->current() : $language;
        $lists = $this->getListsWithModel();
        return isset($lists[$language]) ? $lists[$language] : [];
    }

    public function getListSortedByGroup($language = null)
    {
        $list = $this->getListWithModel($language);
        $sortedList = [];
        foreach ($list as $key => $text) {
            $group = !empty($text->group) ? $text->group : 'other';
            $sortedList[$group][$text->identifier] = $text;
        }

        return $sortedList;
    }

    public function getIdentifiersSortedByGroup($language = null)
    {
        $list = TextModel::all();
        $sortedList = [];
        foreach ($list as $key => $text) {
            $group = !empty($text->group) ? $text->group : 'other';
            $sortedList[$group][$text->identifier] = $text;
        }

        return $sortedList;
    }

    public function slugify($string)
    {
        return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
    }

    public function generatePassword($length = 11, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false) $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if (strpos($available_sets, 'u') !== false) $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if (strpos($available_sets, 'd') !== false) $sets[] = '23456789';
        if (strpos($available_sets, 's') !== false) $sets[] = '!@#$%&*?';
        
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++){
            $password .= $all[array_rand($all)];
        }

        $password = str_shuffle($password);
        if (!$add_dashes) {
            return $password;
        }

        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        
        return $dash_str;
    }
}

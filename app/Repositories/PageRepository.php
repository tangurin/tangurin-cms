<?php
namespace App\Repositories;

use Session;
use App\Repositories\LanguageRepository;
use App\Models\Page AS PageModel;
use App\Models\Page\Translation AS PageTranslation;
use App\Models\Page\Image AS PageImage;

use App\Models\Page;
use App\Models\Page\Image as Image;
use App\Models\Page\Translation as Translation;

class PageRepository
{
    const PUBLISHED = 'published';
    const DRAFT = 'draft';
    const INACTIVE = 'inactive';

    protected $pages = [];

    public function create()
    {

    }

    public function update()
    {

    }

    public function getPages($forceUpdate = false)
    {
        if ($forceUpdate === true) {
            \Cache::forget('pages');
            $this->pages = [];
        }

        if (empty($this->pages)) {
            $this->pages = \Cache::rememberForever('pages', function() {
                return PageModel::with('translations', 'images')->get();;
            });
        }

        return $this->pages;
    }

    public function getInMenu()
    {
        return $this->getPages();
    }

    public function getPublished()
    {
        return $this->getPages()->filter(function($page, $key) {
            return $page->status == static::PUBLISHED;
        });
    }

    public function getStartpage()
    {
        return PageModel::with('translations')->find(1);
    }

    public function getPageBySlug($slug)
    {
        $page = null;
        if (empty($slug)) {
            $page = $this->getStartpage();
        } else {
            $translation = PageTranslation::where('slug', $slug)
                ->where('language', resolve(LanguageRepository::class)->current())
                ->first();

            if (!is_null($translation)) {
                $page = PageModel::with('translations')->find($translation->page_id);
            }
        }

        return $page;
    }

    public function getStatuses()
    {
        return [
            'PUBLISHED' => static::PUBLISHED,
            'DRAFT' => static::DRAFT,
            'INACTIVE' => static::INACTIVE,
        ];
    }

    public function getTemplates()
    {
        return [
            'default' => 'Standard',
            'contact' => 'Kontaktsidan',
        ];
    }
    
}

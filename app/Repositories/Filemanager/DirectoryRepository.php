<?php
namespace App\Repositories\Filemanager;

use App\Models\Filemanager as FilemanagerModel;
use App\Http\Resources\Filemanager\FileResource;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class DirectoryRepository
{
    public function getRootDirectory()
    {
        return config('filemanager.upload_directory');
    }
    
    public function getCurrentDirectory() : string
    {
        $user = Auth::user();
        if (!empty($user)) {
            $directory = userPreference('filemanager.currentDirectory', null);
            if (!empty($directory) && Storage::disk()->exists($directory)) {
                return $directory;
            }
        }

        return $this->getRootDirectory();
    }

    public function getDirectoriesTree() : array
    {
        $rootDirectory = $this->getRootDirectory();
        $tree[] = [
            'path' => $rootDirectory,
            'isRoot' => true,
            'parent' => null,
            'pathinfo' => pathinfo($rootDirectory),
            'children' => $this->_recursiveDirectories($rootDirectory),
        ];

        return $tree;
    }

    public function _recursiveDirectories(string $parentDirectory = null) : array
    {
        $currentDirectories = collect(Storage::directories( $parentDirectory ));

        if ($currentDirectories->isEmpty()) {
            return [];
        }

        //Ignore hidden directories
        if (config('filemanager.ignore_hidden_directories')) {
            $currentDirectories = $currentDirectories->filter(function($value) {
                $basename = basename($value);
                return $basename[0] != '.';
            });
        }

        $ignoredDirectories = config('filemanager.ignored_directories', []);

        $directories = [];
        foreach ($currentDirectories as $directory) {
            $pathinfo = pathinfo($directory);

            //If directory is set as ignored in the config
            if (in_array($pathinfo['basename'], $ignoredDirectories)) {
                continue;
            }
            
            $directories[] = [
                'path' => $directory,
                'isRoot' => false,
                'parent' => $parentDirectory,
                'pathinfo' => $pathinfo,
                'children' => $this->_recursiveDirectories($directory),
            ];
        }

        return $directories;
    }

    public function create(string $target, string $name = null) : bool
    {
        if (is_null($name) == false) {
            $target .= '/'. $name;
        }

        return Storage::makeDirectory($target);
    }

    public function delete(string $target) : bool
    {
        if (Storage::deleteDirectory($target)) {
            FilemanagerModel::where('directory', 'LIKE', $target .'%')->delete();
            return true;
        }

        return false;
    }

    public function rename(string $target, string $newPath) : bool
    {
        if (Storage::move($target, $newPath)) {
            //Update all urls with the new folder name.
            \DB::statement("
                UPDATE filemanager SET
                    path = REPLACE(path, ?, ?),
                    directory = ?
                WHERE
                    directory = ?;
            ", [
                $target,
                $newPath,
                $newPath,
                $target,
            ]);

            return true;
        }
        
        return false;
    }

    public function exists(string $target, string $name = null) : bool
    {
        if (is_null($name) == false) {
            $target .= '/'. $name;
        }

        return Storage::has($target);
    }
}

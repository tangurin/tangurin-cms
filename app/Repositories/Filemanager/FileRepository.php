<?php
namespace App\Repositories\Filemanager;

use App\Models\Filemanager as FilemanagerModel;
use App\Http\Resources\Filemanager\FileResource;
use App\Repositories\Filemanager\FileRepository;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
Use Image;
use Carbon\Carbon;

class FileRepository
{
    protected $config = [];
    protected $defaultPaginationNumber = 20;

    public function findByPath($filepath)
    {
        return FilemanagerModel::where('path', $filepath)->first();
    }

    public function collectFileData($filepath)
    {
        $availableTypes = config('filemanager.types');
        $absolutPath = Storage::disk()->path($filepath);
        $pathinfo = pathinfo($filepath);
        $extension = $pathinfo['extension'];
        $name = !empty($pathinfo['filename']) ? $pathinfo['filename'] : $pathinfo['basename'];
        $hidden = $name[0] == '.';
        $filesize = filesize($absolutPath);
        $imagesize = $hidden ? 0 : getimagesize($absolutPath);
        $isImage = exif_imagetype($absolutPath);
        $mimetype = mime_content_type($absolutPath);
        $type = isset($availableTypes[$mimetype]) ? $availableTypes[$mimetype] : explode('/', $mimetype)[0];
        $type = isset($availableTypes[$extension]) ? $availableTypes[$extension] : $type;
        $directory = $pathinfo['dirname'];

        $fileData['directory'] = $pathinfo['dirname'];
        $fileData['path'] = $filepath;
        $fileData['name'] = $name;
        $fileData['extension'] = strtolower($extension);
        $fileData['size'] = $filesize;
        $fileData['mimetype'] = $mimetype;
        $fileData['type'] = $type;
        $fileData['width'] = isset($imagesize[0]) ? $imagesize[0] : 0;
        $fileData['height'] = isset($imagesize[1]) ? $imagesize[1] : 0;
        $fileData['hidden'] = intval($hidden);
        $fileData['created_at'] = Carbon::createFromTimestamp( filemtime($absolutPath) )->toDateTimeString();

        return $fileData;
    }

    public function getFiles($directory)
    {
        $collection = FileResource::collection(
            FilemanagerModel::where('directory', $directory)
                ->when(config('filemanager.ignore_hidden_files'), function ($query) {
                    return $query->where('hidden', 0);
                })
                ->orderBy('created_at', 'asc')
                ->orderBy('type', 'asc')
                ->paginate(config('filemanager.files_per_page', $this->defaultPaginationNumber))
        );

        $collection = $this->runFileCheck($collection);

        return $collection;
    }

    public function searchFiles($keywords, $directory = null, $request)
    {
        if (empty($keywords)) {
            return [];
        }

        $directory = empty($directory) ? $this->getRootDirectory() : $directory;

        $searchResult = FilemanagerModel::where('directory', $directory)
            ->where(function($query) use ($keywords) {
                return $query->where('name', 'like', '%'. $keywords .'%')
                ->orWhere('extension', '=', $keywords)
                ->orWhere('type', 'like', '%'. $keywords .'%');
            })
            ->paginate(config('filemanager.files_per_page', $this->defaultPaginationNumber));

        return $searchResult;
    }

    public function runFileCheck($collection)
    {
        $missingFilesId = [];
        foreach ($collection as $key => $item) {
            if ($this->isFileExists($item->path) === false) {
                $missingFilesId[] = $item->id;
                $collection->forget($key);
            }
        }

        if (!empty($missingFilesId)) {
            FilemanagerModel::whereIn('id', $missingFilesId)->delete();
        }

        return $collection;
    }

    public function getAvailableFiletypes()
    {
        return FilemanagerModel::select('type')->groupBy('type')->pluck('type');
    }

    public function storeFile($file, $directory)
    {
        $filename = $file->getClientOriginalName();
        $path = $directory . '/' . $filename;
        $replacer = '{{suffix}}';
        $pathinfo = pathinfo($path);

        //Set the default pattern for duplication name finder
        $tryname = $filename . $replacer;
        //If first char are not .
        if ($pathinfo['basename'][0] != '.') {
            //Set pattern for not hidden file names
            $tryname = $pathinfo['filename'] . $replacer . '.' . $pathinfo['extension'];
        }

        $newName = $filename;
        $duplicationSuffix = 0;
        while (Storage::disk()->exists($directory .'/'. $newName)) {
            //If not first iteration
            if ($duplicationSuffix > 0) {
                //Set newname to the filename with an incremented number
                $newName = str_replace($replacer, $duplicationSuffix, $tryname);
            }
            
            $duplicationSuffix++;
        }

        return $file->storeAs($directory, $newName);
    }

    public function isFileExists($filepath)
    {
        $absolutPath = Storage::disk()->path($filepath);
        return file_exists($absolutPath);
    }

    public function getThumbnail($filePath)
    {
        \Debugbar::startMeasure('FilemanagerGetThumbnail','Time for Customfields\Repository@updateClass');
        $model = $this->findByPath($filePath);
        
        $thumbDir = $model->directory .'/'. config('filemanager.thumbnail_folder_name');
        $source = 'storage/'. $model->path;
        $quality = 70;

        $isValidExtension = in_array($model->extension, [
            'jpg', 'jpeg', 'png', 'gif', 'tif', 'bmp', 'ico', 'psd', 'webp',
        ]);

        $target = '';
        if (
            $model->type == 'image' &&
            $isValidExtension &&
            Storage::disk()->exists($model->path)
        ) {
            $target = $thumbDir .'/'. $model->filename;
            //If the thumb not already exists
            if (! Storage::disk()->exists($target)) {
                //Make sure directory exists
                Storage::disk()->makeDirectory($thumbDir);
                //Create a thumb if the source file exists and thumb not
                try {
                    Image::make($source)
                        ->resize(300, 300,  function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })
                        ->save(Storage::disk()->path($target), $quality);

                } catch (\Exception $e) {
                    $target = '';
                }
            }
        }

        \Debugbar::stopMeasure('FilemanagerGetThumbnail');
        return $target;
    }

    public function delete(string $target) : bool
    {
        if (Storage::delete($target)) {
            FilemanagerModel::where('path', $target)->delete();
            return true;
        }

        return false;
    }
}

<?php
namespace App\Repositories\Filemanager;

use App\Http\Resources\Filemanager\FileResource;
use App\Models\Filemanager as FilemanagerModel;
use App\Repositories\Filemanager\IndexRepository;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class IndexRepository
{
    public function refreshIndex()
    {
        //TODO
        //Should not truncate, index ID should always be the same
        FilemanagerModel::truncate();

        $filesWithData = [];
        $rootDirectory = config('filemanager.upload_directory', 'uploads');
        $ignoredDirectories = array_map(function($item) use ($rootDirectory) {
            return $rootDirectory .'/'. $item;
        }, config('filemanager.ignored_directories', []));
        $ignoredFiles = [];
        foreach (Storage::allFiles( $rootDirectory ) as $filepath) {
            //If directory or file is set as ignored in the config
            if (in_array(dirname($filepath), $ignoredDirectories) || in_array(basename($filepath), $ignoredFiles)) {
                continue;
            }
            //Get all file data
            $fileData = resolve(FileRepository::class)->collectFileData($filepath);
            $filesWithData[] = $fileData;
        }
        
        //Insert all files to the index
        if (!empty($filesWithData)) {
            FilemanagerModel::insert($filesWithData);
        }
        return true;
    }

    public function insertToIndex($fileData)
    {
        //Insert the file
        $model = FilemanagerModel::firstOrNew(['path' => $fileData['path']]);
        $model->fill($fileData);
        $model->save();
        return new FileResource($model);
    }

    public function updateInIndex(FilemanagerModel $model, $fileData)
    {
        $model->update($fileData);
        return $model;
    }

    public function deleteFromIndex(FilemanagerModel $file)
    {
        $file->delete();
    }

    public function deleteNotIndexedFiles()
    {
        
    }
}

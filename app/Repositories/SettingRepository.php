<?php
namespace App\Repositories;

use App\Models\Setting AS SettingModel;

class SettingRepository
{
    private $settings;

    public function __construct()
    {
         $this->settings = \Cache::rememberForever('settings', function() {
            return SettingModel::all();
        });
    }

    public function __call($method, $parameters)
    {
         return $this->settings->$method(...$parameters);
    }

    public function save($key, $value = 1)
    {
        $setting = SettingModel::updateOrCreate(
            ['key' => $key],
            ['value' => $value]
        );
    }
}

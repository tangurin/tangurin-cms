<?php
namespace App\Repositories;

use App\Tree;
use Config;
use View;

class ViewRepository
{
    public function shareGlobals()
    {
        View::share($this->getGlobals());
    }

    public function getGlobals()
    {
        if (defined('cacheVersion') === false) {
            $filePath = base_path('cacheVersion');
            if (!file_exists($filePath)) {
                file_put_contents($filePath, time());
            }

            define('cacheVersion', file_get_contents($filePath));
        }

        $viewData['cacheVersion'] = cacheVersion;
        $viewData['languageRepository'] = resolve(LanguageRepository::class);
        $viewData['pageRepository'] = resolve(PageRepository::class);
        $viewData['translation'] = resolve(TranslationRepository::class);
        $viewData['setting'] = resolve(SettingRepository::class);
        $viewData['environment'] = Config::get('app.env');
        $viewData['language'] = Config::get('app.locale');
        $viewData['template'] = Config::get('cms.template');
        $viewData['pageMenu'] = new Tree\Page\Menu(['wrappingTag' => false]);

        return $viewData;
    }
}

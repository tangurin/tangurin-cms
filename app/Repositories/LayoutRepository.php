<?php
namespace App\Repositories;

use App\Http\Controllers\Controller;
use Gate;
use View;

class LayoutRepository
{
    protected $controller = null;

    protected $config = [
        'index' => false,
        'form' => false,
        'create' => false,
        'edit' => false,
        'delete' => true,
        'viewable' => false,
        'createable' => false,
        'updateable' => false,
        'destroyable' => false,
    ];

    public function __construct()
    {
        $this->share();
    }

    public function setController(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function get($key, $default = null)
    {
        return isset($this->config[$key]) ? $this->config[$key] : $default;
    }

    public function set($key, $value)
    {
        if (is_array($key)) {
            $this->config = array_merge($this->config, $key);
            return $this;
        }

        $this->config[$key] = $value;
        $this->share();
        return $this;
    }

    public function disableForm()
    {
        $this->set('form', false);
    }

    public function disableDelete()
    {
        $this->set('deleteable', false);
    }

    public function disableCreate()
    {
        $this->set('createable', false);
    }

    public function passMethod($method)
    {
        if ($method == 'index') {
            $this->set('create', true); 
            $this->set('index', true);
        }

        if ($method == 'create' || $method == 'edit') {
            $this->set('form', true);
        }

        if ($method == 'edit') {
            $this->set('edit', true);
        }

        $this->checkPermissions();
    }

    public function checkPermissions()
    {
        $this->set('viewable', Gate::check('backend_view_'. $this->controller->page) ? true : false);
        $this->set('createable', Gate::check('backend_create_'. $this->controller->page) ? true : false);
        $this->set('updateable', Gate::check('backend_update_'. $this->controller->page) ? true : false);
        $this->set('destroyable', Gate::check('backend_delete_'. $this->controller->page) ? true : false);
    }

    protected function share()
    {
        View::share('layout', $this);
    }

    public function isIndex()
    {
        return $this->get('index');
    }

    public function isForm()
    {
        return $this->get('form');
    }

    public function isEdit()
    {
        return $this->get('edit');
    }

    public function destroyable()
    {
        return $this->get('destroyable');
    }

    public function updateable()
    {
        return $this->get('updateable');
    }

    public function createable()
    {
        return $this->get('createable');
    }

    public function manageable()
    {
        return $this->updateable() || $this->destroyable();
    }
}

<?php
namespace App\Repositories;

use App\Models\Translation AS TranslationModel;

class TranslationRepository
{
    protected $translations;

    public function fetchTranslations($force = false)
    {
        if (is_null($this->translations)) {
            $this->translations = \Cache::rememberForever('translations', function() {
                $translations = [];
                foreach (TranslationModel::with('values')->get() as $translation) {
                    foreach (resolve(LanguageRepository::class)->listAvailable() as $lang => $language) {
                        $translations[$lang][$translation->identifier] = $translation->getValue($lang);
                    }
                }

                return $translations;
            });;
        }

        return $this->translations;
    }

    public function get($identifier, $default = '', $language = null)
    {
        $this->fetchTranslations();
        $language = is_null($language) ? resolve(LanguageRepository::class)->current() : $language;

        if (isset($this->translations[$language][$identifier])) {
            return $this->translations[$language][$identifier];
        }

        static::createNotExistingIdentifier($identifier);

        return !empty($default) ? $default : $identifier;
    }

    public function getList($language = null)
    {
        $this->fetchTranslations();
        
        $language = is_null($language) ? resolve(LanguageRepository::class)->current() : $language;
        return $this->translations[$language];
    }

    public function getAllLists()
    {
        $this->fetchTranslations();
        return $this->translations;
    }

    public function getListsWithModel()
    {
        $translations = [];
        foreach (resolve(LanguageRepository::class)->listAvailable() as $languageCode => $language) {
            $translationsWithValue = TranslationModel::with(['values' => function($query) use ($languageCode) {
                $query->where('language', $languageCode);
            }])->get();

            $translationsWithValue->each(function($translation) use ($languageCode) {
                if ($translation->values->isEmpty()) {
                    $translation->values()->create([
                        'value' => $translation->identifier,
                        'language' => $languageCode,
                    ]);
                }
            });

            foreach ($translationsWithValue as $translation) {
                $translations[$languageCode][$translation->identifier] = $translation;
            }
        }

        return $translations;
    }

    public function getListWithModel($language = null)
    {
        $language = is_null($language) ? resolve(LanguageRepository::class)->current() : $language;
        $lists = $this->getListsWithModel();
        return isset($lists[$language]) ? $lists[$language] : [];
    }

    public function getStructuredList($language = null, $withAllData = false)
    {
        $structuredList = [];
        $list = $withAllData ? $this->getListWithModel($language) : $this->getList($language);
        foreach ($list as $key => $value) {
            array_set($structuredList, $key, $value);
        }
        return $structuredList;
    }

    public function createNotExistingIdentifier($identifier)
    {
        $translation = new TranslationModel();
        $translation = $translation->firstOrCreate(['identifier' => $identifier]);
        resolve(LanguageRepository::class)->listAvailable()->each(function($language, $languageCode) use ($translation, $identifier) {
            $translation->values()->firstOrCreate(['language' => $languageCode, 'value' => $identifier]);
        });
    }
}

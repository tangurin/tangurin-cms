<?php
namespace App\Repositories;

use App\Models\Role;
use App\Models\User;

class RoleRepository
{
    protected $roles = [];

    public function fetchRoles()
    {
        if (empty($this->roles)) {
            $this->roles = Role::all()->mapWithKeys(function($role) {
                return [$role['identifier'] => $role];
            });
        }
        
        return $this->roles;
    }

    public function getRole($identifier, $default = null)
    {
        $this->fetchRoles();

        if (is_string($identifier)) {
            return isset($this->roles[$identifier]) ? $this->roles[$identifier] : $default;
        }

        if (is_numeric($identifier)) {
            $found = $this->roles->filter(function($role, $key) use ($identifier) {
                return $role->id == $identifier;
            })->first();

            if (!empty($found)) {
                return $found;
            }
        }

        return $default;
    }

    public function getMainAdmin() 
    {
        return $this->getRole('mainAdmin');
    }

    public function getAdmin() 
    {
        return $this->getRole('admin');
    }

    public function getDefaultUserRole()
    {
        return $this->getRole( intval(setting('defaultUserRole', 0)) );
    }
}

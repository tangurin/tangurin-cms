<?php
namespace App\Repositories;

use App\Models\Permission AS PermissionModel;

class PermissionRepository
{
    public function getMultidimensional()
    {
        $sorted = [];
        PermissionModel::all()->each(function($permission) use(&$sorted) {
            $name = explode('_', $permission->name);
            $type = $name[0];
            $action = $name[1];
            $model = $name[2];
            $sorted[$type][$model][] = $action;
        });

        return $sorted;
    }

    public function getByName($name)
    {
        if (is_array($name)) {
            return PermissionModel::wherein('name', $name)->get();
        }
        
        return PermissionModel::where('name', $name)->first();
    }
}

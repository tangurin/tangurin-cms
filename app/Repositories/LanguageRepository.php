<?php
namespace App\Repositories;

use App\Models\Language AS LanguageModel;
use Illuminate\Support\Collection;
use LaravelLocalization;
use Session;

class LanguageRepository
{
    public function default()
    {
        return  LaravelLocalization::getDefaultLocale();
    }

    public function current()
    {
        return  LaravelLocalization::getCurrentLocale();
    }

    public function listAvailable()
    {
        return collect(LaravelLocalization::getSupportedLocales());
    }

    public function getFlag($languageCode)
    {
        return $languageCode .'.png';
    }

    public function fillLanguage($class, $input)
    {
        $input = collect($input);

        return $input->map(function($translationFields, $lang) use ($class, $input) {
            return $class::firstOrNew(['id' => $translationFields['id']])
                ->fill(['language' => $lang])
                ->fill($translationFields);
        });
    }
}

<?php
namespace App\Repositories;

use App\Repositories\Filemanager\DirectoryRepository;
use Illuminate\Support\Facades\Storage;
Use Image;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class ThumbnailRepository
{
    public function create($imageSource, $width, $height, $crop = false, $quality = 85)
    {
        if ($imageSource[0] == '/') {
            $imageSource = substr($imageSource, 1);
        }

        $imageFilename = $this->generateName($imageSource, $width, $height, $crop, $quality);
        $thumbnailFolder = resolve(DirectoryRepository::class)->getRootDirectory() . '/thumbnails/';
        $thumbnailPath = $thumbnailFolder . $imageFilename;

        //Create thumbnails directory if not exists
        \Storage::makeDirectory($thumbnailFolder);
        if ( ! Storage::disk()->exists($thumbnailPath)) {
            try {
                $image = Image::make($imageSource);

                if ($crop) {
                    $image->crop($width, $height);
                } else {
                    $image->resize($width, $height,  function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                }
                $image->save(Storage::disk()->path($thumbnailPath), $quality);

                //Optimize thumbnail
                $optimizerChain = OptimizerChainFactory::create();
                $optimizerChain->optimize(Storage::disk()->path($thumbnailPath));

            } catch (\Exception $e) {
                $thumbnailPath = '';
            }
        }

        return $thumbnailPath;
    }

    public function generateName($imageSource, $width, $height, $crop, $quality)
    {
        $filename = pathinfo($imageSource, PATHINFO_FILENAME);
        $filesuffix = [
            'w'. $width,
            'h'. $height,
            'q'. $quality,
        ];
        if ($crop) {
            $filesuffix[] = 'c';
        }

        $filename .= '_' . implode('_', $filesuffix);
        $filename .= '.'. pathinfo($imageSource, PATHINFO_EXTENSION);

        return $filename;
    }
}
?>

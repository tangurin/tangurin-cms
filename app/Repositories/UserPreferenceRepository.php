<?php
namespace App\Repositories;

class UserPreferenceRepository
{
    public function all($user)
    {

    }

    public function save($user, $key, $value = 1)
    {
        return $user->preferences()->updateOrCreate(
            ['key' => $key],
            ['value' => $value]
        );
    }

    public function delete($user, $key)
    {
        $setting = $user->preferences()->where('key', $key)->first();
        if (!empty($setting)) {
            $setting->delete();
            return true;
        }

        return false;
    }
}

<?php
namespace App\Collections;

class SettingCollection extends \Illuminate\Database\Eloquent\Collection
{
    /**
     * Create a new collection instance if the value isn't one already.
     *
     * @param  mixed  $items
     * @return static
     */
    public static function make($items = [])
    {
        return with(new static($items))->mapWithKeys(function($item) {
            return [$item->key => $item];
        });
    }

    public function onlyValue()
    {
        return $this->map(function($item) {
            return $item->value;
        });
    }

    public function listWithoutPrefix()
    {
        return $this
            ->mapWithKeys(function($setting) {
                return [str_after($setting->key, '.') => $setting->value];
            })
            ->toBase();
    }

    public function wherePrefix($prefix)
    {
        return $this->filter(function($setting, $key) use ($prefix) {
            return starts_with($setting->key, $prefix);
        });
    }
}

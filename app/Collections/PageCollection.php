<?php
namespace App\Collections;

use App\Repositories\PageRepository;

class PageCollection extends \Illuminate\Database\Eloquent\Collection
{
    public function inMenu()
    {
        return $this->filter(function($page, $key) {
            return $page->in_menu == 1;
        });
    }

    public function byStatus($status)
    {
        return $this->filter(function($page, $key) use ($status) {
            return $page->status == $status;
        });
    }
    
    public function published()
    {
        return $this->byStatus(PageRepository::PUBLISHED);
    }
}

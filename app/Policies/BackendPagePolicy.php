<?php
namespace App\Policies;

use App\Models\Page;

class BackendPagePolicy
{
    public function destroy(Page $page)
    {
        return $page->isStartpage() === false;
    }
}

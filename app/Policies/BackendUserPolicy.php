<?php
namespace App\Policies;

use App\Models\User;

class BackendUserPolicy
{
    public function update(User $user)
    {
        return true;
    }

    public function destroy(User $user)
    {
        return $user->isMainAdministrator() === false;
    }
}

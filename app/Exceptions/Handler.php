<?php

namespace App\Exceptions;

use Auth;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Mail;
use Route;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
        \Spatie\Permission\Exceptions\PermissionDoesNotExist::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        $exceptionClass = get_class($exception);
        $exceptionIdentifier = 'exception:'. $exceptionClass;
        if (isset($_COOKIE[$exceptionIdentifier]) === false && $this->shouldReport($exception)) {
            //Set cookie to avoid spamming exceptions
            setcookie($exceptionIdentifier, true, time() + (3600 * 24));
            if ($exception instanceof \PDOException) {
                $this->sendRawReport(
                    '[PDO - EXCEPTION] | '. env('APP_URL'),
                    'Database connection not working...'
                );
            } else {
                try {
                    Mail::send(
                        new \App\Mail\Exception(
                            $exception,
                            request()
                        )
                    );
                } catch (\Exception $e) {
                    $this->sendRawReport(
                        '[EXCEPTION] | '. env('APP_URL'),
                        $exception->__toString()
                    );
                }
            }
        }

        parent::report($exception);
    }

    public function sendRawReport($subject, $message)
    {
        \Mail::raw($message, function($message) use ($subject) {
           $message->subject($subject)->to( env('MAIL_ADMINISTRATOR', 'tangurin.se@gmail.com') );
        });
    }

    /**
     * Render the given HttpException.
     *
     * @param  \Symfony\Component\HttpKernel\Exception\HttpException  $e
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderHttpException(HttpException $e)
    {
        $status = $e->getStatusCode();

        if (Auth::user() && Auth::user()->canAccessBackend() && view()->exists("Backend::errors.{$status}")) {
            return response()->view("Backend::errors.{$status}", ['exception' => $e], $status, $e->getHeaders());
        }


        /*if ((!Auth::user() || !Auth::user()->canAccessBackend()) && view()->exists("Frontend::errors.{$status}")) {
            return response()->view("Frontend::errors.{$status}", ['exception' => $e], $status, $e->getHeaders());
        }*/

        return response()->view("Frontend::errors.{$status}", ['exception' => $e], $status, $e->getHeaders());

        return parent::renderHttpException($e);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        if (array_search('backend', $request->route()->computedMiddleware) !== false) {
            return redirect()->guest(route('backend.auth.login'));
        }

        return redirect()->guest(route('frontend.auth.login'));
    }
}

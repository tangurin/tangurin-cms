<?php namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class WhoopsExceptionHandler extends ExceptionHandler {

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        'Symfony\Component\HttpKernel\Exception\HttpException'
    ];

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if (config('app.debug')) {
            $whoops = new \Whoops\Run;

            if ($request->ajax()) {
                //$whoops->pushHandler(new \Whoops\Handler\JsonResponseHandler);
                $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
            } else {
                $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
            }

            return response($whoops->handleException($e),
                $e->getStatusCode(),
                $e->getHeaders()
            );
        }

        return parent::render($request, $e);
    }
}

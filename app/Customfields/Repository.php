<?php
namespace Customfields;

use DbFactory;
use PDO;

class Repository
{
    protected $relationClass;
    protected $relationIdentifier;
    protected $collections = [];
    protected $sortedValues = [];
    protected $executed = false;
    protected $build = [];
    protected $withData = false;
    protected $withSettings = false;
    protected $withTranslations = false;

    public function __construct($relationClass = null, $relationIdentifier = null)
    {
        $this->setRelations($relationClass, $relationIdentifier);
    }

    public function setRelations($relationClass, $relationIdentifier)
    {
        $this->relationClass = $relationClass;
        $this->relationIdentifier = $relationIdentifier;
    }

    protected function find($search, $default)
    {
        //Make sure the values has been built.
        $this->updateClass();
        return \Illuminate\Support\Arr::get($this->build, $search, $default);
    }

    public function collection($collection, $default = [])
    {
        return $this->collectionExists($collection) ? $this->build[$collection] : $default;
    }

    public function repeater($search, $default = [], $limit = 0)
    {
        $find = $this->find($search, $default);

        if (is_array($find)) {
            if (is_int($limit) && $limit > 0) {
                return array_slice($find, 0, $limit);
            }
            return $find;
        }

        return $default;
    }

    public function field($search, $default = null)
    {
        $default = is_null($default) ? new ValueRepository() : $default;

        $find = $this->find($search, false);
        if (!is_object($find)) {
            return $default;
        }

        return $find;
    }

    public function all()
    {
        $this->updateClass();
        return $this->build;
    }

    public function fieldExists($search)
    {
        return $this->find($search, false) !== false;
    }

    public function fieldEmpty($search)
    {
        $field = $this->field($search, false);
        return $field === false;
    }

    public function repeaterEmpty($search)
    {
        $find = $this->find($search, false);
        return !is_array($find) || empty($find);
    }

    public function isRepeater($search)
    {
        return is_array($this->find($search, false));
    }

    public function collectionExists($collection)
    {
        //Make sure the values has been built.
        $this->updateClass();
        return isset($this->build[$collection]);
    }

    protected function buildRecursiveValues($values, $parentId = '0')
    {
        $children = [];

        //Build the field values
        foreach (array_filter($values, function($value) use ($parentId) { return $value['parentValueId'] == $parentId; }) as $child) {
            if ($child["type"] === "Repeater") {
                $children[$child['identifier']][] = $this->buildRecursiveValues($values, $child['id']);
            } else {
                $children[$child['identifier']] = new ValueRepository($this->filterArray($child));
            }
        }

        return $children;
    }

    public function filterArray($data)
    {
        $data['valueId'] = $data['id'];
        unset(
            $data['id'],
            $data['collectionIdentifier'],
            $data['fieldId'],
            $data['collectionId'],
            $data['identifier'],
            $data['parentId'],
            $data['parentValueId']
        );

        return $data;
    }

    public function updateClass($force = false)
    {
        \Debugbar::startMeasure('CustomfieldsUpdateClass','Time for Customfields\Repository@updateClass');
        //Do not run if build is not empty or force is false
        if ($force === false && $this->executed === true) {
            return true;
        }

        $this->executed = true;

        //Be sure that we have relationClass and relationIdentifier
        if (is_null($this->relationClass) || is_null($this->relationIdentifier)) {
            return false;
        }

        $this->collections = ValueRelation::getCollections($this->relationClass, $this->relationIdentifier);

        //Get all values, related to this class and identifier
        $values = ValueRelation::getValues($this->relationClass, $this->relationIdentifier, true);
        //Process value
        $values = array_map(function($value) {
            $fieldClass = Field::getFieldByType($value['type']);
            $value['translation'] = $fieldClass->processOnFetch($value['translation']);
            return $value;
        }, $values);

        if ($this->withData) {
            $values = ValueRelation::appendData($values);
        }
        if ($this->withSettings) {
            $values = ValueRelation::appendSettings($values);
        }
        if ($this->withTranslations) {
            $values = ValueRelation::appendTranslations($values);
        }

        if (empty($values)) {
            return false;
        }
        
        foreach ($this->collections as $collection) {
            $collectionValues = array_filter($values, function($value) use ($collection) { return $value['collectionId'] == $collection['id']; });
            $this->build[$collection['identifier']] = $this->buildRecursiveValues($collectionValues);
        }

        \Debugbar::stopMeasure('CustomfieldsUpdateClass');
        return true;
    }

    public function withData($bool = true)
    {
        $this->withData = $bool;
    }

    public function withSettings($bool = true)
    {
        $this->withSettings = $bool;
    }

    public function withTranslations($bool = true)
    {
        $this->withTranslations = $bool;
    }

    public static function getModuleCustomfields($slug)
    {
        $module = CollectionModule::getModuleBySlug($slug);
        $moduleId = 0;
        if (is_null($module) === false) {
            $moduleId = $module->id;
        }

        return new static('Module', $moduleId);
    }
}

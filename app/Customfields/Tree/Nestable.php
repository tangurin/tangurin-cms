<?php
namespace Customfields\Tree;

class Nestable
{
    public static function generateMenuUlList($tree)
    {
        $html = '<ol class="dd-list">';
        $html .= self::generateMenuLiList($tree);
        $html .= '</ol>';

        return $html;
    }

    public static function generateMenuLiList($tree)
    {
        $makeTree = function($tree) use (&$makeTree) {
            $html = '';
            foreach ($tree as $limb) {
                $field = $limb['field'];
                $children = $makeTree($limb['children']);
                $repeaterClass = strtolower($field->type) == 'repeater' ? 'isRepeater' : 'notRepeater';
                $html .= '<li class="dd-item dd3-item '. $repeaterClass .'" data-id="'. $field->id .'">';
                    $html .= '<div class="dd-handle dd3-handle"></div>';
                    $html .= '<div class="dd3-content">';
                        $html .= '<a>';
                            $html .= $field->name;
                            $html .= '<small> - ('. $field->name .')';
                            $html .= ' | '. $field->type .')</small>';
                        $html .= '</a>';
                    $html .= '</div>';
                    if ($children != '') {
                        $html .= '<ol class="dd-list">';
                            $html .= $children;
                        $html .= '</ol>';
                    }
                $html .= '</li>';
            }
            return $html;
        };
        $html = $makeTree($tree);

        return $html;
    }
}

<?php
namespace Customfields;

use DbFactory;
use PDO;

class ValueRepository
{
    protected $field;
    protected $type;
    protected $data = [];
    protected $settings = [];
    protected $translations = [];
    protected $valueId = 0;
    protected $value = null;

    public function __construct($field = null)
    {
        if (is_null($field)) {
            return false;
        }

        $this->type = $field['type'];
        $this->valueId = $field['valueId'];
        $this->value = $field['translation'];
        
        $this->field = Field::getFieldByType($this->type);
        $this->field->setValue($this->value);

        if (isset($field['data'])) {
            $this->data = $field['data'];
        }

        if (isset($field['settings'])) {
            $this->settings = $field['settings'];
        }

        if (isset($field['translations'])) {
            $this->translations = $field['translations'];
        }
    }

    public function __get($key)
    {
        if (isset($this->{$key})) {
            return $this->{$key};
        }
        
        return null;
    }

    public function value($default = '')
    {
        if (empty($this->value)) {
            return $default;
        }

        return $this->value;
    }

    public function repeat($callback)
    {
        if (is_callable($callback) && is_array($this->value)) {
            foreach ($this->value as $key => $value) {
                $callback($key, $value);
            }
        }
    }

    public function isEmpty()
    {
        return empty($this->value);
    }

    public function isNotEmpty()
    {
        return ! $this->isEmpty();
    }

    public function isEmptyObject()
    {
        return is_null($this->field);
    }

    public function typeof()
    {
        return gettype($this->value);
    }

    public function data()
    {
        return $this->data;
    }

    public function settings()
    {
        return $this->settings;
    }

    public function translations()
    {
        return $this->translations;
    }
}

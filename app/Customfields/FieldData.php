<?php
namespace Customfields;

use DbFactory;
use PDO;

class FieldData extends \C4Media\ActiveRecord\Base
{
    protected static $tableName = 'customfields_collection_field_data';

    public function toArray()
    {
        return [
            'id' => $this->id,
            'fieldId' => $this->fieldId,
            'value' => $this->value,
            'label' => $this->label,
            'sortOrder' => $this->sortOrder,
        ];
    }
}

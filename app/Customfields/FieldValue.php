<?php
namespace Customfields;

use Customfields\FieldValueTranslation AS Translation;
use DbFactory;
use PDO;

class FieldValue extends \C4Media\ActiveRecord\Base
{
    protected static $tableName = 'customfields_collection_field_values';

    private $repeaterIndexMatches = [];
    private $currentRelationId;

    public function toArray()
    {
        return [
            'id' => $this->id,
            'parentValueId' => $this->parentValueId,
        ];
    }

    public function savePost($post)
    {
        $records = [];

        if (empty($post['relationClass']) || empty($post['relationIdentifier'])) {
            return false;
        }

        //Get relation ID
        $this->currentRelationId = $this->getRelationId($post['relationClass'], $post['relationIdentifier']);

        //Save repeaters
        if (isset($post['repeaters'])) {
            foreach ($post['repeaters'] as $repeaterIndex => $repeaterData) {
                $records[] = $this->saveRepeater($repeaterIndex, $repeaterData);
            }
        }

        //Save values
        if (isset($post['values'])) {
            foreach ($post['values'] as $valueIndex => $valueData) {
                $records[] = $this->saveValue($valueIndex, $valueData);
            }
        }

        $this->clearTranslations($records);
        $this->clearValues($records);

        return true;
    }

    public function saveRepeater($index, array $data)
    {
        static $sortOrder = 0;

        if (isset($this->repeaterIndexMatches[$data['parentValueId']])) {
            $data['parentValueId'] = $this->repeaterIndexMatches[$data['parentValueId']];
        }

        $value = static::find($index) ?: new static();
        $value->fieldId = $data['fieldId'];
        $value->relationId = $this->currentRelationId;
        $value->parentValueId = $data['parentValueId'];
        $value->sortOrder = ++$sortOrder;
        $value->save();

        if ($index != $value->id) {
            $this->repeaterIndexMatches[$index] = $value->id;
        }

        return $value;
    }

    public function saveValue($index, array $data)
    {
        if (isset($this->repeaterIndexMatches[$data['parentValueId']])) {
            $data['parentValueId'] = $this->repeaterIndexMatches[$data['parentValueId']];
        }


        $field = Field::getFieldByType($data['type']);
        $value = static::find($index) ?: new static();
        $value->fieldId = $data['fieldId'];
        $value->relationId = $this->currentRelationId;
        $value->parentValueId = $data['parentValueId'];
        $value->save();
        
        if (isset($data['value'])) {
            foreach ($data['value'] as $languageCode => $content) {
                $existingTranslations = FieldValueTranslation::where(['value_id =' => $value->id, 'language =' => $languageCode])->toArray();
                $translation = empty($existingTranslations) ? new Translation() : $existingTranslations[0];
                $translation->valueId = $value->id;
                $translation->content = $field->processOnSave($content);
                $translation->language = $languageCode;
                $translation->save();
            }
        } else {
            $valueId = $value->id;
            $stmt = DbFactory::getConnection()->prepare("
                DELETE FROM customfields_collection_field_value_translations
                    WHERE value_id = :valueId
            ");
            $stmt->bindParam('valueId', $valueId);
            $stmt->execute();
        }

        return $value;
    }

    public function clearTranslations($recordsToKeep)
    {
        $recordIds = implode(', ', array_map(function ($record) { return intval($record->id); }, $recordsToKeep)) ?: 0;

        $stmt = DbFactory::getConnection()->prepare("
            DELETE FROM customfields_collection_field_value_translations
                WHERE value_id IN (
                    SELECT id FROM customfields_collection_field_values
                    WHERE id NOT IN ($recordIds) AND relation_id = :relationId
                )
        ");
        $stmt->bindParam('relationId', $this->currentRelationId);
        $stmt->execute();
    }

    public function clearValues($recordsToKeep)
    {
        $recordIds = implode(', ', array_map(function ($record) { return intval($record->id); }, $recordsToKeep)) ?: 0;

        $stmt = DbFactory::getConnection()->prepare("
            DELETE FROM customfields_collection_field_values
                WHERE id NOT IN ($recordIds) AND relation_id = :relationId
        ");
        $stmt->bindParam('relationId', $this->currentRelationId);
        $stmt->execute();
    }

    public function getRelationId($relationClass, $relationIdentifier)
    {
        $relationId = $this->relationExists($relationClass, $relationIdentifier);
        if ($relationId === false) {
            $relationId = $this->createRelation($relationClass, $relationIdentifier);
        }

        return $relationId;
    }

    public function createRelation($relationClass, $relationIdentifier)
    {
        $valueRelation = new ValueRelation();
        $valueRelation->class = $relationClass;
        $valueRelation->relation = $relationIdentifier;
        $valueRelation->save();

        return $valueRelation->id;
    }

    public function relationExists($relationClass, $relationIdentifier)
    {
        $stmt = DbFactory::getConnection()->prepare("
            SELECT id FROM customfields_collection_field_value_relations AS relation
            WHERE class = :relationClass AND relation = :relationIdentifier
        ");
        $stmt->bindParam('relationClass', $relationClass);
        $stmt->bindParam('relationIdentifier', $relationIdentifier);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    public static function sortedCategories($ids)
    {
        if (empty($ids)) {
            return [];
        }
        $language = resolve(\App\Repositories\LanguageRepository::class)->current();
        $stmt = DbFactory::getConnection()->prepare("
            SELECT c.id, cd.name, (
                SELECT name FROM categories_description WHERE category_id = c.parent_id AND language = '". $language ."'
            ) AS parentName
            FROM categories AS c
            JOIN categories_description AS cd ON cd.category_id = c.id
            WHERE c.id IN (". implode(', ', $ids) .")
        ");
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        
        $sorted = [];
        foreach ($result as $category) {
            $sorted[$category['id']] = $category;
        }
        return $sorted;
    }
    public static function sortedProducts($ids)
    {
        if (empty($ids)) {
            return [];
        }
        $language = resolve(\App\Repositories\LanguageRepository::class)->current();
        $stmt = DbFactory::getConnection()->prepare("
            SELECT p.id, p.code, pd.name FROM products AS p
                JOIN products_description AS pd ON pd.product_id = p.id AND language = '". $language ."'
                WHERE p.id IN (". implode(', ', $ids) .")
        ");
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $sorted = [];
        foreach ($result as $product) {
            $sorted[$product['id']] = $product;
        }
        return $sorted;
    }
}

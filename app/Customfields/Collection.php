<?php
namespace Customfields;

use Customfields\Field;
use Customfields\FieldSetting;
use DbFactory;
use PDO;

class Collection extends \C4Media\ActiveRecord\Base
{
    protected static $tableName = 'customfields_collections';

    public static function getCategories()
    {
        return static::getMergedData('category');
    }

    protected static function getMergedData($column)
    {
        $data = [];
        foreach (Collection::all()->toArray() as $collection) {
            if (!isset($collection->{$column})) break;

            $value = $collection->{$column};
            if (!empty($value) && !in_array($value, $data)) {
                $data[$value] = $value;
            }
        }
        if (isset($_POST['collection'][$column])) {
            $posted = $_POST['collection'][$column];
            if (!empty($posted) && !isset($data[$posted])) {
                $data[] = $posted;
            }
        }
        return $data;
    }

    public static function getFieldTree($collectionId, $parent = 0)
    {
        $fields = Field::where(['collection_id =' => $collectionId, 'parent = ' => $parent])->order('parent', 'ASC')->toArray();

        $processed = [];
        foreach ($fields as $field) {
            $processed[$field->id]['field'] = $field;
            $processed[$field->id]['children'] = self::getFieldTree($collectionId, $field->id);
        }

        return $processed;
    }

    public static function getFields($collectionId)
    {
        $preparedFields = [];
        
        $fields = Field::where(array('collection_id =' => $collectionId))->order('parent_id', 'ASC')->order('sort_order', 'ASC')->toArray();
        foreach ($fields as $field) {
            $field->id = $field->id;
            $field->settings = Field::getStoredSettings($field->id);
            $field->data = Field::getStoredData($field->id);
            $preparedFields[] = $field;
        }

        return $preparedFields;
    }

    public static function getNextSortNumber()
    {
        $stmt = DbFactory::getConnection()->prepare(
            "SELECT MAX(sort_order) AS highest FROM customfields_collections"
        );
        $stmt->execute();

        $highest = $stmt->fetchColumn();
        if (!empty($highest)) {
            return intval($highest) + 1;
        }
        return 0;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'identifier' => $this->identifier,
            'description' => $this->description,
            'category' => $this->category,
            'noRelation' => $this->noRelation,
            'moduleId' => $this->moduleId,
            'sortOrder' => $this->sortOrder,
        ];
    }
}

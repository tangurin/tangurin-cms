<?php
namespace Customfields;

use App\Models\Page;
use App\Repositories\PageRepository;
use Category;
use Category_Tree;
use Product;

class Relation extends \C4Media\ActiveRecord\Base
{
    protected static $tableName = 'customfields_collection_relations';

    public static function getClasses()
    {
        return [
            [
                'value' => 'Page',
                'label' => 'Page'
            ], [
                'value' => 'Module',
                'label' => 'Egen Modul'
            ],
        ];
    }

    public static function getRelations()
    {
        return [
            'Page' => [
                [
                    'value' => '=',
                    'label' => 'Page =',
                    'target' => 'allPages'
                ], [
                    'value' => 'parent',
                    'label' => 'Parent =',
                    'target' => 'allPages'
                ], [
                    'value' => 'template',
                    'label' => 'Template =',
                    'target' => 'allTemplates'
                ], [
                    'value' => 'all',
                    'label' => 'All',
                    'target' => 'all'
                ],
            ],
            'Module' => [
                [
                    'value' => '=',
                    'label' => '=',
                    'target' => 'allModules'
                ], [
                    'value' => 'all',
                    'label' => 'All',
                    'target' => 'all'
                ],
            ],

        ];
    }

    public static function getRelationsId()
    {
        $all = [[
            'id' => 'all',
            'name' => 'All'
        ]];

        $templates = resolve(PageRepository::class)->getTemplates();
        $templates = array_map(function($name) {
            return ['name' => $name];
        }, $templates);

        $pages = Page::with('translations')->get();
        $pages = $pages->map(function($page) {
            return [
                'id' => $page->id,
                'name' => $page->translations[0]->title,
            ];
        });


        return [
            'Page' => [
                'allPages' => $pages,
                'allTemplates' => $templates,
                'all' => $all
            ],
            'Module' => [
                'allModules' => CollectionModule::getCustomFieldsJSON(),
                'all' => $all
            ]
        ];
    }
}

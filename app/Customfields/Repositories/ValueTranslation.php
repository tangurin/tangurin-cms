<?php
namespace Customfields\Repositories;

use Customfields\FieldValueTranslation;

class ValueTranslation
{
    public function create(array $data)
    {
        $translation = new FieldValueTranslation();
        $translation->updateAttributes($data);
        $translation->save();

        return $translation;
    }
}

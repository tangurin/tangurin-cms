<?php
namespace Customfields\Repositories;

use Customfields\Collection as CollectionModel;

class Collection
{
    public function create(array $data)
    {
        $collection = new CollectionModel();
        $collection->updateAttributes($data);
        $collection->save();

        return $collection;
    }
}

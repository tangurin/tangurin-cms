<?php
namespace Customfields\Repositories;

use Customfields\FieldData as DataModel;

class Data
{
    public function create(array $data)
    {
        $dataModel = new DataModel();
        $dataModel->updateAttributes($data);
        $dataModel->save();

        return $dataModel;
    }
}

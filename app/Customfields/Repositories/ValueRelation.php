<?php
namespace Customfields\Repositories;

use Customfields\ValueRelation as ValueRelationModel;

class ValueRelation
{
    public function create(array $data)
    {
        $relation = new ValueRelationModel();
        $relation->updateAttributes($data);
        $relation->save();

        return $relation;
    }
}

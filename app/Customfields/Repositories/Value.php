<?php
namespace Customfields\Repositories;

use Customfields\Value as ValueModel;

class Value
{
    public function create(array $data)
    {
        $value = new ValueModel();
        $value->updateAttributes($data);
        $value->save();

        return $value;
    }
}

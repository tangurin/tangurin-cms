<?php
namespace Customfields\Repositories;

use Customfields\Collection as CollectionModule;
use Customfields\Relation as RelationModel;

class Relation
{
    public function create(CollectionModule $collection, array $data)
    {
        $relation = new RelationModel();
        $data['collection_id'] = $collection->id;
        $relation->updateAttributes($data);
        $relation->save();

        return $relation;
    }
}

<?php
namespace Customfields\Repositories;

use Customfields\FieldSetting as SettingModel;

class Setting
{
    public function create(array $data)
    {
        $setting = new SettingModel();
        $setting->updateAttributes($data);
        $setting->save();

        return $setting;
    }
}

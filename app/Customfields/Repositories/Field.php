<?php
namespace Customfields\Repositories;

use Customfields\Collection as CollectionModule;
use Customfields\Field as FieldModel;

class Field
{
    public function create(CollectionModule $collection, array $data)
    {
        $field = new FieldModel();
        $data['collection_id'] = $collection->id;
        $field->updateAttributes($data);
        $field->save();

        return $field;
    }
}

<?php
namespace Customfields\Repositories;

use Customfields\CollectionModule as ModuleModel;

class Module
{
    public function create(array $data)
    {
        $module = new ModuleModel();
        $module->updateAttributes($data);
        $module->save();

        return $module;
    }
}

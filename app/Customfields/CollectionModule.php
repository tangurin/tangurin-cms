<?php
namespace Customfields;

use DbFactory;
use PDO;

class CollectionModule extends \C4Media\ActiveRecord\Base
{
    protected static $tableName = 'customfields_collection_modules';

    public static function getAllModules()
    {
        $modules = static::all()->toArray();

        return array_map(function($module) {
            return $module->toArray();
        }, $modules);
    }

    public static function getAllUsedModules()
    {
        $stmt = DbFactory::getConnection()->prepare("
            SELECT module.id FROM customfields_collection_modules AS module
                JOIN customfields_collection_relations AS relation ON relation.class = 'Module' AND relation.relation_id = module.id
        ");
        $stmt->execute();

        $modules = [];
        foreach ($stmt->fetchAll(PDO::FETCH_COLUMN) as $moduleId) {
            if (isset($modules[$moduleId])) {
                continue;
            }

            $modules[$moduleId] = static::find($moduleId)->toArray();
        }

        return $modules;
    }

    public static function saveModule($slug, $label)
    {
        if (!empty($slug) && !empty($label)) {
            $exists = static::where(['slug =' => $slug])->toArray();
            if (empty($exists)) {
                $collectionModule = new static();
                $collectionModule->slug = $slug;
                $collectionModule->label = $label;
                $collectionModule->save();
                return $collectionModule;
            }
        }
        return false;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'label' => $this->label,
        ];
    }

    public static function moduleNameAvailable($label, $slug, $except = 0)
    {
        $stmt = DbFactory::getConnection()->prepare("
            SELECT id FROM customfields_collection_modules AS module
            WHERE (label = :label OR slug = :slug) AND id != :except
        ");
        
        $stmt->execute([
            'label' => $label,
            'slug' => $slug,
            'except' => $except
        ]);
        
        $id = $stmt->fetchColumn();
        
        return empty($id);
    }

    public static function getModuleBySlug($slug)
    {
        return static::find($slug, 'slug');
    }

    public static function getCustomFieldsJSON()
    {
        return  array_map(function($module) {
            $module = $module->toArray();
            $module['name'] = $module['label'];
            return $module;
        }, static::all()->toArray());
    }
}

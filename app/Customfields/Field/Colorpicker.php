<?php
namespace Customfields\Field;

use DbFactory;
use PDO;

class Colorpicker extends Base
{
    public static function getSettings()
    {
        $settings = array_merge(self::$predefinedSettings, [
            'isValidColor' => [
                'value' => true,
                'label' => 'Validera att valet är en färg'
            ]
        ]);
        
        return $settings;
    }

    public function getHtml()
    {
        $html = '<input type="color" '. $this->getFieldAttributes() .' value="{{value}}" />';
        return $html;
    }
}
?>

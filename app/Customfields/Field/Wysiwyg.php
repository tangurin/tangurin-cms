<?php
namespace Customfields\Field;

use DbFactory;
use PDO;

class Wysiwyg extends Base
{
    public static function getSettings()
    {
        $settings = array_merge(self::$predefinedSettings, [
            'minLength' => [
                'value' => '',
                'label' => 'Minst antal tecken',
                'editable' => true,
                'onlyNumbers' => true,
                'allowEmpty' =>  true
            ]
        ]);

        return $settings;
    }

    public function getHtml()
    {
        $html = '<textarea '. $this->getFieldAttributes() .'>{{value}}</textarea>';
        return $html;
    }
}
?>

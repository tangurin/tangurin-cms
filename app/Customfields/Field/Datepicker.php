<?php
namespace Customfields\Field;

use DbFactory;
use PDO;

class Datepicker extends Base
{
    public static function getSettings()
    {
        $settings = array_merge(self::$predefinedSettings, [
            'onlyNumbers' => [
                'value' => true,
                'label' => 'Endast numeriska värden'
            ],
            'onlyEmail' => [
                'value' => true,
                'label' => 'Endast E-post'
            ],
            'onlyCamelCase' => [
                'value' => true,
                'label' => 'Endast camelcase är tillåtet'
            ],
        ]);

        return $settings;
    }

    public function getHtml()
    {
        $html = '<input type="text" '. $this->getFieldAttributes() .' value="{{value}}" placeholder="mm/dd/yyyy" />';
        return $html;
    }
}
?>

<?php
namespace Customfields\Field;

use DbFactory;
use PDO;

class File extends Base
{
    public static function getSettings()
    {
        $settings = array_merge(self::$predefinedSettings, [
        ]);
        return $settings;
    }

    public function getHtml()
    {
        $html = '<input type="text" '. $this->getFieldAttributes() .' value="{{value}}" placeholder="Klicka här för att välja en fil..." />';
        return $html;
    }
}
?>

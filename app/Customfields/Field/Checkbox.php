<?php
namespace Customfields\Field;

use DbFactory;
use PDO;

class Checkbox extends Base
{
    public static function getSettings()
    {
        $settings = array_merge(self::$predefinedSettings, [
        ]);

        return $settings;
    }

    public function getHtml()
    {
        $html = '<div class="checkboxes">';
        $extraWrappers = count($this->data) > 1;
        $i = 1;
        foreach ($this->data as $data) {
            $fieldAttributeId = 'checkbox'. $i++ .'-'. $this->attributeId;
            $html .= '<div class="checkbox checkbox-inline">';
                $html .= '<input type="checkbox" '. $this->getFieldAttributes([], false, $extraWrappers) .'  value="'. $data['value'] .'" id="'. $fieldAttributeId .'"> <label for="'. $fieldAttributeId .'">'. $data['label'] .'</label>';
            $html .= '</div>';
        }
        $html .= '</div>';
        return $html;
    }

    public function processOnSave($value)
    {
        if (is_array($value)) {
            return json_encode($value);
        }

        return $value;
    }

    public function processOnFetch($value)
    {
        $decoded = json_decode($value);
        if (json_last_error() === JSON_ERROR_NONE) {
            return $decoded;
        }

        return $value;
    }
}
?>

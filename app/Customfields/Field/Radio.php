<?php
namespace Customfields\Field;

use DbFactory;
use PDO;

class Radio extends Base
{
    public static function getSettings()
    {
        $settings = array_merge(self::$predefinedSettings, [
        ]);
        
        return $settings;
    }

    public function getHtml()
    {
        $html = '<div class="radios">';
        $i = 1;
        foreach ($this->data as $data) {
            $fieldAttributeId = 'radio'. $i .'-'. $this->attributeId;
            $html .= '<div class="radio radio-inline">';
                $html .= '<input type="radio" '. $this->getFieldAttributes([], false) .'  value="'. $data['value'] .'" id="'. $fieldAttributeId .'"'. ($i == 1 ? ' checked' : '') .'> <label for="'. $fieldAttributeId .'">'. $data['label'] .'</label>';
            $html .= '</div>';
            $i++;
        }
        $html .= '</div>';
        return $html;
    }
}
?>

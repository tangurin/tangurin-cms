<?php
namespace Customfields\Field;

use DbFactory;
use PDO;

class Textarea extends Base
{
    public static function getSettings()
    {
        $settings = array_merge(self::$predefinedSettings, [
        ]);
        
        return $settings;
    }

    public function getHtml()
    {
        $html = '<textarea '. $this->getFieldAttributes() .'>{{value}}</textarea>';
        return $html;
    }
}
?>

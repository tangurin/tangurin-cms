<?php
namespace Customfields\Field;

use DbFactory;
use PDO;

class Select extends Base
{
    public static function getSettings()
    {
        $settings = array_merge(self::$predefinedSettings, [
            'multiselect' => [
                'value' => true,
                'label' => 'Multiselect - Flervalsmöjlighet'
            ],
        ]);
        
        return $settings;
    }

    public function getHtml()
    {
        $isMultiSelect = empty($this->setting('multiselect')) === false;
        $html = '<select '. $this->getFieldAttributes([], true, $isMultiSelect) . ($isMultiSelect ? ' multiple' : '') .'>';
            foreach ($this->data as $data) {
                $html .= '<option value="'. $data['value'] .'">'. $data['label'] .'</option>';
            }
        $html .= '</select>';
        return $html;
    }

    public function processOnSave($value)
    {
        if (is_array($value)) {
            return json_encode($value);
        }

        return $value;
    }

    public function processOnFetch($value)
    {
        $decoded = json_decode($value);
        if (json_last_error() === JSON_ERROR_NONE) {
            return $decoded;
        }

        return $value;
    }
}
?>

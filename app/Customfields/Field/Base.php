<?php
namespace Customfields\Field;

use Customfields\FieldSetting;
use Customfields\FieldData;

class Base extends \Customfields\Field
{
    protected $value = null;
    protected $settings = [];
    protected $data = [];
    protected $attributeId = '{{attributeId}}';
    protected static $predefinedSettings = [
        'notEmpty' => [
            'value' => true,
            'label' => 'Ej lämnas tomt'
        ],
        'lgCols' => [
            'value' => '',
            'label' => 'Kolumner (lg) 1-12',
            'editable' => true,
            'onlyNumbers' => true,
            'minNumber' => 1,
            'maxNumber' => 12,
        ],
        'mdCols' => [
            'value' => '',
            'label' => 'Kolumner (md) 1-12',
            'editable' => true,
            'onlyNumbers' => true,
            'minNumber' => 1,
            'maxNumber' => 12,
        ],
        'smCols' => [
            'value' => '',
            'label' => 'Kolumner (sm) 1-12',
            'editable' => true,
            'onlyNumbers' => true,
            'minNumber' => 1,
            'maxNumber' => 12,
        ]
    ];

    public function setFieldData()
    {
        $relatedSettings = FieldSetting::where(['field_id =' => $this->id])->order('id', 'ASC')->toArray();
        foreach ($relatedSettings as $setting) {
            $this->settings[$setting->key] = $setting->value;
        }

        $relatedFieldData = FieldData::where(['field_id =' => $this->id])->order('sort_order', 'ASC')->toArray();
        foreach ($relatedFieldData as $fieldData) {
            $this->data[] = $fieldData->toArray();
        }

        return $this;
    }

    public function getClasses()
    {
        return [
            'form-control',
            'customfieldsField',
            'fieldIdentifier-'. ucfirst($this->identifier),
            'fieldType-'. ucfirst($this->type),
        ];
    }

    public function getHiddenFields()
    {
        $html = '<input type="hidden" name="{{fieldName}}[{{fieldValueId}}][fieldId]" value="'. $this->id.'" />';
        $html .= '<input type="hidden" name="{{fieldName}}[{{fieldValueId}}][type]" value="'. $this->type.'" />';
        $html .= '<input type="hidden" name="{{fieldName}}[{{fieldValueId}}][parentValueId]" value="{{parentValueId}}" />';
        return $html;
    }

    protected function getName()
    {
        return '{{fieldName}}[{{fieldValueId}}][value][{{language}}]';
    }

    protected function getFieldAttributes($classes = [], $id = true, $multiArray = false)
    {
        
        $extraWrappers = $multiArray ? '[]' : '';
        $attributes[] = 'name="'. $this->getName() . $extraWrappers .'"';

        if (is_array($classes)) {
            $mergedClasses = array_merge($this->getClasses(), $classes);
            $attributes[] = 'class="'. implode(' ', $mergedClasses) .'"';
        }

        if ($id === true) {
            $attributes[] = 'id="'. $this->attributeId .'"';
        }

        return implode(' ', $attributes);
    }

    public function getHtml()
    {
        return '';
    }

    public static function getSettings()
    {
        return [
            'notEmpty' => 'Ej tomt',
        ];
    }

    public function setting($key)
    {
        if (isset($this->settings[$key])) {
            return $this->settings[$key];
        }

        return null;
    }

    public function toArray()
    {
        $parentValues = parent::toArray();

        return array_merge($parentValues, [
            'html' => $this->getHtml(),
            'hiddenFields' => $this->getHiddenFields(),
            'isRepeater' => strtolower($this->type) == 'repeater' ? true : false,
            'hasSettings' => empty($settings) == false,
            'hasFieldData' => empty($fieldData) == false,
            'settings' => $this->settings,
            'fieldData' => $this->data
        ]);
    }

    public function processOnSave($value)
    {
        return $value;
    }

    public function processOnFetch($value)
    {
        return $value;
    }

    public function validateValue($value)
    {
        return [];
    }

    public function setValue($value)
    {
        $this->value = $value;
    }
}

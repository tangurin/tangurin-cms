<?php
namespace Customfields;

use DbFactory;
use PDO;

class FieldSetting extends \C4Media\ActiveRecord\Base
{
    protected static $tableName = 'customfields_collection_field_settings';

    public function toArray()
    {
        return [
            'id' => $this->id,
            'fieldId' => $this->fieldId,
            'key' => $this->key,
            'value' => $this->value,
        ];
    }
}

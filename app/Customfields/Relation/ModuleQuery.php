<?php
namespace Customfields\Relation;

class ModuleQuery extends QueryInterface
{
    public function getCollectionIds($identifier)
    {
        return array_unique(
            $this->getModuleCollectionIds($identifier)
                + $this->getAllCollectionIds()
        );
    }

    private function getModuleCollectionIds($identifier)
    {
        return $this->runQuery(
            "SELECT `collection_id`
                FROM `customfields_collection_relations`
                WHERE `class` = 'Module' AND `relation` = '=' AND `relation_id` = :identifier",
            ['identifier' => $identifier]
        );
    }

    private function getAllCollectionIds()
    {
        return $this->runQuery(
            "SELECT `collection_id`
                FROM `customfields_collection_relations`
                WHERE `class` = 'Module' AND `relation` = 'all'"
        );
    }
}

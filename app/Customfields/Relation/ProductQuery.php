<?php
namespace Customfields\Relation;

class ProductQuery extends QueryInterface
{
    public function getCollectionIds($identifier)
    {
        return array_unique(
            $this->getProductCollectionIds($identifier)
                + $this->getParentCollectionIds($identifier)
                + $this->getTemplateCollectionIds($identifier)
                + $this->getAllCollectionIds()
        );
    }

    private function getProductCollectionIds($identifier)
    {
        return $this->runQuery(
            "SELECT `collection_id`
                FROM `customfields_collection_relations`
                WHERE `class` = 'Product' AND `relation` = '=' AND `relation_id` = :identifier",
            ['identifier' => $identifier]
        );
    }

    private function getParentCollectionIds($identifier)
    {
        $parentIds = implode(', ', $this->getParentIds($identifier)) ?: '0';

        return $this->runQuery(
            "SELECT `collection_id`
                FROM `customfields_collection_relations`
                WHERE `class` = 'Product' AND `relation` = 'parent' AND `relation_id` IN ($parentIds)",
            ['identifier' => $identifier]
        );
    }

    private function getParentIds($productId)
    {
        $parent = $this->runQuery(
            "SELECT category FROM products_categories WHERE product_id = :Id",
            ['Id' => $productId]
        );

        if (empty($parent)) {
            return [];
        }

        $parents = $this->getParentIds($parent[0]);
        $parents[] = $parent[0];
        return $parents;
    }

    private function getTemplateCollectionIds($identifier)
    {
        return $this->runQuery(
            "SELECT `collection_id`
                FROM `customfields_collection_relations`
                WHERE `class` = 'Product' AND `relation` = 'template' AND `relation_id` IN (
                    SELECT `template`
                    FROM `products`
                    WHERE `id` = :identifier
                )",
            ['identifier' => $identifier]
        );
    }

    private function getAllCollectionIds()
    {
        return $this->runQuery(
            "SELECT `collection_id`
                FROM `customfields_collection_relations`
                WHERE `class` = 'Product' AND `relation` = 'all'"
        );
    }
}

<?php
namespace Customfields\Relation;

class PageQuery extends QueryInterface
{
    public function getCollectionIds($identifier)
    {
        return array_unique(
            $this->getPageCollectionIds($identifier)
                + $this->getParentCollectionIds($identifier)
                + $this->getTemplateCollectionIds($identifier)
                + $this->getAllCollectionIds()
        );
    }

    private function getPageCollectionIds($identifier)
    {
        return $this->runQuery(
            "SELECT `collection_id`
                FROM `customfields_collection_relations`
                WHERE `class` = 'Page' AND `relation` = '=' AND `relation_id` = :identifier",
            ['identifier' => $identifier]
        );
    }

    private function getParentCollectionIds($identifier)
    {
        $parentIds = implode(', ', $this->getParentIds($identifier)) ?: '0';

        return $this->runQuery(
            "SELECT `collection_id`
                FROM `customfields_collection_relations`
                WHERE `class` = 'Page' AND `relation` = 'parent' AND `relation_id` IN ($parentIds)",
            ['identifier' => $identifier]
        );
    }

    private function getParentIds($pageId)
    {
        $parent = $this->runQuery(
            "SELECT parent FROM pages WHERE id = :Id",
            ['Id' => $pageId]
        );

        if (empty($parent)) {
            return [];
        }

        $parents = $this->getParentIds($parent[0]);
        $parents[] = $parent[0];
        return $parents;
    }

    private function getTemplateCollectionIds($identifier)
    {
        return $this->runQuery(
            "SELECT `collection_id`
                FROM `customfields_collection_relations`
                WHERE `class` = 'Page' AND `relation` = 'template' AND `relation_id` IN (
                    SELECT `template`
                    FROM `pages`
                    WHERE `id` = :identifier
                )",
            ['identifier' => $identifier]
        );
    }

    private function getAllCollectionIds()
    {
        return $this->runQuery(
            "SELECT `collection_id`
                FROM `customfields_collection_relations`
                WHERE `class` = 'Page' AND `relation` = 'all'"
        );
    }
}

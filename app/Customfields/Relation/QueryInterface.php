<?php
namespace Customfields\Relation;

use DbFactory;
use PDO;

abstract class QueryInterface
{
    abstract public function getCollectionIds($identifier);

    protected function runQuery($query, $parameters = [])
    {
        $stmt = DbFactory::getConnection()->prepare($query);

        $stmt->execute($parameters);

        return $stmt->fetchAll(PDO::FETCH_COLUMN);
    }
}

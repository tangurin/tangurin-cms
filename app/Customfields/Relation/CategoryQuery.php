<?php
namespace Customfields\Relation;

class CategoryQuery extends QueryInterface
{
    public function getCollectionIds($identifier)
    {
        return array_unique(
            $this->getCategoryCollectionIds($identifier)
                + $this->getParentCollectionIds($identifier)
                + $this->getTemplateCollectionIds($identifier)
                + $this->getAllCollectionIds()
        );
    }

    private function getCategoryCollectionIds($identifier)
    {
        return $this->runQuery(
            "SELECT `collection_id`
                FROM `customfields_collection_relations`
                WHERE `class` = 'Category' AND `relation` = '=' AND `relation_id` = :identifier",
            ['identifier' => $identifier]
        );
    }

    private function getParentCollectionIds($identifier)
    {
        $parentIds = implode(', ', $this->getParentIds($identifier)) ?: '0';

        return $this->runQuery(
            "SELECT `collection_id`
                FROM `customfields_collection_relations`
                WHERE `class` = 'Category' AND `relation` = 'parent' AND `relation_id` IN ($parentIds)",
            ['identifier' => $identifier]
        );
    }

    private function getParentIds($categoryId)
    {
        $parent = $this->runQuery(
            "SELECT parent FROM categories WHERE id = :Id",
            ['Id' => $categoryId]
        );

        if (empty($parent)) {
            return [];
        }

        $parents = $this->getParentIds($parent[0]);
        $parents[] = $parent[0];
        return $parents;
    }

    private function getTemplateCollectionIds($identifier)
    {
        return $this->runQuery(
            "SELECT `collection_id`
                FROM `customfields_collection_relations`
                WHERE `class` = 'Category' AND `relation` = 'template' AND `relation_id` IN (
                    SELECT `template`
                    FROM `categories`
                    WHERE `id` = :identifier
                )",
            ['identifier' => $identifier]
        );
    }

    private function getAllCollectionIds()
    {
        return $this->runQuery(
            "SELECT `collection_id`
                FROM `customfields_collection_relations`
                WHERE `class` = 'Category' AND `relation` = 'all'"
        );
    }
}

<?php
namespace Customfields;

use DbFactory;
use PDO;

class Field extends \C4Media\ActiveRecord\Base
{
    protected static $tableName = 'customfields_collection_fields';

    public function toArray()
    {
        return [
            'id' => $this->id,
            'collectionId' => intval($this->collectionId),
            'parentId' => intval($this->parentId),
            'name' => $this->name,
            'identifier' => $this->identifier,
            'description' => $this->description,
            'type' => $this->type,
            'active' => $this->active,
        ];
    }

    public static function getFieldTypes($withRepeater = false)
    {
        $fields = [
            'Text' => 'Textf&auml;lt',
            'Textarea' => 'Textarea',
            'Wysiwyg' => 'Wysiwyg Editor',
            'Select' => 'Select / Dropdown',
            'Radio' => 'Radio knappar',
            'Checkbox' => 'Checkboxes',
            'Image' => 'Bildv&auml;ljare',
            'File' => 'Filv&auml;ljare',
            'Datepicker' => 'Datumv&auml;ljare',
            'Colorpicker' => 'F&auml;rgv&auml;ljare',
        ];

        if ($withRepeater) {
            $fields['Repeater'] = 'Repeater';
        }
        return $fields;
    }

    public static function getStoredSettings($fieldId)
    {
        $settings = FieldSetting::where(['field_id =' => $fieldId])->toArray();
        $preparedSettings = [];
        foreach ($settings as $setting) {
            $preparedSettings[] = $setting->toArray();
        }
        return $preparedSettings;
    }

    public static function getStoredData($fieldId)
    {
        $fieldData = FieldData::where(['field_id =' => $fieldId])->toArray();
        $preparedFieldData = [];
        foreach ($fieldData as $data) {
            $preparedFieldData[] = $data->toArray();
        }
        return $preparedFieldData;
    }

    public static function getFieldByType($type, $fieldId = null)
    {
        $class = '\Customfields\Field\\'. $type;
        if (is_null($fieldId)) {
            return new $class();
        }
        return $class::find($fieldId)->setFieldData();
    }

    public static function getFieldById($fieldId)
    {
        $field = static::find($fieldId);
        if (is_null($fieldId)) {
            return false;
        }
        $class = '\Customfields\Field\\'. $field->type;
        return $class::find($fieldId)->setFieldData();

    }
}

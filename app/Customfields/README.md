# Customfields #

## Class - Repository
    C4Media\Customfields\Repository

### Where can I use it
The Customfields repository is assigned to the presentationdata of those objects:

 - Page
 - Category
 - Product

These objects presentation data contains a customfield object. Example:

    $pageData['customfields']
    $productData['customfields']
    $categoryData['customfields']

Now you can fetch customfields for each object:

    $pageData['customfields']->repeater('collection.repeater')
    $productData['customfields']->repeater('collection.repeater')
    $categoryData['customfields']->repeater('collection.repeater')

If you want to fetch Customfields from a "Customfields module" you have to fetch the **Customfields repository** with:

    C4Media\Customfields\Repository::getModuleCustomfields($moduleSlug)

This will return a customfield repository where you can fetch the customfields from.

### Methods
**Fetching values**

 - **collection(\$collection, \$default = [])**
     - Fetch a collection with alla customfield values assigned to it. If none is found, return \$default.
 - **repeater(\$search, \$default = [], $limit = 0)**
     - Fetch a repeater. **\$default** is returned if no repeater was found. **\$limit** is the limit of how many repetitions you will get returned. 0 is unlimited.
 - **field(\$search, \$default = null)**
     - Fetch a specific field. This method will always return a **ValueRepository object** if default is set to null. If field not found an empty **ValueRepository object** will be returned.
 - **all()**
     - Returns all customfield collections.
 - **fieldExists(\$search)**
     - Returns true/false. Checks if the field exists in the collection
 - **fieldEmpty(\$search)**
     - Returns true/false. Checks if the field exists and if it is empty.
 - **isRepeater(\$search)**
     - Returns true/false. Check if a field is a repeater.
 - **collectionExists(\$search)**
     - Returns true/false. Check if a collection exists.

***Before fetching values***

 - **withData(\$bool = true)**
     - Fetch all field data and assign to the **ValueRepository**.
 - **withSettings(\$bool = true)**
     - Fetch all field settings and assign to the **ValueRepository**.
 - **withTranslations(\$bool = true)**
     - Fetch all value translations (values in all languages) and assign to the **ValueRepository**.

If any of those methods is called after a "fetching values" method is executed, you have to run the method :

    updateClass(true)
**Example**

    //If you fetch values with example:
    $customfields->all();
    
    //But then you want to get the settings, data or translations for the values/fields.
    //Then you have to call any of those methods depending on what you need.
    $customfields->withData();
    $customfields->withSettings();
    $customfields->withTranslations();
    
    //When that is done you have to update the customfield values in the class with:
    $customfields->updateClass(true);

*You want to avoid this because this will result in fetching all the customfield values again, which equals to unnecessary SQL-Queries.*

**SUMMARY**
Before fetching the customfields, make sure you call, withData(), withSettings, withTranslations() if you know you will need them. Then you don't need to run the customfields queries again.
Think about only calling them if you need them because each of this methods have to execute extra SQL-Queries for each field.

----------

## Class - ValueRepository
    C4Media\Customfields\ValueRepository
A field value is presented as an object called ValueRepository. It is a repository containing everything you need to easily use the value in the view.

### Methods
 - **value (\$default = '')**
     - Returns the value of the field. **\$default** is the optional argument, which is returned if the value is empty.
 - **repeat(\$callback)**
     - If the value is an array you can use this method to automatically loop the array. The accepted argument **\$callback** has to be a function. This function will be executed at each segment in the array. 
     - You will get **\$key**, **\$value** as arguments to your callback function.
 - **isEmpty()**
     -  Returns true/false. Method checks if the value is empty or not.
 - **isEmptyObject()**
     - Find out if it is an empty ValueRepository Object. 
 - **typeof()**
     - Returns the type of the value.
 - **data()**
     - Returns the data related to the field. Will be empty if not **withData()** is called on the Repository.
 - **settings()**
     - Returns the settings related to the field. Will be empty if not **withSettings()** is called on the Repository.
 - **translations()**
     - Returns the translations related to the value. Will be empty if not **withTranslations()** is called on the Repository.

## Examples

### Results

*Fetch all customfield collections*

    $customfields->all()
    
*Results in:*
   
    [
        'movies' => [
            'movieRepeater' => [
                0 => [
                    'movieTitle' => {ValueRepository},
                    'movieActors' => [
                        0 => [
                            'actorName' => {ValueRepository},
                            'actorOtherMovies' => [
                                0 => [
                                    'actorMovieTitle' => {ValueRepository},
                                    'actorMovieRating' => {ValueRepository}
                                ],
                                1 => [
                                    'actorMovieTitle' => {ValueRepository},
                                    'actorMovieRating' => {ValueRepository}
                                ],
                                2 => [
                                    'actorMovieTitle' => {ValueRepository},
                                    'actorMovieRating' => {ValueRepository}
                                ]
                            ]
                        ],
                        1 => [
                            'actorName' => {ValueRepository},
                            'actorOtherMovies' => [
                                0 => [
                                    'actorMovieTitle' => {ValueRepository},
                                    'actorMovieRating' => {ValueRepository}
                                ],
                                1 => [
                                    'actorMovieTitle' => {ValueRepository},
                                    'actorMovieRating' => {ValueRepository}
                                ],
                                2 => [
                                    'actorMovieTitle' => {ValueRepository},
                                    'actorMovieRating' => {ValueRepository}
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
        'dailyMovie' => [
            'movie' => {ValueRepository}
        ]
    ]

**Levels**

 1. Movies & countries are two different collections. The collection are always in the first level.
 2.  Level 2 contains the first level of fields. If the field is not a repeater, there will be no level 3. If the field is a repeater, there will be a next level (level3) and it will go on like this until there is no more repeaters.
 3. Level 3 contains the fields of a level 2 repeater.
 4. *Infinite of repeater levels...*

### How to fetch the specific fields
Examples of how to fetch what you want from the array tree above:

    //Fetch the movie repeater array.
    $customfields->repeater('movies.movieRepeater');

**movies** = Collection identifier
**movieRepeater** = The field identifier for the repeater

If you want the first movie only, you can write like this:

    $customfields->repeater('movies.movieRepeater.0')

*If there is no repetitions then you will get an empty array or the default value which is the second argument in the method call.*

If you only want a field:

    $customfields->field('dailyMovie.movie');

*This will return a* **ValueRepository** *containing the movie value from the dailyMovie collection. If none was found you will receive an empty* **ValueRepository object** *and you can still fetch the value with: * **->value()** 

#### SUMMARY

**Repetitions**

    $customfields->repeater('COLLECTION.REPEATER_IDENTIFIER1.REPEATER_IDENTIFIER2');
*You can go how far you want with the nested array, ex. Identifer1, identifier2, identifier3 etc.*

**Field**

    $customfields->field('COLLECTION.FIELD_IDENTIFIER);

*First the collection and then the field identifier. You can also find a field inside a repeater:*

    $customfields->field('COLLECTION.REPEATER.0.FIELD);
   
*Collection first, then repeater, then the key of the repetition and the field at last.*

**A field will always return a ValueRepository object if the \$default argument is null.**

This is how you use the **ValueRepository Object:**

    //Return the value of the field
    $valueRepository->value($default = '')
    
    //Find out if the field is empty
    if ($valueRepository->isEmpty() === false) {
        //Not empty
    } 
    
    //Find out if the object is empty because of no field was found
    if ($valueRepository->isEmptyObject() === false) {
        //Field exists
    }
    
    //The value is an array and you want to loop through it
    $valueRepository->repeat(function($key, $value) {
        echo 'Key: '. $key .' | Value: '. $value;
    });
    
    //Get the data, settings, translations
    $valueRepository->data();
    $valueRepository->settings();
    $valueRepository->translations();

<?php
namespace Customfields;

use DbFactory;
use PDO;

class FieldValueTranslation extends \C4Media\ActiveRecord\Base
{
    protected static $tableName = 'customfields_collection_field_value_translations';

    public static function getValueTranslations($relationClass, $relationIdentifier)
    {
        $stmt = \DbFactory::getConnection()->prepare("
            SELECT translation.id, translation.value_id AS valueId, translation.content, translation.language FROM customfields_collection_field_value_translations AS translation
                WHERE value_id IN (SELECT value.id FROM customfields_collection_field_values AS value
                JOIN customfields_collection_field_value_relations AS relation ON relation.id = value.relation_id
                WHERE relation.class = :relationClass AND relation.relation = :relationIdentifier)
        ");
        $stmt->bindParam('relationClass', $relationClass);
        $stmt->bindParam('relationIdentifier', $relationIdentifier);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}

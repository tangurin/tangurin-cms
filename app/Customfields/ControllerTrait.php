<?php
namespace Customfields;

use App\Repositories\LanguageRepository;
use Storage;

trait ControllerTrait
{
    private $customfieldsErrors = [];
    private $customfieldsCollections = [];
    private $customfieldsFields = [];
    private $customfieldsRelationClass;
    private $customfieldsRelationIdentifier;

    public function initCustomfields($relationClass, $relationIdentifier)
    {
        $this->customfieldsRelationClass = $relationClass;
        $this->customfieldsRelationIdentifier = $relationIdentifier;

        $queryClass = 'Customfields\Relation\\'. $relationClass .'Query';
        if (class_exists($queryClass) === false) {
            return false;
        }

        $query = new $queryClass();
        $collectionIds = $query->getCollectionIds($relationIdentifier);

        $this->initCustomfieldsCollections($collectionIds);
        $this->initCustomfieldsFields();

        return true;
    }

    public function initCustomfieldsCollections($collectionIds)
    {
        foreach ($collectionIds as $collectionId) {
            $collection = Collection::find($collectionId);
            if (!empty($collection)) {
                $sort = $collection->sortOrder;
                if (isset($this->customfieldsCollections[$collection->sortOrder])) {
                    $sort = microtime();
                }
                $this->customfieldsCollections[$sort] = $collection->toArray();
            }
        }
        ksort($this->customfieldsCollections);
    }

    public function initCustomfieldsFields()
    {
        foreach ($this->customfieldsCollections as $collection) {
            foreach (Collection::getFields($collection['id']) as $field) {
                $this->customfieldsFields[] = Field::getFieldByType($field->type, $field->id)->toArray();
            }
        }
    }

    public function customfieldsExists()
    {
        if (empty($this->customfieldsFields)) {
            return false;
        }

        return true;
    }

    public function generateCustomfieldsView($extraVars = [])
    {
        if ($this->customfieldsExists() === false) {
            return '<div class="alert alert-info">Det finns inga fält att redigera.</div>';
        }

        if (is_array($this->getCustomFieldsInput())) {
            $fieldValues = $this->getCustomFieldsValuesFromInput();
            $fieldTranslations = $this->getCustomFieldsTranslationsFromInput();
        } else {
            $fieldValues = ValueRelation::getValues($this->customfieldsRelationClass, $this->customfieldsRelationIdentifier);
            $fieldTranslations = FieldValueTranslation::getValueTranslations($this->customfieldsRelationClass, $this->customfieldsRelationIdentifier);
        }

        $sortedCategories = $this->getValueCategories($fieldValues, $fieldTranslations);
        $sortedProducts = $this->getValueProducts($fieldValues, $fieldTranslations);

        $data = [
            'availableLanguages' => resolve(LanguageRepository::class)->listAvailable(),
            'collections' => $this->customfieldsCollections,
            'fields' => $this->customfieldsFields,
            'relationClass' => $this->customfieldsRelationClass,
            'relationIdentifier' => $this->customfieldsRelationIdentifier,
            'fieldValues' => $fieldValues,
            'fieldTranslations' => $fieldTranslations,
            'valueCategories' => $sortedCategories,
            'valueProducts' => $sortedProducts,
            'max_input_vars' => intval(ini_get('max_input_vars')),
        ];

        $data = array_merge($data, $extraVars);

        return view('Backend::customfields.generatedFields', $data);
    }

    private function getValueCategories($fieldValues, $fieldTranslations)
    {
        //Get all values that are of type Category
        $categoryValues = array_filter($fieldValues, function($value) { return isset($value['type']) && $value['type'] == 'Category';});

        //Find the translations for the type, Category
        $categoryIds = [];
        foreach ($categoryValues as $value) {
            foreach ($fieldTranslations as $translation) {
                if (!empty($translation['content']) && $translation['valueId'] == $value['id']) {
                    $ids = is_array($translation['content']) ? $translation['content'] : json_decode($translation['content']);
                    $categoryIds = array_merge($categoryIds, $ids);
                }
            }
        }

        return empty($categoryValues) ? [] : FieldValue::sortedCategories($categoryIds);
    }


    private function getValueProducts($fieldValues, $fieldTranslations)
    {
        //Get all values that are of type Product
        $productValues = array_filter($fieldValues, function($value) {return isset($value['type']) && $value['type'] == 'Product';});

        //Find the translations for the type, Product
        $productIds = [];
        foreach ($productValues as $value) {
            foreach ($fieldTranslations as $translation) {
                if (!empty($translation['content']) && $translation['valueId'] == $value['id']) {
                    $ids = is_array($translation['content']) ? $translation['content'] : json_decode($translation['content']);
                    $productIds = array_merge($productIds, $ids);
                }
            }
        }
        
        return empty($productValues) ? [] : FieldValue::sortedProducts($productIds);
    }

    public function getCustomFieldsInput($key = null, $default = null)
    {
        if (is_null($key)) {
            return isset($_POST['customfieldValues']) ? $_POST['customfieldValues'] : null;
        }

        if (isset($_POST['customfieldValues'][$key])) {
            return $_POST['customfieldValues'][$key];
        }

        return $default;
    }

    public function validateCustomfields()
    {
        return true;
        $values = $this->getCustomFieldsInput('values', []);
        if (empty($values) === false) {
            /* GO THROUGH FIELDS INSTEAD OF VALUES??? */
            foreach ($values as $index => $value) {
                $fieldId = intval($value['fieldId']);
                $fieldType = $value['type'];

                $field = Field::getFieldById($fieldId);
                foreach ($field->validateValue($value) as $error) {
                    $this->customfieldsErrors[] = $error;
                }
            }
        }

        return empty($this->customfieldsErrors);
    }

    public function saveCustomfields()
    {
        $this->validateCustomfields();
        $model = new FieldValue();
        $model->savePost($this->getCustomFieldsInput());
    }

    private function getCustomFieldsValuesFromInput()
    {
        $fieldValues = [];
        foreach ($this->getCustomFieldsInput('values', []) as $id => $value) {
            $field = Field::find($value['fieldId']);
            $fieldValues[] = [
                "fieldId" => $field->id,
                "collectionId" => $field->collectionId,
                "identifier" => $field->identifier,
                "parentId" => $field->parentId,
                "type" => $field->type,
                "parentValueId" => $value['parentValueId'],
                "id" => $id,
            ];
        }
        foreach ($this->getCustomFieldsInput('repeaters', []) as $id => $value) {
            $field = Field::find($value['fieldId']);
            $fieldValues[] = [
                "fieldId" => $field->id,
                "collectionId" => $field->collectionId,
                "identifier" => $field->identifier,
                "parentId" => $field->parentId,
                "parentValueId" => $value['parentValueId'],
                "id" => $id,
            ];
        }

        return $fieldValues;
    }

    private function getCustomFieldsTranslationsFromInput()
    {
        $fieldTranslations = [];
        foreach ($this->getCustomFieldsInput('values', []) as $id => $value) {
            if (isset($value['value']) === false) {
                continue;
            }
            
            foreach ($value['value'] as $languageCode => $content) {
                $fieldTranslations[] = [
                    "valueId" => $id,
                    "content" => is_array($content) ? json_encode($content) : $content,
                    "language" => $languageCode,
                ];
            }
        }

        return $fieldTranslations;
    }
}

<?php
namespace Customfields;

use DbFactory;
use PDO;
use App\Repositories\LanguageRepository;

class ValueRelation extends \C4Media\ActiveRecord\Base
{
    protected static $tableName = 'customfields_collection_field_value_relations';

    public static function getValues($relationClass, $relationIdentifier, $withTranslation = false)
    {
        $select = [
            'collection.identifier AS collectionIdentifier',
            'field.id AS fieldId',
            'field.collection_id AS collectionId',
            'field.identifier',
            'field.type',
            'field.parent_id AS parentId',
            'value.parent_value_id AS parentValueId',
            'value.id AS id',
        ];

        $translationJoin = '';
        if ($withTranslation) {
            $select[] = 'translation.content AS translation';
            $translationJoin = "LEFT JOIN customfields_collection_field_value_translations AS translation ON translation.value_id = value.id AND translation.language = '". resolve(LanguageRepository::class)->current() ."'";
        }

        $stmt = DbFactory::getConnection()->prepare("
            SELECT ". implode(',', $select) ." FROM customfields_collection_field_values AS value
                JOIN customfields_collection_fields AS field ON field.id = value.field_id
                JOIN customfields_collection_field_value_relations AS relation ON relation.id = value.relation_id
                JOIN customfields_collections AS collection ON collection.id = field.collection_id
                ". $translationJoin ."
                WHERE relation.class = :relationClass AND relation.relation = :relationIdentifier
                ORDER BY value.sort_order ASC
        ");
        $stmt->bindParam('relationClass', $relationClass);
        $stmt->bindParam('relationIdentifier', $relationIdentifier);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public static function getCollections($relationClass, $relationIdentifier)
    {
        $stmt = DbFactory::getConnection()->prepare("
            SELECT collection.* FROM customfields_collections AS collection WHERE id IN (
                SELECT collection_id FROM customfields_collection_fields WHERE id IN (
                    SELECT field_id FROM customfields_collection_field_values WHERE relation_id IN (
                        SELECT id FROM customfields_collection_field_value_relations
                            WHERE class = :relationClass AND relation = :relationIdentifier
                        )
                    )
                )
                ORDER BY collection.sort_order ASC
        ");
        $stmt->bindParam('relationClass', $relationClass);
        $stmt->bindParam('relationIdentifier', $relationIdentifier);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public static function appendTranslations($values)
    {
        $valueIds = implode(',', array_map(function($value) { return $value['id']; }, $values));
        $stmt = DbFactory::getConnection()->prepare("
            SELECT `value_id`, `language`, `content` FROM customfields_collection_field_value_translations
                WHERE `value_id` IN (". $valueIds .")
        ");
        $stmt->execute();

        $valueTranslations = [];
        $translations = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($values as $key => $value) {
            $valueTranslations = array_filter($translations, function($translation) use($value) {
                return $value['id'] == $translation['value_id'];
            });
            if (!empty($valueTranslations)) {
                $fieldClass = Field::getFieldByType($value['type']);
                foreach ($valueTranslations as $translation) {
                    $values[$key]['translations'][$translation['language']] = $fieldClass->processOnFetch($translation['content']);
                }
            }
            
        }

        return $values;
    }

    public static function appendData($values)
    {
        $fieldIds = implode(',', array_map(function($value) { return $value['fieldId']; }, $values));
        $stmt = DbFactory::getConnection()->prepare("
            SELECT `field_id`, `value`, `label` FROM customfields_collection_field_data
                WHERE `field_id` IN (". $fieldIds .")
        ");
        $stmt->execute();

        $fieldData = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $data) {
            $fieldData[$data['field_id']][] = [
                'label' => $data['label'],
                'value' => $data['value'],
            ];
        }

        return array_map(function($value) use($fieldData) {
            $value['data'] = [];
            if (isset($fieldData[$value['fieldId']])) {
                $value['data'] = $fieldData[$value['fieldId']];
            }
            return $value;
        }, $values);
    }

    public static function appendSettings(&$values)
    {
        $fieldIds = implode(',', array_map(function($value) { return $value['fieldId']; }, $values));
        $stmt = DbFactory::getConnection()->prepare("
            SELECT `field_id`, `key`, `value` FROM customfields_collection_field_settings
                WHERE `field_id` IN (". $fieldIds .")
        ");
        $stmt->execute();

        $fieldSettings = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $data) {
            $fieldSettings[$data['field_id']][$data['key']] = $data['value'];
        }

        return array_map(function($value) use($fieldSettings) {
            $value['settings'] = [];
            if (isset($fieldSettings[$value['fieldId']])) {
                $value['settings'] = $fieldSettings[$value['fieldId']];
            }
            return $value;
        }, $values);
    }
}

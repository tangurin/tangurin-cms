<?php
namespace Customfields;

use DbFactory;
use PDO;

class Value extends \C4Media\ActiveRecord\Base
{
    protected static $tableName = 'customfields_collection_field_values';
}

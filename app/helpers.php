<?php
if (!function_exists('isWebpackDevServerOnline')) {
    function isWebpackDevServerOnline($port = 8080)
    {
        if (env('APP_ENV') != 'local') {
            return false;
        }

        if (defined('devServerStatus_'. $port) === false) {
            define('devServerStatus_'. $port, @fSockOpen(getHostByName(getHostName()), $port, $errno, $errstr, 10) !== false);
        }

        return constant('devServerStatus_'. $port);
    }
}

if (!function_exists('isHotReloadRunning')) {
    function isHotReloadRunning($port = 8080)
    {
        return isWebpackDevServerOnline($port);
    }
}

if (!function_exists('dynamicUrl')) {
    function dynamicUrl($url, $port = 8080)
    {
        return isWebpackDevServerOnline($port) ? '//'. getHostByName(getHostName()) .':'. $port .'/'. $url : url($url);
    }
}

if (!function_exists('theme')) {
    function theme()
    {
        return env('APP_TEMPLATE', 'default');
    }
}

if (!function_exists('themePath')) {
    function themePath($path = '', $relativeUrl = false)
    {
        $url = theme() . $path .'?v='. cacheVersion;
        if ($relativeUrl) {
            return $url;
        }
        return url($url);
    }
}

if (!function_exists('shared')) {
    function shared($path = '', $relativeUrl = false)
    {
        $url = 'shared/'. $path .'?v='. cacheVersion;
        if ($relativeUrl) {
            return $url;
        }
        return dynamicUrl($url);
    }
}

if (!function_exists('build')) {
    function build($path = '', $relativeUrl = false)
    {
        $url = theme() .'/build/'. $path .'?v='. cacheVersion;
        if ($relativeUrl) {
            return $url;
        }
        return dynamicUrl($url);
    }
}

if (!function_exists('images')) {
    function images($path = '', $relativeUrl = false)
    {
        $url = theme() .'/build/images/'. $path .'?v='. cacheVersion;
        if ($relativeUrl) {
            return $url;
        }
        return dynamicUrl($url);
    }
}

if (!function_exists('backend')) {
    function backend($path = '', $relativeUrl = false)
    {
        $relativeUrl = 'backend/'. $path .'?v='. cacheVersion;
        if ($relativeUrl) {
            return $url;
        }
        return dynamicUrl($url, 8081);
    }
}

if (!function_exists('backendBuild')) {
    function backendBuild($path = '', $relativeUrl = false)
    {
        $url = 'backend/build/'. $path .'?v='. cacheVersion;
        if ($relativeUrl) {
            return $url;
        }
        return dynamicUrl($url, 8081);
    }
}

if (!function_exists('t')) {
    function t($key, $lowercase = false, $language = null)
    {
        $translation = resolve(App\Repositories\TranslationRepository::class)->get($key, $key, $language);

        if ($lowercase) {
            $translation = mb_strtolower($translation);
        }

        return $translation;
    }
}

if (!function_exists('hasSetting')) {
    function hasSetting($key)
    {
        //Check if setting exists and not empty
        return resolve(App\Repositories\SettingRepository::class)->has($key);
    }
}

if (!function_exists('settingEmpty')) {
    function isEmptySetting($key)
    {
        //Check if setting exists and not empty
        return empty( setting($key) );
    }
}

if (!function_exists('setting')) {
    function setting($key, $default = null)
    {
        $settingRepository = resolve(App\Repositories\SettingRepository::class);
        if ($settingRepository->has($key)) {
            return $settingRepository->get($key)->value;
        }

        return $default;
    }
}

if (!function_exists('hasUserPreference')) {
    function hasUserPreference($key, $user = null)
    {
        $user = empty($user) ? Auth::user() : $user;
        if (empty($user)) {
            return false;
        }

        return $user->preferences->has($key);
    }
}

if (!function_exists('userPreference')) {
    function userPreference($key, $default = null, $user = null)
    {
        $user = empty($user) ? Auth::user() : $user;
        if (empty($user)) {
            return $default;
        }

        return $user->preferences->get($key, $default);
    }
}

if (!function_exists('saveUserPreference')) {
    function saveUserPreference($key, $default = null, $user = null)
    {
        $user = empty($user) ? Auth::user() : $user;
        if (empty($user)) {
            return false;
        }

        resolve(App\Repositories\UserPreferenceRepository::class)->save($user, 'subscriptionRequestSent');
        return true;
    }
}

if (!function_exists('text')) {
    function text($key, $default = '', $language = null)
    {
        return resolve(App\Repositories\TextRepository::class)->get($key, $default, $language);
    }
}

if (!function_exists('button')) {
    function button(array $config)
    {
        $default = [
            'type' => '',
            'class' => '',
            'label' => '',
            'btn' => '',
            'fa' => '',
            'element' => 'a',
        ];

        $predefined = [
            'delete' => [
                'class' => 'btn-danger confirmDelete',
                'fa' => 'remove',
                'element' => 'button'
            ],
            'edit' => [
                'class' => 'btn-warning',
                'fa' => 'wrench',
            ],
            'info' => [
                'class' => 'btn-info',
                'fa' => 'eye',
            ],
        ];
        $pre = [];
        if (isset($config['type']) && isset($predefined[ $config['type'] ])){
            $pre = $predefined[ $config['type'] ];
        }

        $config = array_merge($default, $pre, $config);
        $attributes = array_diff_key($config, $default);
        $attributes = join(' ', array_map(function($key) use ($attributes) {
           if (is_bool($attributes[$key])) {
              return $attributes[$key] ? $key : '';
           }
           return $key .'="'. $attributes[$key] .'"';
        }, array_keys($attributes)));

        $class = 'btn waves-effect waves-light '. $config['class'];
        $label = (!empty($config['fa']) ? ' <i class="fa fa-'. $config['fa'] .'"></i> ' : '') . $config['label'];
        return '<'. $config['element'] .' class="'. $class .'" '. $attributes .'>'. $label .'</'. $config['element'] .'>';
    }
}


if (!function_exists('switcher')) {
    function switcher($config)
    {
        $default = [
            'name' => is_string($config) ? $config : '',
            'value' => '',
            'selected' => false,
            'hidden' => false,
            'disabled' => false,
            'attributes' => [
                'data-plugin' => 'switchery',
                'data-color' => '#4c5667'
            ]
        ];

        $config = is_array($config) ? $config : [];
        $config = array_merge($default, $config);
        $config['attributes']['data-disabled'] = $config['disabled'] ? 'true' : 'false';

        $html = '';
        if ($config['hidden'] !== false) {
            $html .= \Form::hidden($name, $config['hidden']);
        }

        $html .= \Form::checkbox($config['name'], $config['value'], $config['selected'], $config['attributes']);

        return $html;
    }
}

if (!function_exists('vueComponent')) {
    function vueComponent($name, $attributes = [])
    {
        $attributesString = '';
        foreach ($attributes as $key => $attr) {
            $attributesString .= ' '. $key .'=\''. $attr .'\'';
        }

        $html = '<div class="component-'. $name .'"><'. $name . $attributesString .'></'. $name .'></div>';
        return $html;
    }
}

if (!function_exists('filemanager')) {
    function filemanager($attributes = [])
    {
        return vueComponent('filemanager-picker', $attributes);
    }
}

if (!function_exists('media')) {
    function media($identifier)
    {
        $file = [];

        if (is_numeric($identifier)) {
            $file = App\Models\Filemanager::find($identifier);
        } elseif (is_string($identifier)) {
            $file = App\Models\Filemanager::where('path', $identifier)->first();
        }

        if (is_object($file)) {
            $file = with(new App\Http\Resources\Filemanager\FileResource($file))->jsonSerialize();
        }

        return collect($file);
    }
}

if (!function_exists('thumb')) {
    function thumb($imageSource, $width, $height, $crop = false, $quality = 85)
    {
        return uploads(
            resolve(App\Repositories\ThumbnailRepository::class)
            ->create($imageSource, $width, $height, $crop, $quality)
        );
    }
}

if(!function_exists('_dd'))
{
    function _dd(...$args)
    {
        header("HTTP/1.0 500 Internal Server Error");
        call_user_func_array('dd', $args);
    }
}

<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends \Spatie\Permission\Models\Role
{
    public function scopeIdentifier($query, $identifier)
    {
        return $query->where('identifier', $identifier);
    }

    public function isAdmin()
    {
        return $this->id == 1;
    }
}

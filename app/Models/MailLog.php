<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailLog extends Model
{
    protected $fillable = [
        'from',
        'to',
        'subject',
        'content',
        'class',
    ];
}

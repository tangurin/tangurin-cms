<?php
namespace App\Models\Page;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	protected $table = 'page_images';
	private static $menu;

	protected $fillable = [
		'image',
		'page_id',
		'sort_order'
	];

	public function page()
	{
		return $this->belongsTo('\App\Models\Page\Page');
	}
}

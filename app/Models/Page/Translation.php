<?php
namespace App\Models\Page;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $table = 'page_translations';
    protected $fillable = [
        'slug',
        'title',
        'content',
        'page_id',
        'language',
        'seo_title',
        'seo_description',
        'seo_keywords'
    ];

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function page()
    {
        return $this->belongsTo(\App\Models\Page::class);
    }
}

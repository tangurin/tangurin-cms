<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Slideshow extends Model
{
    protected $fillable = [
        'identifier',
    ];

    protected static function boot()
    {
        parent::boot();
     
        //static::addGlobalScope('order', function (Builder $builder) {
        //    $builder->orderBy('sort_order', 'asc');
        //});
    }

    public function slides()
    {
        return $this->hasMany(\App\Models\Slideshow\Slide::class);
    }
}

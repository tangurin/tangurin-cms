<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translatable;

class Text extends Model
{
    use Translatable;

    protected $fillable = [
        'content',
    ];

    protected static function boot()
    {
        parent::boot();

        static::saved(function($model) {
            \Cache::forget('texts');
        });
    } 

    public function translations()
    {
        return $this->hasMany(\App\Models\Text\Translation::class);
    }
}

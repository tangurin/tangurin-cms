<?php
namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Active extends Model
{
    protected $table = 'user_active';
    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'active',
        'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}

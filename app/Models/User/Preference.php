<?php
namespace App\Models\User;

use App\Collections\SettingCollection;
use Illuminate\Database\Eloquent\Model;

class Preference extends Model
{
    protected $table = 'user_preferences';
    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'key',
        'value',
    ];

    public function newCollection(array $models = [])
    {
        return new SettingCollection($models);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}

<?php namespace App\Models\Menu;

use Illuminate\Database\Eloquent\Model;

class MenuRow extends Model {

	protected $table = 'menu_row';

	protected $fillable = [
        'menu_id',
        'parent_id',
        'type',
        'name',
		'value',
		'target',
        'class',
		'sort_order',
	];
}

<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Translatable;

class Country extends Model
{
    use SoftDeletes, Translatable;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'country_code'
    ];

    public function translations()
    {
        return $this->hasMany(\App\Models\Country\Translation::class);
    }

    public function counties()
    {
        return $this->hasMany(\App\Models\County::class);
    }
}

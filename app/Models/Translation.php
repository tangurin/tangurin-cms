<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\Language;
use App\Models\Translation\Value;
use App\Traits\Translatable;

class Translation extends Model
{
    use Translatable;

    public $timestamps = false;
    protected $fillable = [
        'identifier',
        'description'
    ];

    protected static function boot()
    {
        parent::boot();

        static::saved(function($model) {
            \Cache::forget('translations');
        });
    } 

    public function values()
    {
        return $this->hasMany(\App\Models\Translation\Value::class);
    }

    public function getValueAttribute()
    {
        return $this->getValue(resolve(LanguageRepository::class)->current());
    }

    public function getValue($language, $default = null)
    {
        $values = $this->values->filter(function($object, $key) use ($language) {
            return $object->language == $language;
        });

        return !$values->isEmpty() ? $values->first()->value : $default;
    }
}

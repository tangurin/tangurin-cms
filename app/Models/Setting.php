<?php
namespace App\Models;

use App\Collections\SettingCollection;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'key',
        'value',
    ];

    protected static function boot()
    {
        parent::boot();

        static::saved(function($model) {
            \Cache::forget('settings');
        });
    }

    public function newCollection(array $models = [])
    {
        return SettingCollection::make($models);
    }
}

<?php
namespace App\Models;

use App\Collections\PageCollection;
use App\Traits\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes, Translatable;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'template',
        'startpage',
        'in_menu',
        'parent',
        'sort_order',
    ];

    protected static function boot()
    {
        parent::boot();
     
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('sort_order', 'asc');
        });

        static::saved(function($model) {
            \Cache::forget('pages');
        });

        static::deleted(function($model) {
            \Cache::forget('pages');
        });
    }

    public function newCollection(array $models = [])
    {
        return new PageCollection($models);
    }

    public function inMenuScope($query)
    {
        return $query->where('in_menu', 1);
    }

    public function translations()
    {
        return $this->hasMany(\App\Models\Page\Translation::class);
    }

    public function images()
    {   
        return $this->hasMany(\App\Models\Page\Image::class);
    }

    public function getViewPath()
    {
        $templatePath = !empty($this->template) ? 'Frontend::templates.'. $this->template : '';
        $viewPath = 'Frontend::page';

        if (!empty($templatePath) && view()->exists($templatePath)) {
            $viewPath = $templatePath;
        } elseif ($this->isStartpage() && view()->exists('Frontend::startpage')) {
            $viewPath = 'Frontend::startpage';
        }

        return $viewPath;
    }

    public function getSeoTitle()
    {
        if (empty($this->translation->seo_title)) {
            return $this->translation->title .' - '. setting('companyName');
        }
        
        return $this->translation->seo_title;
    }

    public function getSeoDescription()
    {
        $content = $this->translation->seo_description;
        //If no SEO description. Try get one from content
        if (empty($content)) {
            $content = $this->translation->content;
            //Remove h1 tag and strip all tags
            $content = trim(strip_tags(preg_replace('/<h1[^>]*>([\s\S]*?)<\/h1[^>]*>/', '', $content)));
            //Substr with care of whole words
            if (preg_match('/^.{1,260}\b/s', $content, $match)) {
                $content = $match[0];
            } else {
                $content = substr($content, 0, 160);
            }
        }

        return !empty($content) ? trim($content) : setting('defaultMetaDescription', '');
    }

    public function getSeoKeywords()
    {
        if (empty($this->translation->seo_keywords)) {
            return setting('defaultMetaKeywords', '');
        }
        
        return trim($this->translation->seo_keywords);
    }

    public function isStartpage()
    {
        return $this->id == 1;
    }

    public function deleteable()
    {
        if ($this->isStartpage()) {
            return false;
        }
        return true;
    }
}

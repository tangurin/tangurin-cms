<?php
namespace App\Models\Slideshow;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = [
        'name',
        'image',
        'content',
        'link',
        'sort_order',
    ];

    protected static function boot()
    {
        parent::boot();
     
        //static::addGlobalScope('order', function (Builder $builder) {
        //    $builder->orderBy('sort_order', 'asc');
        //});
    }

    public function slideshow()
    {
        return $this->belongsTo(\App\Models\Slideshow::class);
    }
}

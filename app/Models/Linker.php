<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Linker extends Model
{
    protected $fillable = [
        'slug',
        'target',
    ];
}

<?php
namespace App\Models\Country;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $table = 'country_translations';
    protected $fillable = [
        'country_id',
        'name',
        'language',
    ];

    public function country()
    {
        return $this->belongsTo(\App\Models\Country::class);
    }
}

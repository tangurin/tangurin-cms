<?php
namespace App\Models\Translation;

use Illuminate\Database\Eloquent\Model;

class value extends Model
{
    protected $table = 'translation_values';
    public $timestamps = false;
    protected $fillable = [
        'translation_id',
        'value',
        'language'
    ];

    public function translation()
    {
        return $this->belongsTo(\app\Models\Translation::class);
    }
}

<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends \Spatie\Permission\Models\Permission
{
    public function ScopeOnlyFrontend($query)
    {
        return $query->where('name', 'LIKE', 'frontend_%');
    }

    public function ScopeOnlyBackend($query)
    {
        return $query->where('name', 'LIKE', 'backend_%');
    }
}

<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Filemanager extends Model
{
    protected $table = 'filemanager';
    public $timestamps = true;
    protected $fillable = [
        'directory',
        'path',
        'name',
        'extension',
        'mimetype',
        'type',
        'size',
        'width',
        'height',
        'hidden',
        'extra',
        'created_at',
        'updated_at',
    ];

    public function getFilenameAttribute()
    {
        if ($this->name[0] == '.') {
            return $this->name;
        }
        
        return $this->name . '.' . $this->extension;
    }
}

<?php
namespace App\Models\Text;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $table = 'text_translations';
    protected $fillable = [
        'text_id',
        'content',
        'language',
    ];

    public function text()
    {
        return $this->belongsTo(\App\Models\Text::class);
    }
}

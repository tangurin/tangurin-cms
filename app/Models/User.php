<?php
namespace App\Models;

use App\Models\Permission;
use App\Repositories\UserPreferenceRepository;
use App\Repositories\RoleRepository;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function active()
    {
        return $this->hasOne(\App\Models\User\Active::class);
    }

    public function preferences()
    {
        return $this->hasMany(\App\Models\User\Preference::class);
    }

    public function canAccessBackend()
    {
        return Permission::onlyBackend()
        ->get()
        ->filter(function ($permission) {
            return $this->hasPermissionTo($permission->name);
        })
        ->isNotEmpty();
    }

    public function scopeOnlyActive($query)
    {
        return $query->whereHas('active', function ($query) {
            return $query->where('active', 1);
        });
    }

    public function scopeOnlyInActive($query)
    {
        return $query->doesntHave('active')->orWhereHas('active', function ($query) {
            return $query->where('active', 0);
        });
    }

    public function scopeWhereHasRole($query, $role)
    {
        return $query->whereHas('roles', function($query) use ($role) {
            return $query->where('identifier', $role);
        });
    }

    public function isActive()
    {
        return !is_null($this->active) && $this->active->active == 1;
    }

    public function isInActive()
    {
        return $this->isActive() === false;
    }

    public function activateAccount()
    {
        $this->active()->updateOrCreate(['user_id' => $this->id], [
            'active' => 1,
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        
        return $this;
    }

    public function deActivateAccount()
    {
        $this->active()->updateOrCreate(['user_id' => $this->id], [
            'active' => 0,
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        return $this;
    }

    public function hasRole($roles) : bool
    {
        if (is_string($roles)) {
            return $this->roles->contains('identifier', $roles);
        }

        if ($roles instanceof Role) {
            return $this->roles->contains('id', $roles->id);
        }

        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }

            return false;
        }

        return $roles->intersect($this->roles)->isNotEmpty();
    }

    public function isMainAdmin()
    {
        return $this->hasRole( resolve(RoleRepository::class)->getMainAdmin() ) ? true : false;
    }

    public function isAdmin()
    {
        return $this->hasRole( resolve(RoleRepository::class)->getAdmin() ) ? true : false;
    }
}

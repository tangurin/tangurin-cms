<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Language extends Model
{
	use SoftDeletes;

	protected $table = 'language';
	protected $fillable = [
		'language_native',
		'language_english',
		'language_code',
		'image'
	];

	public function translations()
	{
		return $this->hasMany('App\Models\Translation');
	}
}

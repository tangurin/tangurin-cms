<?php

namespace App\Providers;

use App\Models\Page;
use App\Models\User;
use App\Policies\BackendPagePolicy;
use App\Policies\BackendUserPolicy;
use App\Policies\FrontendAccountPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Page::class => BackendPagePolicy::class,
        User::class => BackendUserPolicy::class,
        //User::class => FrontendAccountPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}

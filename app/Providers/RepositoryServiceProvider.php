<?php
namespace App\Providers;

use App\Repositories\Filemanager\FileRepository;
use App\Repositories\Filemanager\DirectoryRepository;
use App\Repositories\Filemanager\IndexRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\LayoutRepository;
use App\Repositories\PageRepository;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use App\Repositories\SettingRepository;
use App\Repositories\TextRepository;
use App\Repositories\ThumbnailRepository;
use App\Repositories\TranslationRepository;
use App\Repositories\UserPreferenceRepository;
use App\Repositories\ViewRepository;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    protected $defer = true;

    private $repositories = [
         ViewRepository::class,
         LayoutRepository::class,
         RoleRepository::class,
         PermissionRepository::class,
         PageRepository::class,
         LanguageRepository::class,
         SettingRepository::class,
         UserPreferenceRepository::class,
         TranslationRepository::class,
         TextRepository::class,
         FileRepository::class,
         DirectoryRepository::class,
         IndexRepository::class,
         ThumbnailRepository::class,
     ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->repositories as $repository) {
            $this->app->singleton($repository, function () use ($repository) {
                return new $repository();
            });
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return $this->repositories;
    }

}

<?php
namespace App\Providers;

use App\Repositories\ViewRepository;
use Illuminate\Support\ServiceProvider;
use Config;
use View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        $this->setViewNamespaces();
        $this->shareGlobals();
    }

    public function setViewNamespaces()
    {
        View::addNamespace('Backend', resource_path('backend/views'));
        View::addNamespace('Frontend', resource_path('frontend/'. env('APP_TEMPLATE', 'default') .'/views'));
        View::addNamespace('Mail', resource_path('mail/views'));
    }

    public function shareGlobals()
    {
        $viewRepository = resolve(ViewRepository::class);
        View::composer([
            '*.403',
            '*.404',
            '*.405',
            '*.500',
        ], function($view) use ($viewRepository) {
            $view->with($viewRepository->getGlobals());
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }
}

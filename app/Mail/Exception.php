<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use App\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Exception extends Mailable
{
    use Queueable, SerializesModels;

    protected $exception;
    protected $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($exception, $request = [])
    {
        $this->exception = $exception;
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $administratorEmail = env('MAIL_ADMINISTRATOR', 'tangurin.se@gmail.com');
        return $this->from( setting('mailOut') )
            ->replyTo($administratorEmail, 'Developer')
            ->to($administratorEmail, 'Developer')
            ->subject('[PHP - Exception] | '. env('APP_URL'))
            ->view('Mail::exception', [
                'exception' => $this->exception,
                'request' => $this->request,
            ]);
    }
}

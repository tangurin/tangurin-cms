<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use App\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    protected $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from( setting('mailOut') )
            ->replyTo($this->request->email, $this->request->name)
            ->to(setting('mailIn'), setting('companyName'))
            ->subject( setting('companyName') .' - Kontaktformulär')
            ->view('Mail::contact', [
                'request' => $this->request,
            ])
            ->text('Mail::contact_plain');
    }
}

<?php
namespace App\Traits;

use App\Repositories\LanguageRepository;

trait Translatable
{
    public function getTranslationAttribute()
    {
        return $this->translation();
    }

    public function translation($language = null)
    {
        if (!is_null($this->translations)) {
            $language = is_null($language) ? resolve(LanguageRepository::class)->current() : $language;
            $translation = $this->translations->filter(function($object, $key) use ($language) {
                return $object->language == $language;
            });

            if ($translation->isEmpty() === false) {
                return $translation->first();
            }
        }

        //Return empty translation object
        return $this->translations()->getRelated();
    }
}

<?php
namespace App\Tree\Page;

use App\Tree\TreeInterface;
use App\Repositories\PageRepository;

class MenuInSplit extends \App\Tree\Base implements TreeInterface
{
    public $secondHalf = [];
    public $isSecondHalf = false;

    public function setRawData()
    {
        if (empty($this->rawData)) {
            $pages = resolve(PageRepository::class)->getPages()->inMenu()->published();

            $count = count($pages);
            $this->secondHalf = $pages->slice($count / 2);
            $this->rawData = $pages->slice(0, $count / 2);
        }
    }

    public function beforeGenerate()
    {
        if ($this->isSecondHalf) {
            $this->preparedData = [];
            $this->rawData = $this->secondHalf;
        }

        $this->isSecondHalf = true;
    }

    public function generateStartTag()
    {
        return '<ul>';
    }

    public function generateEndTag()
    {
        return '</ul>';
    }

    public function generateRowStartTag($row, $level)
    {
        return '<li>';
    }

    public function generateRowContent($row, $level)
    {
        $url = url('');
        if ($row->isStartpage() === false) {
            $url = !empty($row->translation->slug) ? url($row->translation->slug) : '';
        };
        return '<a href="'. $url .'">'. $row->translation->title .'</a>';
    }

    public function generateRowEndTag($row, $level)
    {
        return '</li>';
    }

    public function generateRowBeforeChildren($row, $level)
    {
        return '<ul class="sub">';
    }

    public function generateRowAfterChildren($row, $level)
    {
        return '</ul>';
    }
}

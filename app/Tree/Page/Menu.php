<?php
namespace App\Tree\Page;

use App\Tree\TreeInterface;
use App\Repositories\PageRepository;

class Menu extends \App\Tree\Base implements TreeInterface
{
    public function setRawData()
    {
        if (empty($this->rawData)) {
            $this->rawData = resolve(PageRepository::class)->getPages()->inMenu()->published();
        }
    }

    public function generateStartTag()
    {
        return '<ul>';
    }

    public function generateEndTag()
    {
        return '</ul>';
    }

    public function generateRowStartTag($row, $level)
    {
        return '<li>';
    }

    public function generateRowContent($row, $level)
    {
        $url = url('');
        if ($row->isStartpage() === false) {
            $url = !empty($row->translation->slug) ? url($row->translation->slug) : '';
        }
        return '<a href="'. $url .'">'. $row->translation->title .'</a>';
    }

    public function generateRowEndTag($row, $level)
    {
        return '</li>';
    }

    public function generateRowBeforeChildren($row, $level)
    {
        return '<ul class="sub">';
    }

    public function generateRowAfterChildren($row, $level)
    {
        return '</ul>';
    }
}

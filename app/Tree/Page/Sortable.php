<?php
namespace App\Tree\Page;

use App\Tree\TreeInterface;
use App\Repositories\PageRepository;
use Gate;

class Sortable extends \App\Tree\Base implements TreeInterface
{
    public function setRawData()
    {
        if (empty($this->rawData)) {
            $this->rawData = resolve(PageRepository::class)->getPages();
        }
    }

    public function generateStartTag()
    {
        return '<ol class="dd-list">';
    }

    public function generateEndTag()
    {
        return '</ol>';
    }

    public function generateRowStartTag($row, $level)
    {
        return '<li class="dd-item dd3-item" data-id="'. $row->id .'">';
    }
    
    public function generateRowContent($row, $level)
    {
        $editPath = Gate::check('backend_update_page') ? route('page.edit', [$row->id]) : '#';
        $deletePath = route('page.destroy', [$row->id]);
        $indexPath = route('page.index');

        $html = '<div class="dd-handle dd3-handle"></div>';
        $html .= '<div class="dd3-content">';
            $html .= '<a href="'. $editPath .'">'. $row->translation->title .'</a>';
            $html .= '<div class="icons pull-right">';
                $html .= button(['type' => 'info', 'href' => url($row->translation->slug), 'target' => '_blank']);

                if (Gate::check('backend_update_page')) {
                    $html .= button(['type' => 'edit', 'href' => $editPath]);
                }

                if ($row->deleteable() && Gate::check('backend_delete_page')) {
                    $html .= button(['type' => 'delete', 'data-path' => $deletePath, 'data-index' => $indexPath]);
                }
            $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    public function generateRowEndTag($row, $level)
    {
        return '</li>';
    }

    public function generateRowBeforeChildren($row, $level)
    {
        return '<ol class="dd-list">';
    }

    public function generateRowAfterChildren($row, $level)
    {
        return '</ol>';
    }
}

<?php
namespace App\Tree\Page;

use App\Tree\TreeInterface;
use App\Repositories\PageRepository;

class Select extends \App\Tree\Base implements TreeInterface
{
    public function setRawData()
    {
        if (empty($this->rawData)) {
            $this->rawData = resolve(PageRepository::class)->getPages();
        }
    }

    public function generateStartTag()
    {
        $html = '<select name="parent" class="form-control '. $this->getConfig('class') .'" id="'. $this->getConfig('id') .'">';
        if (is_array($this->getConfig('defaultOptions'))) {
            foreach ($this->getConfig('defaultOptions') as $value => $label) {
                $html .= '<option value="'. $value .'">'. $label .'</option>';
            }
        }
        return $html;
    }

    public function generateEndTag()
    {
        return '</select>';
    }

    public function generateRowStartTag($row, $level)
    {
        $disabled = $this->getConfig('disable') == $row->id ? ' disabled' : '';
        $selected = $this->getConfig('selected') == $row->id ? ' selected' : '';
        return '<option value="'. $row->id .'"'. $disabled . $selected .'>';
    }

    public function generateRowContent($row, $level)
    {
        return str_repeat('--', $level) .' '. $row->translation->title;
    }
    
    public function generateRowEndTag($row, $level)
    {
        return '</option>';
    }

    public function generateRowBeforeChildren($row, $level)
    {
        return '';
    }

    public function generateRowAfterChildren($row, $level)
    {
        return '';
    }
}

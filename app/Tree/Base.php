<?php
namespace App\Tree;

abstract class Base
{
    protected $rawData = [];
    protected $preparedData = [];
    protected $config = [];

    public function __construct($config = [], $rawData = [])
    {
        if (!is_array($config)) {
            return false;
        }

        $this->rawData = $rawData;

        $this->config = array_merge([
            'wrappingTag' => true,
            'selected' => 0,
            'ignore' => null,
            'disable' => null,
            'class' => '',
            'id' => '',
            'parentColumn' => 'parent',
            'childColumn' => false,
            'debug' => false,
            'defaultOptions' => [
                '0' => '-- Välj --'
            ],
        ], $config);
    }

    public function beforeGenerate()
    {

    }

    public function generate($config = [])
    {
        $this->setConfig($config);
        $this->setRawData();
        $this->beforeGenerate();

        $rawData = $this->rawData;
        $html = '';

        if ($this->getConfig('wrappingTag')) {
            $html .= $this->generateStartTag();
        }
        
        //Check if the array is multidimensional or not
        if (count($rawData) === count($rawData, COUNT_RECURSIVE)) {
            $this->prepareSingleDimensionalData();
            $html .= $this->recursiveSingleDimensionalGenerator();
        } else {
            $html .= $this->recursiveMultiDimensionalGenerator($rawData);
        }

        if ($this->getConfig('wrappingTag')) {
            $html .= $this->generateEndTag();
        }

        $html = $this->afterGenerate($html);

        return $html;
    }

    public function afterGenerate($html)
    {
        return $html;
    }

    public function setConfig($key, $value = null)
    {
        if (is_string($key)) {
            $this->config[$key] = $value;
        }

        if (is_array($key)) {
            $this->config = array_merge($this->config, $key);
        }
    }

    public function getConfig($key, $default = false)
    {
        if (isset($this->config[$key])) {
            return $this->config[$key];
        }

        return $default;
    }

    protected function recursiveSingleDimensionalGenerator($parent = 0, $level = 0)
    {
        if (!isset($this->preparedData[$parent])) {
            return '';
        }

        $html = '';
        foreach ($this->preparedData[$parent] as $row) {
            $id = $row->id;
            if ($this->getConfig('ignore') == $id || $this->ignoreRow($row) === true) {
                continue;
            }
            
            $row['rowHasChildren'] = isset($this->preparedData[$id]);
            $html .= $this->generateRowStartTag($row, $level);
            $html .= $this->generateRowContent($row, $level);
            if ($row['rowHasChildren']) {
                $html .= $this->generateRowBeforeChildren($row, $level);
                $html .= $this->recursiveSingleDimensionalGenerator($id, $level + 1);
                $html .= $this->generateRowAfterChildren($row, $level);
            }
            $html .= $this->generateRowEndTag($row, $level);
        }

        return $html;
    }

    protected function recursiveMultiDimensionalGenerator($nextDimensionArray, $passedKeyHistory = [], $level = 0)
    {
        if (empty($nextDimensionArray)){
            return '';
        }

        $childColumn = $this->getConfig('childColumn', false);
        $hasChildColumn = is_string($childColumn);

        $html = '';
        foreach ($nextDimensionArray as $key => $childArray) {
            if ($this->ignoreRow($childArray) === true) {
                continue;
            }

            $keyHistory = $passedKeyHistory;
            
            $row = [
                'key' => $key,
                'value' => $childArray,
                'keyHistory' => $keyHistory,
            ];

            if ($hasChildColumn) {
                    $html .= $this->generateRowStartTag($row, $level);
                    $html .= $this->generateRowContent($row, $level);
                    $children = $childArray[$childColumn];
                    if (isset($children) && is_array($children) && !empty($children)) {
                        $html .= $this->generateRowBeforeChildren($row, $level);
                        $html .= $this->recursiveMultiDimensionalGenerator($children, $keyHistory, $level + 1);
                        $html .= $this->generateRowAfterChildren($row, $level);
                    }
                    $html .= $this->generateRowEndTag($row, $level);
            } else {
                if (is_array($childArray) && !empty($childArray)) {
                    $keyHistory[] = $key;
                    $html .= $this->generateRowBeforeChildren($row, $level);
                    $html .= $this->recursiveMultiDimensionalGenerator($childArray, $keyHistory, $level + 1);
                    $html .= $this->generateRowAfterChildren($row, $level);
                } else {
                    $html .= $this->generateRowStartTag($row, $level);
                    $html .= $this->generateRowContent($row, $level);
                    $html .= $this->generateRowEndTag($row, $level);
                }
            }
        }

        return $html;
    }

    protected function prepareSingleDimensionalData()
    {
        if (!empty($this->preparedData)) {
            return true;
        }
        
        $data = $this->rawData;
        foreach ($data as $key => $row) {
            $parent = isset($row->{$this->getConfig('parentColumn')}) ? $row->{$this->getConfig('parentColumn')} : 0;
            $this->preparedData[$parent][] = $row;
        }
    }

    public function ignoreRow($row)
    {
        return false;
    }

    public function setRawData()
    {
        return true;
    }
}

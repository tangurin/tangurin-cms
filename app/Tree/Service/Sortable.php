<?php
namespace App\Tree\Service;

use App\Tree\TreeInterface;

class Sortable extends \App\Tree\Base implements TreeInterface
{
    public function setRawData()
    {
        if (empty($this->rawData)) {
            $this->rawData = Service::all();
        }
    }

    public function generateStartTag()
    {
        return '<ol class="dd-list">';
    }

    public function generateEndTag()
    {
        return '</ol>';
    }

    public function generateRowStartTag($row, $level)
    {
        return '<li class="dd-item dd3-item" data-id="'. $row->id .'">';
    }
    
    public function generateRowContent($row, $level)
    {
        $editPath = route('service.edit', [$row->id]);
        $deletePath = route('service.destroy', [$row->id]);

        $html = '<div class="dd-handle dd3-handle"></div>';
        $html .= '<div class="dd3-content">';
            $html .= '<a href="'. $editPath .'">'. $row->name .'</a>';
            $html .= '<div class="icons pull-right">';
                $html .= '<a href="'. $editPath .'"><i class="fa fa-pencil"></i></a>';
                $html .= '<a href="'. $deletePath .'" class="removeIcon confirmDelete" data-path="'. $deletePath .'"><i class="fa fa-times"></i></a>';
            $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    public function generateRowEndTag($row, $level)
    {
        return '</li>';
    }

    public function generateRowBeforeChildren($row, $level)
    {
        return '<ol class="dd-list">';
    }

    public function generateRowAfterChildren($row, $level)
    {
        return '</ol>';
    }
}

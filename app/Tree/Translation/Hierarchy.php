<?php
namespace App\Tree\Translation;

use App\Repositories\TranslationRepository;
use App\Tree\TreeInterface;

class Hierarchy extends \App\Tree\Base implements TreeInterface
{
    public function setRawData()
    {
        if (empty($this->rawData)) {
            $this->rawData = resolve(TranslationRepository::class)->getStructuredList();
        }
    }

    public function generateStartTag()
    {
        return '<ul class="translationGroups">';
    }

    public function generateEndTag()
    {
        return '</ul>';
    }

    public function generateRowStartTag($data, $level)
    {
        $html = '';
        $html .= '<li class="translation col-md-3 col-sm-4">';
            $html .= '<div class="form-group" data-identifier="'. $data['key'] .'">';
        return $html;
    }
    
    public function generateRowContent($data, $level)
    {
        $translation = $data['value'];
        $value = $translation->values->first();

        $class = $translation->identifier === $value->value ? ' needsTranslation' : '';
        $html = '';
        $html .= '<h6 class="translationTitle'. $class .'">'. __('translation.'. $translation->identifier) .'</h6>';
        $html .= '<textarea name="translation['. $translation->id .']['. $value->language .']" class="form-control'. $class .'" title="'. $translation->identifier .'">'. $value->value .'</textarea>';

        return $html;
    }

    public function generateRowEndTag($data, $level)
    {
        $html = '';
            $html .= '</div>';
        $html .= '</li>';
        return $html;
    }

    public function generateRowBeforeChildren($data, $level)
    {
        $html = '';
        $langKey = $data['key'];
        if (!empty($data['keyHistory'])) {
            $langKey = array_map(function($key) {
                return ucfirst($key);
            }, $data['keyHistory']);
            $langKey = lcfirst(implode('', $langKey));
            $langKey .= ucfirst($data['key']);
        }

        $label = __('translation.groupTitle.'. $langKey);
        $html .= '<div class="clearfix"></div><li class="hasChildren">';
            $html .= '<h2 class="groupTitle">'. $label .'</h2>';
            $html .= '<span class="verticalTitle">'. $label .'</span>';
            $html .= '<ul class="group group-'. $langKey .'" data-grouptitle="'. $langKey .'">';
        return $html;
    }

    public function generateRowAfterChildren($data, $level)
    {
        $html = '';
            $html .= '</ul>';
        $html .= '</li>';
        return $html;
    }
}

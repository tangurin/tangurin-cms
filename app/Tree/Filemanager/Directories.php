<?php
namespace App\Tree\Filemanager;

use App\Tree\TreeInterface;

class Directories extends \App\Tree\Base implements TreeInterface
{
    public function generateStartTag()
    {
        return '<ul class="'. $this->getConfig('class') .'">';
    }

    public function generateEndTag()
    {
        return '</ul>';
    }

    public function generateRowStartTag($data, $level)
    {
        $hasChildren = empty($data['value']['children']) ? false : true;
        $html = '';
        $hasChildrenClasses = $hasChildren ? ' hasChildren open' : '';
        $html .= "<li :class=\"[{selected: isCurrentDirectory('". $data['value']['path'] ."')}, 'directory". $hasChildrenClasses ."']\">";
        return $html;
    }
    
    public function generateRowContent($data, $level)
    {
        $hasChildren = empty($data['value']['children']) ? false : true;
        $html = '';
        $html .= '<a data-path="'. $data['value']['path'] .'" v-on:click="changeDirectory(\''. $data['value']['path'] .'\')">';
            $html .= $data['value']['pathinfo']['basename'];
        $html .= '</a>';
        
        if ($hasChildren) {
            $html .= '<i class="fa toggle"></i>';
        }

        return $html;
    }

    public function generateRowEndTag($data, $level)
    {
        $html = '';
        $html .= '</li>';
        return $html;
    }

    public function generateRowBeforeChildren($data, $level)
    {
        $html = '';
        $html .= '<ul class="subDirectory">';
        return $html;
    }

    public function generateRowAfterChildren($data, $level)
    {
        $html = '';
        $html .= '</ul>';
        return $html;
    }
}

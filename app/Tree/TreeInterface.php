<?php
namespace App\Tree;

interface TreeInterface
{
    public function generateStartTag();
    public function generateEndTag();
    public function generateRowStartTag($row, $level);
    public function generateRowContent($row, $level);
    public function generateRowEndTag($row, $level);
    public function generateRowBeforeChildren($row, $level);
    public function generateRowAfterChildren($row, $level);
}

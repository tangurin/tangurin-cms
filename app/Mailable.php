<?php
namespace App;

use App\Models\MailLog;
use Illuminate\Mail\Mailable AS IlluminateMailable;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Queue\Factory as Queue;
use Illuminate\Contracts\Mail\Mailable AS MailableContract;

class Mailable extends IlluminateMailable implements MailableContract
{
    public function send(Mailer $mailer)
    {
        parent::send($mailer);

        $from = $this->from[0]['address'];
        $to = $this->to[0]['address'];
        $subject = $this->subject;
        $content = view()->exists($this->view) ? \View::make($this->view, $this->viewData)->render() : $this->view;
        $class = get_called_class();

        MailLog::create([
            'from' => $from,
            'to' => $to,
            'subject' => $subject,
            'content' => $content,
            'class' => $class,
        ]);
    }
    public function queue(Queue $queue)
    {
        parent::queue($queue);
    }

    public function later($delay, Queue $queue)
    {
        parent::later($delay, $queue);
    }
}
?>

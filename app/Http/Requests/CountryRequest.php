<?php
namespace App\Http\Requests;

use App\Repositories\LanguageRepository;
use App\Http\Requests\Request;

class CountryRequest extends Request
{
    protected $messages = [];

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $post = Request::all();

        $languages = resolve(LanguageRepository::class)->listAvailable();
        $fields = [];
        resolve(LanguageRepository::class)->listAvailable()->each(function($language, $languageCode) use (&$fields, $post) {
            //Current Translation-id
            //Except current translation + search where language is the same
            $id = intval($post['data'][$languageCode]['id']);
            $uniqueException = $id > 0 ? ','. $id .',id,language,'. $languageCode : '';
            //Field validation rules
            $title = 'data.'. $languageCode .'.name';
            $fields[$title] = 'required|min:2|unique:country_translations,name'. $uniqueException;;
        });
        $fields['country_code'] = 'required|min:2|unique:countries,country_code,'. $post['id'] .',id';

        //Return validation rules
        return $fields;
    }

    public function messages()
    {
        return $this->messages;
    }
}

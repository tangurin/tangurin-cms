<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use Illuminate\Validation\Rule;

class UserRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //'name' => 'required|max:255',
            'email' => [
                'required',
                'email',
                'max:255',
                Rule::unique('users')->ignore(intval(Auth::id())),
            ],
            'password' => 'required|min:6',
            'password_confirmation' => 'required|min:6|same:password',
        ];
    }
}

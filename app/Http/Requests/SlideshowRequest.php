<?php
namespace App\Http\Requests;

use App\Repositories\LanguageRepository;
use App\Http\Requests\Request;

class SlideshowRequest extends Request
{
    protected $messages = [];

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $post = Request::all();

        return [
            'identifier' => [
                'required',
                Rule::unique('slideshows')->ignore($post['identifier'], 'identifier')
            ],
        ];
    }

    public function messages()
    {
        return $this->messages;
    }
}

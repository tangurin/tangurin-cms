<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;

class AccountLoginRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $post = Request::all();
        $globalRules = with(new UserRequest())->rules();
        $rules = [
            'email' => $globalRules['email'],
            'passwordCurrent' => 'required|min:6',
        ];

        //If password is not empty, validate it.
        if (!empty($post['password'])) {
            $rules['password'] = $globalRules['password'];
            $rules['password_confirmation'] = $globalRules['password_confirmation'];
        }

        return $rules;
    }
}

<?php
namespace App\Http\Requests;

use App\Repositories\LanguageRepository;
use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class RoleRequest extends Request
{
    protected $messages = [];

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => [
                'required',
                Rule::unique('roles')->ignore(intval($this->id)),
            ],
            'identifier' => [
                'required',
                Rule::unique('roles')->ignore(intval($this->id)),
            ],
        ];
    }

    public function messages()
    {
        return $this->messages;
    }
}

<?php
namespace App\Http\Requests;

use App\Repositories\LanguageRepository;
use App\Http\Requests\Request;

class PageRequest extends Request
{
    protected $messages = [];

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $post = Request::all();

        $fields = [];

        resolve(LanguageRepository::class)->listAvailable()->each(function($language, $languageCode) use (&$fields, $post) {
            //Current Translation-id
            //Except current translation + search where language is the same
            $id = intval($post['data'][$languageCode]['id']);
            $uniqueException = $id > 0 ? ','. $id .',id,language,'. $languageCode : '';
            //Field validation rules
            $title = 'data.'. $languageCode .'.title';
            $slug = 'data.'. $languageCode .'.slug';
            $fields[$title] = 'required|min:2';
            $fields[$slug] = 'required|min:2|unique:page_translations,slug'. $uniqueException;
        });

        //Return validation rules
        return $fields;
    }

    public function messages()
    {
        return $this->messages;
    }
}

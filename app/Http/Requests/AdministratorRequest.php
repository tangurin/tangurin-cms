<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ManageAdministratorRequest extends Request
{
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$method = $this->method();

		$id = '';
		$password = 'required|min:8';
		$passwordRepeat = 'required|same:password';

		if($method == 'PUT' || $method == 'PATCH') {
			$id = ','. $this->administrator;

			$inputPassword = $this->get('password');
			if( empty($inputPassword) ) {
				$password       = '';
				$passwordRepeat = '';
			}
		}

		return  [
			'username'       => 'required|unique:administrator,username'. $id,
			'email'          => 'required|email|unique:administrator,email'. $id,
			'name'           => 'required',
			'password'       => $password,
			'passwordRepeat' => $passwordRepeat,
		];
	}
}

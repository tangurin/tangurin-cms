<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class CityRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $post = Request::all();

        $uniqueException = ','. $post['id'] .',id,county_id,'. $post['county_id'];

        return [
            'county_id' => 'required',
            'name' => [
                'required',
                'min:2',
                Rule::unique('cities')->ignore($post['id'])->where(function ($query) use ($post) {
                    $query->where('county_id', $post['county_id']);
                }),
            ],
        ];
    }
}

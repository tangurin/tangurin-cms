<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class LinkerRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $post = Request::all();

        return [
            'slug' => [
                'required',
                Rule::unique('linkers')->ignore($post['slug'], 'slug')
            ],
            'target' => 'required|min:4',
        ];
    }
}

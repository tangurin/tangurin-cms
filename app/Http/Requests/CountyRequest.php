<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;

class CountyRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $post = Request::all();

        $uniqueException = ','. $post['id'] .',id,country_id,'. $post['country_id'];

        return [
            'country_id' => 'required',
            'name' => 'required|min:2|unique:counties,name'. $uniqueException
        ];
    }
}

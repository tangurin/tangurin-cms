<?php
namespace App\Http\Middleware;

class ForceSSL
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        // Getting production To avoid local uri.
        if (env('APP_SSL', true) && $request->secure() === false) {
            return redirect()->secure($request->getRequestUri());
        }
        return $next($request);
    }
}

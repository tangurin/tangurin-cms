<?php

namespace App\Http\Controllers\Filemanager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Filemanager\FileRepository;
use App\Repositories\Filemanager\DirectoryRepository;
use App\Repositories\Filemanager\IndexRepository;

class DirectoryController extends Controller
{

    protected $fileRepository;
    protected $directoryRepository;
    protected $indexRepository;

    public function __construct()
    { 
        $this->fileRepository = resolve(FileRepository::class);
        $this->directoryRepository = resolve(DirectoryRepository::class);
        $this->indexRepository = resolve(IndexRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->directoryRepository->getDirectoriesTree();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $target = $request->input('target');
        $name = str_slug($request->input('name'));

        if ($this->directoryRepository->exists($target, $name)) {
            return response('Målkatalogen finns redan.', 422);
        }

        if ($this->directoryRepository->create($target, $name)) {
            return response('Katalogen "'. $name .'" skapades i "'. $target .'"');
        }

        return response('Något gick fel', 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $currentDirectory = $request->input('currentDirectory');
        $parentDirectory = $request->input('parentDirectory');
        $name = str_slug($request->input('name'));
        $newPath = $parentDirectory . '/' . $name;

        if ($this->directoryRepository->exists($newPath)) {
            return response('Det finns redan en katalog med det här namnet.', 422);
        }

        if ($this->directoryRepository->rename($currentDirectory, $newPath)) {
            return response('Katalogen "'. $currentDirectory .'" döptes om till '. $newPath);
        }

        return response('Något gick fel', 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $target = $request->input('target');

        if ($this->directoryRepository->delete($target)) {
            return response('Katalogen "'. $target .'" togs bort');
        }

        return response('Något gick fel', 422);
    }
}

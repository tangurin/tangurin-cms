<?php

namespace App\Http\Controllers\Filemanager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Filemanager;
use App\Repositories\Filemanager\FileRepository;
use App\Repositories\Filemanager\DirectoryRepository;
use App\Repositories\Filemanager\IndexRepository;

class IndexController extends Controller
{

    protected $fileRepository;
    protected $directoryRepository;
    protected $indexRepository;

    public function __construct()
    { 
        $this->fileRepository = resolve(FileRepository::class);
        $this->directoryRepository = resolve(DirectoryRepository::class);
        $this->indexRepository = resolve(IndexRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function validateIndex(Filemanager $file, Request $request)
    {
        if ($this->fileRepository->isFileExists($file->path) === false) {
            $this->indexRepository->deleteFromIndex($file);
            return 0;
        }
        return 1;
    }
}

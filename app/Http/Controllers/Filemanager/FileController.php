<?php

namespace App\Http\Controllers\Filemanager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Filemanager\FileRepository;
use App\Repositories\Filemanager\DirectoryRepository;
use App\Repositories\Filemanager\IndexRepository;

class FileController extends Controller
{

    protected $fileRepository;
    protected $directoryRepository;
    protected $indexRepository;

    public function __construct()
    { 
        $this->fileRepository = resolve(FileRepository::class);
        $this->directoryRepository = resolve(DirectoryRepository::class);
        $this->indexRepository = resolve(IndexRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $path = $request->get('path');
        saveUserPreference('currentDirectory', $path);
        return $this->fileRepository->getFiles($path);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has('file') === false) {
            return null;
        }
        
        $directory = $request->input(
            'directory',
            userPreference('filemanager.currentDirectory', $this->directoryRepository->getRootDirectory())
        );

        $filepath = $this->fileRepository->storeFile($request->file('file'), $directory);
        return $this->indexRepository->insertToIndex( $this->fileRepository->collectFileData($filepath) );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $target = $request->input('target');

        if ($this->fileRepository->delete($target)) {
            return response('Filen "'. $target .'" togs bort');
        }

        return response('Något gick fel', 422);
    }
}

<?php
namespace App\Http\Controllers\Frontend;

use View;
use Config;
use App\Models\Translation;
use App\Models\Setting;

class FrontendController extends \App\Http\Controllers\Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->addBreadcrumb('main.index', t('navigation.breadcrumbs.start'));
    }
}

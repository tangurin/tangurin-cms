<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\View\View;
use App\Models\Page;

class ErrorController extends FrontendController
{
    public function error404()
    {
        return response()->view('errors.404', [
            'page' => new Page(),
            'metaTitle' => '404',
        ], 404);
    }
}

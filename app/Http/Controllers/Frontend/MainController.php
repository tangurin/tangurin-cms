<?php
namespace App\Http\Controllers\Frontend;

use App\Models\Page;
use App\Models\Linker;
use App\Repositories\PageRepository;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Session;
use Customfields\Repository as CustomfieldsRepository;

class MainController extends FrontendController
{
    public function index($slug = null, Request $request)
    {
        $page = resolve(PageRepository::class)->getPageBySlug($slug);
        
        //If we successfully recieved a page object
        if(!empty($page)) {
            //Create hierarchical breadcrumbs
            $currentParent = $page->parent;
            while ($currentParent != 0) {
                $parent = Page::with('translations')->find($currentParent);
                $this->addBreadcrumb($this->route['index'], $parent->translation->title, ['slug' => $parent->translation->slug]);
                $currentParent = $parent->parent;
            }
            $this->addBreadcrumb($this->route['index'], $page->translation->title, ['slug' => $slug]);

            $viewData = [
                'page' => $page,
                'title' => $page->translation->title,
                'content' => $page->translation->content,
                'user' => Auth::user(),
                'metaDescription' => $page->getSeoDescription(),
                'metaKeywords' => $page->getSeoKeywords(),
                'metaTitle' => $page->getSeoTitle(),
                'customfields' => new CustomfieldsRepository('Page', $page->id),
            ];

            if ($page->isStartpage()) {
                $page->template = 'startpage';
            }

            //Run template method if exists
            $templateMethod = 'template'. ucfirst($page->template);
            if (!empty($page->template) && method_exists($this, $templateMethod)) {
                $templateReturn = $this->{$templateMethod}($page, $request);
                if ($templateReturn instanceof \Illuminate\Http\RedirectResponse) {
                    return $templateReturn;
                }
                if (is_array($templateReturn)) {
                    $viewData = array_merge($viewData, $templateReturn);
                }
            }

            return view($page->getViewPath(), $viewData);
        }

        $linker = Linker::where('slug', $slug)->first();
        if (!empty($linker)) {
            return redirect($linker->target);
        }
        
        return abort(404);
    }

    public function templateStartpage()
    {
        $viewData = [];
        return $viewData;
    }

    public function templateContact($page, $request)
    {
        if ($request->has('email')) {
            Mail::send( new \App\Mail\Contact($request) );
            Session::flash('success', t('success.contactForm'));
            return redirect()->route('main.index', [$page->translation->slug]);
        }

        $viewData = [];

        return $viewData;
    }
}

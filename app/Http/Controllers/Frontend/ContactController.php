<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Setting;

class ContactController extends \App\Http\Controllers\Controller
{
    public function send(Requests\ContactRequest $request)
    {
        $settings = new \stdClass();
        $settings->to = Setting::value('siteEmail');
        $settings->siteName = Setting::value('siteName');
        $settings->template = Template::email('contact');

        \Mail::send($settings->template, ['request' => $request], function ($message) use ($settings, $request) {
            $message->to($settings->to, $settings->siteName)->from($request->email, $request->name)->subject('Kontaktformulär '. $settings->siteName .': '. $request->name);
        });
    }
}

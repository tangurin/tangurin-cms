<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Requests\AccountLoginRequest;
use App\Models\User;
use App\Models\User\CompanyImage;
use Auth;
use Hash;
use Illuminate\Http\Request;

class AccountController extends FrontendController
{
    protected $maximumFileSize = 5;
    protected $imageSlots = 3;

    public function index(Request $request)
    {
        $this->addBreadcrumb($this->route['index'], t('account.myAccount'));
        $user = Auth::user();

        return view('Frontend::account.index', array_merge([
                'user' => $user,
                'openedTab' => $request->get('openedTab', null), 
            ])
        );
    }

    public function storeLogin(AccountLoginRequest $request)
    {
        $this->authorize('frontend_update_emailAndPassword');
        $user = Auth::user();
        
        if (Hash::check($request->passwordCurrent, $user->password) === false) {
            return response()->json(['passwordCurrent' => t('error.currentPasswordNotMatch')], 422);
        }

        $newData = [
            'email' => $request->email,
        ];

        if (!empty($request->password)) {
            $newData['password'] = Hash::make($request->password);
        }

        $user->update($newData);

        return [];
    }
}

<?php

namespace App\Http\Controllers;

use App\Repositories\ViewRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Route;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $breadcrumbs = [];
    public $controller = '';
    public $page = '';
    public $route = [];

    public function __construct()
    {
        $this->assignLayoutInformation();
        resolve(ViewRepository::class)->shareGlobals();
    }

    public function addBreadcrumb($routeName, $label, $segments = [])
    {
        $breadcrumbs = View::shared('breadcrumbs', []);
        if ($routeName != '#') {
            if (!Route::has($routeName)) {
                return false;
            }

            $url = route($routeName, $segments);
        } else {
            $url = '#';
        }

        $breadcrumbs[$url] = $label;
        View::share('breadcrumbs', $breadcrumbs);
        View::share('metaTitle', View::shared('headTitle', $label .' - '. setting('companyName')));
    }

    public function assignLayoutInformation()
    {
        //Get Controller name, this will be used when calling action() function.
        $reflectionClass = new \ReflectionClass(get_class($this));
        $classPath = $reflectionClass->getName();
        $className = $reflectionClass->getShortName();
        
        $find = 'Controllers\\';
        $strStart = strpos($classPath, $find) + strlen($find);
        $controller = substr($classPath, $strStart);
        $currentRoute = Route::currentRouteName();
        $routeNames = collect( explode('.', $currentRoute) );
        $routeBase = $routeNames->slice(0, -1)->implode('.');

        $this->controller = $controller;
        $this->page = lcfirst(str_replace('Controller', '', $className));
        $this->assignRoutes([
            'current' => $currentRoute,
            'base' => $routeBase,
            'index' => $routeBase .'.index',
            'create' => $routeBase .'.create',
            'store' => $routeBase .'.store',
            'edit' => $routeBase .'.edit',
            'update' => $routeBase .'.update',
            'destroy' => $routeBase .'.destroy',
        ]);

        View::share('controller', $this->controller);
        View::share('page', $this->page);
    }

    public function assignRoutes($routes = [])
    {
        $this->route = array_merge(View::shared('route', []), $routes);
        View::share('route', $this->route);
    }
}

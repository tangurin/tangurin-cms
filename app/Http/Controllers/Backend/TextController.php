<?php
namespace App\Http\Controllers\Backend;

use App\Http\Requests\TextRequest;
use App\Models\Text;
use App\Models\Text\Translation;
use App\Repositories\LanguageRepository;
use App\Repositories\TextRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class TextController extends BackendController
{
    use Authorizable;
    
    public function __construct()
    {
        parent::__construct();
        $this->addBreadcrumb($this->route['index'], __('cms.text'));
    }

    public function index()
    {
        $this->layout->disableCreate();
        return view('Backend::'. $this->page .'.index', [
            'collection' => resolve(TextRepository::class)->getIdentifiersSortedByGroup(),
        ]);
    }

    public function edit(Text $text)
    {
        $this->layout->disableCreate();
        $this->layout->disableDelete();

        $name = __('texts.identifiers.'. $text->identifier .'.name');
        $description = __('texts.identifiers.'. $text->identifier .'.description');
        $this->addBreadcrumb($this->route['edit'], trans('cms.edit') .' '. $name, ['id' => $text->id]);

        return view('Backend::'. $this->page .'.form', [
            'model' => $text,
            'name' => $name,
            'description' => $description,
        ]);
    }

    public function update(Text $text, TextRequest $request, LanguageRepository $languageRepository)
    {
        $text->update($request->all());
        $languageModels = $languageRepository->fillLanguage(Translation::class, $request->data);
        $text->translations()->saveMany( $languageModels );

        return [
            'model' => $text,
            'languageModels' => $languageModels->toArray(),
        ];
    }

    public function destroy(Text $text)
    {
        if ($text->isStartpage()) {
            return response()->json([
                'errors' => [
                    'swal' => 'Startsidan får ej tas bort.',
                ]
            ], 422);
        }

        $text->delete();
    }
}

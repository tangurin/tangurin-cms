<?php
namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Models\Language\Language;
use App\Models\Translation;
use App\Models\Translation\Value;
use App\Repositories\LanguageRepository;
use App\Repositories\TranslationRepository;
use App\Tree\Translation\Hierarchy;
use Gate;
use Illuminate\Http\Request;

class TranslationController extends BackendController
{

    public function __construct()
    {
        parent::__construct();

        $this->addBreadcrumb($this->route['edit'], trans('cms.translations'));
    }

    public function edit()
    {
        $this->layout->disableForm();
        if (Gate::check('backend_view_translation') === false && Gate::check('backend_update_translation') === false) {
            abort(403);
        }

        $hierarchies = [];
        resolve(LanguageRepository::class)->listAvailable()->each(function($language, $languageCode) use(&$hierarchies) {
            $hierarchies[$languageCode] = new Hierarchy([], resolve(TranslationRepository::class)->getStructuredList($languageCode, true));
        });
        
        return view('Backend::'. $this->page .'.form', [
            'collection' => Translation::with('values')->get(),
            'hierarchies' => $hierarchies,
        ]);
    }

    public function update(Request $request)
    {
        if (Gate::check('backend_update_translation') === false) {
            throw new \Illuminate\Auth\AuthenticationException();
        }

        $post = $request->all();
        foreach ($post['translation'] as $id => $values) {
            foreach ($values as $lang => $value) {
                Value::updateOrCreate(['translation_id' => $id, 'language' => $lang], [
                    'value' => $value,
                ]);
            }
        }
    }
}

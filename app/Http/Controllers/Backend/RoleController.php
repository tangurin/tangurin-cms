<?php
namespace App\Http\Controllers\Backend;

use App\Http\Requests\RoleRequest;
use App\Models\Role;
use App\Models\Permission;
use App\Repositories\PermissionRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class RoleController extends BackendController
{
    use Authorizable;

    public function __construct()
    {
        parent::__construct();
        $this->addBreadcrumb($this->route['index'], 'Roller');
    }

    public function index()
    {
        $roles = Role::all();

        return view('Backend::'. $this->page .'.index', [
            'collection' => $roles,
        ]);
    }
    
    public function create(Role $role)
    {
        return view('Backend::'. $this->page .'.form', [
            'permissions' => resolve(PermissionRepository::class)->getMultidimensional(),
            'model' => $role,
        ]);
    }

    public function store(RoleRequest $request)
    {
        $role = Role::create($request->except('permissions'));

        $permissions = $request->get('permissions', []);
        $role->syncPermissions($permissions);
    }

    public function edit(Role $role)
    {
        $this->addBreadcrumb($this->route['edit'], trans('cms.edit') .' '. $role->name, ['id' => $role->id]);

        return view('Backend::'. $this->page .'.form', [
            'model' => $role,
            'permissions' => resolve(PermissionRepository::class)->getMultidimensional(),
        ]);
    }

    public function update(RoleRequest $request, Role $role)
    {
        $role->fill($request->except('permissions'));
        $role->save();

        // Admin role has everything
        if($role->isAdmin()) {
            $role->syncPermissions(Permission::all());
            return;
        }

        $permissions = $request->get('permissions', []);
        $role->syncPermissions($permissions);
    }
}

<?php
namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Models\User;
use App\Models\Permission;
use App\Models\Role;
use App\Repositories\PermissionRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Auth;

class UserController extends BackendController
{
    use Authorizable;

    public function __construct()
    {
        parent::__construct();
        $this->addBreadcrumb($this->route['index'], trans('cms.users'));
    }

    public function index()
    {
        $users = User::all();

        return view ('Backend::'. $this->page .'.index', [
            'collection' => $users,
        ]);
    }

    public function create()
    {
        $this->addBreadcrumb($this->route['create'], trans('cms.create') .' '. trans('cms.user'));
        $roles = Role::pluck('name', 'id');
        return view ('Backend::'. $this->page .'.form', [
            'model' => new User(),
            'roles' => $roles,
            'selectedRoles' => [],
            'permissions' => resolve(PermissionRepository::class)->getMultidimensional(),
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'roles' => 'required|min:1',
        ]);

        // hash password
        $request->merge(['password' => bcrypt($request->get('password'))]);

        // Create the user
        if ( $user = User::create($request->except('roles', 'permissions')) ) {
            $this->syncPermissions($request, $user);
        } else {

        }
    }

    public function edit(User $user, Request $request)
    {
        $this->addBreadcrumb($this->route['edit'], trans('cms.edit') .' '. $user->name, ['id' => $user->id]);
        $roles = Role::pluck('name', 'id');

        return view ('Backend::'. $this->page .'.form', [
            'model' => $user,
            'roles' => $roles,
            'permissions' => resolve(PermissionRepository::class)->getMultidimensional(),
            'selectedRoles' => $user->roles->pluck('id')->toArray(),
        ]);
    }

    public function update(Request $request, $user)
    {
        $rules = [
            'email' => 'required|email|unique:users,email,' . $user->id,
            'roles' => 'required|min:1'
        ];
        
        if ($request->get('password')) {
            $rules['password'] = 'required|min:6';
        }

        $this->validate($request, $rules);

        // Get the user
        $user = User::findOrFail($user->id);

        // Update user
        $user->fill($request->except('roles', 'permissions', 'password'));

        // check for password change
        if ($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }

        // Handle the user roles
        $this->syncPermissions($request, $user);

        $user->save();
    }

    public function destroy(User $user)
    {
        if ( Auth::user()->id == $user->id ) {
            return response()->json([
                'errors' => [
                    'swal' => 'Du kan ej ta bort dig själv.',
                ]
            ], 422);
        }

        if( $user->delete() ) {
            return [];
        } else {
            return response()->json([
                'errors' => [
                    'swal' => 'Det gick ej att ta bort användaren.',
                ]
            ], 422);
        }
    }

    public function activate(User $user)
    {
        $user->activateAccount();
        return redirect()->route('user.edit', [$user->id]);
    }

    public function deActivate(User $user)
    {
        $user->deActivateAccount();
        return redirect()->route('user.edit', [$user->id]);
    }

    private function syncPermissions(Request $request, $user)
    {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);

        // Get the roles
        $roles = Role::find($roles);

        // check for current role changes
        if( ! $user->hasAllRoles( $roles ) ) {
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else {
            // handle permissions
            $user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);
        return $user;
    }
}

<?php
namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\County;
use App\Models\City;
use App\Models\Page\PageContent;
use App\Models\Page\Tree;

class AjaxController extends \App\Http\Controllers\Controller
{
    public function convertToSlug()
    {
        if($string = Request::input('slug')) {
            if(Request::input('model') && Request::input('column')) {
                return App\Text::findAvailableSlug($string, Request::input('model'), Request::input('column'));
            }
            return Tangurin\Helper::convertToSlug($string);
        }

        return '';
    }

    public function getParents()
    {
        return generateParentsSelect('name="parent"');
    }

    public function counties(Request $request)
    {
        $post = $request->all();

        $options = '<option value="0">'. trans('cms.noLocations') .'</option>';
        if(!empty($post['value'])) {
            $counties = County::where(['country_id' => $post['value']])->get();
            $options = '';
            foreach($counties as $county) {
                $options .= '<option value="'. $county->id .'"'. ($county->id == $post['selected'] ? ' selected' : '') .'>'. $county->name .'</option>';
            }
        }

        return $options;
    }

    public function cities()
    {
        $id = Request::input('id');
        if($id) {
            $cities = City::where(['county_id' => $id])->get();
            $options = '';
            foreach($cities as $city) {
                $options .= '<option value="'. $city->id .'">'. $city->name .'</option>';
            }
        }

        return (isset($options) && !empty($options)) ? $options : '<option value="">'. trans('cms.noCities') .'</option>';
    }

    public function searchPages()
    {
        $searchWords = Request::input('searchWords');
        $pages = PageContent::where('title', 'LIKE', '%'. $searchWords .'%')
            ->orWhere('slug', 'LIKE', '%'. $searchWords .'%')
            ->get();

        $return = 'Inget resultat...';
        if (!$pages->isEmpty()) {
            $return = '<ul>';
            foreach ($pages as $page) {
                $return .= '<li>';
                    $return .= '<a data-id="'. $page->page_id .'" data-name="'. $page->title .'" class="pageLink" title="'. $page->page_id .' '. $page->title .' ('. $page->slug .')">'. substr($page->title, 0, 40) .'</a>';
                $return .= '</li>';
            }
            $return .= '</ul>';
        }

        return $return;
    }

    public function permissions(Reqeust $request)
    {
        $this->validate($request, [
            'type' => 'required|unique:roles'
        ]);
    }
}

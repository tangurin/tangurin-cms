<?php
namespace App\Http\Controllers\Backend;

use App\Http\Requests\PageRequest;
use App\Models\Language;
use App\Models\Page;
use App\Models\Page\Image as Image;
use App\Models\Page\Translation as Translation;
use App\Repositories\LanguageRepository;
use App\Repositories\PageRepository;
use App\Traits\Authorizable;
use App\Tree;
use Auth;
use Customfields\ControllerTrait as CustomfieldsController;
use Illuminate\Http\Request;

class PageController extends BackendController
{
    use Authorizable, CustomfieldsController;
    
    public function __construct()
    {
        parent::__construct();
        $this->addBreadcrumb($this->route['index'], 'Sidor');
    }

    public function index()
    {
        return view('Backend::'. $this->page .'.index', []);
    }

    public function create(LanguageRepository $languageRepository)
    {
        $this->addBreadcrumb($this->route['create'], trans('cms.create') .' '. trans('cms.page'));

        return view('Backend::'. $this->page .'.form', [
            'model' => new Page(),
            'parents' => $this->getParentSelect(),
        ]);
    }

    public function store(Page $page, PageRequest $request, LanguageRepository $languageRepository)
    {
        $page->fill($request->all());
        $page->save();

        $page->translations()->saveMany( $languageRepository->fillLanguage(Translation::class, collect($request->data)) );

        if ($this->validateCustomfields()) {
            $this->saveCustomfields();
        }

        return [
            'created' => [
                'name' => $request->data[$languageRepository->current()]['title'],
                'link' => route($this->route['edit'], [$page->id]),
            ],
            'replace' => [
                [
                    'target' => '#parentSelect',
                    'html' => $this->getParentSelect(),
                ]
            ]
        ];
    }

    public function edit(Page $page)
    {
        if ($page->isStartpage()) {
            $this->layout->disableDelete();
        }

        $this->initCustomfields('Page', $page->id);

        $this->addBreadcrumb($this->route['edit'], trans('cms.edit') .' '. $page->translation->title, ['id' => $page->id]);

        return view('Backend::'. $this->page .'.form', [
            'model' => $page,
            'parents' => $this->getParentSelect($page->parent, $page->id),
            'images' => $page->images,
            'generatedCustomfieldsView' => $this->generateCustomfieldsView(),
        ]);
    }

    public function update(Page $page, PageRequest $request, LanguageRepository $languageRepository)
    {
        $page->update($request->all());
        $page->translations()->saveMany( $languageRepository->fillLanguage(Translation::class, $request->data) );

        if ($this->validateCustomfields()) {
            $this->saveCustomfields();
        }

        //Update form elements after save
        $replace = [
            [
                'target' => '#parentSelect',
                'html' => $this->getParentSelect($page->parent, $page->id),
            ], [
                'target' => 'ol.breadcrumb li:last a span',
                'html' => '<span>'. trans('cms.edit') .' '. $page->translation->title .'</span>',
            ],
        ];
        //Update all slug fields when saved
        $page->translations->each(function($data) use (&$replace) {
            $replace[] = [
                'target' => 'input.'. $data->language .'Slug',
                'val' => $data->slug,
            ];
        });

        return ['replace' => $replace];
    }

    public function destroy(Page $page)
    {
        if ($page->deleteable()) {
            $page->delete();
            return '';
        }

        return response()->json([
            'errors' => [
                'swal' => 'Startsidan får ej tas bort.',
            ]
        ], 422);
    }

    public function sortableView()
    {
        $sortable = new Tree\Page\Sortable();
        return view('Backend::'. $this->page .'.sortable', [
            'tree' => $sortable->generate()
        ]);
    }

    public function datatableView(PageRepository $pageRepository)
    {
        return view('Backend::'. $this->page .'.datatable', [
            'collection' => $pageRepository->getPages(),
        ]);
    }

    public function saveSort(Request $request)
    {
        $sort = $request->input('sort');
        $decoded = json_decode($sort, true);

        if (!is_null($sort) && is_array($decoded)) {
            $this->_recursiveSort($decoded);
        }
        die();
    }

    private function _recursiveSort($data, $parent = 0)
    {
        $this->sortCounter = 0;
        foreach ($data as $row) {
            if (!isset($row['id'])) continue;

            $id = $row['id'];
            $page = Page::find($id);

            if (!is_null($page)) {
                $page->parent = $parent;
                $page->sort_order = $this->sortCounter;
                $page->save();
                unset($page);
            }

            $this->sortCounter++;

            if (isset($row['children'])) {
                $this->_recursiveSort($row['children'], $id);
            }
        }
    }

    private function getParentSelect($selected = 0, $disable = null)
    {
        return with(new Tree\Page\Select([
            'defaultOptions' => [0 => '-- Välj Förälder --'],
            'id' => 'parentSelect',
            'selected' => $selected,
            'disable' => $disable,
        ]));
    }
}

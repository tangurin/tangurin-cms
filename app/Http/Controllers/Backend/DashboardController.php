<?php
namespace App\Http\Controllers\Backend;

use Analytics;
use App\Models\User;
use App\Http\Requests;
use Illuminate\Http\Request;
//use Spatie\Analytics\Period;
use Carbon\Carbon;

class DashboardController extends BackendController
{
    public function __construct()
    {
        parent::__construct();
        $this->addBreadcrumb($this->route['index'], trans('cms.dashboard'));
    }
    public function index()
    {
        return view('Backend::dashboard.index', [
        ]);
    }
}

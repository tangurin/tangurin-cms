<?php
namespace App\Http\Controllers\Backend;

use App\Http\Requests\LinkerRequest;
use App\Models\Linker;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class LinkerController extends BackendController
{
    use Authorizable;
    
    public function __construct()
    {
        parent::__construct();
        $this->addBreadcrumb($this->route['index'], __('cms.linker'));
    }

    public function index()
    {
        return view('Backend::'. $this->page .'.index', [
            'collection' => Linker::all(),
        ]);
    }

    public function create()
    {
        return view('Backend::'. $this->page .'.form', [
            'model' => new Linker(),
        ]);
    }

    public function store(Linker $linker, LinkerRequest $request)
    {
        $linker->fill($request->all());
        $linker->save();
    }

    public function edit(Linker $linker)
    {
        $this->addBreadcrumb($this->route['edit'], trans('cms.linker') .' '. $linker->slug, ['id' => $linker->id]);

        return view('Backend::'. $this->page .'.form', [
            'model' => $linker,
        ]);
    }

    public function update(Linker $linker, LinkerRequest $request)
    {
        $linker->update($request->all());

        return [
            'model' => $linker,
        ];
    }

    public function destroy(Linker $linker)
    {
        $linker->delete();
    }
}

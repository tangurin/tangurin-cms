<?php
namespace App\Http\Controllers\Backend;

use App\Traits\Authorizable;
use Illuminate\Http\Request;

class MediamanagerController extends BackendController
{
    use Authorizable;

    public function index()
    {
        return view('Backend::'. $this->page .'.index', []);
    }
}

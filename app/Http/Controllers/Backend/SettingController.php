<?php
namespace App\Http\Controllers\Backend;

use App\Http\Requests;

use App\Models\Setting;
use App\Repositories\SettingRepository;
use App\Traits\Authorizable;
use Gate;
use Request;

class SettingController extends BackendController
{
    public function __construct()
    {
        parent::__construct();

        $this->addBreadcrumb($this->route['edit'], __('cms.settings'));
    }

    public function edit(SettingRepository $settingRepository)
    {
        $this->layout->disableForm();
        if (Gate::check('backend_view_setting') === false && Gate::check('backend_update_setting') === false) {
            abort(403);
        }

        return view('Backend::'. $this->page .'.form', [
            'collection' => $settingRepository->onlyValue()->all(),
        ]);
    }

    public function update()
    {
        if (Gate::check('backend_update_setting') === false) {
            throw new \Illuminate\Auth\AuthenticationException();
        }

        foreach(Request::input('settings') as $key => $value) {
            Setting::updateOrCreate(['key' => $key], [
                'value' => $value
            ]);
        }
    }
}

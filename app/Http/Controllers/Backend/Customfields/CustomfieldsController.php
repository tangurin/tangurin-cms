<?php
namespace App\Http\Controllers\Backend\Customfields;

use App\Http\Controllers\Backend\BackendController;
use Customfields\Collection;
use Customfields\Relation;
use Customfields\Field;
use Customfields\FieldSetting;
use Customfields\FieldData;
use Customfields\FieldValue;
use Customfields\FieldValueTranslation;
use Customfields\CollectionModule;
use Session;
use DbFactory;

class CustomfieldsController extends BackendController
{
    public $sortOrder = 0;

    public function __construct()
    {
        parent::__construct();
        $this->addBreadcrumb($this->route['index'], 'Customfields');
    }

    public function index()
    {
        $collections = Collection::all()->order('sort_order', 'ASC')->toArray();
        return view('Backend::customfields.index', [
            'collections' => $collections,
        ]);
    }

    public function form($collectionId = 0)
    {
        $errors = array();
        $preparedRelations = [];
        $preparedFields = [];

        if ($collectionId > 0) {
            $this->addBreadcrumb('backend.customfields.edit', 'Customfields', [$collectionId]);
            $collection = Collection::find($collectionId);
            //Add property ID (Prepare for JSON)
            $preparedRelations = Relation::where(array('collection_id =' => $collectionId))->toArray();
            array_walk($preparedRelations, function($item, $key) use($preparedRelations) {
                $preparedRelations[$key]->id = intval($item->id);
            });

            $preparedFields = Collection::getFields($collectionId);
        } else {
            $this->addBreadcrumb('backend.customfields.create', 'Skapa kollektion');
            $collection = new Collection();
        }
        if (isset($_POST['removeRelation'])) {
            $this->removeRelations($_POST['removeRelation']);
        }

        if (isset($_POST['removeField'])) {
            $this->removeFields($_POST['removeField']);
        }

        if (isset($_POST['collection'])) {
            $collectionPost = $_POST['collection'];
            $collection->updateAttributes($collectionPost);

            if (empty($collectionPost['name'])) Session::flash('error', 'Namn måste vara ifyllt.');
            if (empty($collectionPost['identifier'])) Session::flash('error', 'Identifieraren måste vara ifylld.');

            if (isset($_POST['field'])) {
                $fieldPost = $_POST['field'];
                $preparedFields = $this->prepareFieldPost($fieldPost);
            } else {
                Session::flash('error', 'Det krävs minst ett fält i kollektionen.');
            }

            if (isset($_POST['relation'])) {
                $relationPost = $_POST['relation'];
                $preparedRelations = $this->prepareRelationPost($relationPost);
            } else {
                Session::flash('error', 'Det krävs att minst en relation finns, eller att valet "Ingen relation" är markerat.');
            }

            if (!Session::has('error')) {
                //Save the collection
                $collection->sortOrder = Collection::getNextSortNumber();
                $collection->save();
                $collectionId = $collection->id;
                
                //Save all the fields
                $preparedFields = $this->prepareFieldPost($fieldPost, $collectionId);
                $this->saveFields($preparedFields);

                //Save the relations if not a module
                $preparedRelations = $this->prepareRelationPost($relationPost, $collectionId);
                $this->saveRelations($preparedRelations);

                Session::flash('success', 'Fältkollektionen är nu sparad');
                return redirect()->route('backend.customfields.edit', [$collection->id]);
            }
        }

        $allFields = Field::getFieldTypes(true);
        $fieldSettings = [];
        foreach ($allFields as $key => $label) {
            $class = 'Customfields\Field\\'. $key;
            if (class_exists($class)) {
                $fieldSettings[$key] = $class::getSettings();
            }
        }

        return view('Backend::customfields.form', [
            'collection' => $collection,
            'collectionCategories' => Collection::getCategories(),
            'allModules' => json_encode(CollectionModule::getAllModules()),
            'fieldTypes' => Field::getFieldTypes(true),
            'templateJSON' => json_encode([
                'classJSON' => Relation::getClasses(),
                'relationJSON' => Relation::getRelations(),
                'relationIdJSON' => Relation::getRelationsId(),
                'fieldSettings' => $fieldSettings,
                'relations' => $preparedRelations,
                'fields' => array_values($preparedFields),
            ]),
            'errors' => $errors
        ]);
    }

    private function removeRelations($relationIds)
    {
        foreach ($relationIds as $id) {
            $relation = Relation::find($id);
            if (!empty($relation)) {
                $relation->delete();
            }
        }
    }

    private function saveRelations($relations)
    {
        foreach ($relations as $relation) {
            $relation->save();
        }
    }

    private function removeFields($fieldIds)
    {
        foreach ($fieldIds as $id) {
            $id = intval($id);
            $field = Field::find($id);
            if (!empty($field)) {
                $this->removeFieldParent($id);
                $field->delete();
            }
        }
    }

    private function removeFieldParent($parentId)
    {
        $parentFields = Field::where(['parent_id =' => $parentId])->toArray();
        if (!empty($parentFields)) {
            foreach ($parentFields as $field) {
                if ($field->type == 'Repeater') {
                    $this->removeFieldParent($field->id);
                }
                $field->delete();
            }
        }
    }

    private function saveFields($fields)
    {
        foreach ($fields as $field) {
            $field->save();
            $this->saveSettings($field->settings, $field->id);
            $this->saveData($field->data, $field->id);
        }

        //Remove all inactive fields older than 1 hour
        $stmt = DbFactory::getConnection()->prepare("
            DELETE FROM customfields_collection_fields
                WHERE active = 0
                AND collection_id IS NULL
                AND created_at < (UNIX_TIMESTAMP() - 3600)
        ");
        $stmt->execute();
    }

    private function saveSettings($settings, $fieldId)
    {
        //Remove this field settings before inserting new settings.
        $stmt = DbFactory::getConnection()->prepare("
            DELETE FROM customfields_collection_field_settings
            WHERE field_id = :fieldId
        ");
        $stmt->bindParam('fieldId', $fieldId);
        $stmt->execute();

        foreach ($settings as $key => $value) {
            $setting = new FieldSetting();
            $setting->fieldId = $fieldId;
            $setting->key = $key;
            $setting->value = $value;
            $setting->save();

            unset($setting);
        }
    }

    private function saveData($data, $fieldId)
    {
        //Remove this field data before inserting new data.
        $stmt = DbFactory::getConnection()->prepare("
            DELETE FROM customfields_collection_field_data
            WHERE field_id = :fieldId
        ");
        $stmt->bindParam('fieldId', $fieldId);
        $stmt->execute();

        $i = 1;
        foreach ($data as $key => $values) {
            $data = new fieldData();
            $data->fieldId = $fieldId;
            $data->value = $values['dataValue'];
            $data->label = $values['dataLabel'];
            $data->sortOrder = $i;
            $data->save();

            unset($data);
            $i++;
        }
    }

    protected function prepareRelationPost($post, $collectionId = 0)
    {
        $data = [];
        $relationsCount = count($post['relation']);
        for ($i = 0; $i < $relationsCount; $i++) {
            if (!empty($post['id'][$i])) {
                $data[$i] = Relation::find(intval($post['id'][$i]));
            } else {
                $data[$i] = new Relation();
            }
            if (empty($data[$i])) {
                unset($data[$i]);
                continue;
            }
            $data[$i]->collectionId = intval($collectionId);
            $data[$i]->relationId = $post['relationId'][$i];
            $data[$i]->relation = $post['relation'][$i];
            $data[$i]->class = $post['class'][$i];
            $data[$i]->sort_order = $i;
        }
        return $data;
    }

    protected function prepareFieldPost($post, $collectionId = 0)
    {
        $data = [];
        foreach ($post as $parentId => $fields) {
            foreach ($fields as $fieldId => $field) {
                $fieldId = intval($fieldId);
                $fieldObject = Field::find($fieldId);
                if (empty($fieldObject)) {
                    continue;
                }
                $fieldObject->collectionId = intval($collectionId);
                $fieldObject->name = $field['name'];
                $fieldObject->identifier = $field['identifier'];
                $fieldObject->type = $field['type'];
                $fieldObject->description = $field['description'];
                $fieldObject->parentId = $parentId;
                $fieldObject->sortOrder = ++$this->sortOrder;

                //When we save to the database, active will be 1
                $fieldObject->active = 1;

                $settings = [];
                $dataFields  = isset($field['data']) ? $field['data'] : [];

                //Add multilanguage if the field is not a repeater
                if ($field['type'] != 'repeater') {
                    $settings['multilanguage'] = intval($field['multilanguage']);
                }

                //If repeaterMin and repeaterMax exists, add it to settings
                if (isset($field['repeaterMin']) && isset($field['repeaterMax'])) {
                    $settings['repeaterMin'] = intval($field['repeaterMin']);
                    $settings['repeaterMax'] = intval($field['repeaterMax']);
                }

                //Merge settings with field specific settings
                if (isset($field['settings'])) {
                    $settings = array_merge($settings, $field['settings']);
                }

                $fieldObject->settings = $settings;
                $fieldObject->data = $dataFields;

                $data[$fieldId] = $fieldObject;
            }
        }

        return $data;
    }

    public function delete($collectionId)
    {
        $collection = Collection::find(intval($collectionId));
        if (!empty($collection)) {
            //Delete the collection relations
            $relations = Relation::where(['collection_id =' => $collectionId])->toArray();
            if (!empty($relations)) {
                foreach ($relations as $relation) {
                    $relation->delete();
                }
            }

            //Delete the collection fields
            $fields = Field::where(['collection_id =' => $collectionId])->toArray();
            if (!empty($fields)) {
                foreach ($fields as $field) {
                    $values = FieldValue::where(['field_id =' => $field->id])->toArray();
                    $settings = FieldSetting::where(['field_id =' => $field->id])->toArray();
                    $datas = FieldData::where(['field_id =' => $field->id])->toArray();
                    if (!empty($settings)) {
                        foreach ($settings as $setting) {$setting->delete();}
                    }
                    if (!empty($datas)) {
                        foreach ($datas as $data) {$data->delete();}
                    }
                    if (!empty($values)) {
                        foreach ($values as $value) {
                            $translation = FieldValueTranslation::where(['value_id =' => $value->id])->toArray();
                            if (!empty($translation)) {
                                foreach ($translation as $translation) {
                                    $translation->delete();
                                }
                            }
                            $value->delete();
                        }
                    }
                    $field->delete();
                }
            }

            $collection->delete();
        }
        return redirect()->route('backend.customfields.index');
    }

    public function addFieldAjax()
    {
        if (!isset($_GET['parentId'])) {
            return false;
        }
        $parentId = intval($_GET['parentId']);
        $field = new Field();
        $field->updateAttributes(array(
            'parent_id' => $parentId,
        ));
        $field->save();
        echo $field->id;
    }

    public function sortCollections()
    {
        if (!isset($_POST['order'])) {
            return false;
        }
        $order = explode(',', $_POST['order']);
        $i = 0;
        foreach ($order as $id) {
            $id = intval($id);
            $collection = Collection::find($id);
            if (!empty($collection)) {
                $collection->sortOrder = $i;
                $collection->save();
                $i++;
            }
        }
    }
}

<?php
namespace App\Http\Controllers\Backend\Customfields;

use App\Http\Controllers\Backend\BackendController;
use Customfields\CollectionModule;
use Customfields\Collection;
use Customfields\Relation;
use Customfields\FieldValue;
use Customfields\FieldValueTranslation;
use Customfields\ValueRelation;
use Customfields\Field;
use Illuminate\Http\Request;
use Session;

use Customfields\ControllerTrait AS CustomfieldsController;

class CustomfieldModulesController extends BackendController
{
    use CustomfieldsController;

    public function index(Request $request, $id = 0)
    {
        $this->layout->disableForm();

        $errors = [];
        $label = '';
        $slug = '';
        $successMessage = 'Modulen är nu skapad';
        $edit = false;
        if ($id > 0) {
            $module = CollectionModule::find($id);
            if (empty($module)) {
                return redirect()->route('backend.customfields.modules');
            }
            $edit = true;
            $successMessage = 'Moduel är nu ändrad.';
            $label = $module->label;
            $slug = $module->slug;
        }

        if ($request->has('label')) {
            $label = $request->input('label');
            $slug = $request->input('slug');
            if (empty($label) || empty($slug)) {
                $errors[] = 'Fälten får ej lämnas tomma.';
            }

            if (CollectionModule::moduleNameAvailable($label, $slug, $id) === false) {
                $errors[] = 'Modulen existerar redan';
            }

            if (empty($errors)) {
                if (empty($module)) {
                    $module = new CollectionModule();
                }
                $module->label = $label;
                $module->slug = $slug;
                $module->save();
                Session::put('succes', $successMessage);
                return redirect()->route('backend.customfields.modules');
            }
        }

        return view('Backend::customfields.module.index', [
            'modules' => CollectionModule::getAllModules(),
            'errors' => $errors,
            'label' => $label,
            'slug' => $slug,
            'edit' => $edit,
            'id' => $id,
        ]);
    }

    public function edit(Request $request, $id)
    {
        return $this->index($request, $id);
    }

    public function delete($id)
    {
        $module = CollectionModule::find($id);
        if (!empty($module)) {
            //Remove collection relations
            $relations = Relation::where(['class =' => 'Module'])->where(['relation =' => '='])->where(['relation_id =' => $id])->toArray();
            if (!empty($relations)) {
                foreach ($relations as $relation) {
                    $relation->delete();
                }
            }
            //Remove value relations
            $valueRelations = ValueRelation::where(['class =' => 'Module'])->where(['relation =' => $id])->toArray();
            if (!empty($valueRelations)) {
                foreach ($valueRelations as $relation) {
                    $relationId = $relation->id;
                    //Remove values
                    $values = FieldValue::where(['relation_id =' => $relationId])->toArray();
                    if (!empty($values)) {
                        foreach ($values as $value) {
                            $valueId = $value->id;
                            //Remove value translations
                            $translations = FieldValueTranslation::where(['value_id =' => $valueId])->toArray();
                            if (!empty($translations)) {
                                foreach ($translations as $translation) {
                                    $translation->delete();
                                }
                            }
                            $value->delete();
                        }
                    }
                    $relation->delete();
                }
            }
            $module->delete();
            Session::put('succes', 'Modul borttagen');
        }

        return redirect()->action('Customfields\CustomfieldModulesController@index');
    }

    public function module($slug)
    {
        $this->layout->disableForm();
        $module = CollectionModule::find($slug, 'slug');

        if (empty($module) || $this->initCustomfields('Module', $module->id) === false) {
            return redirect()->to('/');
        }

        if (is_null($this->getCustomFieldsInput()) === false && $this->validateCustomfields()) {
            $this->saveCustomfields();
            return redirect()->back();
        }

        return view('Backend::customfields.module.show', [
            'module' => $module,
            'generatedCustomfieldsView' => $this->generateCustomfieldsView([
                'noModal' => true,
            ]),
            'errors' => $this->customfieldsErrors,
        ]);
    }
}

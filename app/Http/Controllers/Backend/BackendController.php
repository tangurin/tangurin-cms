<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\LayoutRepository;
use App\Repositories\PageRepository;
use View;

class BackendController extends Controller
{
    protected $layout = null;
    
    public function __construct()
    {
        parent::__construct();
        $this->layout = resolve(LayoutRepository::class);
        $this->layout->setController($this);

        $this->addBreadcrumb('dashboard.index', 'Administration');
        $pageRepository = resolve(PageRepository::class);
        View::share('pages', $pageRepository->getPages());
    }

    public function callAction($method, $parameters)
    {
        if (!is_null($this->layout)) {
            $this->layout->passMethod($method);
        }

        return parent::callAction($method, $parameters);
    }

    protected function setPage($name)
    {
        View::share('page', $name);
    }
}

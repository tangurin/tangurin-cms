<?php
namespace App\Http\Controllers\Backend;

use App\Traits\Authorizable;
use App\Repositories\Filemanager\FileRepository;
use App\Repositories\Filemanager\DirectoryRepository;
use App\Repositories\Filemanager\IndexRepository;
use App\Repositories\UserPreferenceRepository;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FilemanagerController extends BackendController
{
    public function __construct()
    { 
        $this->FileRepository = resolve(FileRepository::class);
        $this->DirectoryRepository = resolve(DirectoryRepository::class);
        $this->IndexRepository = resolve(IndexRepository::class);
    }

    public function bootstrap(Request $request)
    {

    }

    public function search(Request $request)
    {
        $keywords = $request->get('keywords');
        $directory = $request->get('directory');
        return $this->FileRepository->searchFiles($keywords, $directory, $request);
    }


    public function checkFiles(Request $request)
    {
        $this->FileRepository->checkFile($request->get('path'));
    }

    public function saveSetting(Request $request)
    {
        if ($request->has(['key', 'value'])) {
            return $this->filemanager->saveSetting($request->user(), $request->get('key'), $request->get('value'));
        }

        return false;
    }
}

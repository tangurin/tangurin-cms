<?php
namespace App\Http\Controllers\Backend\Location;

use App\Http\Controllers\Backend\BackendController;
use App\Models\City;
use App\Models\Country;
use App\Models\County;
use App\Http\Requests;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class CityController extends BackendController
{
    use Authorizable;
    
    public function __construct()
    {
        parent::__construct();
        $this->addBreadcrumb($this->route['index'], trans('cms.locations'));
        $this->addBreadcrumb($this->route['index'], trans('cms.city'));
    }

    public function index()
    {
        return view ('Backend::location.'. $this->page .'.index', [
            'collection' => City::with('county.country.translations')->get(),
        ]);
    }

    public function create()
    {
        $this->addBreadcrumb($this->route['create'], trans('cms.create') .' '. trans('cms.city'));

        return view ('Backend::location.'. $this->page .'.form', [
            'model' => new City(),
            'countries' => Country::with('translations')->get(),
            'counties' => [
                0 => trans('cms.firstSelectCountry')
            ],
        ]);
    }

    public function store(Requests\CityRequest $request)
    {
        City::create($request->all());
    }

    public function edit(City $city)
    {
        $this->addBreadcrumb($this->route['edit'], trans('cms.edit') .' '. $city->name, ['id' => $city->id]);

        return view ('Backend::location.'. $this->page .'.form', [
            'model' => $city,
            'countries' => Country::with('translations')->get(),
            'counties' => [
                0 => trans('cms.firstSelectCountry')
            ],
        ]);
    }

    public function update($id, Requests\CityRequest $request)
    {
        $city = City::findOrFail($id);
        $city->update($request->all());
    }

    public function destroy($id)
    {
        $page = City::find($id);
        $page->delete();
    }
}

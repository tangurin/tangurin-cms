<?php
namespace App\Http\Controllers\Backend\Location;

use App\Http\Controllers\Backend\BackendController;
use App\Http\Requests;
use App\Models\Country;
use App\Models\County;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class CountyController extends BackendController
{
    use Authorizable;
    
    public function __construct()
    {
        parent::__construct();
        $this->addBreadcrumb($this->route['index'], trans('cms.locations'));
        $this->addBreadcrumb($this->route['index'], trans('cms.county'));
    }

    public function index()
    {
        return view ('Backend::location.'. $this->page .'.index', [
            'collection' => County::with('country')->get(),
        ]);
    }

    public function create()
    {
        return view ('Backend::location.'. $this->page .'.form', [
            'model' => new County(),
            'countries' => Country::with('translations')->get(),
        ]);
    }

    public function store(Requests\CountyRequest $request)
    {
        $county = new County();
        $county->fill($request->all());
        $county->save();
    }

    public function edit(County $county)
    {
        $this->addBreadcrumb($this->route['index'], 'Länder');
        $this->addBreadcrumb($this->route['edit'], trans('cms.edit') .' '. $county->name, ['id' => $county->id]);
        
        return view ('Backend::location.'. $this->page .'.form', [
            'model' => $county,
            'countries' => Country::all(),
        ]);
    }

    public function update($id, Requests\CountyRequest $request)
    {
        $county = County::findOrFail($id);
        $county->update($request->all());
    }

    public function destroy($id)
    {
        $county = County::find($id);
        $county->delete();
    }
}

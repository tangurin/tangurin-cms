<?php
namespace App\Http\Controllers\Backend\Location;

use App\Http\Controllers\Backend\BackendController;
use App\Http\Requests;
use App\Models\Country;
use App\Models\Country\Translation AS Translation;
use App\Repositories\LanguageRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class CountryController extends BackendController
{
    use Authorizable;
    
    public function __construct()
    {
        parent::__construct();
        $this->addBreadcrumb($this->route['index'], trans('cms.locations'));
        $this->addBreadcrumb($this->route['index'], trans('cms.country'));
    }

    public function index()
    {
        return view ('Backend::location.'. $this->page .'.index', [
            'collection' => Country::all(),
        ]);
    }

    public function create(Country $country)
    {
        return view ('Backend::location.'. $this->page .'.form', [
            'model' => $country,
        ]);
    }

    public function store(Country $country, Requests\CountryRequest $request, LanguageRepository $languageRepository)
    {
        $country->fill($request->all());
        $country->save();
        $country->translations()->saveMany( $languageRepository->fillLanguage(Translation::class, $request->data) );

        return [
            'created' => [
                'name' => $request->data[$languageRepository->current()]['name'],
                'link' => route($this->route['edit'], [$country->id]),
            ]
        ];
    }

    public function edit(Country $country)
    {
        $this->addBreadcrumb($this->route['index'], 'Länder');
        $this->addBreadcrumb($this->route['edit'], trans('cms.edit') .' '. $country->translation->name, ['id' => $country->id]);
        
        return view ('Backend::location.'. $this->page .'.form', [
            'model' => $country,
        ]);
    }

    public function update(Country $country, Requests\CountryRequest $request, LanguageRepository $languageRepository)
    {
        $country->update($request->all());
        $country->translations()->saveMany( $languageRepository->fillLanguage(Translation::class, $request->data) );
    }

    public function destroy(Country $country)
    {
        $country->delete();
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Repositories\RoleRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        parent::__construct();
        $this->addBreadcrumb('main.index', t('navigation.breadcrumbs.start'));
    }

    public function showRegistrationForm()
    {
        $this->addBreadcrumb('frontend.auth.register', t('account.register'));
        return view('Frontend::auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, with(new UserRequest())->rules());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => !empty($data['name']) ? $data['name'] : '',
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $defaultRole = resolve(RoleRepository::class)->getDefaultUserRole();

        if (!empty($defaultRole)) {
            $user->assignRole($defaultRole);
        }

        $activateOnRegister = (bool) intval(setting('activateUserOnRegistration', 0));
        if ($activateOnRegister) {
            $user->activateAccount();
        }

        Session::flash('success', t('success.registered')); 

        return $user;
    }

    public function redirectPath()
    {
        return route('account.index');
    }
}

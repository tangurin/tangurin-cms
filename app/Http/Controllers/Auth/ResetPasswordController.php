<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->addBreadcrumb('main.index', t('navigation.breadcrumbs.start'));
        $this->middleware('guest');
        $this->redirectTo = route('account.index');
    }

    public function showResetForm(Request $request, $token = null)
    {
        $this->addBreadCrumb('password.reset', t('navigation.breadcrumbs.resetPassword'), [$token]);

        return view('Frontend::auth.password.reset', [
            'token' => $token,
            'email' => $request->email,
        ]);
    }

    protected function sendResetResponse($response)
    {
        return redirect($this->redirectPath())
            ->with('success', trans($response));
    }
}

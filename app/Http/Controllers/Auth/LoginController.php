<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        parent::__construct();
    }

    public function backendLoginForm()
    {
        return view('Backend::auth.login');
    }

    public function frontendLoginForm()
    {
        $this->addBreadcrumb('main.index', t('navigation.breadcrumbs.start'));
        $this->addBreadcrumb('frontend.auth.login', t('login'));

        return view('Frontend::auth.login');
    }

    public function redirectTo()
    {
        $route = \Route::getCurrentRoute();
        
        if (array_search('backend', $route->computedMiddleware) !== false) {
            return route('dashboard.index');
        }

        $defaultRoute = route('account.index');
        $redirectWhenAuthenticated = Session::get('redirectWhenAuthenticated', null);

        return !empty($redirectWhenAuthenticated) ? $redirectWhenAuthenticated : $defaultRoute;
    }
}

<?php
namespace App\Http\Resources\Filemanager;

Use App\Repositories\Filemanager\FileRepository;
use Illuminate\Http\Resources\Json\Resource;
Use Storage;

class FileResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $fileRepository = resolve(FileRepository::class);
        $thumbnail = $fileRepository->getThumbnail($this->path);
        return [
            'id' => $this->id,
            'directory' => $this->directory,
            'path' => $this->path,
            'fullPath' => Storage::url($this->path),
            'relativeFullPath' => '/storage/'. $this->path,
            'name' => $this->name,
            'extension' => $this->extension,
            'filename' => $this->filename,
            'mimetype' => $this->mimetype,
            'type' => $this->type,
            'size' => $this->size,
            'width' => $this->width,
            'height' => $this->height,
            'hidden' => $this->hidden,
            'extra' => $this->extra,
            'thumb' => [
                'full' => Storage::url($this->path),
                'filemanager' => empty($thumbnail) ? '' : \Storage::url($thumbnail),
            ],
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}

<?php
namespace C4Media;

class DbFactory
{
    protected static $connection = null;

    public static function getConnection()
    {
        if (is_null(static::$connection)) {
            static::$connection = \DB::connection()->getPdo();
        }
        
        return static::$connection;
    }
}

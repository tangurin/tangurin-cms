<?php
namespace C4Media\ActiveRecord;

abstract class Base extends \Mnl\ActiveRecord\Base
{
    protected $validationErrors;

    public function validate()
    {
        $this->validationErrors = array();
    }

    public function isValid()
    {
        if ($this->validationErrors == null) {
            $this->validate();
        }
        if (count($this->validationErrors) == 0) {
            return true;
        }
        return false;
    }

    public function getValidationErrors()
    {
        return $this->validationErrors;
    }

    public function toArray()
    {
        return array_map(
            function($property) {
                if (is_array($property) || is_string($property)) {
                    return $property;
                }
                if (is_object($property) && method_exists($property, 'toArray')) {
                    return $property->toArray();
                }
                return null;
            },
            get_object_vars($this)
        );
    }

    public function jsonSerialize()
    {
        return array_map(
            function($property) {
                if (is_array($property) || is_string($property)) {
                    return $property;
                }
                if (is_object($property) && method_exists($property, 'jsonSerialize')) {
                    return $property->jsonSerialize();
                }
                return null;
            },
            get_object_vars($this)
        );
    }
}

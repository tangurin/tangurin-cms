$(function() {
    FlashData.initialize();
});

var FlashData = {
    initialize: function() {
        if (FlashDataJson.success != null) {
            FlashData.success(FlashDataJson.success);
        }
        if (FlashDataJson.error != null) {
            FlashData.error(FlashDataJson.error);
        }
        if (FlashDataJson.info != null) {
            FlashData.info(FlashDataJson.info);
        }
    },
    success: function(message) {
        SuccessHandler.swal(message)
    },
    error: function(message) {
        ErrorHandler.swal(message)
    },
    info: function(message) {
        InfoHandler.swal(message)
    }
};

var SuccessHandler = {
    __construct: function() {
        ErrorHandler.removeErrors();
        SuccessHandler.emptyOnSave();
    },
    swal: function(message, title, extraOptions) {
        SuccessHandler.__construct();
        
        if (typeof message == 'object') {
            message = message.join('<br>');
        }

        var options = {
            type: 'success',
            title: title || Language.success.success || 'Success',
            text: message || Language.success.saved || 'Saved',
        };

        swal($.extend(options, extraOptions || {}));
    },
    emptyOnSave: function() {
        $('.emptyOnSave').val('');
    },
};

var ErrorHandler = {
    __construct: function() {
        ErrorHandler.removeErrors();
    },
    swal: function(message, title, extraOptions) {
        ErrorHandler.__construct();

        if (typeof message == 'object') {
            console.log(message);
            message = message.join('<br>');
        }

        var options = {
            type: 'error',
            title: title || Language.error.error || 'Error',
            text: message || Language.error.notSaved || 'Form not saved',
        };

        swal($.extend(options, extraOptions || {}));
    },
    fields: function(json) {
        ErrorHandler.__construct();

        if (typeof json == 'string') {
            if (ErrorHandler.isValidJson(json)) {
                json = JSON.parse(json);
            } else {
                return false;
            }
        }

        var errors = json;

        $.each(errors, function(fieldName, errors) {
            if (typeof fieldName != 'string' || $.isNumeric(fieldName) === true) {
                return true;
            }
            
            var split = fieldName.split('.');
            if (split.length > 1) {
                fieldName = split[0];
                split.splice(0, 1);
                for (i in split) {
                    fieldName += '['+ split[i] +']';
                }
            }
            var fields = [
                'input[name="'+ fieldName +'"]',
                'textarea[name="'+ fieldName +'"]',
                'select[name="'+ fieldName +'"]',
            ];

            var $field = $(fields.join(',')).not('[type="hidden"]');
            var $placeholder = $('.errorPlaceholder.'+ fieldName);
            
            var error = errors;
            if (typeof error == 'object') {
                error = error.join('<br />');
            }

            var html = '<div class="formError">'+ error +'</div>';
            if ($field.length > 0) {
                if ($field.is('select')) {
                    $field = $field.closest('.select');
                }
                $field.after(html);
            } else if ($placeholder.length > 0) {
                $placeholder.html(html);
            }
        });
    },
    removeErrors: function() {
        $('.errorPlaceholder').html('');
        $('.formError').remove();
    },
    isValidJson: function(json) {
        try {
            JSON.parse(json);
        } catch (e) {
            swal({
                type: 'error',
                title: Language.error.error || 'Error',
                text: Language.error.unknownError || 'Unknown error',
            });
            return false;
        }
        return true;
    },
};

var InfoHandler = {
    __construct: function() {
    },
    swal: function(message, title, extraOptions) {
        Info.__construct();

        if (typeof message == 'object') {
            message = message.join('<br>');
        }

        var options = {
            type: 'info',
            title: title || Language.word.info || 'Info',
            text: message || '',
        };

        swal($.extend(options, extraOptions || {}));
    },
};

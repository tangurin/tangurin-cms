const webpack = require('webpack');
const dotenv = require('dotenv').config();
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = function(vars = {}) {
    var App = {};

    App.compileTarget = vars.compile_target || 'frontend';
    App.env = process.env.APP_ENV || 'local';
    if (vars.production) {
        App.env = 'production';
    }
    App.internalIp = require('quick-local-ip').getLocalIP4();
    App.isLocal = App.env == 'local';
    App.isFrontend = App.compileTarget == 'frontend';
    App.isHot = process.argv[1].indexOf('webpack-dev-server') !== -1;
    App.port = App.isFrontend ? 8080 : 8081;
    App.publicPathTarget = App.isHot ? 'http://'+ App.internalIp +':'+ App.port +'/' : '/';
    App.template = process.env.APP_TEMPLATE || 'default';
    App.assetsPath = App.publicPathTarget + (App.isFrontend ? App.template + '/build/' : 'backend/build/');
    App.cssExt = (App.isLocal ? '' : '.min') + '.css';
    App.cssFileName = {
        frontend: 'app',
        backend: 'admin',
        login: 'login',
    }

    /* Configuration */
    var configuration = {
        target: 'web',
        devtool: 'source-map',
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules|bower_componentes)/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                presets: ['env'],
                            },
                        },
                        //Make sure all modules allow HMR
                        {
                            loader: 'webpack-module-hot-accept',
                        }
                    ]
                },
                {
                    test: /\.vue$/,
                    loader: 'vue-loader',
                },
                {
                    test: /\.(png|jpe?g|gif)$/,
                    loader: 'file-loader?name=images/[name].[ext]&publicPath=' + App.assetsPath,
                },
                {
                    test: /\.svg$/,
                    loader: 'file-loader?outputPath=images/&name=[name].[ext]&publicPath=' + App.assetsPath,
                },
                {
                    test: /\.(eot|woff|woff2|ttf)(\?\S*)?$/,
                    loader: 'file-loader?outputPath=fonts/&name=[name].[ext]&publicPath=' + App.assetsPath,
                },
                {
                    test: /datatables.net\/js\/jquery\.dataTables\.js/,
                    loader: 'imports-loader?define=>false'
                },
                {
                    test: /\.handlebars$/,
                    loader: 'handlebars-loader'
                },
            ],
        },
        resolve: {
            alias: {
                vue: 'vue/dist/vue.js',
                handlebars: 'handlebars/dist/handlebars.min.js',
            }
        },
        plugins: [
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
            }),

            //Copy images from assets/images directory
            new CopyWebpackPlugin([{
                    from: './resources/frontend/' + App.template + '/assets/images',
                    to: 'images',
                },
            ]),
        ],
        devServer: {
            //The public folder is the root of the project
            contentBase: './public',
            //Use hot reloading. No browser refresh
            hot: true,
            host: App.internalIp,
            //Port
            port: App.port,
            //Server headers
            headers: {
                //Make sure server allow external calls
                'Access-Control-Allow-Origin': '*',
            },
        },
    };

    var cssUse = [{
        loader: 'css-loader',
        options: {
            importLoaders: 2,
            minimize: App.isLocal ? false : true,
        }
    }, {
        loader: 'postcss-loader',
        options: {
            plugins: function(loader) {
                return [
                    require('postcss-import')({ root: loader.resourcePath }),
                    require('postcss-cssnext')(),
                ];
            }
        }
    }, {
        loader: 'group-css-media-queries-loader',
    }, {
        loader: 'sass-loader',
    }];

    //If hot reloading is active
    if (App.isHot) {
        //Use style loader if hot
        cssUse.unshift({
            loader: 'style-loader'
        });
        //Run HotModuleReplacementPlugin
        configuration.plugins.push(new webpack.HotModuleReplacementPlugin());
        configuration.plugins.push(new webpack.NamedModulesPlugin());
    } else {
        //If no hot reloading, extract css to files
        configuration.plugins.push(new ExtractTextPlugin(App.cssFileName[App.compileTarget] + App.cssExt));
    }

    if (! App.isLocal) {
        configuration.plugins.push(new UglifyJSPlugin({
            uglifyOptions: {
                output: {
                    comments: false,
                }
            }
        }));
        configuration.devtool = 'none';
    } else {
        //Show OS notifications
        const WebpackNotifierPlugin = require('webpack-notifier');
        configuration.plugins.push(
            new WebpackNotifierPlugin({
                alwaysNotify: true,
                excludeWarnings: true,
            })
        );
    }

    configuration.module.rules.push({
        test: /\.(css|scss)$/,
        use: ! App.isHot ? ExtractTextPlugin.extract({fallback: 'style-loader', use: cssUse}) : cssUse,
    });
    /* //Configuration// */

    if (App.compileTarget == 'frontend') {
        return Object.assign({}, configuration, {
            entry: {
                'app': './resources/frontend/' + App.template + '/assets/app.bundle.js',
            },
            output: {
                path: __dirname +'/public/' + App.template + '/build/',
                publicPath: App.publicPathTarget + App.template + '/build/',
                filename: App.isLocal ? '[name].js' : '[name].min.js',
                sourceMapFilename: App.isLocal ? '[name].map' : '[name].min.map',
            },
        });
    } else if (App.compileTarget == 'backend') {
        //Backend
        return Object.assign({}, configuration, {
            entry: {
                'theme': './resources/backend/assets/theme.bundle.js',
            },
            output: {
                path: __dirname +'/public/backend/build',
                publicPath: App.publicPathTarget + 'backend/build/',
                filename: App.isLocal ? '[name].js' : '[name].min.js',
                sourceMapFilename: App.isLocal ? '[name].map' : '[name].min.map',
            },
        });
    } else if (App.compileTarget == 'login') {
        //Backend
        return Object.assign({}, configuration, {
            entry: {
                'login': './resources/backend/assets/login.bundle.js',
            },
            output: {
                path: __dirname +'/public/backend/build',
                publicPath: App.publicPathTarget + 'backend/build/',
                filename: App.isLocal ? '[name].js' : '[name].min.js',
                sourceMapFilename: App.isLocal ? '[name].map' : '[name].min.map',
            },
        });
    }
};

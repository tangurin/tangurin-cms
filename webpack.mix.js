const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


var theme = 'theTheme',
    url = 'http://tangurin/',
    resource = {
        backend: 'resources/backend/assets/',
        frontend: 'resources/frontend/'+ theme +'/assets/',
    },
    public = {
        backend: 'public/backend/assets/',
        frontend: 'public/'+ theme +'/assets/',
    };


mix.js(resource.backend +'js/app.js', public.backend +'js/')
    .sass(resource.backend +'scss/app.scss', public.backend +'css')
    .js(resource.frontend +'js/general.js', public.frontend +'js/')
    .js(resource.frontend +'js/form.js', public.frontend +'js/')
    .js(resource.frontend +'js/app.js', public.frontend +'js/')
    .sass(resource.frontend +'scss/app.scss', public.frontend +'css')
    .browserSync(url);

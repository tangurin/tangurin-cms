<?php
Route::group(['prefix' => 'admin'], function () {
    //Set location of views

    Route::get('login', 'Auth\LoginController@backendLoginForm')->name('backend.auth.login');
    Route::post('login', 'Auth\LoginController@login')->name('backend.auth.login');

    // Registration Routes...
    //Route::get('register', 'Auth\AuthController@showRegistrationForm')->name('backend.auth.register');
    //Route::post('register', 'Auth\AuthController@register')->name('backend.auth.register');

    // Password Reset Routes...
    Route::get('password/reset/{token?}', 'Auth\ForgotPasswordController@showResetForm')->name('backend.auth.password.reset');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('backend.auth.password.email');
    Route::post('password/reset', 'Auth\ForgotPasswordController@reset')->name('backend.auth.password.reset');

    //Logout
    Route::get('logout', 'Auth\LoginController@logout')->name('backend.auth.logout');

    //Login
    Route::group(['namespace' => 'Backend', 'middleware' => ['auth', 'backend']], function () {
        //Dashboard
        Route::get('/', 'DashboardController@index')->name('dashboard.index');

        //User
        Route::resource('user', 'UserController');
        Route::get('user/activate/{user}', 'UserController@activate')->name('user.activate');
        Route::get('user/deActivate/{user}', 'UserController@deActivate')->name('user.deactivate');

        //Role
        Route::resource('role', 'RoleController');

        //Page
        Route::group(['prefix' => 'page'], function () {
            Route::get('datatable', 'PageController@datatableView')->name('page.datatable');
            Route::get('sortable', 'PageController@sortableView')->name('page.sortable');
            Route::post('saveSort', 'PageController@saveSort')->name('page.savesort');
        });

        Route::resource('page', 'PageController');

        //Translation
        Route::get('translation', 'TranslationController@edit')->name('translation.edit');
        Route::patch('translation/update', 'TranslationController@update')->name('translation.update');

        //Mediamanager
        Route::get('mediamanager', 'MediamanagerController@index')->name('mediamanager.index');

        //Texts
        Route::resource('text', 'TextController');

        //Slideshow
        Route::resource('slideshow', 'SlideshowController');

        //Settings
        Route::get('setting', 'SettingController@edit')->name('setting.edit');
        Route::patch('setting/update', 'SettingController@update')->name('setting.update');

        //Locations
        Route::group(['prefix' => 'location', 'namespace' => 'Location'], function () {
            Route::resource('country', 'CountryController');
            Route::resource('county', 'CountyController');
            Route::resource('city', 'CityController');
        });

        //Linker
        Route::resource('linker', 'LinkerController');

        //AJAX
        Route::group(['prefix' => 'ajax'], function() {
            Route::post('counties', 'AjaxController@counties')->name('ajax.counties');
            Route::post('permissions', 'AjaxController@permissions')->name('ajax.permissions');
        });

        //Customfields
        Route::group(array('prefix' => 'customfields', 'namespace' => 'Customfields'), function() {
            Route::get('index', 'CustomfieldsController@index')->name('backend.customfields.index');
            Route::any('create', 'CustomfieldsController@form')->name('backend.customfields.create');
            Route::any('edit/{id}', 'CustomfieldsController@form')->name('backend.customfields.edit');
            Route::get('delete/{id}', 'CustomfieldsController@delete')->name('backend.customfields.delete');
            Route::any('modules', 'CustomfieldModulesController@index')->name('backend.customfields.modules');
            Route::any('module/edit/{id}', 'CustomfieldModulesController@edit')->name('backend.customfields.module.edit');
            Route::any('module/{id}', 'CustomfieldModulesController@module')->name('backend.customfields.module');
            Route::get('module/delete/{id}', 'CustomfieldModulesController@delete')->name('backend.customfields.module.delete');
            Route::get('addField', 'CustomfieldsController@addFieldAjax')->name('backend.customfields.addField');
            Route::post('sortCollections', 'CustomfieldsController@sortCollections')->name('backend.customfields.sortCollections');
        });
    });
});

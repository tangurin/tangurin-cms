<?php
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

Route::group(['prefix' => LaravelLocalization::setLocale()], function() {

   Route::get('sitemap.xml', function() {

        $sitemap = Sitemap::create();

        //Get all published pages
        $pages = resolve(App\Repositories\PageRepository::class)->getPages()->published();
        //Find the default language on the site
        $defaultLanguage = resolve(App\Repositories\LanguageRepository::class)->default();
        foreach ($pages as $page) {
            //Create URL object with the default site language
            $url = Url::create(url($page->translation['slug']));
            //Find out if there is other translations
            $otherTranslations = $page->translations->filter(function($translation) use ($defaultLanguage) {
                return $translation['language'] != $defaultLanguage;
            });
            foreach ($otherTranslations as $translation) {
                //Add page in different language. Alternate page
                $url->addAlternate(url($translation['slug']), $translation['language']);
            }
            //Add URL with alternate languages to the sitemap
            $sitemap->add($url);
        }
        return response($sitemap->render())->header('Content-Type', 'text/xml');

    })->name('frontend.sitemap');


/*    Route::get('sitemap.xml', function() {
        $sitemap = App::make('sitemap');
        $sitemap->setCache('laravel.sitemap', 60);
        if (!$sitemap->isCached()) {
            //Get all published pages
            $pages = resolve(App\Repositories\PageRepository::class)->getPages()->published();
            //Find the default language on the site
            $defaultLanguage = resolve(App\Repositories\LanguageRepository::class)->default();
            foreach ($pages as $page) {
                //Find out if there is other translations
                $otherTranslations = $page->translations->filter(function($translation) use ($defaultLanguage) {
                    return $translation['language'] != $defaultLanguage;
                })->map(function($translation) {
                    return [
                        'url' => url($translation['slug']),
                        'language' => $translation['language'],
                    ];
                });
                //Add URL with alternate languages to the sitemap
                $sitemap->add(url($page->translation['slug']), $page->updated_at, null, null, [], null, $otherTranslations->toArray());
            }
        }
        return $sitemap->render('xml');
    });*/


    Route::get('login', 'Auth\LoginController@frontendLoginForm')->name('frontend.auth.login');
    Route::post('login', 'Auth\LoginController@login')->name('frontend.auth.login');
    Route::get('logout', 'Auth\LoginController@logout')->name('frontend.auth.logout');
    
    Route::group(['prefix' => 'password'], function() {
        Route::get('reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('frontend.auth.password.showSendResetEmailForm');
        Route::get('reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('frontend.auth.password.showResetForm');
        Route::post('reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('frontend.auth.password.sendResetLink');
        Route::post('reset', 'Auth\ResetPasswordController@reset')->name('frontend.auth.password.reset');
    });
    
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('frontend.auth.register');
    Route::post('register', 'Auth\RegisterController@register')->name('frontend.auth.register');

    Route::group(['namespace' => 'Frontend'], function () {
        Route::group(['prefix' => 'account', 'middleware' => 'auth'], function () {
            Route::get('/', 'AccountController@index')->name('account.index');
            Route::post('storeLogin', 'AccountController@storeLogin')->name('account.storeLogin');
        });

        //All slugs and no slug is pointed to index
        Route::any('{slug?}', 'MainController@index')->name('main.index');
    });
});

<?php
//Filemanager
$prefix = config('filemanager.route_prefix', 'filemanager');
Route::group(['prefix' => $prefix, 'namespace' => 'Filemanager', 'as' => $prefix .'.'], function() {
    Route::resource('bootstrap', 'BootstrapController');
    
    Route::group(['prefix' => 'file', 'as' => 'file.'], function() {
        Route::get('index', 'FileController@index')->name('index');
        Route::post('index', 'FileController@store')->name('store');
        Route::put('index', 'FileController@update')->name('update');
        Route::delete('index', 'FileController@destroy')->name('destroy');
    });

    Route::group(['prefix' => 'directory', 'as' => 'directory.'], function() {
        Route::get('index', 'DirectoryController@index')->name('index');
        Route::post('index', 'DirectoryController@store')->name('store');
        Route::put('index', 'DirectoryController@update')->name('update');
        Route::delete('index', 'DirectoryController@destroy')->name('destroy');
    });

    Route::group(['prefix' => 'index'], function() {
        Route::get('validate/{file?}', 'IndexController@validateIndex')->name('filemanager.index.validate');
    });
});

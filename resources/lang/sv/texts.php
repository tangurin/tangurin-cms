<?php
return [
    'groups' => [
        'startpage' => 'Startsidan',
        'account' => 'Konto',
        'footer' => 'Sidfot',
        'other' => 'Övrigt',
    ],
    'identifiers' => [
        'activeAccount' => [
            'name' => 'Aktivt konto',
            'description' => 'Text vid aktivt konto',
        ],
        'inactiveAccount' => [
            'name' => 'Inaktivt konto',
            'description' => 'Text vid inaktiverat konto',
        ],
        'startBox1' => [
            'name' => 'Startsidan kolumn 1',
            'description' => 'Startsidan - Kolumn 1/3',
        ],
        'startBox2' => [
            'name' => 'Startsidan kolumn 2',
            'description' => 'Startsidan - Kolumn 2/3',
        ],
        'startBox3' => [
            'name' => 'Startsidan kolumn 3',
            'description' => 'Startsidan - Kolumn 3/3',
        ],
        'footerCol1' => [
            'name' => 'Sidfot kolumn 1',
            'description' => 'Sidfotens första kolumn',
        ],
        'footerCol2' => [
            'name' => 'Sidfot kolumn 2',
            'description' => 'Sidfotens andra kolumn',
        ],
        'footerCol4' => [
            'name' => 'Sidfot kolumn 4',
            'description' => 'Sidfotens fjärde kolumn',
        ],
    ],
];

import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
//import 'material-design-iconic-font/dist/css/material-design-iconic-font.min.css';
//import 'ionicons/dist/css/ionicons.min.css';
import './theme/css/core.css';
import './theme/css/components.css';
import './theme/css/pages.css';
import './theme/css/responsive.css';

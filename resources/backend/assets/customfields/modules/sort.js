$(function() {
    require(['lib/nestable/nestableWithRejection'], function(nestable){
        $('#fields_list').nestable({
            maxDepth : 10,
            reject: [{
              rule: function() { // The this object refers to dragRootEl i.e. the dragged element. The drag action is cancelled if this function returns true
                var $this = $(this);
                var $find = $this.find('.notRepeater ol').length > 0;
                return $find;
              },
              action: function(nestable) { // This optional function defines what to do when such a rule applies. The this object still refers to the dragged element, and nestable is, well, the nestable root element
              }
            }]
        }).on('change', function() {
            var order = $('#fields_list').nestable('serialize');
            $('#fieldSortInput').val(JSON.stringify(order));
        }).trigger('change');
    });
});

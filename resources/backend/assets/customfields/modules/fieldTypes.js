import ComponentHandler from '../../modules/ComponentHandler.js';
import Filemanager from '../.././filemanager/filemanager.js';

var FieldTypes = {
    initialize: function() {
        $('body').on('click', '.customfieldsFieldWrapper-Wysiwyg.inActive', function() {FieldWysiwyg.initialize($(this))});
        $('body').on('click', '.fieldType-Image', FieldImage.chooseImage);
        $('body').on('blur', '.fieldType-Image', FieldImage.imageBlur);
        $('body').on('click', '.fieldType-File', FieldFile.chooseFile);
        $('body').on('click', '.productPicker .openPopup', FieldProduct.openPopup);
        $('body').on('click', '.productPicker .removeRow', SearchModals.remove);
        $('body').on('click', '.categoryPicker .openPopup', FieldCategory.openPopup);
        $('body').on('click', '.categoryPicker .removeRow', SearchModals.remove);
        
        $('.fieldType-Datepicker').datepicker({
            dateFormat: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        FieldImage.initialize();
    },
};

var FieldWysiwyg = {
    i: 0,
    initialize: function($target) {
        var $target = $target;
        var $textarea = $('textarea', $target);
        var id = 'textarea-'+ FieldWysiwyg.i;
        var config = ComponentHandler.tinymce.getConfig();
        //Give the textarea an ID
        $textarea.prop('id', id);
        //Modify tinymce config to target this textarea
        config.selector = '#'+ id;
        //Initialize the textarea
        tinymce.init(config);
        //Remove the inActive class
        $target.removeClass('inActive')
        //Increase the iteration number for the textarea ID:s
        FieldWysiwyg.i++;
    },
};

var FieldFile = {
    chooseFile: function() {
        var $this = $(this);
        Filemanager.open().onSave((file) => {
            $this.val( file.fullPath );
        });
    }
}

var FieldImage = {
    initialize: function() {
        $('.fieldType-Image').each(function() {
            FieldImage.addImageHover($(this));
        });
    },
    chooseImage: function() {
        var $this = $(this);
        Filemanager.open().onSave((file) => {
            $this.val( file.fullPath );
            FieldImage.addImageHover($this, true);
        });
    },
    imageBlur: function() {
        FieldImage.addImageHover($(this));
    },
    addImageHover: function($field, isInsert) {
        var $customfieldsFieldWrapper = $field.closest('.customfieldsFieldWrapper');
        var $imagePreview = $customfieldsFieldWrapper.find('.imagePreview');
        
        var isInsert = isInsert || false;
        var imagePath = $.trim($field.val());
        
        if (imagePath == '' || $imagePreview.length == 0) {
            return false;
        }

        imagePath = imagePath;
        FieldImage.imageExists(imagePath, function(exists) {
            if (exists) {
                $imagePreview.css({
                    'background-image': 'url("'+ imagePath +'")'
                }).addClass('hasImage');

                if (isInsert) {
                    $customfieldsFieldWrapper.addClass('hover');
                    setTimeout(function() {
                        $customfieldsFieldWrapper.removeClass('hover');
                    }, 2000);
                }
            } else {
                FieldImage.removeImageHover($imagePreview);
            }
        });
    },
    removeImageHover: function($imagePreview) {
        if ($imagePreview.length > 0) {
            $imagePreview.css({
                'background-image': ''
            }).removeClass('hasImage');
        }
    },
    imageExists: function(url, callback) {
        var img = new Image();
        img.onload = function() { callback(true); };
        img.onerror = function() { callback(false); };
        img.src = url;
    }
};

var FieldProduct = {
    selector: '.productPicker',
    button: null,
    searchModal: null,
    openPopup: function() {
        FieldProduct.button = $(this);
        FieldProduct.searchModal = ProductSearchModal;
        SearchModals.openPopup(FieldProduct);
    },
    getTemplate: function(item, fieldName) {
        var template = Handlebars.compile( $('#productPickerRow').html() )
        return template({
            fieldName: fieldName,
            productId: item.id,
            productCode: item.code,
            productName: item.name,
        });
    }
};

var FieldCategory = {
    selector: '.categoryPicker',
    button: null,
    searchModal: null,
    openPopup: function() {
        FieldCategory.button = $(this);
        FieldCategory.searchModal = CategorySearchModal;
        SearchModals.openPopup(FieldCategory);
    },
    getTemplate: function(item, fieldName) {
        var template = Handlebars.compile( $('#categoryPickerRow').html() )
        return template({
            fieldName: fieldName,
            categoryId: item.id,
            categoryName: item.name,
            categoryParent: item.parent != null ? item.parent.name : '--',
        });
    }
};

var SearchModals = {
    openPopup: function(obj) {
        var wrapperSelector = obj.selector;
        var $wrapper = obj.button.closest(wrapperSelector);
        var $table = $('table', $wrapper);
        var $target = $('tbody.selectedItems', $table);

        var isMultiSelect = $wrapper.hasClass('multiselect');
        var onlyUnique = $wrapper.data('unique') == '1';
        var fieldName = $wrapper.data('fieldname');
        obj.searchModal.run(function(item) {
            if (onlyUnique && $(wrapperSelector +' .itemId-'+ item.id).length > 0) {
                return true;
            }

            html = obj.getTemplate(item, fieldName);

            $table.fadeIn(300);
            if (isMultiSelect) {
                $target.append(html);
                return true;
            } else {
                $target.html(html);
                return false;
            }
        });
    },
    remove: function() {
        $(this).closest('tr').remove();
    }
}

module.exports = {
    FieldTypes: FieldTypes,
    FieldWysiwyg: FieldWysiwyg,
    FieldFile: FieldFile,
    FieldImage: FieldImage,
    FieldProduct: FieldProduct,
    FieldCategory: FieldCategory,
    SearchModals: SearchModals,
}

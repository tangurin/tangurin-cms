$(function() {
    $('tbody.sortable').nestable({
        items: '> tr',
        placeholder: 'ui-state-highlight',
        opacity: 0.6,
        cursor: 'move',
        update: function() {
            var $sortable = $('tbody.sortable');
            var order = '';
            $('> tr', $sortable).each(function() {
                order += $(this).data('id') +',';
            });

            $.post(sortUrl, {order: order.substring(0, order.length - 1)});
        }
    });
});

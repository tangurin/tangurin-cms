var Cleaner = require('./cleaner');

(function () {
    $('#fieldsSetup').nestable({
        maxDepth : 1,
    }).on('change', function() {
        var order = $('#fieldsSetup').nestable('serialize');
        //$('#fieldSortInput').val(JSON.stringify(order));
    }).trigger('change');
    
    $('form').on('click', '.toggleButton', function(e) {
        var $this = $(this);
        var $wrapper = $this.closest($this.data('wrapper'));
        var $drop = $('.toggleDrop', $wrapper).first();
        $drop.slideToggle(400);
    });

    //Change noRelation checkbox
    $('#noRelation').on('change', function() {
        var $this = $(this);
        var $hide;
        var $show;
        if ($this.is(':checked')) {
            $hide = $('.relationsContent');
            $show = $('.noRelationsContent');
        } else {
            $hide = $('.noRelationsContent');
            $show = $('.relationsContent');
        }

        $hide.fadeOut(400, function() {
            $show.fadeIn(400);
        });
    });

    //Clean collection identifier from not a-zA-Z0-9
    $('input.collectionIdentifier').on('keypress', Cleaner.cleanKeyPress)
        .on('blur', Cleaner.cleanField);
})();

module.exports = {};

var Handlebars = require('handlebars');

var RelationHandler = {
    classTemplate: null,
    rowTemplate: {
        relation: null,
        relationId: null,
        remove: null,
    },
    JSON: null,
    relationRowsEl: null,
    initialize: function() {
        this.JSON = templateJSON;
        this.rowTemplate.class = Handlebars.compile( $('#relationClassSelectTemplate').html() );
        this.rowTemplate.relation = Handlebars.compile( $('#relationRelationSelectTemplate').html() );
        this.rowTemplate.relationId = Handlebars.compile( $('#relationRelationIdSelectTemplate').html() );
        this.rowTemplate.remove = Handlebars.compile( $('#relationRemoveButtonTemplate').html() );

        this.relationRowsEl = $('.relationRows');

        this.insertExistingRelations();

        $('a.addRelation').on('click', this.addRelationButtonTriggered);
        this.relationRowsEl.on('change', 'select.classSelect', this.classChanged);
        this.relationRowsEl.on('change', 'select.relationSelect', this.relationChanged);
        this.relationRowsEl.on('click', 'a.duplicateRow', this.cloneButtonTriggered);
        this.relationRowsEl.on('click', 'a.removeRow', this.removeButtonTriggered);
    },
    insertExistingRelations: function() {
        var relations = this.JSON.relations;
        if (relations) {
            $.each(relations, function(key, relation) {
                if (typeof relation.id == 'undefined') {
                    relation.id = 0;
                }
                RelationHandler.addRow(relation.class, relation.relation, relation.relationId, relation.id);
            });
        }
    },
    prepareRow: function(selectedClass, selectedRelation, selectedRelationId, relationId) {
        var JSON = RelationHandler.JSON;
        var rowTemplate = this.rowTemplate;

        //Fetch all classJson
        var classJSON = JSON.classJSON;
        selectedClass = selectedClass || classJSON[0].value;

        //Fetch all relations by the selected class
        var relationJSON = JSON.relationJSON[selectedClass];
        selectedRelation = selectedRelation || relationJSON[0].value;

        //Find which relations is selected
        var searchRelation = RelationHandler.filterRelationByOperator(relationJSON, selectedRelation)
        if (searchRelation === undefined) {
            searchRelation = relationJSON[0];
        }

        //Fetch all relation id's where relation target equals
        var relationIdJSON = JSON.relationIdJSON[selectedClass][searchRelation.target];

        //Fetch all template HTML
        var classHtml = rowTemplate.class({options: classJSON});
        var relationHtml = rowTemplate.relation({options: relationJSON});

        var relationIdHtml = rowTemplate.relationId({options: relationIdJSON});
        var deleteButtonHtml = rowTemplate.remove();

        var html = '<tr>';
            html += '<td>';
                html += classHtml
            html += '</td>';
            html += '<td>';
                html += relationHtml
            html += '</td>';
            html += '<td>';
                html += relationIdHtml.replace(/PADDING/g, '&rarr;')
            html += '</td>';
            html += '<td class="text-center">';
                html += '<input type="hidden" name="relation[id][]" class="rowId" value="'+ relationId +'" />'
                html += deleteButtonHtml
            html += '</td>';
        html += '</tr>';
        
        var $row = $(html);

        return $row
    },
    addRow: function(selectedClass, selectedRelation, selectedRelationId, relationId) {
        relationId = typeof relationId != 'undefined' ? relationId : 0;
        var $newRow = this.prepareRow(selectedClass, selectedRelation, selectedRelationId, relationId);
        $newRow = this.setValues($newRow, selectedClass, selectedRelation, selectedRelationId, relationId);

        this.relationRowsEl.append($newRow);
    },
    replaceRow: function($row, selectedClass, selectedRelation, selectedRelationId) {
        var $rowId = $('.rowId', $row).val();
        var $newRow = this.prepareRow(selectedClass, selectedRelation, selectedRelationId, $rowId);
        $newRow = this.setValues($newRow, selectedClass, selectedRelation, selectedRelationId);

        $row.replaceWith($newRow)
    },
    setValues: function($row, selectedClass, selectedRelation, selectedRelationId) {
        if (typeof selectedClass != 'undefined' && $('select.classSelect option[value="'+ selectedClass +'"]', $row).length > 0) {
            $('select.classSelect', $row).val(selectedClass);
        }

        if (typeof selectedRelation != 'undefined' && $('select.relationSelect option[value="'+ selectedRelation +'"]', $row).length > 0) {
            $('select.relationSelect', $row).val(selectedRelation);
        }

        if (typeof selectedRelationId != 'undefined' && $('select.relationIdSelect option[value="'+ selectedRelationId +'"]', $row).length > 0) {
            $('select.relationIdSelect', $row).val(selectedRelationId);
        }

        return $row;
    },
    classChanged: function() {
        var $self = RelationHandler;
        var $this = $(this);
        var $row = $this.closest('tr');

        $self.replaceRow($row, $this.val());
    },
    relationChanged: function() {
        var $self = RelationHandler;
        var $this = $(this);
        var $row = $this.closest('tr');
        var $selectedRelationClass = $('.classSelect', $row).val();

        $self.replaceRow($row, $selectedRelationClass, $this.val());
    },
    addRelationButtonTriggered: function() {
        RelationHandler.addRow();
    },
    removeButtonTriggered: function() {
        var $row = $(this).closest('tr');
        RelationHandler.removeRow($row);
    },
    cloneButtonTriggered: function() {
        var $row = $(this).closest('tr');
        var classSelect = $('select.classSelect', $row).val();
        var relationSelect = $('select.relationSelect', $row).val();
        var relationIdSelect = $('select.relationIdSelect', $row).val();

        RelationHandler.addRow(classSelect, relationSelect, relationIdSelect);
    },
    removeRow: function($row) {
        if ($row.length > 0) {
            var relationId = parseInt($row.find('.rowId').val());
            if (relationId > 0) {
                RelationHandler.relationRowsEl.append('<input type="hidden" name="removeRelation[]" value="'+ relationId +'">');
            }
            $row.remove();
        }
    },
    filterRelationByOperator: function(JSON, needle) {
        return JSON.filter(function(relation) {
            return relation.value == needle
        })[0];
    }
};

$(function() {
    if ($('.customfieldsForm').length > 0) {
        RelationHandler.initialize();
    }
});

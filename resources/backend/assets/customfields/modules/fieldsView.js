var swal = require('sweetalert2');

var fieldTypesModule = require ('./fieldTypes.js');

var FieldTypes = fieldTypesModule.FieldTypes;
var FieldWysiwyg = fieldTypesModule.FieldWysiwyg;
var FieldFile = fieldTypesModule.FieldFile;
var FieldImage = fieldTypesModule.FieldImage;
var FieldColorpicker = fieldTypesModule.FieldColorpicker;
var FieldProduct = fieldTypesModule.FieldProduct;
var FieldCategory = fieldTypesModule.FieldCategory;
var SearchModals = fieldTypesModule.SearchModals;

var $wrapper;

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

$.fn.serializeObject = function() {
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};

var Customfields = {
    initialize: function() {
        if (typeof jsonCollections == 'undefined') {
            return false;
        }

        $wrapper.on('click', '.removeRepeaterGroup', Repeater.removeRepeaterGroupTriggered);
        $wrapper.on('click', '.newRepeaterGroup', Repeater.newRepeaterGroupTriggered);
        Customfields.buildCollections();
        if (typeof FieldTypes != 'undefined') {
            FieldTypes.initialize();
        }
    },
    buildCollections: function() {
        var numberOfCollections = Object.keys(jsonCollections).length;
        $.each(jsonCollections, function(key, collection) {
            var collectionId = collection.id;

            var fields = Field.filterByCollectionId(jsonFields, collectionId);
            fields = Field.filterByParentId(fields, 0);
            var html = '<div class="collectionWrapper collectionWrapper-'+ collectionId +' card-box" data-id="'+ collectionId +'">';
                if (typeof showIdentifiers != 'undefined') {
                    html += '<h2 class="collectionTitle">'+ collection.name +' <small>('+ collection.identifier +')</small></h2>';
                } else {
                    html += '<h2 class="collectionTitle">'+ collection.name +'</h2>';
                }

                var collectionState = 'style="display: block;"';
                if (numberOfCollections > 1) {
                    html += '<a class="btn btn-primary toggleCollection"><span class="showFields">Visa fält</span><span class="hideFields">Dölj fält</span></a>';
                    collectionState = 'style="display: none;"';
                }
                html += '<div class="collectionDescription">'+ collection.description +'</div>';
                html += '<div class="collectionFields collectionId-'+ collectionId +'" '+ collectionState +'>';
                    html += Customfields.buildFields(fields, 0);
                    html += '<div style="clear: both"></div>';
                html += '</div>';
            html += '</div>';

            $wrapper.append(html);
        });
        Repeater.updateIterationNumbers();
    },
    buildFields: function(fields, parentValueId) {
        //If fields with no parent does not exists.
        if ($.isEmptyObject(fields)) {
            return '';
        }

        var html = '';
        $.each(fields, function(fieldId, field) {
            if (field.isRepeater) {
                html += Repeater.html(field, parentValueId);
                return;
            }

            html += Field.html(field, parentValueId);
        });

        return html;
    },
    compareMaxInputVars: function(callback) {
        if (typeof callback == 'function') {
            var $form = $('.container form:not(.app-search)').first();
            var $inputs = $('input, textarea, select');
            var inputsLength = $inputs.length;
            callback(maxInputVars < inputsLength, inputsLength, $form);
        }
    },
    runCompareMaxInputVars: function(withoutMessage) {
        var withoutMessage = withoutMessage || false;
        Customfields.compareMaxInputVars(function(isTooMany, inputsLength, $form) {
            var $submitButton = $('button[type="submit"]', $form)
            if (isTooMany) {
                if (withoutMessage === false) {
                    swal({
                        type: 'warning',
                        title: 'För många inputs',
                        text: 'Maximalt antal inputs som tillåts på servern är: '+ maxInputVars +', formuläret innehåller ca: '+ inputsLength,
                    });
                }
                $submitButton.hide();
            } else {
                $submitButton.show();
            }
        });
    }
};

var Field = {
    fieldName: 'customfieldValues[values]',
    index: 1,
    iterator: 1,
    html: function(field, parentValueId) {
        var settings = field.settings;
        var multilanguage = settings.multilanguage == 1 ? true : false;
        var languages = multilanguage ? availableLanguages : {sv: availableLanguages.sv};
        var showFlag = Object.keys(languages).length > 1;
        var languageBoxClass = multilanguage ? ' isMultilanguage' : '';
        
        var lgCols = settings.lgCols || 12;
        var mdCols = settings.mdCols || 12;
        var smCols = settings.smCols || 12;

        var html = '<div class="form-group field-'+ field.id + ' col-lg-'+ lgCols +' col-md-'+ mdCols +' col-sm-'+ smCols +'">';
            var values = FieldValue.filterByFieldId(jsonValues, field.id);
            var value = FieldValue.filterByParentValueId(values, parentValueId)[0];

            var fieldName = Field.fieldName;
            var fieldValueId = value === undefined ? 'new_' + Field.getIndex() : value.id;

            var i = 1;
            $.each(languages, function(languageCode, language) {
                var fieldAttributeId = field.identifier +'-'+ Field.getIterator();
                var translation = FieldTranslation.find(fieldValueId, languageCode, field.settings.defaultValue);

                var fieldHtml = field.html;
                fieldHtml = fieldHtml.replaceAll('{{attributeId}}', fieldAttributeId);
                fieldHtml = fieldHtml.replaceAll('{{fieldName}}', fieldName);
                fieldHtml = fieldHtml.replaceAll('{{fieldValueId}}', fieldValueId);
                fieldHtml = fieldHtml.replaceAll('{{language}}', languageCode);
                fieldHtml = fieldHtml.replace('{{value}}', translation);

                var hiddenFields = field.hiddenFields;
                hiddenFields = hiddenFields.replaceAll('{{fieldType}}', field.type);
                hiddenFields = hiddenFields.replaceAll('{{fieldName}}', fieldName);
                hiddenFields = hiddenFields.replaceAll('{{fieldValueId}}', fieldValueId);
                hiddenFields = hiddenFields.replaceAll('{{parentValueId}}', parentValueId);

                var $html = $(fieldHtml);
                var tagName = $html.prop('tagName');
                if ($.trim(translation) != '') {
                    if (tagName == 'SELECT') {
                        fieldHtml = Field.prepareSelect($html, translation);
                    }

                    if ($html.is('.checkboxes')) {
                        fieldHtml = Field.prepareCheckbox($html, translation);
                    }

                    if ($html.is('.radios')) {
                        fieldHtml = Field.prepareRadio($html, translation);
                    }

                    if (field.type == 'Product') {
                        fieldHtml = Field.prepareProduct($html, translation);
                    }

                    if (field.type == 'Category') {
                        fieldHtml = Field.prepareCategory($html, translation);
                    }
                }

                var hide = i > 1 ? 'style="display: none"' : '';
                html += '<div class="languageBox languageCode-'+ languageCode + languageBoxClass +'"'+ hide +'>';
                    html += '<label for="'+ fieldAttributeId +'">';
                        if (typeof showIdentifiers != 'undefined') {
                            html += field.name +' <small> ('+ field.identifier +')</small>';
                        } else {
                            html += field.name;
                        }
                    html += '</label>';
                    html += '<div class="customfieldsFieldWrapper customfieldsFieldWrapper-'+ field.type + (field.type == 'Wysiwyg' ? ' inActive' : '') +'">';
                        html += hiddenFields;
                        html += fieldHtml;
                        html += showFlag ? '<img class="fieldFlag" src="'+ FLAGS_DIR + language.CountryCode.toLowerCase() +'.png" />' : '';
                    html += '</div>';
                html += '</div>';
                i++;
            });
        html += '</div>';

        return html;
    },
    prepareSelect: function($html, translation) {
        if (Field.isJson(translation)) {
            var values = JSON.parse(translation);
            var valuesLength = values.length;
            for (var iterator = 0; iterator < valuesLength; iterator++) {
                var $option = $('option[value="'+ values[iterator] +'"]', $html);
                if ($option.length > 0) {
                    $option.attr('selected', true).attr('data-selected', 'true');
                }
            }
        } else {
            var $option = $('option[value="'+ translation +'"]', $html);
            if ($option.length) {
                $option.attr('selected', true).attr('data-selected', 'true');
            }
        }
        return $html[0].outerHTML;
    },
    prepareCheckbox: function($html, translation) {
        if (Field.isJson(translation)) {
            var values = JSON.parse(translation);
            var valuesLength = values.length;
            for (var iterator = 0; iterator < valuesLength; iterator++) {
                var $checkbox = $('[value="'+ values[iterator] +'"]', $html);
                if ($checkbox.length > 0) {
                    $checkbox.attr('checked', true);
                }
            }
        } else {
            var $checkbox = $('[value="'+ translation +'"]', $html);
            if ($checkbox.length) {
                $checkbox.attr('checked', true);
            }
        }
        return $html[0].outerHTML;
    },
    prepareRadio: function($html, translation) {
        var $radio = $('[value="'+ translation +'"]', $html);
        if ($radio.length) {
            $radio.attr('checked', true);
        }
        return $html[0].outerHTML;
    },
    prepareProduct: function($html, translation) {
        var template = Handlebars.compile( $('#productPickerRow').html() )
        
        var values = JSON.parse(translation);
        var fieldName = $html.data('fieldname');
        _.each(values, function(productId) {
            var item = jsonValueProducts[productId]
            if (typeof item == 'object') {
                var itemRow = template({
                    fieldName: fieldName,
                    productId: item.id,
                    productCode: item.code,
                    productName: item.name,
                });
                $('.selectedItems', $html).append(itemRow);
            }
        });
        $('table', $html).show();
        return $html[0].outerHTML;
    },
    prepareCategory: function($html, translation) {
        var template = Handlebars.compile( $('#categoryPickerRow').html() )
        
        var values = JSON.parse(translation);
        var fieldName = $html.data('fieldname');
        _.each(values, function(categoryId) {
            var item = jsonValueCategories[categoryId]
            if (typeof item == 'object') {
                var itemRow = template({
                    fieldName: fieldName,
                    categoryId: item.id,
                    categoryName: item.name,
                    categoryParent: item.parentName == null ? '--' : item.parentName,
                });
                $('.selectedItems', $html).append(itemRow);
            }
        });
        $('table', $html).show();
        return $html[0].outerHTML;
    },
    find: function(fieldId) {
        return jsonFields.filter(function(field) {
            return field.id == fieldId;
        })[0];
    },
    filterByCollectionId: function(fields, collectionId) {
        return fields.filter(function(field) {
            return field.collectionId == collectionId;
        });
    },
    filterByParentId: function(fields, parentId) {
        return fields.filter(function(field) {
            return field.parentId == parentId;
        });
    },
    getIndex: function() {
        return Field.index++;
    },
    getIterator: function() {
        return Field.iterator++;
    },
    isJson: function(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
};

var Repeater = {
    fieldName: 'customfieldValues[repeaters]',
    index: 1,
    html: function(field, parentValueId) {
        var fieldId = field.id;
        var collectionId = field.collectionId;
        var min = 0;

        var attributes = 'data-id="'+ field.id +'"';
        $.map(field.settings, function(value, key) {
            if (key == 'id') return;
            attributes += ' data-'+ key +'="'+ value +'"';
        });

        var repetitions = FieldValue.filterByFieldId(jsonValues, field.id);
        var repetitions = FieldValue.filterByParentValueId(repetitions, parentValueId);
        var repeaterMax = field.settings.repeaterMax;
        var repeaterMin = field.settings.repeaterMin;

        var addBtn = true;
        if (repeaterMin > 0 && repeaterMin == repeaterMax) {
            addBtn = false;
        }

        var missingRepetitions = repeaterMin - repetitions.length
        if (missingRepetitions > 0) {
            for (var i = 0; i < missingRepetitions; i++) {
                repetitions.push({
                    id: 'new_' + Repeater.getIndex()
                });
            }
        }

        var html = '<div class="clearfix"></div>';
        html += '<div class="repeater field-'+ field.id + (repetitions.length > 0 ? ' hasRepetitions' : '') +'" '+ attributes +'>';
            html += '<div class="form-group card-box repeaterWrapper">';
                if (typeof showIdentifiers != 'undefined') {
                    html += '<h4 class="repeaterTitle">'+ field.name +' ('+ field.identifier +')</h4>';
                } else {
                    html += '<h4 class="repeaterTitle">'+ field.name +'</h4>';
                }
                if (addBtn) {
                    html += '<a class="btn btn-primary btn-sm newRepeaterGroup" title="Ny ('+ field.name +') repetition"><i class="fa fa-plus"></i></a>';
                }
                html += '<div class="repeaterGroups sortable">';
                    $.each(repetitions, function(index, repetition) {
                        html += Repeater.buildRepetition(collectionId, field.id, repetition.id, parentValueId);
                    });
                html += '</div>';
                if (addBtn) {
                    html += '<a class="btn btn-primary btn-sm newRepeaterGroup bottom" title="Ny ('+ field.name +') repetition"><i class="fa fa-plus"></i></a>';
                }
            html += '</div>';
        html += '</div>';

        return html;
    },
    buildRepetition: function(collectionId, repeaterFieldId, repetitionValueId, parentValueId) {
        var repeaterField = Field.find(repeaterFieldId);

        var fields = Field.filterByCollectionId(jsonFields, collectionId);
        fields = Field.filterByParentId(fields, repeaterField.id);
        var hiddenFields = repeaterField.hiddenFields;
        hiddenFields = hiddenFields.replaceAll('{{fieldName}}', Repeater.fieldName);
        hiddenFields = hiddenFields.replaceAll('{{fieldValueId}}', repetitionValueId);
        hiddenFields = hiddenFields.replaceAll('{{parentValueId}}', parentValueId);

        var settings = repeaterField.settings;
        var repeaterMin = settings.repeaterMin;
        var repeaterMax = settings.repeaterMax;
        var lgCols = settings.lgCols || 12;
        var mdCols = settings.mdCols || 12;
        var smCols = settings.smCols || 12;

        var deleteBtn = true;
        if (repeaterMin > 0 && repeaterMin == repeaterMax) {
            deleteBtn = false;
        }

        var html = '';
        html += '<div class="repeaterGroup repetition-'+ repetitionValueId +' col-lg-'+ lgCols +' col-md-'+ mdCols +' col-sm-'+ smCols +'" data-valueid="'+ repetitionValueId +'">';
            html += '<div class="handle" title="Dra för att flytta denna repetition"></div>'
            html += '<span class="iterationNumber"></span>';
            html += hiddenFields;
            if (deleteBtn) {
                html += '<a class="removeRepeaterGroup btn btn-danger btn-xs" data-repetition="'+ repetitionValueId +'" title="Ta bort repetition">Ta bort</a>';
            }
            html += '<div class="fields">';
                html += Customfields.buildFields(fields, repetitionValueId);
                html += '<div class="clearBoth"></div>';
            html += '</div>';
        html += '</div>';

        return html;
    },
    remove: function(repetitionId) {
        var $repetition = $('.repeaterGroup.repetition-'+ repetitionId);

        if ($repetition.length > 0) {
            var $repeater = $repetition.closest('.repeater');

            var repeaterMin = parseInt($repeater.data('repeatermin'));
            var repeaterGroupsCount = $('.repeaterGroups > .repeaterGroup', $repeater).length;
            if (repeaterMin > 0 && repeaterGroupsCount <= repeaterMin) {
                alert('Minst: '+ repeaterMin +' repetitioner måste finnas.');
                return false;
            }
            
            $repetition.remove();
            Repeater.updateIterationNumbers();

            //Remove class hasRepetitions if the last repetition is removed
            if (repeaterGroupsCount == 1) {
                $repeater.removeClass('hasRepetitions');
            }
        }
    },
    newRepeaterGroupTriggered: function(e) {
        var $this = $(this);
        var $collection = $this.closest('.collectionWrapper');
        var $repeater = $this.closest('.repeater');

        var repeaterGroupsCount = $('.repeaterGroups > .repeaterGroup', $repeater).length;
        var repeaterMax = parseInt($repeater.data('repeatermax'));
        if (repeaterMax > 0 && repeaterGroupsCount >= repeaterMax) {
            alert('Max tillåtna repetitioner är '+ repeaterMax +'st.');
            return false;
        }

        var collectionId = $collection.data('id');
        var repeaterId = $repeater.data('id');
        var parentValueId = $this.closest('.repeaterGroup').data('valueid') || 0;

        var $builtRepetition = $(Repeater.buildRepetition(collectionId, repeaterId, 'new_' + Repeater.getIndex(), parentValueId));
        $('> .repeaterWrapper > .repeaterGroups', $repeater).append( $builtRepetition );
        $repeater.addClass('hasRepetitions');
        Repeater.updateIterationNumbers();

        FieldColorpicker.initialize($repeater);
        FieldWysiwyg.initialize( $('.customfieldsFieldWrapper-Wysiwyg', $builtRepetition) );

        Customfields.runCompareMaxInputVars();
    },
    removeRepeaterGroupTriggered: function(e) {
        Repeater.remove($(this).data('repetition'));
        Customfields.runCompareMaxInputVars(true);
    },
    getIndex: function() {
        return Repeater.index++;
    },
    updateIterationNumbers: function() {
        $('.repeaterGroups').each(function() {
            var i = 1;
            $('> .repeaterGroup > .iterationNumber', $(this)).each(function() {
                $(this).html(i);
                i++;
            });
        });
    }
};

var FieldValue = {
    filterByFieldId: function(values, fieldId) {
        return values.filter(function(value) {
            return value.fieldId == fieldId;
        });
    },
    filterByParentValueId: function(values, parentValueId) {
        return values.filter(function(value) {
            return value.parentValueId == parentValueId;
        });
    },
}

var FieldTranslation = {
    find: function(valueId, languageCode, defaultValue) {
        var translation = jsonTranslations.filter(function(translation) {
            return translation.valueId == valueId && translation.language == languageCode;
        });

        var defaultValue = typeof defaultValue != 'undefined' ? defaultValue : '';
        return typeof translation[0] != 'undefined' ? translation[0].content : defaultValue;
    },
}

var FieldLanguage = {
    switch: function(language) {
        var $languageFields = $('.languageCode-'+ language);
        if ($languageFields.length > 0) {
            $('.languageBox.isMultilanguage').hide();
            $languageFields.show();

            $fields = $('input, textarea, select', $languageFields);
            $fields.css('border-color', '#ffbd4a');
            setTimeout(function() {
                $fields.css('border-color', '');
            }, 300);
        }
    }
};

(function() {
    if (typeof jsonCollections == 'undefined') {
        return false;
    }
    $wrapper = $('.customfieldsWrapper');
    Customfields.initialize();

    $('.viewButtons .btn').on('click', function() {
        var $this = $(this);
        if ($this.hasClass('selected')) {
            return false;
        }

        var viewName = $this.data('view');
        $.cookie('customfieldsView', viewName, { expires: 365 });
        $('.viewButtons .btn').removeClass('selected').addClass('btn-custom');
        $this.addClass('selected').removeClass('btn-custom');

        $('.customfieldsWrapper').attr('data-view', viewName);
    });

    $('.languageSwitcher a').on('click', function(e) {
        e.preventDefault();
        var language = $(this).data('language');
        FieldLanguage.switch(language);
    });

    /*$('.sortable').sortable({
        items: '> .repeaterGroup',
        placeholder: 'ui-state-highlight',
        opacity: 0.8,
        cursor: 'move',
        handle: '.handle',
        stop: function(event, ui) {
            Repeater.updateIterationNumbers();
        }
    });*/

    $('.toggleCollection').on('click', function() {
        var $this = $(this);
        var $collectionWrapper = $this.closest('.collectionWrapper');
        $('.collectionFields', $collectionWrapper).slideToggle(500);
        $this.toggleClass('open');
    });

    Customfields.runCompareMaxInputVars();
})();


var Handlebars = require('handlebars');
var Cleaner = require('./cleaner');

var FieldHandler = {
    classTemplate: null,
    rowTemplate: null,
    dataRowTemplate: null,
    repeaterTemplate: null,
    JSON: null,
    topFieldHolder: null,
    fieldRowsEl: null,
    fieldCounter: 0,
    toggleDropClosed: false,
    initialize: function() {
        FieldHandler.JSON = templateJSON;
        FieldHandler.rowTemplate = Handlebars.compile( $('#rowTemplate').html() );
        FieldHandler.dataRowTemplate = Handlebars.compile( $('#dataRowTemplate').html() );
        FieldHandler.settingsRowTemplate = Handlebars.compile( $('#settingsRowTemplate').html() );
        FieldHandler.repeaterTemplate = Handlebars.compile( $('#repeaterTemplate').html() );
        FieldHandler.topFieldHolder = $('#fieldSetup > .fieldHolder');
        FieldHandler.fieldRowsEl = $('#fieldHandler');
        var $wrapper = FieldHandler.fieldRowsEl;

        FieldHandler.insertExistingFields();

        $wrapper.on('keypress', 'input.identifierInput', Cleaner.cleanKeyPress);
        $wrapper.on('blur', 'input.identifierInput', Cleaner.cleanField);
        $wrapper.on('blur', 'input.identifierInput', FieldHandler.identifierBlur);
        $wrapper.on('blur', 'input.nameInput', FieldHandler.nameBlur);
        $wrapper.on('change', 'select.fieldTypeSelect', FieldHandler.typeChanged);

        $wrapper.on('click', 'a.addField', FieldHandler.addTriggered);
        $wrapper.on('click', 'a.cloneFieldButton', FieldHandler.cloneTriggered);
        $wrapper.on('click', 'a.removeFieldButton', FieldHandler.removeTriggered);
        
        $wrapper.on('click', 'a.addDataButton', DataHandler.addTriggered);
        $wrapper.on('click', 'a.cloneDataButton', DataHandler.cloneTriggered);
        $wrapper.on('click', 'a.removeDataButton', DataHandler.removeTriggered);

        $wrapper.on('click', 'a.addSettingButton', SettingsHandler.addTriggered);
        $wrapper.on('click', '.settingsValueField', SettingsHandler.editTriggered);
        $wrapper.on('click', 'a.removeSettingButton', SettingsHandler.removeTriggered);

        $('a.toggleAll', $wrapper).on('click', FieldHandler.toggleAll);
    },
    insertExistingFields: function() {
        var self = FieldHandler;
        var fields = FieldHandler.JSON.fields;

        if (fields) {
            $.each(fields, function(sortOrder, field) {
                var fieldId = field.id;
                var $fieldHolder = FieldHandler.topFieldHolder;
                field.fieldId = fieldId;

                if (field.parentId > 0) {
                    self.addRepeater(field.parentId);
                    $fieldHolder = $('.fieldHolder-'+ field.parentId);
                }
                FieldHandler.addExistingField($fieldHolder, field);
                SettingsHandler.fillSettingsSelect(fieldId);
                SettingsHandler.fillSettings(fieldId);
                DataHandler.fillData(fieldId);
            });
        }
    },
    addEmptyField: function($fieldHolder) {
        var self = FieldHandler;
        var fieldParentId = $fieldHolder.data('fieldid') || 0;
        //Loader.show();
        $.ajax({
            method: 'GET',
            url: '/admin/customfields/addField',
            data: { parentId: fieldParentId },
            success: function(response) {
                //Loader.hide();
                var fieldId = response;
                var noDataWrapper = DataHandler.hasDataWrapper('Text') === false;
                var $row = $(self.rowTemplate({fieldId: fieldId, fieldParentId: fieldParentId, noDataWrapper: noDataWrapper}));
                $fieldHolder.append($row);
                SettingsHandler.fillSettingsSelect(fieldId);
            }
        });
    },
    addExistingField: function($fieldHolder, field) {
        var self = FieldHandler;
        var noDataWrapper = DataHandler.hasDataWrapper(field.type) === false;
        var $row = $(self.rowTemplate({fieldId: field.fieldId, fieldParentId: field.parentId, noDataWrapper: noDataWrapper}));
        $row = self.setValues($row, field);

        $fieldHolder.append($row).show();
    },
    setValues: function($row, field, data) {
        if (typeof field == 'undefined') return $row;

        if (typeof field.name != 'undefined') {
            $('input.nameInput', $row).val(field.name);
        }

        if (typeof field.identifier != 'undefined') {
            $('input.identifierInput', $row).val(field.identifier);
        }

        if (typeof field.description != 'undefined') {
            $('textarea.descriptionTextarea', $row).val(field.description);
        }

        if (typeof field.settings.multilanguage != 'undefined') {
            $('select.multilanguageSelect', $row).val(field.settings.multilanguage);
        }

        if (typeof field.type != 'undefined') {
            $('select.fieldTypeSelect', $row).val(field.type);
        }

        if (typeof data != 'undefined') {
            var $target = $('select.fieldDataSelect', $row);
            var type = $('select.fieldTypeSelect', $row).val();
            if (typeof data[type] != 'undefined') {
                $.each(data[type], function(value, label) {
                    $target.append('<option value="'+ value +'">'+ label +'</option>');
                });
            }
            if (field.data != '') {
                var parsedJSON = JSON.parse(field.data);
                $.each(parsedJSON, function(key, value) {
                    var $option = $('select.fieldDataSelect option[value="'+ key +'"]', $row);
                    if ($option.length > 0) {
                        $option.attr('selected', true);
                    }
                });
            }
        }

        return $row;
    },
    addTriggered: function(e) {
        e.preventDefault();
        var $this = $(this);
        var $wrapper = $($this.data('wrapper'));
        var $fieldHolder = $wrapper.find('.fieldHolder').first();
        FieldHandler.addEmptyField($fieldHolder);
    },
    removeTriggered: function() {
        var $row = $(this).closest('li');
        FieldHandler.removeRow($row);
    },
    cloneTriggered: function() {
        var $row = $(this).closest('li');
        var $newRow = $row.clone();
        //Clear Identifier on clone, must be unique
        $('.identifierInput', $newRow).val('');

        var selects = [
            '.multilanguageSelect',
            '.fieldTypeSelect',
        ];
        for (i in selects) {
            $(selects[i], $newRow).val($(selects[i], $row).val());
        }

        $newRow.insertAfter($row);
    },    
    identifierBlur: function() {
        var $this = $(this);
        var currentIdentifier = $.trim( $this.val() );
        //Find if the identifier isn't unique
        if (currentIdentifier != '') {
            var $allIdentifierInputs = $('input.identifierInput', FieldHandler.fieldRowsEl).not($(this));
            $allIdentifierInputs.each(function() {
                if (currentIdentifier == $(this).val()) {
                    $this.val('');
                    alert('Identifieraren måste var unik');
                }
            });
        }
    },
    nameBlur: function() {
    },
    typeChanged: function() {
        var self = FieldHandler;
        var $this = $(this);
        var fieldId = $this.data('fieldid');
        var type = $this.val();

        SettingsHandler.fillSettingsSelect(fieldId, true);

        if (DataHandler.hasDataWrapper(type)) {
            DataHandler.showForm(fieldId);
        } else {
            DataHandler.hideForm(fieldId);
        }

        if (type == 'Repeater') {
            self.addRepeater(fieldId);
        } else {
            self.removeRepeater(fieldId);
        }
    },
    addRepeater: function(fieldId) {
        var alreadyExists = $('.repeaterTemplate-'+ fieldId).length > 0;
        if (alreadyExists) return true;

        var $repeaterWrapper = $('.repeaterWrapper-'+ fieldId);
        var fieldParentId = $repeaterWrapper.data('fieldparentid') || 0;

        $('.hideIfRepeater-'+ fieldId).fadeOut(400);

        var field = FieldHandler.filterById(fieldId);
        var repeaterMin = 0;
        var repeaterMax = 0;
        if (typeof field != 'undefined') {
            repeaterMin = SettingsHandler.getSettingValue(field.settings, 'repeaterMin');
            repeaterMax = SettingsHandler.getSettingValue(field.settings, 'repeaterMax');
        }

        $repeaterWrapper.append(FieldHandler.repeaterTemplate({
            fieldId: fieldId,
            fieldParentId: fieldParentId,
            repeaterMin: repeaterMin,
            repeaterMax: repeaterMax,
        })).slideDown(400);
    },
    removeRepeater: function(fieldId) {
        var $target = $('.repeaterTemplate-'+ fieldId);
        if ($target.length == 0) return false;

        $('.repeaterWrapper-'+ fieldId).slideUp(400, function() {
            $target.remove();
        });
        $('.hideIfRepeater-'+ fieldId).fadeIn(400);
    },
    removeRow: function($row) {
        if ($row.length > 0) {
            var fieldId = typeof $row.data('fieldid') != 'undefined' ? parseInt($row.data('fieldid')) : 0;
            if (fieldId > 0) {
                FieldHandler.fieldRowsEl.append('<input type="hidden" name="removeField[]" value="'+ fieldId +'">')
            }
            $row.remove();
        }
    },
    getFieldTypeById: function(fieldId) {
        var $typeField = $('.fieldTypeSelect-'+ fieldId);
        if ($typeField.length > 0) {
            return $typeField.val();
        }
        return false;
    },
    toggleAll: function() {
        var self = FieldHandler;
        var $targets = $('.subFieldContainer > .toggleDrop', self.fieldRowsEl);
        
        if (self.toggleDropClosed) {
            $targets.slideDown(500);
            self.toggleDropClosed = false;
        } else {
            $targets.slideUp(500);
            self.toggleDropClosed = true;
        }
    },
    filterById: function(fieldId) {
        return templateJSON.fields.filter(function(field) {
            return field.id == fieldId;
        })[0];
    },
}

var DataHandler = {
    unique: 0,
    hasDataWrapperTypes: [
        'Select',
        'Radio',
        'Checkbox',
    ],
    addDataRow: function(fieldId, fieldParentId, value, label, insertAfter) {
        var value = (typeof value != 'undefined') ? value : '';
        var label = (typeof label != 'undefined') ? label : '';
        var insertAfter = (typeof insertAfter != 'undefined') ? insertAfter : false;
        var $target = $('.dataRows-'+ fieldId);
        var unique = DataHandler.updateUnique();

        var $template = $(FieldHandler.dataRowTemplate({
            fieldId: fieldId,
            fieldParentId: fieldParentId,
            unique: unique,
            value: value,
            label: label,
        }));

        if (insertAfter) {
            $template.insertAfter(insertAfter);
            return true;
        }

        $target.append($template);
    },
    fillData: function(fieldId) {
        var $target = $('.dataRows-'+ fieldId);
        if ($target.length == 0) return false;

        var field = FieldHandler.filterById(fieldId);
        if (field.data !== undefined) {
            $.each(field.data, function(key, data) {
                DataHandler.addDataRow(fieldId, field.parentId, data.value, data.label);
            });
        }
    },
    hasDataWrapper: function(type) {
        return DataHandler.hasDataWrapperTypes.indexOf(type) >= 0;
    },
    showForm: function(fieldId) {
        var $target = $('.fieldId-'+ fieldId);
        if ($target.length == 0) return false;
        $('.dataWrapper', $target).slideDown(300);
    },
    hideForm: function(fieldId) {
        var $target = $('.fieldId-'+ fieldId);
        if ($target.length == 0) return false;
        $('.dataWrapper', $target).slideUp(300);
    }, 
    addTriggered: function() {
        var $this = $(this);
        var fieldId = $this.data('fieldid');
        var fieldParentId = $this.data('fieldparentid');

        DataHandler.addDataRow(fieldId, fieldParentId);
    },
    cloneTriggered: function() {
        var $this = $(this);
        var fieldId = $this.data('fieldid');
        var fieldParentId = $this.data('fieldparentid');
        var currentUnique = $this.data('unique');

        var dataValueField = $('.dataValueField.unique-'+ currentUnique).val();
        var dataLabelField = $('.dataLabelField.unique-'+ currentUnique).val();

        var $insertAfter = $('.dataRow.unique-'+ currentUnique);

        DataHandler.addDataRow(
            fieldId,
            fieldParentId,
            dataValueField,
            dataLabelField,
            $insertAfter
        );
    },
    removeTriggered: function() {
        var $this = $(this);
        var currentUnique = $this.data('unique');
        var $target = $('.dataRow.unique-'+ currentUnique);
        $target.remove();
    },
    updateUnique: function() {
        return DataHandler.unique++;
    }
}

var SettingsHandler = {
    unique: 0,
    selected: {},
    fillSettingsSelect: function(fieldId, highlight) {
        var highlight = highlight || false;
        var availableSettings = FieldHandler.JSON.fieldSettings;
        var $settingsSelect = $('.settingsSelect-'+ fieldId);
        var type = $('.fieldTypeSelect-'+ fieldId).val();

        var settings = {}
        if (type in availableSettings) {
            settings = availableSettings[type];
        }

        var options = '<option value="">-- Inga inställningar finns --</option>';

        if (!$.isEmptyObject(settings)) {
            options = '<option value="">Välj...</option>';
            $.each(settings, function(key, data) {
                var disabled = '';
                if (fieldId in SettingsHandler.selected && key in SettingsHandler.selected[fieldId]) {
                    disabled = ' disabled';
                }
                options += '<option value="'+ data.value +'" data-key="'+ key +'"'+ disabled +'>'+ data.label +'</option>';
            });
        }

        $settingsSelect.html(options);

        if (highlight) {
            $settingsSelect.css('border-color', '#ffbd4a');
            setTimeout(function() {
                $settingsSelect.css('border-color', '');
            }, 300)
        }
    },
    fillSettings: function(fieldId) {
        var $target = $('.settingsRows-'+ fieldId);
        if ($target.length == 0) return false;

        var field = FieldHandler.filterById(fieldId);
        if (field.settings !== undefined) {
            var availableSettings = templateJSON.fieldSettings;
            var currentField = availableSettings[field.type];

            if (currentField === undefined) return false;
            $.each(field.settings, function(key, setting) {
                var settingLabel = currentField[setting.key];
                if (settingLabel !== undefined) {
                    settingLabel = settingLabel.label;
                    SettingsHandler.addSettingsRow(fieldId, field.parentId, setting.key, setting.value, settingLabel, true);
                }
            });
        }
    },
    addSettingsRow: function(fieldId, fieldParentId, key, value, label, noProcess) {
        if (fieldId in SettingsHandler.selected && key in SettingsHandler.selected[fieldId]) {
            alert('Inställningen finns redan i listan.');
            return false;
        }

        var $target = $('.settingsRows-'+ fieldId);
        var unique = SettingsHandler.updateUnique();
        if (noProcess !== true) {
            var value = SettingsHandler.processValue(fieldId, key, value);
        }

        if ($.trim(value) == '') {
            return false;
        }

        var $template = $(FieldHandler.settingsRowTemplate({
            fieldId: fieldId,
            fieldParentId: fieldParentId,
            unique: unique,
            key: key,
            value: value,
            label: label
        }));

        $target.append($template);

        if (!(fieldId in SettingsHandler.selected)) {
            SettingsHandler.selected[fieldId] = {};
        }

        SettingsHandler.selected[fieldId][key] = key;

        SettingsHandler.fillSettingsSelect(fieldId)
    },
    processValue: function(fieldId, key, value) {
        var availableSettings = FieldHandler.JSON.fieldSettings;
        var value = typeof value != 'undefined' ? value : '';
        var type = FieldHandler.getFieldTypeById(fieldId);

        if (availableSettings[type] === undefined) {
            return value;
        }

        var setting = availableSettings[type][key];
        if ('editable' in setting && setting.editable == true) {
            var allowEmpty = ('allowEmpty' in setting) && setting.allowEmpty == true;
            var onlyNumbers = ('onlyNumbers' in setting) && setting.onlyNumbers == true;
            var minNumber = ('minNumber' in setting) ? parseInt(setting.minNumber) : false;
            var maxNumber = ('maxNumber' in setting) ? parseInt(setting.maxNumber) : false;
            var regexp = ('regexp' in setting) ? setting.regexp : false;

            var promptLabel = 'Värde';
            var promptLabelRules = [];

            if (!allowEmpty) promptLabelRules.push('Ej tomt värde');
            if (onlyNumbers) promptLabelRules.push('Endast numerisk värde');
            if (minNumber !== false) promptLabelRules.push('Minst '+ minNumber);
            if (maxNumber !== false) promptLabelRules.push('Max '+ maxNumber);
            if (regexp !== false) promptLabelRules.push('Måste följa regexp: '+ regexp);

            if (promptLabelRules.length > 0) {
                promptLabel += ' ('+ promptLabelRules.join(', ') +')';
            }

            while (true) {
                value = prompt(promptLabel, value);
                
                if (value === null) break;
                if (allowEmpty === false && $.trim(value) == '') continue;
                if (regexp !== false) {
                    var pattern = new RegExp(regexp);
                    if (pattern.test(value) === false) continue;
                }
                if (onlyNumbers !== false) {
                    if (!$.isNumeric(value)) {
                        continue;
                    } else {
                        value = parseInt(value);
                        if (minNumber !== false && value < minNumber) continue;
                        if (maxNumber !== false && value > maxNumber) continue;
                    }
                }

                break;
            }
        }

        return value;
    },
    editSettingsRow: function(fieldId, key, value) {
        var newValue = SettingsHandler.processValue(fieldId, key, value);
        
        if (value != newValue) {
            var $target = $('.settingsValueField-'+ key +'-'+ fieldId);
            if ($target.length > 0 && $.trim(newValue) != '') {
                $target.val(newValue);
            }
        }
    },
    addTriggered: function() {
        var $this = $(this),
            fieldId = $this.data('fieldid'),
            fieldParentId = $this.data('fieldparentid'),
            $select = $('.settingsSelect-'+ fieldId),
            $option = $('option:selected', $select),
            key = $option.data('key'),
            value = $.trim($select.val()),
            label = $option.html();

        if (typeof $option.data('key') == 'undefined') {
            alert('En inställning måste väljas innan du kan lägga till den i listan.');
            return false;
        }

        SettingsHandler.addSettingsRow(fieldId, fieldParentId, key, value, label);
    },
    editTriggered: function() {
        var $this = $(this);
        var key = $this.data('key')
        var fieldId = $this.data('fieldid')
        var currentValue = $this.val();

        SettingsHandler.editSettingsRow(fieldId, key, currentValue);
    },
    removeSettingsRow: function(fieldId, key) {
        var $target = $('.settingRow-'+ fieldId +'-'+ key);
        if ($target.length > 0) {
            $target.slideUp(400, function() {
                $target.remove();
            });
            delete SettingsHandler.selected[fieldId][key];
        }
        SettingsHandler.fillSettingsSelect(fieldId);
    },
    removeTriggered: function() {
        var $this = $(this);
        var fieldId = $this.data('fieldid');
        var key = $this.data('key');

        SettingsHandler.removeSettingsRow(fieldId, key);
    },
    updateUnique: function() {
        return SettingsHandler.unique++;
    },
    getSettingValue: function(settings, key) {
        var setting = settings.filter(function(setting) {
            return setting.key == key;
        })[0];

        if (typeof setting == 'undefined') {
            return null;
        }

        return setting.value;
    }
};

$(function() {
    if ($('.customfieldsForm').length > 0) {
        FieldHandler.initialize();

        $('.sortable').nestable({
            items: '> li',
            placeholder: 'ui-state-highlight',
            opacity: 0.6,
            cursor: 'move',
            handle: '> .handle',
            update: function() {
                //var order = $(this).sortable("serialize");
            }
        });

        $('.dataRows').nestable({
            items: '> .dataRow',
            placeholder: 'ui-state-highlight',
            opacity: 0.6,
            cursor: 'move',
            handle: '.handle',
            update: function() {
                //var order = $(this).sortable("serialize");
            }
        });
    }
});

module.exports = FieldHandler;

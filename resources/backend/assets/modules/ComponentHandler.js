var DataTable = require('datatables.net')(window, $);
require('datatables.net-bs/css/dataTables.bootstrap.css');

require('./plugins/nestable/jquery.nestable.js')();
require('./plugins/nestable/jquery.nestable.css');
require('./plugins/tinymce/tinymce.js');

var easytabs = require('./plugins/easytabs/jquery.easytabs.min.js');

var ConfirmLeaving = require('./ConfirmLeaving');

var ComponentHandler = {
    debug: false,
    initOnLoad: [
        'tinymce',
        'dataTable',
        'sortable',
        'easytabs',
    ],
    initialize: function () {
        //Go through "initOnLoad", construct and initialize the components
        var index;
        var componentName;
        var component;
        for (index in this.initOnLoad) {
            this.debugLog('------------------------------------------------')   
            componentName = this.initOnLoad[index];
            component = this[componentName];
            if (typeof component == 'undefined') {
                this.debugLog('Couldn\'t find component'+ componentName);
                continue;
            }
            
            if (typeof component.construct != 'function') {
                this.debugLog('Component '+ componentName +' requires a construct method.');
                continue;
            }

            if (typeof component.initialize != 'function') {
                this.debugLog('Component '+ componentName +' requires an initialize method.');
                continue;
            }

            component.active = false;
            component.parent = this;
            component.construct();
            this.debugLog('Component: '+ componentName +'.construct() has been called.');
            if (component.selector.length > 0) {
                this.debugLog('Component: '+ componentName +'.initialize() has been called.');
                component.initialize();
                component.active = true;
                continue;
            }

            this.debugLog('Component: '+ componentName +' has length: 0, '+ componentName +'.initialize() was NOT called.');
        }
    },
    debugLog: function(message) {
        if (this.debug) {
            console.log(message);
        }
    },
    tinymce: {
        selector: null,
        component: null,
        config: {},
        construct: function () {
            ComponentHandler.tinymce.config = this.getConfig();
        },
        initialize: function () {
            this.component = tinymce.init(ComponentHandler.tinymce.config);
        },
        getConfig: function() {
            this.selector = $('textarea.tinymce, textarea.wysiwyg');
            var onlySelector = this.selector.selector;
            return {
                convert_urls: false,
                selector: onlySelector,
                theme: "modern",
                height: 300,
                browser_spellcheck : true,
                skin: false,
                extended_valid_elements: 'span[*]',
                content_css: 'https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
                noneditable_noneditable_class: 'fa',
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview fullscreen | forecolor fontawesome | code', 
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualchars code fullscreen insertdatetime media nonbreaking',
                    'save table contextmenu directionality paste textcolor fontawesome',
                ],
                style_formats: [
                    {title: 'Headers', items: [
                        {title: 'h1', block: 'h1'},
                        {title: 'h2', block: 'h2'},
                        {title: 'h3', block: 'h3'},
                        {title: 'h4', block: 'h4'},
                        {title: 'h5', block: 'h5'},
                        {title: 'h6', block: 'h6'}
                    ]},

                    {title: 'Inline', items: [
                        {title: 'Bold', inline: 'b', icon: 'bold'},
                        {title: 'Italic', inline: 'i', icon: 'italic'},
                        {title: 'Underline', inline: 'span', styles : {textDecoration : 'underline'}, icon: 'underline'},
                        {title: 'Strikethrough', inline: 'span', styles : {textDecoration : 'line-through'}, icon: 'strikethrough'},
                        {title: 'Superscript', inline: 'sup', icon: 'superscript'},
                        {title: 'Subscript', inline: 'sub', icon: 'subscript'},
                        {title: 'Code', inline: 'code', icon: 'code'},
                    ]},

                    {title: 'Blocks', items: [
                        {title: 'Paragraph', block: 'p'},
                        {title: 'Blockquote', block: 'blockquote'},
                        {title: 'Div', block: 'div'},
                        {title: 'Pre', block: 'pre'}
                    ]},

                    {title: 'Alignment', items: [
                        {title: 'Left', block: 'div', styles : {textAlign : 'left'}, icon: 'alignleft'},
                        {title: 'Center', block: 'div', styles : {textAlign : 'center'}, icon: 'aligncenter'},
                        {title: 'Right', block: 'div', styles : {textAlign : 'right'}, icon: 'alignright'},
                        {title: 'Justify', block: 'div', styles : {textAlign : 'justify'}, icon: 'alignjustify'}
                    ]}
                ],
                templates: [
                  //{title: 'Kolumner 2', description: 'Kolumner 2', content: '<div class="col2">kolumner 2</div>'},
                  //{title: 'Kolumner 3', description: 'Kolumner 3', content: '<div class="col3">kolumn 3</div>'},
                ],
                setup: function (editor) {
                  editor.on('change', function (e) {
                     ConfirmLeaving.triggerLeave();
                  });
                  editor.on('paste', function (e) {
                      ConfirmLeaving.triggerLeave();
                  });
                  editor.on('cut', function (e) {
                      ConfirmLeaving.triggerLeave();
                  });

                  editor.shortcuts.add('meta+1', 'h1 shortcut', ['FormatBlock', false, 'h1']);
                  editor.shortcuts.add('meta+2', 'h2 shortcut', ['FormatBlock', false, 'h2']);
                  editor.shortcuts.add('meta+3', 'h3 shortcut', ['FormatBlock', false, 'h3']);
                  editor.shortcuts.add('meta+4', 'h4 shortcut', ['FormatBlock', false, 'h4']);
                  editor.shortcuts.add('meta+5', 'h5 shortcut', ['FormatBlock', false, 'h5']);
                  editor.shortcuts.add('meta+6', 'h6 shortcut', ['FormatBlock', false, 'h6']);
                },
                save_onsavecallback () {
                    $(this.targetElm).closest('form').trigger('submit');
                },
                file_browser_callback: function(field_name, url, type, win) {
                    Filemanager.open().onSave((file) => {
                        $('#' + field_name).val(file.relativeFullPath);
                    });
                }
            };
        }
    },
    dataTable: {
        selector: null,
        component: null,
        construct: function () {
            this.selector = $('.dataTable');
        },
        initialize: function () {
            this.component = this.selector.DataTable({
                destroy: true,
                pageLength: 50,
                aaSorting: [],
            });
        }
    },
    sortable: {
        selector: null,
        component: null,
        construct: function () {
            this.selector = $('.sortable');
        },
        initialize: function () {
            this.component = this.selector.nestable({
                maxDepth: this.selector.data('depth') || 20,
            }).on('change', function (e) {
                var $this = $(this);
                var $field = $('input.sortJSON');
                if ($field.length > 0) {
                    var $data = window.JSON.stringify($this.nestable('serialize'));
                    $field.val($data);
                }
            });
        }
    },
    easytabs: {
        selector: null,
        component: null,
        construct: function() {
            this.selector = $('.easytabs');
        },
        initialize: function() {
            this.selector.easytabs({
                transitionIn: 'fadeIn',
                transitionOut: 'fadeOut',
            });
            this.selector.on('easytabs:ajax:complete', function() {
                ComponentHandler.initialize();
            });
        }
    }
};

module.exports = ComponentHandler;

var Helper = {
    getInputElements: function($where) {
        $where = $where || $('body');
        return $('input, textarea, select', $where)
            .not(':input[type="button"], :input[type="submit"], :input[type="reset"]');
    },
    isValidJson: function(json) {
        try {
            JSON.parse(json);
        } catch (e) {
            return false;
        }
        return true;
    },
};

module.exports = Helper;

var ConfirmLeaving = {
    triggerActive: false,
    watchChanges: function() {
        var $keyupTrigger = $('form').on('keyup', 'input, textarea', function() {
            ConfirmLeaving.triggerLeave();
            $keyupTrigger.unbind('keyup');
        });
    },
    triggerLeave: function() {
        return false;
        if (this.triggerActive == false) {
          Selector.window.on('beforeunload', function() {
              return 'Ändringar har utförts på sidan, är du ändå säker att du vill lämna denna sidan?';
          });
          this.triggerActive = true;
        }
    }
};

module.exports = ConfirmLeaving;

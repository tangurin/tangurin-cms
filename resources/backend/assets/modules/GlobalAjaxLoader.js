var swal = require('sweetalert2');

var GlobalAjaxLoader = {
    ajaxLoader: null,
    popupInterval: null,
    popupOpen: false,
    initialize: function() {
        this.ajaxLoader = $('#ajaxLoader');
        
        var $document = $(document);
        $document.ajaxStart(this.onAjaxStart);
        $document.ajaxStop(this.onAjaxStop);
        $document.ajaxComplete(this.onAjaxComplete);
    },
    onAjaxStart: function() {
        GlobalAjaxLoader.popupInterval = setInterval($.proxy(GlobalAjaxLoader.interval, GlobalAjaxLoader), 5000);
        GlobalAjaxLoader.ajaxLoader.addClass('loading');
        GlobalAjaxLoader.popupOpen = false;
    },
    onAjaxStop: function() {
        GlobalAjaxLoader.ajaxLoader.removeClass('loading');
        clearInterval(GlobalAjaxLoader.popupInterval);
    },
    onAjaxComplete: function(event, xhr, settings) {
        //Redirect to login if status is "Unauthorized"
        if (xhr.status == 401 && typeof stayIfInauthorized == 'undefined') {
            ErrorHandler.swal('Unauthorized');
        }

        //Remove loading class
        GlobalAjaxLoader.ajaxLoader.removeClass('loading');
    },
    interval: function() {
        if (!this.popupOpen) {
          this.popupOpen = true;
          this.openPopup();
        }
    },
    openPopup: function() {
        swal({
            title: 'Avbryt anropet?',
            text: 'Vill du avbryta det pågående anropet?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Ja, avbryt',
            cancelButtonText: 'Nej, vänta',
            closeOnConfirm: false,
            closeOnCancel: true 
        }).then(function(isConfirm) {
            if (isConfirm) {
              GlobalAjaxLoader.ajaxLoader.removeClass('loading');
              clearInterval(GlobalAjaxLoader.popupInterval);
            }
            GlobalAjaxLoader.popupOpen = false;
            swal.close();
        });
    }
};

module.exports = GlobalAjaxLoader;

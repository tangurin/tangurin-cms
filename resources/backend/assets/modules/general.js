var Selector = {
    document: null,
    window: null,
    html: null,
    body: null,
    initialize: function() {
        this.setSelectors();
    },
    setSelectors: function() {
        this.document = $(document);
        this.window = $(window);
        this.html = $('html');
        this.body = $('body');
    }
}

var Redirect = {
    login: function() {
        this.goTo(URL.admin +'/login');
    },
    goTo: function(url) {
        window.location.href = url;
    }
}


$(function() {
    Selector.initialize();
    var $isEditing = $('.cancelEditing').length > 0;


    $('.toggleAllCheckboxes').on('click', function() {
        var $this = $(this);
        if ($this.data('wrapper') == undefined) {
            return false;
        }
        var $wrapper = $this.closest($this.data('wrapper'));
        var isChecked = $this.hasClass('isChecked');
        if (!isChecked) {
            //Set all to not checked and then trigger click to check all
            $('input[type="checkbox"]', $wrapper).prop('checked', false).trigger('click');
            $this.addClass('isChecked');
        } else {
            //Set all checkboxes to checked and then trigger click to uncheck all
            $('input[type="checkbox"]', $wrapper).prop('checked', true).trigger('click');
            $this.removeClass('isChecked');
        }
    });

    // Indicate the current page in the navigation bar (if a link exists there).
    $('#sidebar-menu a').each(function() {
        var $this = $(this);
        var url = window.location.pathname;

        if (this.href == window.location) {
            $this.add( $this.parents('li') ).addClass('active');
        }
    });
    
    //Show all submenus if a link is active 
    $('#sidebar-menu ul li a.active:last').parents('ul').show()
});

import vue from 'vue';
import FilemanagerPicker from '../filemanager/components/Filemanager-Picker.vue';


var VueInstance = {
    idPrefix: 'vueInstance-',
    id: 1,
    instanceAll () {
        var self = this;
        $.each(self.components, function(name, component) {
            self.createInstance(name);
        });
    },
    createInstance (name) {
        var self = this;
        var component = self.components[name];
        $('.component-'+ name).each(function() {
            $(this).attr('id', self.idPrefix + self.id)
            new vue({
                el: '#'+ self.idPrefix + self.id,
                components: {
                    [name]: component,
                },
            });
            
            self.id++;
        });

    },
    components: {
        'filemanager-picker': FilemanagerPicker,
    },
};

VueInstance.instanceAll();

export default VueInstance;

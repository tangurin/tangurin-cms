var Helper = require('./Helper');
var Ubold = require('./ubold.js')
var swal = require('sweetalert2');

var FieldLabels = {
    initialize: function() {
        FieldLabels.selects();
    },
    selects: function() {
        $('select[data-placeholder]').each(function() {
            var $this = $(this);
            var label = $this.data('placeholder');
            var $wrapper = $this.closest('.form-group');
            var $label = $('<label>'+ label +'</label>');
            $wrapper.addClass('selectWrapper').append($label);
        });
    }
};

var FieldSettings = {
    fields: [],
    initialize: function() {
        $('input, textarea').each(this.addButton);
        $('.form-group .fieldSettings').on('click', this.buttonTriggered);
    },
    addButton: function() {
        var $this = $(this);
        var $formGroup = $this.parent('.form-group');
        
        if ($formGroup.length > 0) {
            $formGroup.append('<a class="fieldSettings" style="display: none;"><i class="fa fa-bars"></i></a>');
            setTimeout(function() {
                $('.fieldSettings', $formGroup).fadeIn(500);
            }, 400);
        }
    },
    buttonTriggered: function() {
        var $this = $(this);
        var fieldName = $this.attr('name');
        FieldSettings.openPopup(fieldName);
    },
    openPopup: function(fieldName) {
        console.log(fieldName);
    }
};

var Dependers = {
    initialize: function() {
        $('form .form-group [data-dependon]').each(function() {
            var $this = $(this);
            var $source = $($this.data('dependon'));

            if ($source.length == 0) {
                return true;
            }

            var selected = $this.data('selected') || 0;
            $source.on('change', function() {
                var $input = $(this);
                $.ajax({
                    url: $input.data('ajax'),
                    method: 'POST',
                    dataType: 'HTML',
                    data: {
                        value: $input.val(),
                        selected: selected,
                    },
                    success: function(htmlResponse) {
                        $this.html(htmlResponse);
                    }
                });
            }).trigger('change');
        });
    }
};

var FormSubmit = {
    form: null,
    method: null,
    url: null,
    edit: false,
    initialize: function() {
        $('body').on('submit', 'form[data-remote]', FormSubmit.saveData);
    },
    saveData: function(e) {
        e.preventDefault();
        FormSubmit.form = $(this);
        FormSubmit.method = $('input[name="_method"]', FormSubmit.form).val() || 'post';
        FormSubmit.method = 'post'
        FormSubmit.url = FormSubmit.form.prop('action');
        FormSubmit.edit = FormSubmit.form.hasClass('edit');
        
        //Save tinymce data to the textarea
        tinyMCE.triggerSave();

        $.ajax({
            type: FormSubmit.method,
            url: FormSubmit.url,
            data: FormSubmit.form.serialize(),
            success: function(response) {
                if (response.redirect) {
                    window.location.href = response.redirect;
                    return false;
                }

                FormSubmit.handleModel(response.model);
                FormSubmit.handleLanguageModels(response.languageModels);
                FormSubmit.removeAllErrors();
                FormSubmit.displaySuccess(response);
            },
            error: function(response) {
                if (response.status == 403) {
                    swal({
                        type: 'error',
                        title: 'Behörighet saknas',
                        text: 'Behörighet saknas för att utföra detta',
                    });
                    return false;
                }
                if (Helper.isValidJson(response.responseText) === false) {
                    swal({
                        type: 'error',
                        title: 'Ett fel inträffade',
                        text: 'Data kunde inte sparas pga ett okänt fel',
                    });
                    return false;
                }
                var response = $.parseJSON(response.responseText);

                if(response) {
                    FormSubmit.displayErrors(response.errors);
                    return false;
                }
                alert('Något gick fel.');
            },
        });
    },
    displayErrors: function(errors) {
        var errorsHtml = '';

        //Remove all current displaying error.
        this.removeAllErrors();


        if (errors.swal) {
            swal({
                type: 'error',
                title: 'Fel',
                text: errors.swal
            });

            delete errors.swal;
        }

        if ($.isEmptyObject(errors)) {
            return false;
        }

        //Add errors to the 
        $.each(errors, function(fieldName, error) {
            errorsHtml += error +'<br />';

            var split = fieldName.split('.');
            if (split.length > 1) {
                fieldName = split[0];
                split.splice(0, 1);
                for (var i in split) {
                    fieldName += '['+ split[i] +']';
                }
            }
            var $field = $('[name="'+ fieldName +'"]');
            if ($field.length > 0) {
                $field.addClass('parsley-error');
                $field.after('<ul class="parsley-errors-list filled" id="parsley-id-8"><li class="parsley-required">'+ error +'</li></ul>');
            }
        });
        $.notify({
            title: 'Error',
            message: errorsHtml,
        }, {
            type: 'danger',
            delay: 100000,
            placement: {
                from: 'bottom',
                alignment: 'right',
            }
        });
    },
    displaySuccess: function(response) {
        var $message = this.form.data('remote-success') || 'Allting sparades utan problem';
        swal({
            title: 'Sparat!',
            text: $message,
            type: 'success',
            confirmButtonColor: "#DD6B55",
        });

        if (typeof response != 'object') {
            return true;
        }
        
        this.addToCreatedList(response.created);
        this.replace(response.replace);
        this.clearAllFields();
    },
    removeAllErrors: function() {
        var $notify = $('.notifyjs-container');
        if ($notify.length > 0) {
            $notify.trigger('click');
        }

        var $currentErrors = $('.parsley-error');
        if ($currentErrors.length > 0) {
            $currentErrors.removeClass('parsley-error');
            $('.parsley-errors-list').remove();
        }
    },
    addToCreatedList: function(created) {
        if (created == undefined || created.name == undefined || created.link == undefined) {
            return false;
        }
        $('ul.sessionCreated').append('<li><a href="'+ created.link +'">'+ created.name +'</a></li>').show();
    },
    replace: function(replace) {
        if (typeof replace != 'object') {
            return false;
        }

        $.each(replace, function(key, data) {
            if (data.target == undefined) {
                return true;
            }
            var $target = $(data.target);
            if ($target.length > 0) {
                if (data.html != undefined) {
                    $target.replaceWith(data.html);
                }
                if (data.val != undefined) {
                    $target.val(data.val);
                }
            }
        });
    },
    clearAllFields: function() {
        if (FormSubmit.edit) {
            return false;
        }
        var $form = this.form;
        Helper.getInputElements($form).each(function() {
            var $this = $(this);
            $this.val('');
        });

        $('select', $form).prop('selected', function() {
            return this.defaultSelected;
        });

        $('input[type="checkbox"], input[type="radio"]', $form).prop('checked', function() {
            return this.defaultChecked;
        });

        //Remove images
        $('#images .image').slideUp(300, function() {
            $(this).remove();
        });

        var $imageStack  = $('.imageStack');
        if($imageStack.length > 0) {
            $imageStack.remove();
        }
    },
    handleModel: function(model) {
        if (model == undefined) {
            return false;
        }

        var $idField = $('input[type="hidden"][name="id"]');
        if ($idField.length > 0) {
            $idField.attr('value', model.id);
        }
    },
    handleLanguageModels: function(models) {
        if (models == undefined) {
            return false;
        }

        $.each(models, function(key, item) {
            var $languageIdField = $('input[type="hidden"][name*="['+ item.language +'][id]"]');
            if ($languageIdField.length > 0) {
                $languageIdField.attr('value', item.id);
            }
        });
    },
};

module.exports = {
    FormSubmit: FormSubmit,
    Dependers: Dependers,
    FieldLabels: FieldLabels,
};

var ConfirmDelete = {
    id: null,
    path: null,
    indexPath: null,
    title: null,
    text: null,
    initialize: function() {
        $('body').on('click', '.confirmDelete', this.triggered);
    },
    triggered: function(e) {
        e.preventDefault();
        var $this = $(this);
        ConfirmDelete.path = $this.data('path');
        ConfirmDelete.indexPath = $this.data('index');
        ConfirmDelete.id = $this.data('id');

        var word = $this.data('word');
        var title = $this.data('title');
        var text = $this.data('text');

        if (typeof word == 'undefined') {
          word = 'denna raden'
        }
        if (typeof title == 'undefined') {
          title = 'Bekräfta borttagning';
        }
        if (typeof text == 'undefined') {
          text = 'Vill du verkligen ta bort '+ word +'?';
        }

        ConfirmDelete.title = title;
        ConfirmDelete.text = text;

        ConfirmDelete.popup($this);
    },
    popup: function($button) {
        swal({
            title: this.title,
            text: this.text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Ja',
            cancelButtonText: 'Nej, avbryt',
        }).then((confirmed) => {
            if (confirmed) {
                $.ajax({
                  method: 'POST',
                  url: ConfirmDelete.path,
                  data: {_method: 'DELETE'},
                  success: function(response) {
                    if (ConfirmDelete.indexPath) {
                        window.location.href = ConfirmDelete.indexPath;
                        return false;
                    }

                    var $tr = $button.closest('tr');
                    var $li = $button.closest('li');
                    if ($tr.length > 0) {
                        ConfirmDelete.refreshDataTable($button);
                    } else if ($li.length > 0) {
                        ConfirmDelete.refreshSortable($button);
                    }
                  }
                });
                swal('Borttagen!', 'Raden är nu borttagen.', 'success');
            }
        });
    },
    refreshDataTable: function($button) {
        $button.closest('tr').slideUp(1000, function() {
            //Remove from datatable and draw new table
            if (ComponentHandler.dataTable.active) {
                ComponentHandler.dataTable.component.row($(this)).remove().draw();
            }
        });
    },
    refreshSortable: function($button) {
        var $id = $button.closest('.tab-pane').attr('id');
        if (typeof $id == 'undefined') return false;

        var $target = $('a[href="#'+ $id +'"]');
        if ($target.length > 0) {
            $target.trigger('click');
        }
    }
}

module.exports = ConfirmDelete;

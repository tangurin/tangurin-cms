var TreeMenu = function($main) {
    var self = this;
    self.$main = $main;

    self.openCurrent();

    $('a', self.$main).on('click', function() {
        self.open($(this).parent('li'));
    });
};

TreeMenu.prototype.openCurrent = function($li) {
    var self = this;

    $('a', self.$main).filter(function() {
        return this.href == window.location || window.location.href.indexOf(this.href) == 0;
    })
    .addClass('active')
    .parents('li')
    .addClass('active')
    .children('ul')
    .slideDown(300);
};

TreeMenu.prototype.open = function($li) {
    var $ul = $('> ul', $li);
    if ($ul.length > 0) {
        if ($ul.is(':visible') === false) {
            $ul.slideDown(500);
            $li.addClass('active');
            $('> a', $li).addClass('active');
        } else {
            $ul.slideUp(500);
            $li.removeClass('active');
            $('> a', $li).removeClass('active');
        }
    }
};


module.exports = TreeMenu;

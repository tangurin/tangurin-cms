import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './theme/css/core.css';
import './theme/css/components.css';
import './theme/css/icons.css';
import './theme/css/pages.css';
import './theme/css/responsive.css';
import './theme/css/custom.css';
import 'sweetalert2/dist/sweetalert2.min.css';
import './scss/app.scss';

//import bootstrapJs from 'bootstrap/dist/js/bootstrap';
//import fastclick from 'fastclick';
//import waves from 'waves/dist/waves.min.js';
//import switchery from 'switchery';
var swal = require('sweetalert2');
window.swal = swal;

import vue from 'vue';
import axios from 'axios';
import 'bootstrap';
import 'bootstrap-notify';
import 'bootstrap-datepicker-webpack';

import ComponentHandler from './modules/ComponentHandler.js';
import ConfirmLeaving from './modules/ConfirmLeaving';
import GlobalAjaxLoader from './modules/GlobalAjaxLoader';
import ConfirmDelete from './modules/ConfirmDelete';
import Form from './modules/Form';
import TreeMenu from './modules/TreeMenu';
import Customfields from './customfields/customfields.js';
import Filemanager from './filemanager/filemanager.js';
import VueInstance from './modules/VueInstance.js';
window.Filemanager = Filemanager;

$('input.filemanager').on('click', function() {
    Filemanager.open().onSave((files) => {
        $(this).val(files[0].fullPath);
    });
});

ComponentHandler.initialize();
GlobalAjaxLoader.initialize();
ConfirmDelete.initialize();
ConfirmLeaving.watchChanges();
Form.FieldLabels.initialize();
Form.Dependers.initialize();
Form.FormSubmit.initialize();

var sidebarMenu = new TreeMenu($('#sidebar-menu'));

var $document = $(document);

//Add CSRF-Token to all AJAX Post requests
$document.ajaxSend(function(elm, xhr, settings){
   if (settings.type == 'POST') {
       xhr.setRequestHeader('x-csrf-token', CSRFTOKEN);
   }
});

import Vue from 'vue';
import axios from 'axios';

Vue.mixin({
    methods: {
        isFileSelected (file) {
            return this.$store.state.selectedFiles.find(selectedFile => selectedFile.id == file.id) !== undefined;
        },
        saveSetting (key, value) {
            axios.get(URL.filemanager.saveSetting, {
                params: {
                    key: key,
                    value: value,
                }
            });
        },
        prefixSize (size) {
            var kb = Math.round(size / 1000);
            if (kb > 1024) {
                return (kb / 1000).toFixed(2) +'mb';
            }

            return kb +'kb';
        },
        getFileIcon (file) {
            if (file.name.charAt(0) == '.') {
                return 'hidden';
            }

            return file.extension;
        }
    },
});

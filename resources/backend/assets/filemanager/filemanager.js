import FilemanagerController from './controller.js';

export default {
    open (config) { FilemanagerController.open(config); return this},
    close () { FilemanagerController.close(); return this},
    save () { FilemanagerController.save(); return this},
    onSave (callback) { FilemanagerController.on('save', callback) },
    onClose (callback) { FilemanagerController.on('close', callback) },
};

import Vue from 'vue';
import Vuex from 'vuex';
import FilemanagerVue from './components/Filemanager.vue';
import store from './store';
import mixins from './mixins.js';
import directives from './directives.js';

const FilemanagerController = {
    vue: null,
    callbacks: {},
    initialize () {
        this.setEmptyCallbacks();
        return this;
    },
    open (config) {
        if (this.vue == null) {
            this.initializeVue();
        }

        this.vue.open(config);
        return this;
    },
    close () {
        if (this.vue != null) {
            this.setEmptyCallbacks();
            this.vue.close();
        }
        return this;
    },
    save () {
        if (this.vue != null) {
            this.vue.save();
            this.close();
        }
        return this;
    },
    initializeVue () {
        if (document.getElementById('filemanagerHolder') == null) {
            var div = document.createElement('div');
            div.setAttribute('id', 'filemanagerHolder');
            div.innerHTML = '<filemanager></filemanager>';
            document.getElementsByTagName('body')[0].appendChild(div);
        }
        this.vue = new Vue({
            el: '#filemanagerHolder',
            store,
            components: {
                Filemanager: FilemanagerVue,
            },
            methods: {
                ...Vuex.mapActions([
                    'open',
                    'close',
                    'save',
                ]),
            }
        });
    },
    on (callbackName, callback) {
        if (this.callbacks.hasOwnProperty(callbackName) && typeof callback == 'function') {
            this.callbacks[callbackName] = callback;
        }
    },
    setEmptyCallbacks () {
        this.callbacks = {
            close: null,
            save: null,
        };
    },
    runCallback: function() {
        var args = arguments;
        var callback = this.callbacks[args[0]];
        if (typeof callback == 'function') {
            var filteredArgs = [];
            for (var i in args) {
                if (i > 0)  {
                    filteredArgs.push(args[i]);
                }
            }
            var callbackResponse = callback.apply(null, filteredArgs);
            this.callbacks[args[0]] = null;
            return callbackResponse;
        }
        return false;
    },
};

FilemanagerController.initialize();

export default FilemanagerController;

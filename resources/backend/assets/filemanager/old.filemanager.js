import Vue from 'vue';
import axios from 'axios';

var Filemanager = function() {
    var self = this;
    self.callbacks = self.getDefaultCallbacks();
    self.vue = null;
    self.fileSelector = '#uploadFile';
    self.directoriesSelector = '.directoriesTree';
    self.body = null;
    self.bodyClass = 'filemanagerOpen';
    self.data = null;
    self.settings = null;
    
    self.bindEvents();
};

Filemanager.prototype.getDefaultCallbacks = function() {
    return {
        onClose: null,
    };
};

Filemanager.prototype.open = function() {
    var self = this;
    
    //Try close if previous is already opened
    self.close();

    //Fetch files with ajax and render filemanager popup
    $.ajax({
        type: 'GET',
        url: URL.filemanager.show,
        dataType: 'html',
        success: function(html) {
            var $body = $('body');
            
            $body.append('<div id="overlay"></div>')
                .append(html)
                .addClass(self.bodyClass);

            self.data = FilemanagerData  || null;
            self.renderFiles();
        }
    });
};

Filemanager.prototype.close = function(files) {
    var self = this;

    //Run onClose callback
    this.runCallback('onClose', files);

    //Remove from DOM
    $('#filemanager').remove();

    //Remove class from <body> 
    $('body').removeClass(self.bodyClass);

    //Remove the overlay
    $('#overlay').remove();

    self.reset();
};

Filemanager.prototype.reset = function() {
    this.callbacks = this.getDefaultCallbacks();
};

Filemanager.prototype.bindEvents = function() {
    var self = this;
    $(document).on('change', self.fileSelector, function (e) {
        var $this = $(this);
        var file = $this[0].files[0];
        self.upload(file);
    });

    $(document).on('click', self.directoriesSelector +' .toggle', function (e) {
        $(this).parent('li').toggleClass('open');
    });
};

Filemanager.prototype.upload = function(file) {
    var formData = new FormData();
    formData.append('file', file, file.name);

    $.ajax({
        type: 'POST',
        url: URL.filemanager.upload,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        timeout: 60000,
        async: true,
        xhr: function() {
            var xhr = $.ajaxSettings.xhr();
            if (xhr.upload) {
                xhr.upload.addEventListener('progress', Filemanager.progressHandling, false);
            }
            return xhr;
        },
        success: function(data) {
            console.log('success');
            Filemanager.vue.insertRemoteData(data);
        },
        error: function(error) {
            console.log('error');
            console.log(error);
        },
    });
};

Filemanager.prototype.progressHandler = function(event) {
    var percent = 0;
    var position = event.loaded || event.position;
    var total = event.total;
    var progress_bar_id = "#progress-wrp";
    if (event.lengthComputable) {
        percent = Math.ceil(position / total * 100);
    }
    // update progressbars classes so it fits your code
    $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
    $(progress_bar_id + " .status").text(percent + "%");
};

Filemanager.prototype.getUserSetting = function(key, defaultValue) {
    var self = this;

    if (typeof self.data.settings[key] != 'undefined') {
        return self.data.settings[key];
    }

    return defaultValue || null;
};

Filemanager.prototype.setUserSetting = function(key, defaultValue) {
    var self = this;
    var key = 'filemanager_'+ key;
    if (typeof self.data.settings[key] != 'undefined') {
        return self.data.settings[key];
    }

    return defaultValue || null;
};

Filemanager.prototype.renderFiles = function(filesJson) {
        var self = this;
        
        self.vue = new Vue({
            el: '#filemanager',
            data: {
                views: {
                    media: true,
                    bulkEdit: false,
                },
                loading: false,
                currentDirectory: self.getUserSetting('currentDirectory', self.data.currentDirectory),
                fallbackFiles: self.data.collection.data,
                files: self.data.collection.data,
                collection: self.data.collection,
                columns: self.getUserSetting('columns', 6),
                filterFiles: [],
                storedPreviewFile: null,
                selectedFiles: [],
                searchField: '',
                searchInSameDirectory: false,
                pagination: {},
            },
            watch: {
                filterFiles: function(filterValues) {
                    if (filterValues.length > 0) {
                        var filteredFiles = [];
                        this.files = this.files.filter(function(file) {
                            return filterValues.indexOf(file.type) > -1;
                        });
                        return true;
                    }

                    this.files = self.data.files.data;
                },
            },
            computed: {
                previewFile: function() {
                    if (this.selectedFiles.length > 0) {
                        return this.selectedFiles[0];
                    }

                    return this.storedPreviewFile;
                },
                selectedFilesCount: function() {
                    return this.selectedFiles.length;
                }
            },
            methods: {
                changeView: function(view) {
                    var self = this;
                    if (this.views[view] == undefined) {
                        return false;
                    }

                    $.each(this.views, function(key, item) {
                        self.views[key] = false;
                    });

                    this.views[view] = true;
                },
                fileHover: function(file) {
                    if (this.selectedFiles.length > 0) {
                        return false;
                    }
                    
                    this.storedPreviewFile = file;
                },
                fileClicked: function(file) {
                    //Try remove file from selected files
                    if (this.removeFromSelectedFiles(file)) {
                        return true;
                    }
                    //Push the selected file to the selectedFiles array
                    this.selectedFiles.push(file);
                },
                removeFromSelectedFiles(file) {
                    for (var i in this.selectedFiles) {
                        if (this.selectedFiles[i].id == file.id) {
                            this.selectedFiles.splice(i, 1);
                            return true;
                        }
                    }
                    return false;
                },
                removeAllSelectedFiles: function() {
                    this.selectedFiles = [];
                },
                isFileSelected: function(file) {
                    return this.selectedFiles.filter(function(fileToCompare) {
                        return file.id == fileToCompare.id;
                    }).length > 0;
                },
                isCurrentDirectory: function(path) {
                    return path == this.currentDirectory;
                },
                prefixSize: function(size) {
                    var kb = Math.round(size / 1000);
                    if (kb > 1024) {
                        return (kb / 1000).toFixed(2) +'mb';
                    }

                    return kb +'kb';
                },
                removeFile: function(item) {
                    if (typeof item == 'number') {
                        this.files.splice(item, 1);
                        return true;
                    }
                    if (typeof item == 'object') {
                        for (var i in this.files) {
                            if (this.files[i].id == item.id) {
                                this.files.splice(i, 1);
                                return true;
                            }
                        }
                    }
                    
                    return false;
                },
                changeDirectory: function(path) {
                    const vm = this;
                    this.currentDirectory = path;
                    this.files = [];
                    this.loading = true;
                    axios.get(URL.filemanager.files, {
                        params: {
                          path: path,
                        }
                    })
                    .then(function(response) {
                        vm.loading = false;
                        //Store the current directory as a user setting
                        vm.storeUserSetting('currentDirectory', path);
                        //Insert new request
                        vm.insertRemoteData(response.data);
                    });
                },
                insertRemoteData: function(response, doNotChangeFallback) {
                    //Set fallback files
                    if (doNotChangeFallback !== true) {
                        this.fallbackFiles = response.data;
                    }

                    //Render new files
                    this.files = response.data;

                    //Render new pagination
                    this.pagination = {
                        current_page: response.current_page,
                        per_page: response.per_page,
                        total: response.total,
                    }
                },
                searchChanged: function() {
                    var keywords = this.getSearchKeywords();
                    if (keywords == '') {
                        //Reset files list with default files
                        this.fallback();
                    }
                },
                searchFiles: function(event) {
                    const vm = this;
                    var keywords = this.getSearchKeywords();
                    var directory = this.searchInSameDirectory ? this.currentDirectory : FilemanagerData.rootDirectory;
                    if (keywords != '') {
                        this.files = [];
                        this.loading = true;
                        axios.get(URL.filemanager.search, {
                            params: {
                              keywords: keywords,
                              directory: directory
                            }
                        })
                        .then(function(response) {
                            vm.loading = false;
                            //Update current collection
                            vm.insertRemoteData(response.data, true);
                        });
                    }
                },
                fallback: function() {
                    this.files = this.fallbackFiles;
                },
                save: function() {
                    this.close(this.selectedFiles);
                },
                close: function () {
                    self.close(this.selectedFiles);
                },
                storeUserSetting: function(key, value) {
                    axios.post(URL.filemanager.saveSetting, {
                        key: key,
                        value: value,
                    });
                },
                imageLoadFailed: function(file) {
                    //Remove file from view
                    this.removeFile(file)
                    //Remove file from index
                    axios.get(URL.filemanager.checkFiles, {
                        params: {
                          path: file.path,
                        }
                    });
                }
            }
        });
    }

Filemanager.prototype.runCallback = function() {
    var args = arguments;
    var callback = args[0];
    
    callback = typeof callback == 'string' ? this.callbacks[callback] : callback;
    if (typeof callback == 'function') {
        var filteredArgs = [];
        for (var i in args) {
            if (i > 0)  {
                filteredArgs.push(args[i]);
            }
        }
        return callback.apply(null, filteredArgs);
    }

    return false;
};

var FileDrop = {
    initialize: function($element, options) {
        var defaultOptions = {
            hoverClass: 'fileHover',
            onSuccess: null,
        };
        var options = $.extend({}, defaultOptions, options);
        FileDrop.bindEvents($element, options);
    },
    bindEvents: function($element, options) {
        $element.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
          e.preventDefault();
          e.stopPropagation();
        })
        .on('dragover dragenter', function() {
          $element.addClass(options.hoverClass);
        })
        .on('dragleave dragend drop', function() {
          $element.removeClass(options.hoverClass);
        })
        .on('drop', function(e) {
            FileDrop.runCallback(options.onSuccess, e.originalEvent.dataTransfer.files);
        });
    },
    runCallback: function() {
        var args = arguments;
        var callback = args[0];
        if (typeof callback == 'function') {
            var filteredArgs = [];
            for (var i in args) {
                if (i > 0)  {
                    filteredArgs.push(args[i]);
                }
            }
            return callback.apply(null, filteredArgs);
        }
        return false;
    },
};

module.exports = Filemanager;

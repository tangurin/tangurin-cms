import Vue from 'vue';
import Vuex from 'vuex';

import * as getters from './getters'
import * as actions from './actions'
import * as mutations from './mutations'

Vue.use(Vuex)

const defaultConfig = {
    selectMultiple: false,
    functions: {
        save: true,
        close: true,
    },
};

const state = {
    config: defaultConfig,
    defaultConfig: defaultConfig,
    isOpen: false,
    mainView: 'mediamanager',
    files: [],
    temporaryFiles: null,
    selectedFiles: [],
    directories: [],
    directory: '',
    latestFileHovered: null,
    uploaderVisible: false,
    uploadedFiles: [],
    overlayIsVisible: false,
    loaderIsVisible: false,
    popup: null,
    popupData: {},
    userPreferences: {},
};

const store = new Vuex.Store({
    state,
    getters,
    actions,
    mutations
})

if (module.hot) {
    module.hot.accept([
        './getters',
        './actions',
        './mutations'
    ], () => {
        store.hotUpdate({
            getters: require('./getters'),
            actions: require('./actions'),
            mutations: require('./mutations')
        });
    });
}

export default store;

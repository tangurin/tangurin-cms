import axios from 'axios';
import FilemanagerController from '../controller.js';

export const open = ({commit}, config = {}) => {
    commit('setConfiguration', config);
    commit('open');
}

export const close = ({commit, dispatch}) => {
    commit('close');
}

export const save = ({state, dispatch, commit}) => {
    var files = state.selectedFiles.length == 1 ? state.selectedFiles[0] : state.selectedFiles;
    FilemanagerController.runCallback('save', files)
    dispatch('close');
}

export const removeFile = ({state, commit, dispatch}, file) => {
    var fileIndex = state.files.findIndex(selectedFile => selectedFile.id == file.id);
    if (fileIndex > -1) {
        commit('removeFile', fileIndex);
        return true;
    }
}

export const deleteFile = ({state, commit, dispatch}, file) => {
    axios.delete(URL.filemanager.file, {
        data: {
            target: file.path,
        }
    }).then(response => {
        dispatch('deselectFile', file);
        dispatch('removeFile', file);
        swal('Borttagen', response.data, 'success');
    }).catch(error => {
        swal('Fel', error.response.data, 'error');
    });
}

export const selectFile = ({state, commit}, file) => {
    if (state.config.selectMultiple === false) {
        commit('emptySelectedFiles');
    }

    var fileIndex = state.files.findIndex(f => f.id == file.id);
    if (fileIndex > -1) {
        commit('selectFile', fileIndex);
    }
}

export const deselectFile = ({state, commit, dispatch}, file) => {
    var selectedFilesIndex = state.selectedFiles.findIndex(selectedFile => selectedFile.id == file.id);
    if (selectedFilesIndex > -1) {
        commit('deselectFile', selectedFilesIndex);
        return true;
    }
}

export const updateLatestFileHovered = ({state, commit}, file) => {
    var fileIndex = state.files.findIndex(f => f.id == file.id);
    if (fileIndex > -1) {
        commit('updateLatestFileHovered', fileIndex);
    }
}

export const removeLatestFileHovered = ({state, commit}, file) => {
    commit('removeLatestFileHovered');
}

export const searchFilesGlobally = ({commit}, keywords) => {

}

export const searchFilesLocally = ({commit, state, dispatch}, keywords) => {
    var keywords = keywords.toLowerCase();
    var filteredFiles = state.files.filter((file) => {
        return file.name.toLowerCase().includes(keywords) || file.extension.toLowerCase() == keywords;
    });

    commit('updateTemporaryFiles', filteredFiles);
}

export const fetchFiles = ({commit}, path) => {
    commit('showLoader');
    commit('updateDirectory', path);
    axios.get(URL.filemanager.file, {
        params: {
            path: path,
        }
    }).then(function (response) {
        commit('updateFiles', response.data.data);
        commit('hideLoader');
    });
}

export const fetchBootstrapData = ({commit}) => {
    commit('showLoader');
    axios.get(URL.filemanager.bootstrap, {
        params: {
        }
    }).then(function (response) {
        let data = response.data;
        commit('updateDirectory', data.directory);
        commit('updateDirectories', data.directories);
        commit('updateFiles', data.files);
        commit('updateUserPreferences', data.userPreferences);

        commit('hideLoader');
    });
}

export const updateDirectories = ({commit}) => {
    commit('showLoader');
    axios.get(URL.filemanager.directory, {
        params: {
        }
    }).then(function (response) {
        commit('updateDirectories', response.data);
        commit('hideLoader');
    });
}

export const showUploader = ({commit}) => {
    commit('showOverlay');
    commit('showUploader');
};

export const hideUploader = ({commit}) => {
    commit('hideOverlay');
    commit('hideUploader');
};

//Upload files
export const uploadFiles = ({dispatch, commit}, rawFiles) => {
    _.forEach(rawFiles, (rawFile, key) => {
        var file = {
            name: rawFile.name,
            oldName: '',
            size: rawFile.size,
            extension: rawFile.name.split('.').pop().toLowerCase(),
            mimetype: rawFile.type,
            type: _.split(rawFile.type, '/', 1)[0] || rawFile.type || null,
            isImage: rawFile.type == 'image',
            lastModified: rawFile.lastModified,
            lastModifiedDate: rawFile.lastModifiedDate,
            base64: null,
            icon: 'spinner',
            raw: rawFile,
            progress: 0,
            uploaded: false,
        };
        commit('pushUploadedFile', file);
        dispatch('runFileReader', file);
        dispatch('runFileUpload', file);
    });
};

export const runFileReader = ({dispatch}, file) => {
    if (file.base64 != null && file.isImage) {
        return true;
    }

    let filereader = new FileReader();

    filereader.onload = function(event) {
        file.base64 = event.target.result;
    };

    filereader.readAsDataURL(file.raw);
};

export const runFileUpload = ({dispatch, commit, state}, file) => {
    commit('showUploader');
    var formData = new FormData();
    formData.append('file', file.raw);
    formData.append('directory', state.directory);
    axios.post(URL.filemanager.upload, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        onUploadProgress: function(progressEvent) {
            var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
            file.progress = percentCompleted;
        },
    }).then((response) => {
        let createdFile = response.data.data;
        commit('pushFile', createdFile);
        file.uploaded = true;
        if (file.name != createdFile.filename) {
            file.oldName = file.name
            file.name = createdFile.filename;
        }
        file.icon = 'check';
    });
};

//Popups
export const hidePopup = ({commit}) => {
    commit('assignPopupData', {});
    commit('hidePopup');
    commit('hideOverlay');
}

export const showPopup = ({commit}, {target, data}) => {
    commit('showOverlay');
    commit('assignPopupData', data);
    commit('showPopup', target);
}

export const openCreateDirectory = ({dispatch}, data = {}) => {
    dispatch('showPopup', {
        target: 'createDirectory',
        data: data,
    });
};

export const openEditDirectory = ({dispatch}, data = {}) => {
    dispatch('showPopup', {
        target: 'editDirectory',
        data: data,
    });
};

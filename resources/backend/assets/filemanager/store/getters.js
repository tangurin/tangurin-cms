/* Bootstrap */
export const config = state => state.config;
export const isOpen = state => state.isOpen;
export const mainView = state => state.mainView;
export const files = state => state.files;
export const temporaryFiles = state => state.temporaryFiles;
export const directory = state => state.directory;
export const directories = state => state.directories;
export const userPreferences = state => state.userPreferences;

export const selectedFiles = state => state.selectedFiles;
export const latestFileHovered = state => state.latestFileHovered;
export const overlayIsVisible = state => state.overlayIsVisible;
export const loaderIsVisible = state => state.loaderIsVisible;
export const popup = state => state.popup;
export const popupData = state => state.popupData;

export const uploaderVisible = state => state.uploaderVisible;
export const uploadedFiles = state => state.uploadedFiles;

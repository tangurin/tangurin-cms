import _ from 'lodash';
export const setConfiguration = (state, config) => {
    state.config = _.merge(state.defaultConfig, config);
}

export const open = (state) => {
    state.isOpen = true;
    state.mainView = 'mediamanager';
    state.selectedFiles = [];
    state.temporaryFiles = null;
    state.latestFileHovered = null;
}
export const close = (state) => { state.isOpen = false; }

export const mainView = (state, mainView) => { state.mainView = mainView }
export const updateUserPreferences = (state, preferences) => { state.userPreferences = preferences }
export const updateFiles = (state, files) => { state.files = files }
export const pushFile = (state, file) => { state.files.push(file) }
export const removeFile = (state, fileIndex) => { state.files.splice(fileIndex, 1) }
export const updateTemporaryFiles = (state, files) => { state.temporaryFiles = files }
export const selectFile = (state, fileIndex) => { state.selectedFiles.push(state.files[fileIndex]) }
export const deselectFile = (state, fileIndex) => { state.selectedFiles.splice(fileIndex, 1) }
export const updateLatestFileHovered = (state, fileIndex) => { state.latestFileHovered = state.files[fileIndex] }
export const removeLatestFileHovered = (state) => { state.latestFileHovered = null }
export const updateDirectory = (state, path) => { state.directory = path }
export const updateDirectories = (state, directories) => { state.directories = directories }
export const emptySelectedFiles = (state) => { state.selectedFiles = [] }

export const showUploader = (state) => { state.uploaderVisible = true }
export const hideUploader = (state) => { state.uploaderVisible = false }
export const pushUploadedFile = (state, file) => { state.uploadedFiles.push(file) }

/* Overlay */
export const showOverlay = (state) => { state.overlayIsVisible = true }
export const hideOverlay = (state) => { state.overlayIsVisible = false }
/* Loader */
export const showLoader = (state) => { state.loaderIsVisible = true }
export const hideLoader = (state) => { state.loaderIsVisible = false }
/* Popup */
export const showPopup = (state, popupComponent) => { state.popup = popupComponent }
export const assignPopupData = (state, popupData = {}) => { state.popupData = popupData }
export const hidePopup = (state) => { state.popup = null }

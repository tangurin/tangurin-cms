@extends ('Backend::layout.layout', [
    'buttons' => [
        'create' => true,
    ]
])

@section('content')
    <div class="card-box">
        <div class="row">
            <div class="col-lg-12">
                @include('Backend::'. $page .'.datatable')
            </div>
        </div>
    </div>
@stop

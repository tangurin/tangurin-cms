@extends ('Backend::layout.layout')

@section ('content')
<div class="card-box">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>@lang('cms.slug')</label>
                <input type="text" name="slug" value="{{ $model->slug }}" class="form-control" placeholder="...">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>@lang('cms.target')</label>
                <input type="text" name="target" value="{{ $model->target }}" class="form-control" placeholder="http://...">
            </div>
        </div>
    </div>
</div>
@stop

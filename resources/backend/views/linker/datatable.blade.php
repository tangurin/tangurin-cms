<table class="dataTable table table-striped table-bordered">
    <thead>
        <tr>
            @section('thead')
                <th>{{ Lang::get('cms.id') }}</th>
                <th>{{ Lang::get('cms.slug') }}</th>
                <th>{{ Lang::get('cms.target') }}</th>
                <th>{{ Lang::get('cms.created') }}</th>
                <th>{{ Lang::get('cms.manage') }}</th>
            @show
        </tr>
    </thead>
    <tbody>
        @foreach ($collection as $row)
            <?php $editPath = $layout->updateable() ? route($route['edit'], ['id' => $row->id]) : '#'; ?>
            <tr>
                <td>
                    <a href="{{ $editPath }}">
                        {{ $row->id or '0' }}
                    </a>
                </td>
                <td>
                    <a href="{{ $editPath }}">
                        {{ $row->slug or '' }}
                    </a>
                </td>
                <td>
                    <a href="{{ $editPath }}">
                        {{ $row->target or '' }}
                    </a>
                </td>
                <td>
                    {{ date('Y-m-d', strtotime($row->created_at)) }}
                </td>
                <td class="manageCol">
                    @include ('Backend::shared.buttons._manageButtons')
                </td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            @yield('thead')
        </tr>
    </tfoot>
</table>

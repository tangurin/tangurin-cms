@extends ('Backend::layout.layout')

@section('content')
    <div class="card-box">
        <div id="filemanagerHolder">
            <filemanager></filemanager>
        </div>
    </div>

    <style>
        #filemanager {
            position: relative;
            top: 0;
            left: 0;
            transform: none;
            background: #fff;
            box-shadow: 0px 0px 7px 0px rgba(0, 0, 0, 0.3);
            box-sizing: border-box;
            border-radius: 5px;
            overflow: auto;
            max-width: none;
            max-height: none;
            min-height: 600px;
            height: 80vh;
        }
    </style>
@stop

@section ('foot')
<script>
    Filemanager.open({
        selectMultiple: true,
        functions: {
            save: false,
            close: false,
        },
    });
</script>
@stop

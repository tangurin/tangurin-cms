@extends('Backend::layout.layout')

@section('content')
<div class="card-box translations">
    <div class="row">
        {!! Form::open([
             'method' => 'PATCH',
             'action' => [$controller .'@update'],
             'data-remote' => 'AJAX',
         ]
        ) !!}
        <div class="easytabs">
            @include ('Backend::shared._languageTabs')
            <div class="tab-content"> 
                <?php $i = 0; ?>
                @foreach ($languageRepository->listAvailable() as $lang => $language)
                <div id="tabGroup{{ $lang }}" class="tab-pane @if ($i == 0)active @endif ">
                    <div class="row">
                        {!! $hierarchies[$lang]->generate() !!}
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg waves-effect waves-light">Spara</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script>
var stayIfInauthorized = true;
</script>
@stop

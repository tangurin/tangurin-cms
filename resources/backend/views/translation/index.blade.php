@extends('Backend::layout.layout')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h1>Sidor</h1>
                <div class="easytabs">
                    <ul class="nav nav-tabs tabs">
                        <li class="tab" class="active">
                            <a href="{{ action($controller .'@datatableView') }}" data-target="#datatable"><span>Lista</span></a>
                        </li>
                        <li class="tab">
                            <a href="{{ action($controller .'@sortableView') }}" data-target="#sortableList"><span>Sortering</span></a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="datatable" class="tab-pane active"></div>
                        <div id="sortableList" class="tab-pane"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-b-20">
        <div class="col-sm-12 text-right">
            <a href="{{ action($controller .'@create') }}" class="btn btn-primary btn-lg waves-effect waves-light">Skapa sida</a>
        </div>
    </div>
@stop

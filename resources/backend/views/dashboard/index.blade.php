@extends('Backend::layout.layout')

@section('content')

<filemanager></filemanager>

<div class="row">
    <div class="col-md-6 col-lg-3">
        <div class="widget-bg-color-icon card-box fadeInDown animated">
            <div class="bg-icon bg-icon-info pull-left">
                <i class="fa fa-briefcase text-info"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{ 0 }}</b></h3>
                <p class="text-muted">-------</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-md-6 col-lg-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-pink pull-left">
                <i class="fa fa-user text-pink"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{ 0 }}</b></h3>
                <p class="text-muted">-------</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-md-6 col-lg-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-success pull-left">
                <i class="md md-remove-red-eye text-success"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{ 0 }}</b></h3>
                <p class="text-muted">-------</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    
    <div class="col-md-6 col-lg-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-purple pull-left">
                <i class="fa fa-file-text-o text-purple"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{ 0 }}</b></h3>
                <p class="text-muted">-------</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

</div>
@stop

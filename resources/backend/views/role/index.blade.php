@extends('Backend::layout.layout')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                @include('Backend::'. $page .'.datatable')
            </div>
        </div>
    </div>
@stop

@extends('Backend::layout.layout')

@section('content')
<div class="card-box">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="name">@lang('cms.name')</label>
                <input type="text" name="name" class="form-control" value="{{ $model->name or '' }}" placeholder="@lang('cms.name')" />
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="identifier">@lang('cms.identifier')</label>
                <input type="text" name="identifier" class="form-control" value="{{ $model->identifier or '' }}" placeholder="@lang('cms.identifier')" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" style="display: {{ $model->is_user ? 'none' : 'block' }};">
            @include('Backend::shared._permissions', [
                'title' => __('cms.permissions'),
                'disabled' => $model->isAdmin(),
                'role' => $model,
            ])
        </div>
    </div>
</div>
@stop


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{ $metaTitle or 'Administration' }}</title>

        @include ('Backend::layout.head')

        @yield('head')
    </head>

    <body class="fixed-left">
        <div id="ajaxLoader">
            <i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
        </div>
        <div id="wrapper">
            <div class="topbar">
                @include ('Backend::layout.topBar')
            </div>

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <div id="sidebar-menu">
                        @include ('Backend::layout.menu')
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="content-page">
                <div class="content">
                    <div id="mainContent" class="container">
                        @if (Session::has('error'))
                        <div class="alert alert-danger">
                            <strong>{{ __('cms.error') }}:</strong>
                            {{ Session::get('error') }}
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-sm-6">
                                @include ('Backend::shared._title')
                                @include ('Backend::shared._breadcrumbs')
                            </div>
                            <div class="col-sm-6">
                                @if ($layout->isEdit())
                                    @include ('Backend::shared.buttons._cancelEditing')
                                @endif
                            </div>
                        </div>

                        @if ($layout->isForm())
                            @include ('Backend::shared._formOpen')
                        @endif
                        
                        @yield('content')

                        @if ($layout->isIndex() && $layout->createable())
                            <div class="pull-right">
                                @include ('Backend::shared.buttons._create')
                            </div>
                        @elseif ($layout->isForm())
                            @include ('Backend::shared._formBottomRow')
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>

                <footer class="footer">
                    <div class="pull-right text-right">
                        @yield ('bottomContent')
                        @if (!empty($layout->isForm()) && Route::has($route['index']))
                        <a href="{{ route($route['index']) }}" class="btn btn-inverse btn-sg waves-effect waves-light">@lang('cms.backToMenu')</a>
                        @endif
                    </div>
                </footer>
            </div>
        </div>
        
        @yield ('beforeFoot')
        @include ('Backend::layout.foot')
        @yield ('foot')
    </body>
</html>

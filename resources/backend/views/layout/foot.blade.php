<script type="text/javascript">
    var resizefunc = [];
    var CSRFTOKEN = '{{ csrf_token() }}';
    var URL = {
        base: '{{ url('') }}',
        admin: '{{ route('dashboard.index') }}',
        filemanager: {
            bootstrap: '{{ route('filemanager.bootstrap.index') }}',
            file: '{{ route('filemanager.file.index') }}',
            directory: '{{ route('filemanager.directory.index') }}',
            search: '{{ route('filemanager.file.index') }}',
            upload: '{{ route('filemanager.file.store') }}',
            saveSetting: '{{ route('filemanager.file.index') }}',
        }
    };
    var Language = {
        success: {
            success: 'Klart',
            saved: 'Sparat',
        },
        error: {
            error: 'Fel',
            notSaved: 'Datan sparades inte',
            unknownError: 'Okänt fel uppstod',
        }
    };
    var FlashDataJson = {!! json_encode([
        'success' => Session::has('success') ? Session::get('success') : null,
        'error' => Session::has('error') ? Session::get('error') : null,
        'info' => Session::has('info') ? Session::get('info') : null,
    ]) !!}
</script>

<script src="{{ backendBuild( env('APP_ENV') == 'local' ? 'theme.js' : 'theme.min.js') }}"></script>

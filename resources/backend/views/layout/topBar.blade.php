<!-- LOGO -->
<a href="{{ url('admin') }}" class="logo"><i class="fa fa-magic"></i> @lang('cms.administration')</span></a>

<!-- Button mobile view to collapse sidebar menu -->
<div class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="pull-left">
            <button class="button-menu-mobile open-left">
                <i class="ion-navicon"></i>
            </button>
            <span class="clearfix"></span>
        </div>

        {{-- <form role="search" class="navbar-left app-search pull-left hidden-xs">
             <input type="text" placeholder="Search..." class="form-control">
             <a href=""><i class="fa fa-search"></i></a>
        </form> --}}


        <ul class="nav navbar-nav navbar-right pull-right">
            @include('Backend::layout.notifications')
            <li>
                <a href="{{ route('setting.edit') }}" class="waves-effect waves-light" title="@lang('cms.settings')"><i class="fa fa-cog"></i></a>
            </li>
            <li>
                <a href="{{ url('') }}" target="_blank" title="@lang('cms.openesInNewTab')"> <i class="fa fa-columns"></i> </a>
            </li>
            <li>
                <a href="{{ route('backend.auth.logout') }}" title="@lang('cms.logout')"> @lang('cms.logout') </a>
            </li>
        </ul>
        <!--/.nav-collapse -->
    </div>
</div>

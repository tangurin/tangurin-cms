<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="CMS - With Limitless developing">
        <meta name="author" content="Tangurin">
        <title>{{ !empty($headTitle) ? $headTitle : 'Tangurin - Administration' }}</title>
        <link href="{{ backendBuild( env('APP_ENV') == 'local' ? 'login.css' : 'login.min.css' ) }}" rel="stylesheet">
        @yield('head')
    </head>

    <body>
        @yield ('content')
        
        @yield ('foot')
        <script src="{{ backendBuild( env('APP_ENV') == 'local' ? 'login.js' : 'login.min.js') }}"></script>
    </body>
</html>

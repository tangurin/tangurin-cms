<ul class="treeMenu">
    <li class="text-muted menu-title">@lang('cms.navigation')</li>

    @can('backend_view_page')
    <li>
        <a href="{{ route('page.index') }}" class="waves-effect"><i class="fa fa-files-o"></i> <span> @lang('cms.menu_pages') </span> </a>
    </li>
    @endcan

    <li class="has_sub">
        <a href="#" class="waves-effect"><i class="fa fa-list"></i> <span> Innehåll </span> </a>
        <ul class="list-unstyled">
            @can('backend_view_text')
            <!-- TEXTS -->
            <li><a href="{{ route('text.index') }}"><i class="fa fa-text-width"></i> <span>@lang('cms.menu_texts')</span></a></li>
            @endcan
            @can('backend_view_translation')
            <!-- TRANSLATIONS -->
            <li><a href="{{ route('translation.edit') }}"><i class="fa fa-language"></i> @lang('cms.menu_translations')</a></li>
            @endcan
            @can('backend_view_mediamanager')
            <!-- TRANSLATIONS -->
            <li><a href="{{ route('mediamanager.index') }}"><i class="fa fa-image"></i> Mediahanterare</a></li>
            @endcan
            {{-- @can('backend_view_slideshow')
            <!-- SLIDESHOWS -->
            <li><a href="{{ route('slideshow.index') }}"><i class="fa fa-picture-o"></i> @lang('cms.menu_slideshows')</a></li>
            @endcan --}}
        </ul>
    </li>

    @can('backend_view_user')
    <li class="has_sub">
        <a href="{{ route('user.index') }}"><i class="fa fa-user"></i> <span> @lang('cms.menu_users') </span></a>
        <ul class="list-unstyled">
            <li><a href="{{ route('user.index') }}"><i class="fa fa-eye"></i> @lang('cms.show_all') @lang('cms.users')</a></li>
            <li><a href="{{ route('user.create') }}"><i class="fa fa-plus"></i> @lang('cms.create') @lang('cms.user')</a></li>
            @can('backend_view_role')
            <li class="has_sub">
                <a href="javascript: void(0)"><i class="fa fa-lock"></i> <span> @lang('cms.menu_rolesandpermissions') </span></a>
                <ul class="list-unstyled">
                    <li><a href="{{ route('role.index') }}"><i class="fa fa-eye"></i> @lang('cms.menu_roles')</a></li>
                    <li><a href="{{ route('role.create') }}"><i class="fa fa-plus"></i> @lang('cms.create') @lang('cms.role')</a></li>
                </ul>
            </li>
            @endcan
        </ul>
    </li>
    @endcan

    @can('backend_view_customfields')
    <li>
        <a href="{{ route('backend.customfields.index') }}"><i class="fa fa-star"></i> <span> Customfields </span></a>
    </li>
    @endcan

    <!-- SETTINGS -->
    <li class="has_sub">
        <a href="#"><i class="fa fa-cogs"></i> <span>@lang('cms.menu_system')</span></a>
        <ul>
            @can('backend_view_setting')
            <!-- SETTINGS -->
            <li><a href="{{ route('setting.edit') }}"><i class="fa fa-cog"></i> @lang('cms.menu_settings')</a></li>
            @endcan

            <!-- LOCATIONS -->
            @if (Gate::check('backend_view_country') || Gate::check('backend_view_county') || Gate::check('backend_view_city'))
            <li>
                <a href="javascript: void(0)"><i class="fa fa-globe"></i> <span>@lang('cms.menu_location')</span></a>
                <ul>
                    @can('backend_view_country')
                    <li><a href="{{ route('country.index') }}"> <span>@lang('cms.menu_location_country')</span></a></li>
                    @endcan
                    
                    @can('backend_view_county')
                    <li><a href="{{ route('county.index') }}"> <span>@lang('cms.menu_location_county')</span></a></li>
                    @endcan

                    @can('backend_view_city')
                    <li><a href="{{ route('city.index') }}"> <span>@lang('cms.menu_location_cities')</span></a></li>
                    @endcan
                </ul>
            </li>
            @endif

            @can('backend_view_linker')
            <!-- LINKER -->
            <li><a href="{{ route('linker.index') }}"><i class="fa fa-link"></i> @lang('cms.menu_linker')</a></li>
            @endcan
        </ul>
    </li>

    <?php $customfieldsModules = Customfields\CollectionModule::getAllUsedModules(); ?>
    @if (!empty($customfieldsModules))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect">
            <span><i class="fa fa-plug"></i> Moduler</span>
            <span class="pull-right"><i class="md md-add"></i></span>
        </a>
        <ul class="list-unstyled">
            @if (!empty($customfieldsModules))
                <li>
                    <a href="javascript:void(0);" class="waves-effect subDrop"><i class="fa fa-sitemap"></i> <span>Anpassade fält</span></a>
                    <ul class="list-unstyled">
                    @foreach ($customfieldsModules as $key => $module)
                        <li>
                            <a href="{{ route('backend.customfields.module', [$module['slug']]) }}"><i class="fa fa-square"></i> <span>{{ $module['label'] }}</span></a>
                        </li>
                    @endforeach
                    </ul>
                </li>
            @endif
        </ul>
    </li>
    @endif
</ul>

<ul class="sessionCreated m-t-40" style="display: none;">
    <li class="text-muted menu-title">@lang('cms.createdThisSession')</li>
</ul>

@extends ('Backend::layout.layout', [
    'buttons' => [
        'create' => true,
    ]
])

@section('content')
    <div class="card-box">
        <div class="easytabs">
            <ul class="nav nav-tabs tabs">
                <li class="tab">
                    <a href="{{ route($route['base'] .'.sortable') }}" data-target="#sortableList"><span>Sortering</span></a>
                </li>
                <li class="tab">
                    <a href="{{ route($route['base'] .'.datatable') }}" data-target="#datatable"><span>Tabell</span></a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="sortableList" class="tab-pane"></div>
                <div id="datatable" class="tab-pane"></div>
            </div>
        </div>
    </div>
@stop

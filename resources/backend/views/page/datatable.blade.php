<table class="dataTable table table-striped table-bordered">
    <thead>
        <tr>
            @section('thead')
                <th>{{ Lang::get('cms.id') }}</th>
                <th>{{ Lang::get('cms.title') }}</th>
                <th>{{ Lang::get('cms.slug') }}</th>
                <th>{{ Lang::get('cms.created') }}</th>
                <th>{{ Lang::get('cms.last_change') }}</th>
                <th>{{ Lang::get('cms.manage') }}</th>
            @show
        </tr>
    </thead>
    <tbody>
        @foreach ($collection as $row)
            <?php $editPath = $layout->updateable() ? route($route['edit'], ['id' => $row->id]) : '#'; ?>
            <tr <?=$row->isStartpage() ? 'class="rowStartpage"' : ''; ?>>
                <td>
                    <a href="{{ $editPath }}">
                        {{ $row->id or '0' }}
                    </a>
                </td>
                <td>
                    <a href="{{ $editPath }}">
                        {{ $row->translation->title or '' }}
                    </a>
                </td>
                <td>
                    <a href="{{ $editPath }}">
                        {{ $row->translation->slug or '' }}
                    </a>
                </td>
                <td>
                    {{ date('Y-m-d', strtotime($row->created_at)) }}
                </td>
                <td>
                    {{ date('Y-m-d', strtotime($row->updated_at)) }}
                </td>
                <td class="manageCol">
                    @include ('Backend::shared.buttons._manageButtons')
                    {!! button(['type' => 'info', 'href' => url($row->translation->slug), 'target' => '_blank']) !!}
                </td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            @yield('thead')
        </tr>
    </tfoot>
</table>

<div class="custom-dd-empty dd sortable" id="nestable_list_3">
    {!! $tree !!}
    
    {!! Form::open ([
            'method' => 'POST',
            'action' => $controller .'@saveSort',
            'data-remote' => 'AJAX'
        ]
    ) !!}

    <input type="hidden" name="sort" class="sortJSON" value="" />
    <button type="submit" class="btn btn-success btn-sg waves-effect waves-light m-t-20">Spara sortering</button>

    {!! Form::close() !!}
</div>

@extends ('Backend::layout.layout')

@section ('content')
<div class="card-box">
    <div class="row">
        <div class="easytabs">
            @include ('Backend::shared._languageTabs')
            <div class="tab-content"> 
                @foreach ($languageRepository->listAvailable() as $lang => $language)
                    <div id="tabGroup{{ $lang }}" class="tab-pane">
                        <input type="hidden" name="data[{{ $lang }}][id]" value="{{ $model->translation($lang)->id or 0 }}" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="data[{{ $lang }}][title]" class="form-control" value="{{ $model->translation($lang)->title or '' }}" placeholder="@lang('cms.title')" />
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="data[{{ $lang }}][slug]" class="form-control {{ $lang }}Slug" value="{{ $model->translation($lang)->slug or '' }}" placeholder="@lang('cms.slug')" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea name="data[{{ $lang }}][content]" class="wysiwyg">{{ $model->translation($lang)->content or '' }}</textarea>
                                </div>
                            </div>
                        </div>
            
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="data[{{ $lang }}][seo_title]" class="form-control" value="{{ $model->translation($lang)->seo_title or '' }}" placeholder="@lang('cms.seoTitle')" />
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="data[{{ $lang }}][seo_keywords]" class="form-control" value="{{ $model->translation($lang)->seo_keywords or '' }}" placeholder="@lang('cms.seoKeywords')" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea name="data[{{ $lang }}][seo_description]" class="form-control" placeholder="@lang('cms.seoDescription')">{{ $model->translation($lang)->seo_description or '' }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div> 
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::select('template', $pageRepository->getTemplates(), $model->template, ['class' => 'form-control', 'data-placeholder' => 'Utseende']) !!}
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    {!! $parents->generate() !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::select('in_menu', [1 => 'Visa i menyn', 0 => 'Visa inte i menyn'], $model->in_menu, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                @if (isset($generatedCustomfieldsView))
                    {!! $generatedCustomfieldsView !!}
                @else
                    <div class="alert alert-info">
                        <strong>OBS!</strong> Du måste skapa sidan innan du kan redigera anpassade fält.
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>


@include('Backend::shared._filemanager')

@stop

@section ('formBottomRow')
    @if ($layout->isEdit())
        <a href="{{ url($model->translation->slug) }}" class="btn btn-lg waves-effect waves-light btn-info" target="_blank">@lang('cms.show') @lang('cms.page')</a>
    @endif
@stop

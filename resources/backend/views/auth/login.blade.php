@extends('Backend::layout.authLayout')

@section('content')
<div class="wrapper-page">
    <div class=" card-box">
        <div class="panel-heading"> 
            <h3 class="text-center">@lang('cms.login')  </h3>
        </div> 

        <div class="panel-body">
            <form class="form-horizontal m-t-20" role="form" method="POST" action="{{ route('backend.auth.login') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="E-mail">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input id="password" type="password" class="form-control" name="password" required placeholder="Password">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-primary">
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="checkbox-signup">
                            <label for="checkbox-signup">
                                Remember me
                            </label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-block text-uppercase waves-effect waves-light" type="submit">@lang('cms.login')</button>
                    </div>
                </div>

                <div class="form-group m-t-30 m-b-0">
                    <div class="col-sm-12">
                        <a class="text-dark" href="{{ action('Auth\ForgotPasswordController@showResetForm') }}">
                            <i class="fa fa-lock m-r-5"></i> Forgot your password?
                        </a>
                    </div>
                </div>
            </form> 
        </div>
    </div>
</div>
@endsection

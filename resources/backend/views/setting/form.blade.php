@extends('Backend::layout.layout')

@section('content')
{!! Form::open([
        'method' => 'PATCH',
        'route' => $route['update'],
        'data-remote' => 'AJAX',
    ]
) !!}
    <div class="row">
        <div class="col-lg-4 col-md-6">
            <div class="card-box">
                <h3>@lang('settings.group.theCompany')</h3>
                <div class="form-group">
                    <label>@lang('settings.label.logo')</label>
                    {!! filemanager([
                        'name' => 'settings[logo]',
                        'files' => media($collection['logo']),
                    ]) !!}
                </div>

                <div class="form-group">
                    <label>@lang('settings.label.companyName')</label>
                    <input type="text" name="settings[companyName]" value="{{ $collection['companyName'] or '' }}" class="form-control">
                </div>
                <div class="form-group">
                    <label>@lang('settings.label.fullCompanyName')</label>
                    <input type="text" name="settings[fullCompanyName]" value="{{ $collection['fullCompanyName'] or '' }}" class="form-control">
                </div>
                <div class="form-group">
                    <label>@lang('settings.label.companyCorporateNr')</label>
                    <input type="text" name="settings[companyCorporateNr]" value="{{ $collection['companyCorporateNr'] or '' }}" class="form-control">
                </div>
                <div class="form-group">
                    <label>@lang('settings.label.companyPhone')</label>
                    <input type="text" name="settings[companyPhone]" value="{{ $collection['companyPhone'] or '' }}" class="form-control">
                </div>
                <div class="form-group">
                    <label>@lang('settings.label.companyZipcode')</label>
                    <input type="text" name="settings[companyZipcode]" value="{{ $collection['companyZipcode'] or '' }}" class="form-control">
                </div>
                <div class="form-group">
                    <label>@lang('settings.label.companyCity')</label>
                    <input type="text" name="settings[companyCity]" value="{{ $collection['companyCity'] or '' }}" class="form-control">
                </div>
                <div class="form-group">
                    <label>@lang('settings.label.companyAddress')</label>
                    <input type="text" name="settings[companyAddress]" value="{{ $collection['companyAddress'] or '' }}" class="form-control">
                </div>
            </div>
            <div class="card-box">
                <h3>@lang('settings.group.SEO')</h3>
                <div class="form-group">
                    <label>@lang('settings.label.defaultMetaDescription') <small>(Fallback)</small></label>
                    <textarea name="settings[defaultMetaDescription]" class="form-control">{{ $collection['defaultMetaDescription'] or '' }}</textarea>
                </div>
                <div class="form-group">
                    <label>@lang('settings.label.defaultMetaKeywords') <small>(Fallback)</small></label>
                    <input type="text" name="settings[defaultMetaKeywords]" value="{{ $collection['defaultMetaKeywords'] or '' }}" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="card-box">
                <h3>@lang('settings.group.email')</h3>
                <div class="form-group">
                    <label>@lang('settings.label.mailOut')</label>
                    <input type="text" name="settings[mailOut]" value="{{ $collection['mailOut'] or '' }}" class="form-control">
                </div>
                <div class="form-group">
                    <label>@lang('settings.label.mailIn')</label>
                    <input type="text" name="settings[mailIn]" value="{{ $collection['mailIn'] or '' }}" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="card-box">
                <h3>@lang('settings.group.user')</h3>
                <div class="form-group">
                    <label>@lang('settings.label.mailOut')</label>
                    {!! Form::select(
                        'settings[defaultUserRole]', 
                        App\Models\Role::all()->except(1)->pluck('name', 'id'),
                        $collection['defaultUserRole'] ?? null,
                        ['class' => 'form-control'])
                    !!}
                </div>
                <div class="form-group">
                    <label>@lang('settings.label.activateUserOnRegistration')</label>
                    {!! Form::select(
                        'settings[activateUserOnRegistration]', 
                        [1  => 'Ja', 0 => 'Nej',],
                        $collection['activateUserOnRegistration'] ?? null,
                        ['class' => 'form-control'])
                    !!}
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="card-box">
                <h3>@lang('settings.group.API')</h3>
                <div class="form-group">
                    <label>@lang('settings.label.googleMaps')</label>
                    <input type="text" name="settings[googleMaps]" value="{{ $collection['googleMaps'] or '' }}" class="form-control">
                </div>
                <div class="form-group">
                    <label>@lang('settings.label.googleAnalytics')</label>
                    <input type="text" name="settings[googleAnalytics]" value="{{ $collection['googleAnalytics'] or '' }}" class="form-control">
                </div>
                <div class="form-group">
                    <label>@lang('settings.label.googleTagManager')</label>
                    <input type="text" name="settings[googleTagManager]" value="{{ $collection['googleTagManager'] or '' }}" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="card-box">
                <h3>@lang('settings.group.socialMedias')</h3>
                <div class="form-group">
                    <label>@lang('settings.label.facebookLink')</label>
                    <input type="text" name="settings[facebookLink]" value="{{ $collection['facebookLink'] or '' }}" class="form-control">
                </div>
                <div class="form-group">
                    <label>@lang('settings.label.instagramLink')</label>
                    <input type="text" name="settings[instagramLink]" value="{{ $collection['instagramLink'] or '' }}" class="form-control">
                </div>
                <div class="form-group">
                    <label>@lang('settings.label.twitterLink')</label>
                    <input type="text" name="settings[twitterLink]" value="{{ $collection['twitterLink'] or '' }}" class="form-control">
                </div>
                <div class="form-group">
                    <label>@lang('settings.label.linkedinLink')</label>
                    <input type="text" name="settings[linkedinLink]" value="{{ $collection['linkedinLink'] or '' }}" class="form-control">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="card-box">
            @include ('Backend::shared.buttons._save')
        </div>
    </div>
{!! Form::close() !!}
@stop

@section('foot')
<script>
var stayIfInauthorized = true;
</script>
@stop

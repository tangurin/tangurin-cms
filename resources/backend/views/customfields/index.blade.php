@extends ('Backend::layout.layout')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <a href="{{ route('backend.customfields.create') }}" class="btn btn-default">
                <i class="glyphicon glyphicon-plus"></i>
            </a>
            <a href="{{ route('backend.customfields.modules') }}" class="btn btn-default" style="float: right;">
                Moduler
            </a>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="card-box">
            @if (!empty($collections))
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Kollektion</th>
                        <th>identifierare</th>
                        <th>Skapad</th>
                    </tr>
                </thead>
                <tbody class="fieldRows sortable">
                    @foreach ($collections as $collection)
                        <?php $link = '/admin/customfields/edit/'. $collection->id; ?>
                        <tr data-id="{{ $collection->id }}">
                            <td><a href="{{ $link }}">{{ !empty($collection->name) ? $collection->name : '---' }}</a></td>
                            <td><a href="{{ $link }}">{{ !empty($collection->identifier) ? $collection->identifier : '---' }}</a></td>
                            <td><a href="{{ $link }}">{{ !empty($collection->createdAt) ? date('Y-m-d', $collection->createdAt) : '---' }}</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            Du har inte skapat några kollektioner än.
            @endif
        </div>
    </div>
</div>

<script type="text/javascript">
    var sortUrl = '{{ route('backend.customfields.sortCollections') }}';
</script>
@stop

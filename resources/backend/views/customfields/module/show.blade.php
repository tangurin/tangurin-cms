@extends('Backend::layout.layout')

@section('content')
<div class="card-box">
    <h1>{{ $module->label }}</h1>
    <hr>
    <form action="" method="post">
        {!! $generatedCustomfieldsView !!}
        <br />
        <button type="submit" class="btn w-sm btn-default waves-effect waves-light pull-right">@lang('cms.save')</button>
        <div class="clearfix"></div>
    </form>

</div>
@stop

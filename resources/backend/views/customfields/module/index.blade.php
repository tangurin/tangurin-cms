@extends('Backend::layout.layout')

@section('content')
  <div class="col-sm-12">
    <div class="card-box">
      <div class="row">
      <h3>Moduler</h3>
        @if (!empty($modules))
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Label</th>
                    <th>Slug</th>
                </tr>
            </thead>
            <tbody class="fieldRows sortable">
                @foreach ($modules as $module)
                    <?php $link = route('backend.customfields.module.edit', $module['id']); ?>
                    <tr>
                        <td><a href="{{ $link }}">{{ $module['label'] }}</a></td>
                        <td><a href="{{ $link }}">{{ $module['slug'] }}</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @else
        Du har inte skapat några moduler än.
        @endif
      </div>
    </div>
  </div>

<form method="post" action="" enctype="multipart/form-data" class="moduleForm">
  {!! Form::token() !!}
  <div class="col-sm-12">
    <div class="card-box">
      <div class="row">
        @if ($edit)
        <h3>Ändra modul</h3>
        @else
        <h3>Skapa modul</h3>
        @endif
        <div class="form-group">
          <input type="text" name="label" value="{{ $label }}" placeholder="Modulnamn..." class="form-control">
        </div>
        <div class="form-group">
          <input type="text" name="slug" value="{{ $slug }}" placeholder="Modulslug..." class="form-control">
        </div>
        <div class="form-group">
          <button type="submit" class="btn w-sm btn-default waves-effect waves-light">@lang('cms.save')</button>
          @if ($edit)
          <a href="{{ route('backend.customfields.module.delete', [$id]) }}" class="btn w-sm btn-danger waves-effect waves-light">{{ trans('Ta bort') }}</a>
          @endif
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  $(function() {
    $('.moduleForm').on('submit', function(e) {
      var isEmpty = false;
      $('input.form-control', $(this)).each(function() {
        var value = $(this).val();
        if ($.trim(value) == '') {
          isEmpty = true;
        }
      });
      if (isEmpty) {
        e.preventDefault();
        alert('Alla fält måste vara ifyllda.');
      }
    });
  });
</script>
@stop

<script type="text/javascript">
    var templateJSON = <?=$templateJSON?>;
    var allModulesJSON = <?=$allModules?>;
    var selectedModuleId = <?=isset($collection->moduleId) ? $collection->moduleId : 0?>;
</script>

<!-- Relation templates -->

<script id="relationClassSelectTemplate" type="text/x-handlebars-template">
    <select name="relation[class][]" class="classSelect form-control">
        {{#each options}}
        <option value="{{value}}">{{label}}</option>
        {{/each}}
    </select>
</script>

<script id="relationRelationSelectTemplate" type="text/x-handlebars-template">
    <select name="relation[relation][]" class="relationSelect form-control">
        {{#each options}}
        <option value="{{value}}">{{label}}</option>
        {{/each}}
    </select>
</script>

<script id="relationRelationIdSelectTemplate" type="text/x-handlebars-template">
    <select name="relation[relationId][]" class="relationIdSelect form-control">
        {{#each options}}
        <option value="{{id}}">{{padding}} {{name}}</option>
        {{/each}}
    </select>
</script>

<script id="relationRemoveButtonTemplate" type="text/x-handlebars-template">
    <a class="btn btn-primary duplicateRow"><i class="fa fa-clone"></i></a>
    <a class="btn btn-danger removeRow"><i class="glyphicon glyphicon-trash"></i></a>
</script>

<!-- Field templates -->
<script id="rowTemplate" type="text/x-handlebars-template">
    <li class="dd-item dd3-item fieldId-{{fieldId}}" data-fieldId="{{fieldId}}">
        <div class="handle dd-handle dd3-handle"></div>
        <div class="dd3-content">
            <div class="topRow">
                <label class="col-lg-6">
                    <span class="fieldLabel ">Titel</span>
                    <input type="text" class="form-control nameInput" name="field[{{fieldParentId}}][{{fieldId}}][name]" value="" />
                </label>
                <label class="col-lg-6">
                    <span class="fieldLabel">Identifierare</span>
                    <input type="text" class="form-control identifierInput" name="field[{{fieldParentId}}][{{fieldId}}][identifier]" value="" />
                </label>
                <div class="clearfix"></div>
                <div class="fieldControl">
                    <a class="btn btn-primary cloneFieldButton" tabindex="-1"><i class="fa fa-clone"></i></a>
                    <a class="btn btn-danger removeFieldButton" tabindex="-1"><i class="glyphicon glyphicon-trash"></i></a>
                    <a class="btn toggleButton" data-wrapper=".dd3-content" tabindex="-1"><span class="toggleIcon"><i class="md md-unfold-less"></i></span></a>
                </div>
            </div>
            <div class="subFieldContainer">
                <div class="toggleDrop" style="margin-top: 5px;">
                    <label class="col-lg-12">
                        <span class="fieldLabel">Beskrivning</span>
                        <textarea class="form-control descriptionTextarea" name="field[{{fieldParentId}}][{{fieldId}}][description]" rows="2" style="min-height: 0;"></textarea>
                    </label>
                    <label class="col-lg-6">
                        <span class="fieldLabel hideIfRepeater-{{fieldId}}">Flerspråkigt</span>
                        <select name="field[{{fieldParentId}}][{{fieldId}}][multilanguage]" class="multilanguageSelect hideIfRepeater-{{fieldId}} form-control">
                            <option value="1">Ja</option>
                            <option value="0">Nej</option>
                        </select>
                    </label>
                    <label class="col-lg-6">
                        <span class="fieldLabel">Fälttyp</span>
                        <select name="field[{{fieldParentId}}][{{fieldId}}][type]" class="fieldTypeSelect fieldTypeSelect-{{fieldId}} form-control" data-fieldid="{{fieldId}}">
                            <?php foreach ($fieldTypes as $value => $label) : ?>
                            <option value="<?=$value?>"><?=$label?></option>
                            <?php endforeach;?>
                        </select>
                    </label>
                    <div class="clearfix"></div>
                    <div class="dataWrapper hideIfRepeater-{{fieldId}}"{{#if noDataWrapper}} style="display: none;"{{/if}}>
                        <div class="col-lg-12 noFloat">
                            <h4 class="toggleButton" data-wrapper=".dataWrapper">Fältdata<span class="toggleIcon"><i class="md md-unfold-less"></i></span></h4>
                        </div>
                        <div class="dataHolder toggleDrop">
                            <div class="col-lg-12">
                                <a class="btn btn-default addDataButton" data-fieldparentid="{{fieldParentId}}" data-fieldid="{{fieldId}}"><i class="glyphicon glyphicon-plus"></i></a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="dataRows dataRows-{{fieldId}}">
                                
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="settingsWrapper">
                        <div class="col-lg-12 noFloat">
                            <h4 class="toggleButton" data-wrapper=".settingsWrapper">Fältinställningar<span class="toggleIcon"><i class="md md-unfold-less"></i></span></h4>
                        </div>
                        <div class="settingsHolder toggleDrop">
                            <label class="col-lg-5">
                                <span class="fieldLabel">Applicera inställning</span>
                                <select class="settingsSelect settingsSelect-{{fieldId}} form-control">
                                </select>
                            </label>
                            <div class="col-lg-5">
                                <a class="btn btn-default addSettingButton" data-fieldparentid="{{fieldParentId}}" data-fieldid="{{fieldId}}" data-rowswrapper=".settingsRows-{{fieldId}}"><i class="glyphicon glyphicon-plus"></i></a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="settingsRows settingsRows-{{fieldId}}">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="repeaterWrapper repeaterWrapper-{{fieldId}}" data-fieldparentid="{{fieldParentId}}" style="display: none;">
                    
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </li>
</script>

<script id="dataRowTemplate" type="text/x-handlebars-template">
    <div class="dataRow unique-{{unique}}">
        <label class="col-lg-5">
            <span class="fieldLabel">Värde</span>
            <input type="text" name="field[{{fieldParentId}}][{{fieldId}}][data][{{unique}}][dataValue]" class="dataValueField form-control unique-{{unique}}" value="{{value}}" />
        </label>
        <label class="col-lg-5">
            <span class="fieldLabel">Label</span>
            <input type="text" name="field[{{fieldParentId}}][{{fieldId}}][data][{{unique}}][dataLabel]" class="dataLabelField form-control unique-{{unique}}" value="{{label}}" />
        </label>
        <div class="col-lg-2">
            <a class="btn btn-primary cloneDataButton" data-unique="{{unique}}" data-fieldparentid="{{fieldParentId}}" data-fieldid="{{fieldId}}"><i class="fa fa-clone"></i></a>
            <a class="btn btn-danger removeDataButton" data-unique="{{unique}}" data-fieldparentid="{{fieldParentId}}" data-fieldid="{{fieldId}}"><i class="glyphicon glyphicon-trash"></i></a>
            <a class="btn btn-default moveDataButton handle"><i class="fa fa-arrows-v"></i></a>
        </div>
    </div>
</script>

<script id="settingsRowTemplate" type="text/x-handlebars-template">
    <div class="settingRow settingRow-{{fieldId}}-{{key}}">
        <label class="col-lg-5">
            <span class="fieldLabel">Inställning</span>
            <input type="text" class="settingsLabelField form-control unique-{{unique}}" value="{{label}}" readonly />
        </label>
        <label class="col-lg-5">
            <span class="fieldLabel">Värde</span>
            <input type="text" name="field[{{fieldParentId}}][{{fieldId}}][settings][{{key}}]" value="{{value}}" class="settingsValueField settingsValueField-{{key}}-{{fieldId}} form-control unique-{{unique}}" data-key="{{key}}" data-fieldid="{{fieldId}}" readonly />
        </label>
        <div class="col-lg-2">
            <a class="btn btn-danger removeSettingButton" data-unique="{{unique}}" data-fieldid="{{fieldId}}" data-key="{{key}}"><i class="glyphicon glyphicon-trash"></i></a>
        </div>
    </div>
</script>

<script id="repeaterTemplate" type="text/x-handlebars-template">
    <div class="repeaterTemplate repeaterTemplate-{{fieldId}}">
        <a class="btn btn-default addField m-b-20 pull-right" data-wrapper=".repeaterWrapper-{{fieldId}}"><i class="glyphicon glyphicon-plus"></i> Nytt fält</a>
        <div class="clearfix"></div>
        <div class="row">
            <label class="col-lg-6">
                <span class="fieldLabel">Minst antal repetitioner</span>
                <input type="number" name="field[{{fieldParentId}}][{{fieldId}}][repeaterMin]" value="{{repeaterMin}}" min="0" class="repeaterMinField form-control" />
            </label>
            <label class="col-lg-6">
                <span class="fieldLabel">Max antal repetitioner (0 = &infin;)</span>
                <input type="number" name="field[{{fieldParentId}}][{{fieldId}}][repeaterMax]" value="{{repeaterMax}}" min="0" class="repeaterMaxField form-control" />
            </label>
            <div class="clearfix"></div>
        </div>

        <ol class="repeaterOl fieldHolder fieldHolder-{{fieldId}} sortable" data-fieldid="{{fieldId}}">
            
        </ol>
    </div>
</script>

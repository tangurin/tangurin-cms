<div class="row">
    <div class="relationsContent">
        <div class="col-lg-12">
            <h2 class="m-t-20 header-title pull-left m-b-30">Relationer</h2>
            <a href="javascript:void(0);" class="btn btn-default addRelation m-t-20 pull-right">
                <i class="glyphicon glyphicon-plus"></i> Ny relation
            </a>
            <div style="clear: both;"></div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Relationstyp</th>
                        <th>Relation</th>
                        <th>Mål</th>
                        <th>Hantera</th>
                    </tr>
                </thead>
                <tbody class="relationRows">
                </tbody>
            </table>
        </div>
    </div>
</div>

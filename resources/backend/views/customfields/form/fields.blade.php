<div id="fieldHandler">
    <h2 class="m-t-20 header-title pull-left m-b-30">Fält</h2>

    @section ('wrappedButtons')
        <a class="btn btn-default m-t-20 pull-right addField" data-wrapper="#fieldSetup"><i class="glyphicon glyphicon-plus"></i> Nytt fält</a>
        <a class="btn btn-default m-t-20 pull-right toggleAll" style="margin-right: 20px;"><i class="fa fa-magic" style="padding-right: 5px;"></i> Toggle all</a>
        <div class="clearfix"></div>
    @show
    <div class="clearfix"></div>

    <div class="custom-dd-empty dd" id="fieldSetup">
        <ol class="dd-list fieldRows fieldHolder sortable">
            
        </ol>
    </div>

    <div class="clearfix"></div>

    @yield ('wrappedButtons')

</div>

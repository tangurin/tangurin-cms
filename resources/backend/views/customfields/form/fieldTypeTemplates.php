<script id="productPickerRow" type="text/x-handlebars-template">
<tr class="itemId-{{productId}}">
    <input type="hidden" name="{{fieldName}}" value="{{productId}}" />
    <td>{{productName}}</td>
    <td>{{productCode}}</td>
    <td><a class="btn btn-danger removeRow"><i class="fa fa-times"></i></a></td>
</tr>
</script>

<script id="categoryPickerRow" type="text/x-handlebars-template">
<tr class="categoryId-{{categoryId}}">
    <input type="hidden" name="{{fieldName}}" value="{{categoryId}}" />
    <td>{{categoryName}}</td>
    <td>{{categoryParent}}</td>
    <td><a class="btn btn-danger removeRow"><i class="fa fa-times"></i></a></td>
</tr>
</script>

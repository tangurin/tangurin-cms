<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <label>Visningsnamn:</label>
            <input type="text" name="collection[name]" value="<?=$collection->name?>" class="form-control" />
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <label>Identifierare:</label>
            <input type="text" name="collection[identifier]" value="<?=$collection->identifier?>" class="form-control collectionIdentifier" />
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group">
            <label>Beskrivning:</label>
            <textarea name="collection[description]" class="form-control tinymce"><?=$collection->description?></textarea>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <label for="categorySelect">Kategori: <a class="appendToSelect" data-target="#categorySelect" style="cursor: pointer; font-size: 12px;">(<i class="fa fa-plus"></i> Ny kategori)</a></label>
            <select name="collection[category]" class="form-control" id="categorySelect">
                <option value="">Okategoriserad</option>
                @foreach ($collectionCategories as $categoryOption)
                    <option value="{{ $categoryOption }}"<?=$categoryOption == $collection->category ? ' selected' : ''?>>{{ $categoryOption }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

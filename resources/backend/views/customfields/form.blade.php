@extends('Backend::layout.layout')

@section('content')

<form method="post" action="" enctype="multipart/form-data" class="customfieldsForm">
  {!! Form::token() !!}
  <div class="card-box">
    <div class="row">
      <ul class="nav nav-tabs tabs">
          <li class="tab active">
              <a href="#tab-collection" data-toggle="tab">
                  <span class="visible-xs"><i class="md md-label"></i></span>
                  <span class="hidden-xs"><i class="md md-label"></i> Generellt</span>
              </a>
          </li>
          <li class="tab">
              <a href="#tab-relations" data-toggle="tab">
                  <span class="visible-xs"><i class="md md-shuffle"></i></span>
                  <span class="hidden-xs"><i class="md md-shuffle"></i> Relationer</span>
              </a>
          </li>
          <li class="tab">
              <a href="#tab-fields" data-toggle="tab">
                  <span class="visible-xs"><i class="md md-view-headline"></i></span>
                  <span class="hidden-xs"><i class="md md-view-headline"></i> Fält</span>
              </a>
          </li>
      </ul>
      <div class="tab-content">
          <div class="tab-pane active" id="tab-collection">
              @include ('Backend::customfields.form.collection')
          </div>
          <div class="tab-pane" id="tab-relations">
              @include ('Backend::customfields.form.relations')
          </div>
          <div class="tab-pane" id="tab-fields">
              @include ('Backend::customfields.form.fields')
          </div>
      </div>
    </div>
  </div>
  <div class="col-sm-12">
    <hr>
    <div class="text-center p-20">
      <a href="/admin/customfields/index" class="btn w-sm btn-white waves-effect">@lang('cms.cancel')</a>
      <button type="submit" class="btn w-sm btn-default waves-effect waves-light">@lang('cms.save')</button>
      @if ($collection->id > 0)
      <a href="/admin/customfields/delete/{{ $collection->id }} " target="" class="btn w-sm btn-danger waves-effect waves-light confirm" data-confirmtext="{{ trans('Vill du verkligen ta bort fältkollektionen?') }}" title="{{ trans('Ta bort fältkollektion') }}" >@lang('cms.delete')</a>
      @endif
    </div>
  </div>
</form>

@stop

@section('foot')
  @include ('Backend::customfields.form.customfieldsTemplates')
@stop

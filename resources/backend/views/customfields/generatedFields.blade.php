<?php
$views = [
    'normal' => 'Normal vy',
    'small' => 'Liten vy',
];
$cookieView = isset($_COOKIE['customfieldsView']) ? $_COOKIE['customfieldsView'] : 'normal';
?>

<script type="text/javascript">
    //All existing field and repeater setups
    var jsonCollections = {!! json_encode($collections) !!};
    var jsonFields = {!! json_encode($fields) !!};
    var jsonValues = {!! json_encode($fieldValues) !!};
    var jsonTranslations = {!! json_encode($fieldTranslations) !!};
    var jsonValueCategories = {!! json_encode($valueCategories) !!}
    var jsonValueProducts = {!! json_encode($valueProducts) !!}
    var availableLanguages = {!! json_encode($availableLanguages) !!};
    var maxInputVars = {{ $max_input_vars }};
    var showIdentifiers = true;
</script>

<div class="viewButtons" style="display: none;">
    @foreach ($views as $view =>$viewLabel)
        <a class="btn btn-xs btn-primary waves-effect waves-light<?=$view == $cookieView ? ' selected' : ' btn-custom' ?>" data-view="{{ $view }}">{{ $viewLabel }}</a>
    @endforeach
</div>

<ul class="nav nav-tabs languageSwitcher" style="display: none;">
    <?php $i = 1; ?>
    @foreach ($availableLanguages as $code => $language)
    <li class="<?=$i == 1 ? 'active' : ''?> tab">
        <a data-language="{{ $code }}" data-toggle="tab">
            <span class="visible-xs"><img class="flag" src="{{ '' or strtolower($code) .'.png' }}" /></span>
            <span class="hidden-xs"><img class="flag" src="{{ '' or strtolower($code) .'.png' }}" /> {{ $language['name'] }}</span>
        </a>
    </li>
    <?php $i++; ?>
    @endforeach
</ul>
<div class="row">
    <div class="customfieldsWrapper" data-view="{{ $cookieView }}"></div>
</div>

{!! Form::token() !!}
<input type="hidden" name="customfieldValues[relationClass]" value="{{ $relationClass }}" id="relationClass" />
<input type="hidden" name="customfieldValues[relationIdentifier]" value="{{ $relationIdentifier }}" id="relationIdentifier" />
@yield ('customfieldsNoModal')

@include ('Backend::customfields.form.fieldTypeTemplates')

@extends ('Backend::layout.layout')

@section ('content')
<div class="card-box">
    <div class="row">
        <div class="col-lg-12">
            <h3>{{ $name or '' }}</h3>
            <p>{{ $description or '' }}</p>
            <div class="easytabs">
                @include ('Backend::shared._languageTabs')
                <div class="tab-content"> 
                    @foreach ($languageRepository->listAvailable() as $lang => $language)
                        <div id="tabGroup{{ $lang }}" class="tab-pane">
                            <input type="hidden" name="data[{{ $lang }}][id]" value="{{ $model->translation($lang)->id or 0 }}" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>@lang('cms.content')</label>
                                        <textarea name="data[{{ $lang }}][content]" class="wysiwyg">{{ $model->translation($lang)->content or '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div> 
            </div>
        </div>
    </div>
</div>
@stop

@extends ('Backend::layout.layout', [
    'buttons' => [
        'create' => true,
    ]
])

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h2>Texter</h2>
                @foreach ($collection as $group => $texts)
                    <div class="list-group">
                        <a class="list-group-item active">@lang('texts.groups.'. $group)</a>
                        @foreach ($texts as $row)
                            <a href="{{ $layout->updateable() ? route($route['edit'], ['id' => $row->id]) : '#' }}" class="list-group-item" title="@lang('texts.identifiers.'. $row->identifier .'.description')">@lang('texts.identifiers.'. $row->identifier .'.name')</a>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop

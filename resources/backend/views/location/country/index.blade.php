@extends('Backend::layout.layout')

@section('content')
    <div class="card-box">
        <div class="row">
            <div class="col-sm-12">
                <div id="datatable" class="tab-pane active">
                    @include ('Backend::location.country.datatable')
                </div>
            </div>
        </div>
    </div>
@stop

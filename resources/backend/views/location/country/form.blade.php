@extends('Backend::layout.layout')

@section('content')
<div class="card-box">
    <div class="row">
        <div class="easytabs">
            @include ('Backend::shared._languageTabs')
            <div class="tab-content"> 
                @foreach ($languageRepository->listAvailable() as $lang => $language)
                    <div id="tabGroup{{ $lang }}" class="tab-pane">
                        <input type="hidden" name="data[{{ $lang }}][id]" value="{{ $model->translation($lang)->id or 0 }}" />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="data[{{ $lang }}][name]" class="form-control" value="{{ $model->translation($lang)->name or '' }}" placeholder="@lang('cms.name')" />
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <input type="text" name="country_code" class="form-control" value="{{ $model->country_code or '' }}" placeholder="@lang('cms.countryCode')" />
            </div>
        </div>
    </div>
</div>
@stop

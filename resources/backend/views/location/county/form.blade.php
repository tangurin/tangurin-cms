@extends('Backend::layout.layout')

@section('content')
<div class="card-box">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <select name="country_id" class="form-control">
                    @foreach ($countries as $country)
                        <option value="{{ $country->id }}" @if ($country->id == $model->country_id)selected @endif>{{ $country->translation->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <input type="text" name="name" class="form-control" value="{{ $model->name or '' }}" placeholder="@lang('cms.county')" />
            </div>
        </div>
    </div>
</div>
@stop

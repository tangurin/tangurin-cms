<table class="dataTable table table-striped table-bordered">
    <thead>
        <tr>
            @section('thead')
                <th>{{ Lang::get('cms.county') }}</th>
                <th>{{ Lang::get('cms.country') }}</th>
                @if ($layout->manageable())
                <th>{{ Lang::get('cms.manage') }}</th>
                @endif
            @show
        </tr>
    </thead>
    <tbody>
        @if (!$collection->isEmpty())
            @foreach ($collection as $row)
                <?php $editPath = route($route['edit'], ['id' => $row->id]); ?>
                <tr>
                    <td><a href="{{ $editPath }}">{{ $row->name or '' }}</a></td>
                    <td><a href="{{ $editPath }}">{{ $row->country->translation->name or '' }}</a></td>
                    @if ($layout->manageable())
                    <td class="manageCol">
                        @include ('Backend::shared.buttons._manageButtons')
                    </td>
                    @endif
                </tr>
            @endforeach
        @else
            <tr><td>Inga län är skapade</td></tr>
        @endif
    </tbody>
    <tfoot>
        <tr>
            @yield('thead')
        </tr>
    </tfoot>
</table>

@extends('Backend::layout.layout')

@section('content')
<div class="card-box">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <select class="form-control countrySelect" data-ajax="{{ route('ajax.counties') }}">
                    @foreach ($countries as $country)
                        <option value="{{ $country->id }}" @if ($layout->isEdit() && $country->id == $model->county->country->id)selected @endif>{{ $country->translation->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <select name="county_id" class="form-control" data-dependon=".countrySelect" data-selected="{{ $model->county_id }}">
                    @foreach ($counties as $key => $county)
                        <option value="{{ $key }}">{{ $county }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <input type="text" name="name" class="form-control" value="{{ $model->name or '' }}" placeholder="@lang('cms.county')" />
            </div>
        </div>
    </div>
</div>
@stop

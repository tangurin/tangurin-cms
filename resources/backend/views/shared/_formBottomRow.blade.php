<div class="card-box">
    <div class="row">
        <div class="col-lg-12">
            @include ('Backend::shared.buttons._save')
            
            @if ($layout->isEdit() && $layout->destroyable())
            @include ('Backend::shared.buttons._delete')
            @endif

            @if ($layout->isEdit() && $layout->createable())
                <div class="pull-right">
                    @include ('Backend::shared.buttons._create')
                </div>
            @endif

            @yield ('formBottomRow')
        </div>
    </div>
</div>

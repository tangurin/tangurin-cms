<?php $languageList = $languageRepository->listAvailable(); ?>
@if (!empty($languageList))
    <ul class="nav nav-tabs tabs">
        <?php $i = 0; ?>
        @foreach ($languageList as $languageCode => $language)
            <li class="tab">
                <a href="#tabGroup{{ $languageCode }}" id="tab{{ $languageCode }}" class="@if ($i == 0)current @endif">
                    <span>{{ $language['native'] }}</span>
                </a>
            </li>
            <?php $i++?>
        @endforeach
    </ul>
@endif

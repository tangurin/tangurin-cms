

<div class="card-box">
    <div class="row">
        <div class="col-md-6">
            

        </div>
        <div class="col-md-6 text-right">
            @if ($layout->isEdit())
                @include ('shared.buttons.create')
            @endif
        </div>
    </div>
</div>

<div class="card-box">
    <div class="row">
        @if (! $layout->isEdit())
        <div class="col-md-2">
            <label>
                @lang('cms.clearAllFields')<br>
                {!! switcher('clearAllFields') !!}
            </label>
        </div>
        @endif
    </div>
</div>

<?php $disabled = isset($disabled) ? $disabled : false; ?>
@foreach($permissions as $permissionType => $permissionGroups)
    <div class="permissionsWrapper">
        <div class="lineTitle">
            <h3 class="pull-left">@lang('permission.title'. ucfirst($permissionType))</h3>
            @if (!$disabled)
            <div class="pull-right">
                <i class="fa toggleAllCheckboxes" data-wrapper=".permissionsWrapper"></i>
            </div>
            @endif
        </div>
        <div class="row">
        @foreach($permissionGroups as $permissionGroup => $permissionValues)
        <?php
        $title = trans('permission.'. $permissionType .'.'. $permissionGroup);
        $slug = str_slug($title);
        ?>
        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading" role="tab" id="{{ $slug }}">
                    <h4 class="panel-title cf">
                        <div class="pull-left">
                            {{ $title }}
                        </div>
                        @if (!$disabled)
                        <div class="pull-right">
                            <i class="fa toggleAllCheckboxes" data-wrapper=".panel-primary"></i>
                        </div>
                        @endif
                    </h4>
                </div>
                <div id="dd-{{ $slug }}" class="panel-collapse collapse {{ $closed or 'in' }}" role="tabpanel" aria-labelledby="dd-{{ $slug }}">
                    <div class="panel-body">
                        @foreach($permissionValues as $key => $permissionValue)
                            <?php
                            $name = $permissionType .'_'. $permissionValue .'_'. $permissionGroup;
                            $selected = null;
                            if (isset($role) ) {
                                $selected = $role->hasPermissionTo($name);
                            }
                            if (isset($user)) {
                                $selected = $user->hasDirectPermission($name);
                            }
                            ?>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="{{ str_contains($permissionValue, 'delete') ? 'text-danger' : '' }}">{{ trans('permission.'. $permissionValue) }}</label><br>
                                    {!! switcher([
                                        'name' => 'permissions[]',
                                        'value' => $name,
                                        'selected' => $selected,
                                        'disabled' => $disabled,
                                    ]) !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        </div>
    </div>
@endforeach

<div class="row">
    <div class="col-lg-6">
        <h4 class="page-title">
            @if ($layout->isEdit() && isset($model) && is_object($model))
                @lang('cms.edit') {{ $model->name }}
            @else
                {{ Lang::get('cms.'. $page) }}
            @endif
        </h4>
    </div>
</div>

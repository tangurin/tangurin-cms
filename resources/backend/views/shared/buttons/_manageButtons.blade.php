@if ($layout->updateable())
{!! button(['type' => 'edit', 'href' => $editPath]) !!}
@endif

@if ($layout->destroyable())
{!! button(['type' => 'delete', 'data-path' => route($route['destroy'], [$row->id]), 'data-index' => route($route['index'])]) !!}
@endif

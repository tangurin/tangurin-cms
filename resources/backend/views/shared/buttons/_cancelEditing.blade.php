@if (Route::has($route['index']))
<div class="text-right">
    <a href="{{ route($route['index']) }}" class="btn btn-danger btn-sm waves-effect waves-light cancelEditing">@lang('cms.cancelEditing')</a>
</div>
@endif

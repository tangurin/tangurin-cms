@if (Route::has($route['create']))
    <a href="{{ route($route['create']) }}" class="btn btn-primary btn-lg waves-effect waves-light">@lang('cms.create') @lang('cms.'. $page)</a>
@endif

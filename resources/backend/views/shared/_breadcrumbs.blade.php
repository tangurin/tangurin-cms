@if (!empty($breadcrumbs))
    <ol class="breadcrumb">
        @foreach ($breadcrumbs as $url => $crumb)
            <li><a href="{{ $url }}"><span>{!! $crumb !!}</span></a></li>
        @endforeach
    </ol>
@endif

@if ($layout->isEdit())
    {!! Form::model($model, [
            'method' => 'PATCH',
            'route' => [$route['update'], $model->id],
            'data-remote' => 'AJAX',
            'class' => 'edit'
        ]
    ) !!}
@else
    {!! Form::open([
            'route' => $route['store'],
            'data-remote' => 'AJAX',
        ]
    ) !!}
@endif
<input type="hidden" name="id" value="{{ $model->id or 0 }}" />

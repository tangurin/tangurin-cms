<table class="dataTable table table-striped table-bordered">
    <thead>
        <tr>
            @section('thead')
                <th>{{ Lang::get('cms.id') }}</th>
                <th>{{ Lang::get('cms.name') }}</th>
                <th>{{ Lang::get('cms.email') }}</th>
                <th>{{ Lang::get('cms.role') }}</th>
                <th>{{ Lang::get('cms.created') }}</th>
                @if ($layout->manageable())
                <th>{{ Lang::get('cms.manage') }}</th>
                @endif
            @show
        </tr>
    </thead>
    <tbody>
        @foreach ($collection as $row)
            <?php $editPath = route($route['edit'], ['id' => $row->id]); ?>
            <tr>
                <td>
                    <a href="{{ $editPath }}">
                        {{ $row->id or '0' }}
                    </a>
                </td>
                <td>
                    <a href="{{ $editPath }}">
                        {{ $row->firstname or '' }} {{ $row->lastname or '' }}
                    </a>
                </td>
                <td>
                    <a href="{{ $editPath }}">
                        {{ $row->email or '' }}
                    </a>
                </td>
                <td>
                    <a href="{{ $editPath }}">
                        {{ $row->roles->implode('name', ', ') }}
                    </a>
                </td>
                <td>
                    {{ date('Y-m-d', strtotime($row->created_at)) }}
                </td>
                @if ($layout->manageable())
                <td class="manageCol">
                    @include ('Backend::shared.buttons._manageButtons')
                </td>
                @endif
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            @yield('thead')
        </tr>
    </tfoot>
</table>

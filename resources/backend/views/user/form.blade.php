@extends('Backend::layout.layout')

@section('content')

@if ($layout->isEdit())
<div class="card-box">
    <div class="row">
        <div class="col-lg-12">
            @if ($model->isActive())
            <a href="{{ route('user.deactivate', [$model->id]) }}" class="btn btn-danger btn-m waves-effect waves-light">Inaktivera konto</a>
            @else
            <a href="{{ route('user.activate', [$model->id]) }}" class="btn btn-success btn-m waves-effect waves-light">Aktivera konto</a>
            @endif
        </div>
    </div>
</div>
@endif

<div class="card-box">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>@lang('cms.firstname')</label>
                <input type="text" name="firstname" class="form-control" value="{{ $model->firstname or '' }}" placeholder="@lang('cms.firstname')" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>@lang('cms.lastname')</label>
                <input type="text" name="lastname" class="form-control" value="{{ $model->lastname or '' }}" placeholder="@lang('cms.lastname')" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>@lang('cms.email')</label>
                <input type="text" name="email" class="form-control" value="{{ $model->email or '' }}" placeholder="@lang('cms.email')" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>@lang('cms.password') <small>(Lämnas tomt för oförändrat lösenord)</small></label>
                <input type="password" name="password" class="form-control" value="" placeholder="@lang('cms.password')" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group {{ $errors->has('roles') ? 'has-error' : '' }}">
                {!! Form::label('roles[]', 'Roles') !!}
                {!! Form::select('roles[]', $roles, $selectedRoles,  ['class' => 'form-control', 'multiple']) !!}
                @if ($errors->has('roles')) <p class="help-block">{{ $errors->first('roles') }}</p> @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @include ('Backend::shared._permissions', [
                'user' => $model,
            ])
        </div>
    </div>
</div>
@stop

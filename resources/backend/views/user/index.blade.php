@extends('Backend::layout.layout')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h1>@lang('cms.users')</h1>
                @include ('Backend::user.datatable')
            </div>
        </div>
    </div>
@stop


<div class="flex">
    <div class="leftBar">
        <div class="searchWrapper">
            <div class="fields">
                <input type="text" class="searchField form-control" v-model="searchField" placeholder="Sök..."> @{{ searchField }}
                <i class="fa fa-search searchButton" v-on:click="searchFiles"></i>
            </div>
            <ul class="searchSettings">
                <li>
                    <label><input type="checkbox" class="searchInSameDirectory" v-model="searchInSameDirectory"> Sök i denna katalog</label>
                </li>
            </ul>
        </div>
        
        <h4>Kataloger</h4>
        {!! $directories->generate(['class' => 'directoriesTree']) !!}
    </div>
    <div class="mediaWrapper">
        <ul class="files">
            <li v-for="(file, key) in files" 
                v-bind:class="[{selected: isFileSelected(file)}, 'file col-'+ columns]"
                v-on:mouseover="fileHover(file)"
                v-on:click="fileClicked(file)"
                :title="file.name +'.'+ file.extension +' ('+ file.path +')'"
                :data-file-id="file.id"
            >
                <div class="inner">
                    <div class="graphic">
                        <img :src="'{{ url('storage') }}/'+ file.path" alt="" @error="imageLoadFailed(file)" v-if="file.type == 'image'">
                        <span v-else :class="'icon icon-'+ file.extension" :data-extension="file.extension"></span>
                    </div>
                    <span class="name">@{{ file.name }}.@{{ file.extension }}</span>
                </div>
            </li>
        </ul>
        <div class="bottomBar">
            <div class="leftSide">
                <div class="columnSlider">
                    <span>Kolumner (@{{ columns }}) </span> <input type="range" v-model="columns" min="1" max="12">
                </div>
            </div>
            <div class="rightSide">

            </div>
        </div>
    </div>
    <div class="rightBar">
        <div class="previewFile" v-if="previewFile != null && selectedFiles.length < 2">
            <div class="graphic">
                <img :src="'{{ url('storage') }}/'+ previewFile.path" alt="" v-if="previewFile.type == 'image'">
                <span v-else :class="'icon icon-'+ previewFile.extension" :data-extension="previewFile.extension"></span>
            </div>
            <ul class="fileInfo">
                <li class="name">@{{ previewFile.name }}.@{{ previewFile.extension }}</li>
                <li class="path">@{{ previewFile.path }}</li>
                <li class="type"><span>Typ</span><span>@{{ previewFile.type }}</span></li>
                <li class="mimetype"><span>Mime</span><span>@{{ previewFile.mimetype }}</span></li>
                <li class="size"><span>Storlek</span><span>@{{ prefixSize(previewFile.size) }}</span></li>
                <li class="width" v-if="previewFile.width > 0"><span>Bredd</span><span>@{{ previewFile.width }}px</span></li>
                <li class="height" v-if="previewFile.height > 0"><span>Höjd</span><span>@{{ previewFile.height }}px</span></li>
                <li class="hidden" v-if="previewFile.hidden == 0">Dold</li>
                <li class="extra" v-if="previewFile.extra">@{{ previewFile.extra }}</li>
                <li class="newTab"><a :href="'{{ url('storage') }}/'+ previewFile.path" target="_blank">Öppna i ny flik</a></li>
            </ul>
        </div>
        <div class="selectedFilesWrapper" v-if="selectedFiles.length > 0">
            <hr v-if="selectedFiles.length == 1">
            <h4 class="title">Valda objekt (@{{ selectedFilesCount }}) <i class="fa fa-trash removeAllSelectedFiles" v-on:click="removeAllSelectedFiles()"></i></h4>
            <ul class="selectedFiles">
                <li v-for="(file, key) in selectedFiles">
                    <i class="fa removeSelected" v-on:click="removeFromSelectedFiles(file)"></i> @{{ file.name }}.@{{ file.extension }}
                </li>
            </ul>
            {{-- <a class="bulkEdit" v-on:click="changeView('bulkEdit')" v-if="selectedFiles.length > 1"><i class="fa fa-bars"></i> Massredigering</a> --}}
        </div>
        <div class="actionButtons">
            <a v-if="selectedFiles.length > 0" v-on:click="save" class="btn btn-md btn-success saveFilemanager">Spara</a>
            <a v-on:click="close" class="btn btn-md btn-danger closeFilemanager">Stäng</a>
        </div>
    </div>
</div>

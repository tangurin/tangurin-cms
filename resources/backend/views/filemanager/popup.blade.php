<div id="filemanager" :class="{loading : loading}">
    <div class="loader"><i class="fa fa-cog fa-spin loaderIcon"></i></div>
    <div class="view mediaView" v-if="views.media">
        @include('Backend::filemanager.media')
    </div>
    <div class="view bulkEdit" v-if="views.bulkEdit">
        @include('Backend::filemanager.bulkEdit')
    </div>
</div>

<script type="text/javascript">
    var FilemanagerData = {
        collection: {!! $files->toJson() !!},
        rootDirectory: '{!! $rootDirectory !!}',
        currentDirectory: '{!! $currentDirectory !!}',
        settings: {!! json_encode($settings) !!}
    };
</script>

@extends('Backend::layout.layout')

@section('content')
    <div class="container-fluid">
        <h1>@lang('cms.'. $page.'H1POST')</h1>
        <a href="{{ action($controller .'@create') }}" class="button create flr">@lang('cms.create')</a>
        <div class="clear"></div>
        <table class="generateDataTable">
            <thead>
                <tr>
                    @section('thead')
                        <td>{{ Lang::get('cms.id') }}</td>
                        <th>{{ Lang::get('cms.language_native') }}</th>
                        <th>{{ Lang::get('cms.language_english') }}</th>
                        <th>{{ Lang::get('cms.language_code') }}</th>
                        <th>{{ Lang::get('cms.created') }}</th>
                        <th>{{ Lang::get('cms.last_change') }}</th>
                    @show
                </tr>
            </thead>

            <tfoot>
                <tr>
                    @yield('thead')
                </tr>
            </tfoot>

            <tbody>
                @foreach ($data as $row)
                    <?php $editPath = route($route .'.edit', array('id' => $row->id)); ?>
                    <tr>
                        <td>
                            <a href="{{ $editPath }}">
                                {{ $row->id }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ $editPath }}">
                                {{ $row->language_native }}
                            </a>
                        </td>

                        <td>
                            <a href="{{ $editPath }}">
                                {{ $row->language_english }}
                            </a>
                        </td>

                        <td>
                            <a href="{{ $editPath }}">
                                {{ $row->language_code }}
                            </a>
                        </td>

                        <td>
                            {{ $row->created_at }}
                        </td>

                        <td>
                            {{ $row->updated_at }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clear"></div>
        <a href="{{ action($controller .'@create') }}" class="button create flr bottom">@lang('cms.create')</a>
        <div class="clear"></div>
    </div>
    <!-- /.container-fluid -->
@stop


@extends('Backend::layout.layout')

@section('content')
    <div class="manageContent">

        @if ($method == 'POST')
            {!! Form::open ([
                    'method'                => $method,
                    'route'                 => $route .'.store',
                    'data-remote'           => 'AJAX',
                    'data-remote-success'   => Lang::get('cms.success')
                ]
            ) !!}
        @else
            {!! Form::model ($data, [
                    'method'                => $method,
                    'route'                 => [$route .'.update', $data->id],
                    'data-remote'           => 'AJAX',
                    'data-remote-success'   => Lang::get('cms.success')
                ]
            ) !!}
        @endif

        <div id="leftSide">
            <h1>{{ Lang::get('cms.'. $method)  }} {{ Lang::get('cms.'. $page)  }}</h1>

            <div class="cf"></div>

            <div class="fieldWrapper animate w30 fl">
                {!! Form::label('language_native', Lang::get('cms.language_native')) !!}
                {!! Form::text('language_native', null, ['class' => 'form-control', 'maxlength' => 100, 'placeholder' => Lang::get('cms.language_native')]) !!}
            </div>

            <div class="fieldWrapper animate w30 ml2 fl">
                {!! Form::label('language_english', Lang::get('cms.language_english')) !!}
                {!! Form::text('language_english', null, ['class' => 'form-control', 'maxlength' => 100, 'placeholder' => Lang::get('cms.language_english')]) !!}
            </div>

            <div class="fieldWrapper animate w30 ml2 fl">
                {!! Form::label('language_code', Lang::get('cms.language_code')) !!}
                {!! Form::text('language_code', null, ['class' => 'form-control', 'maxlength' => 100, 'placeholder' => Lang::get('cms.language_code')]) !!}
            </div>

            <div class="fieldWrapper cf">
                @include ('_filemanager', [
                    'multiple' => false,
                    'title' => Lang::get('cms.flag'),
                ])
            </div>
        </div>

        <div id="rightSide">
            @include ('_rightPanel')
        </div>
        {!! Form::close() !!}
    </div>
@stop

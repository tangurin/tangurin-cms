@extends ('Frontend::layout.layout')

@section ('content')
    <div class="headText">
        <h1>{{ t('account.myAccount') }}</h1>
    </div>

    <div class="easytabs">
        <ul class="">
            @can ('frontend_update_emailAndPassword')
            <li class="tab-emailAndPassword"><a href="#emailAndPassword" class="button">{{ t('account.emailAndPassword') }}</a></li>
            @endcan
        </ul>

        @can ('frontend_update_emailAndPassword')
        <div id="emailAndPassword" class="tabContent">
            @include ('Frontend::account.emailAndPassword')
        </div>
        @endcan
    </div>
@stop

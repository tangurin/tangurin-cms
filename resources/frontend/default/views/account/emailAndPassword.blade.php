<form action="{{ route('account.storeLogin') }}" method="post" class="formLayout ajaxForm">
    {{ Form::token() }}
    <div>
        <div class="cf">
            <div class="left w48">
                <label>{{ t('form.email') }}</label>
                <input type="email" name="email" value="{{ old('email', $user->email) }}" required placeholder="...">
            </div>
            <div class="right w48">
                <label>{{ t('form.passwordCurrent') }}</label>
                <input type="password" name="passwordCurrent" value="" class="emptyOnSave" required placeholder="...">
            </div>
        </div>
    </div>
    <div>
        <div class="cf">
            <div class="left w48">
                <label>{{ t('form.password') }}</label>
                <input type="password" name="password" value="" class="emptyOnSave" placeholder="...">
            </div>
            <div class="right w48">
                <label>{{ t('form.passwordConfirm') }}</label>
                <input type="password" name="password_confirmation" value="" class="emptyOnSave" placeholder="...">
            </div>
        </div>
    </div>
    <div>
        <button type="submit">{{ t('form.save') }} {{ t('account.emailAndPassword') }}</button>
    </div>
</form>

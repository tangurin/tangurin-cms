@extends ('Frontend::layout.layout')

@section ('content')
    <div class="page404 httpErrorPage">
        <i class="fa fa-chain-broken icon" aria-hidden="true"></i>
        <h1>405</h1>
        <h2>{{ t('error.methodNotAllowed') }}</h2>
    </div>
@stop

@extends ('Frontend::layout.layout')

@section ('content')
    <div class="page404 httpErrorPage">
        <i class="fa fa-chain-broken icon" aria-hidden="true"></i>
        <h1>403</h1>
        <h2>{{ t('error.pageUnauthorize') }}</h2>
    </div>
@stop

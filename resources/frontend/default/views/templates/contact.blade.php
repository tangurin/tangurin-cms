@extends ('Frontend::layout.layout')

@section ('content')
    <div class="contactPage">
        <div class="gr-6">
            {!! $page->translation->content !!}
        </div>
        <div class="gr-6">
            <form action="" method="post" class="formLayout">
                {{ Form::token() }}
                <div>
                    <input type="text" name="name" placeholder="{{ t('form.name') }}" value="{{ old('name') }}" required autofocus>
                </div>
                <div>
                    <input type="email" name="email" placeholder="{{ t('form.email') }}" {{ old('email') }} required>
                </div>
                <div>
                    <textarea name="message" placeholder="{{ t('form.message') }}" required>{{ old('message') }}</textarea>
                </div>
                <div>
                    <button type="submit" class="w100">{{ t('form.send') }}</button>
                </div>
            </form>
        </div>
    </div>
@stop

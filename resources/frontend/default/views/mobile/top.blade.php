<nav id="mobileNav" canvas="">
    <a class="slidebarLeft menuIcon" data-slidebar="menu">
        <span class="x1"></span>
        <span class="x2"></span>
        <span class="x3"></span>
    </a>
    <a href="{{ url('') }}" class="logo">
        <img src={{ images('logo.svg') }}" alt="{{ setting('companyName') }}" />
    </a>
</nav>

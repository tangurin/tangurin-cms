<div off-canvas="id-menu left push" class="slidebar slidebarMenu">
    <div class="innerWrapper">
        <ul class="navigation treeMenu">
          {!! $pageMenu->generate() !!}
        </ul>
    </div>
</div>

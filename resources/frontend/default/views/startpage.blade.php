@extends ('Frontend::layout.layout', ['noBreadcrumbs' => true])

@section ('content')
    <div class="startText">
        {!! $content !!}
    </div>
@stop

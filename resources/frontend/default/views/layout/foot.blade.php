<script type="text/javascript">
    var csrfToken = '{{ csrf_token() }}';
    var Language = {!! json_encode($translation->getStructuredList()) !!};
    var ValidationErrors = {!! isset($errors) ? json_encode($errors->getMessages()) : '[]' !!};
    var FlashDataJson = {!! json_encode([
        'success' => Session::has('success') ? Session::get('success') : null,
        'error' => Session::has('error') ? Session::get('error') : null,
        'info' => Session::has('info') ? Session::get('info') : null,
    ]) !!}
    var openedTab = '{{ $openedTab or '' }}'
</script>

<script src="{{ build( env('APP_ENV') == 'local' ? 'app.bundler.js' : 'app.bundler.min.js') }}"></script>

@yield ('foot')

<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">

@if (isHotReloadRunning() === false)
    <link href="{{ build( env('APP_ENV') == 'local' ? 'app.css' : 'app.min.css' ) }}" rel="stylesheet">
@endif

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="{{ $metaDescription or setting('defaultMetaDescription', '')  }}">
        <meta name="keywords" content="{{ $metaKeywords or setting('defaultMetaKeywords', '') }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="{{ url('favicon.ico') }}">
        <title>{{ $metaTitle or '' }}</title>

        @include ('Frontend::layout.head')
    </head>

    <body>
        <div id="overlay"><i class="fa fa-cog fa-spin loaderIcon" aria-hidden="true"></i></div>
        
        @include ('Frontend::mobile.top')

        <div canvas="container" class="canvasContainer">
            <header class="header">
                <div class="wrap">
                    <a href="{{ url('') }}" class="logo">
                        <img src="{{ images('logo.svg') }}" />
                    </a>
                    <nav class="mainMenu">
                        <ul class="dropDown smallDrop">
                            {!! $pageMenu->generate() !!}
                            @if (Auth::check())
                                <li><a href="{{ route('account.index') }}" class="accountButton">{{ t('navigation.account') }}</a></li>
                            @else
                                <li><a href="{{ route('frontend.auth.register') }}" class="registerButton">{{ t('account.register') }}</a></li>
                                <li><a href="{{ route('frontend.auth.login') }}" class="loginButton">{{ t('account.login') }}</a></li>
                            @endif
                        </ul>
                    </nav>
                </div>
            </header>

            <main class="contentWrapper">
                @yield('aboveWrap')
                @if (!isset($noBreadcrumbs))
                    @include ('Frontend::shared._breadcrumbs')
                @endif
                <div class="wrap">
                    @yield ('content')
                </div>
                @yield('belowWrap')
            </main>
            
            <footer class="footer">
                <div class="wrap">
                    <div class="cols">
                        <div class="col col1">
                            {!! text('footerCol1') !!}
                        </div>
                        <div class="col col2">
                            {!! text('footerCol2') !!}
                        </div>
                        <div class="col col3">
                            <h4 class="title">{{ t('account.myAccount') }}</h4>
                            <ul>
                                @if (Auth::check())
                                    <li><a href="{{ route('account.index') }}" class="accountLink">{{ t('account.myAccount') }}</a></li>
                                @else
                                    <li><a href="{{ route('frontend.auth.login') }}" class="loginLink">{{ t('account.login') }}</a></li>
                                    <li><a href="{{ route('frontend.auth.register') }}" class="registerLink">{{ t('account.register') }}</a></li>
                                    <li><a href="{{ route('frontend.auth.password.showSendResetEmailForm') }}" class="forgotPasswordLink">{{ t('account.forgotPassword') }}</a></li>
                                @endif
                            </ul>
                        </div>
                        <div class="col col4 textRight">
                            {!! text('footerCol4') !!}
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        @include ('Frontend::mobile.slidebars')
        @include ('Frontend::layout.foot')
    </body>
</html>

@if (!empty($breadcrumbs))
<div class="breadcrumbs">
    <div class="wrap">
        <ul>
            @foreach ($breadcrumbs as $url => $crumb)
                <li><a href="{{ $url }}">{!! $crumb !!}</a></li>
            @endforeach
        </ul>
    </div>
</div>
@endif

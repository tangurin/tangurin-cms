@extends('Frontend::layout.layout')

@section('content')
<div class="forgotPasswordPage">
    <div class="textCenter headText">
        <h1>{{ t('account.forgotPassword') }}</h1>
    </div>
    <form method="POST" action="{{ route('frontend.auth.password.sendResetLink') }}" class="loginForm formLayout smallForm">
        {{ csrf_field() }}
        <div>
            <label>{{ t('form.email') }}</label>
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ t('form.email') }}" required autofocus>
        </div>
        <div>
            <button type="submit">{{ t('account.resetPasswordButton') }}</button>
        </div>
    </form> 
</div>
@endsection

@extends('Frontend::layout.layout')


@section('content')
<div class="forgotPasswordPage">
    <div class="textCenter headText">
        <h1>{{ t('account.resetPassword') }}</h1>
    </div>
    <form method="POST" action="{{ route('frontend.auth.password.reset') }}" class="loginForm formLayout smallForm">
        {{ csrf_field() }}
        <input type="hidden" name="token" value="{{ $token }}">
        <div>
            <label>{{ t('form.email') }}</label>
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ t('form.email') }}" required autofocus>
        </div>
        <div>
            <label>{{ t('form.password') }}</label>
            <input type="password" class="form-control" name="password" value="" placeholder="{{ t('form.password') }}" required>
        </div>
        <div>
            <label>{{ t('form.passwordConfirm') }}</label>
            <input type="password" class="form-control" name="password_confirmation" value="" placeholder="{{ t('form.passwordConfirm') }}" required>
        </div>
        <div>
            <button type="submit">{{ t('account.resetPasswordButton') }}</button>
        </div>
    </form> 
</div>
@endsection

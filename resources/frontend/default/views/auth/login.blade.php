@extends('Frontend::layout.layout')

@section('content')
<div class="loginPage">
    <div class="textCenter headText">
        <h1>{{ t('account.login') }}</h1>
    </div>
    <form method="POST" action="{{ route('frontend.auth.login') }}" class="loginForm formLayout smallForm">
        {{ csrf_field() }}
        <div>
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ t('form.email') }}" required autofocus>
        </div>
        <div>
            <input type="password" class="form-control" name="password" placeholder="{{ t('form.password') }}" required>
        </div>
        <div>
            <button type="submit">{{ t('account.login') }}</button>
        </div>
        <div class="cf">
            <div class="left">
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="loginRememberMe">
                <label for="loginRememberMe" style="display: inline-block;">
                    {{ t('account.rememberMe') }}
                </label>
            </div>
            <div class="right">
                <a href="{{ route('frontend.auth.password.showSendResetEmailForm') }}" class="forgotPasswordLink">
                    <i class="fa fa-lock m-r-5"></i> {{ t('account.forgotYourPassword') }}
                </a>
            </div>
        </div>
    </form> 
</div>
@endsection

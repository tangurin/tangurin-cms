@extends('Frontend::layout.layout')

@section('content')
<div class="registerPage">
    <div class="textCenter headText">
        <h1>{{ t('account.register') }}</h1>
    </div>
    <form method="POST" action="{{ route('frontend.auth.register') }}" class="registerForm formLayout smallForm">
        {{ csrf_field() }}
        {{-- <div>
            <input type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="{{ t('name') }}">
        </div> --}}
        <input type="hidden" name="name" value="">
        <div>
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="{{ t('form.email') }}">
        </div>
        <div>
            <input type="password" class="form-control" name="password" required placeholder="{{ t('form.password') }}">
        </div>
        <div>
            <input type="password" class="form-control" name="password_confirmation" required placeholder="{{ t('form.passwordConfirm') }}">
        </div>
        <div>
            <button type="submit">{{ t('account.register') }}</button>
        </div>
    </form> 
</div>
@endsection

<h2>Error</h2>
<h5>{{ get_class($exception) }}</h5>
<?php
var_dump($exception->getMessage());
?>
<br><br>
<hr>
<br><br>
<h2>Session</h2>
<pre>
<?php
var_dump(\Session::all());
?>
</pre>

<h2>Request</h2>
<pre>
<?php
var_dump($request->all());
?>
</pre>

<h2>SERVER</h2>
<pre>
<?php
$server = $_SERVER;
//Remove db variables
foreach ($server as $key => $value) {
    $keyLC = strtolower($key);
    if (strpos($keyLC, 'password') > -1 ||
        strpos($keyLC, 'username') > -1 ||
        strpos($keyLC, 'secret') > -1 ||
        strpos($keyLC, 'key') > -1) {
        //Remove from array
        unset($server[$key]);
    }
}
var_dump($server);
?>
</pre>
